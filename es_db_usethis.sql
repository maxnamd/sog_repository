-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2017 at 04:02 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `es_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admins`
--

CREATE TABLE IF NOT EXISTS `tbl_admins` (
  `admin_id` varchar(100) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `firstname` varchar(60) NOT NULL,
  `middlename` varchar(60) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` text CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `password_temp` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `paths` text NOT NULL,
  `userlevel` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admins`
--

INSERT INTO `tbl_admins` (`admin_id`, `lastname`, `firstname`, `middlename`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `username`, `password`, `password_temp`, `paths`, `userlevel`, `email`) VALUES
('admin', 'Liwanag', 'Aris', ' ', 'College of Information Technology Education', '4', 'Dept Head', 'D1', '2013', '4', 'admin', 'c47feaececaec397b36c150ed7fb95d1', 'LIWANAG', '../assets/uploaded_files/accounts/siraris.jpg', 'ADMIN', 'siraris@gmail.com'),
('admin2', 'Sierota', 'Sydney', ' ', 'College of Health and Sciences', '1', 'Departmen Head', 'D1', '2016', '1', 'admin2', '8f0f72214298ba76511e06e1e1c3998f', 'SIEROTA', '../assets/uploaded_files/accounts/syd.png', 'ADMIN', 'sydsyd@gmail.com'),
('superadmin', 'Clauna', 'Levi Marion', 'Almazan', 'Human Resource Department', '7', 'Dean', 'Regular', '', '7', 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 'CLAUNA', '../assets/uploaded_files/accounts/default.png', 'SUPERADMIN', 'aysonjustinmax@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeights` int(11) NOT NULL,
  `p1_rating` int(11) NOT NULL,
  `p1_weightedRating` float NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1`
--

INSERT INTO `tbl_ans_eformp1` (`id`, `kraID`, `kpiTitle`, `kpiWeights`, `p1_rating`, `p1_weightedRating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 1, 'KRA1', 25, 2, 0.5, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 2, 'KRA2', 25, 3, 0.75, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 3, 'KRA3', 15, 3, 0.45, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(4, 4, 'KRA4', 35, 4, 1.4, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(5, 5, 'ANOTHER ONE', 100, 4, 4, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1kpi` (
  `id` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiDesc` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1kpi`
--

INSERT INTO `tbl_ans_eformp1kpi` (`id`, `kpiID`, `kraID`, `kpiDesc`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 1, 1, 'A', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 2, 1, 'A2', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 3, 1, 'A3', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(4, 4, 1, 'A4', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(5, 1, 2, 'B1', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(6, 2, 2, 'B2', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(7, 3, 2, 'B3', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(8, 1, 3, 'C1', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(9, 1, 5, '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(10, 2, 5, 'Wawa', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1_achievements`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1_achievements` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiAchievement` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1_achievements`
--

INSERT INTO `tbl_ans_eformp1_achievements` (`id`, `kraID`, `kpiAchievement`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 1, '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 1, '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 1, '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(4, 1, '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(5, 2, '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(6, 2, '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(7, 2, '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(8, 3, '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(9, 5, '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(10, 5, '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` varchar(30) NOT NULL,
  `competencyDescription` longtext NOT NULL,
  `proficiencyLevel` text NOT NULL,
  `p2_weights` float NOT NULL,
  `p2_rating` int(11) NOT NULL,
  `p2_weightedRating` float NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2a`
--

INSERT INTO `tbl_ans_eformp2a` (`id`, `competency`, `competencyDescription`, `proficiencyLevel`, `p2_weights`, `p2_rating`, `p2_weightedRating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community. Able to adjust to changes and exhibits a positive attitude towards different situations.</p>\r\n', 'B', 11.4286, 4, 0.5, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approaches to identify opportunities, implement solutions, measure impact and serve customers.</p>\r\n', 'C', 11.4286, 3, 0.3, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', 'B', 21.4286, 3, 0.6, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(4, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.Â </p>\r\n', 'A', 21.4286, 3, 0.6, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(5, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession. Â Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'D', 11.4286, 4, 0.5, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(6, 'Leadership Excellence', '', 'A', 11.4286, 4, 0.5, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(7, 'Management Excellence', '', 'B', 11.4286, 3, 0.3, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(8, 'People Development', '', 'NE', 0, 0, 0, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(9, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community. Able to adjust to changes and exhibits a positive attitude towards different situations.</p>\r\n', 'B Contributing with Expertise Specialist. Expert implementor. Customer advocate. Problem Solver.', 10, 4, 0.4, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(10, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approaches to identify opportunities, implement solutions, measure impact and serve customers.</p>\r\n', 'B Contributing with Expertise Specialist. Expert implementor. Customer advocate. Problem Solver.', 10, 4, 0.4, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(11, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', 'A Contributing Independently Learner-Doer. Individual contributor. Follower, team-player. Customer touchpoint.', 20, 4, 0.8, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(12, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.Â </p>\r\n', 'A Contributing Independently Learner-Doer. Individual contributor. Follower, team-player. Customer touchpoint.', 20, 4, 0.8, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(13, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession. Â Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'B Contributing with Expertise Specialist. Expert implementor. Customer advocate. Problem Solver.', 10, 4, 0.4, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(14, 'Leadership Excellence', '', 'B Contributing with Expertise Specialist. Expert implementor. Customer advocate. Problem Solver.', 10, 4, 0.4, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(15, 'Management Excellence', '', 'B Contributing with Expertise Specialist. Expert implementor. Customer advocate. Problem Solver.', 10, 4, 0.4, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(16, 'People Development', '', 'B Contributing with Expertise Specialist. Expert implementor. Customer advocate. Problem Solver.', 10, 4, 0.4, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(30) NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2b`
--

INSERT INTO `tbl_ans_eformp2b` (`id`, `competency`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'Creativity and Innovation', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'Customer Focus', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(4, 'Team Orientation', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(5, 'Technical Knowledge', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(6, 'Leadership Excellence', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(7, 'Management Excellence', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(8, 'People Development', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(9, 'Adaptability', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(10, 'Creativity and Innovation', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(11, 'Customer Focus', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(12, 'Team Orientation', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(13, 'Technical Knowledge', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(14, 'Leadership Excellence', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(15, 'Management Excellence', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(16, 'People Development', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2b_critical_incident`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2b_critical_incident` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(30) NOT NULL,
  `criticalIncident` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2b_critical_incident`
--

INSERT INTO `tbl_ans_eformp2b_critical_incident` (`id`, `competencyName`, `criticalIncident`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', 'Critical Incident #1:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'People Development', 'Critical Incident #1:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'People Development', 'Critical Incident #2:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'People Development', 'Critical Incident #3:', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'Adaptability', 'Critical Incident #1:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(1, 'People Development', 'Critical Incident #1:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'People Development', 'Critical Incident #2:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'People Development', 'Critical Incident #3:', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL,
  `score` int(11) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp3`
--

INSERT INTO `tbl_ans_eformp3` (`id`, `areas`, `score`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'DISCIPLINE', 4, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'ATTENDANCE', 3, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'PUNCTUALITY', 4, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(4, 'COMPLETION OF TRAINING', 4, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(5, 'DISCIPLINE', 4, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(6, 'ATTENDANCE', 3, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(7, 'PUNCTUALITY', 1, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(8, 'COMPLETION OF TRAINING', 1, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp3_rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp3_rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` varchar(200) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp3_rubric`
--

INSERT INTO `tbl_ans_eformp3_rubric` (`id`, `areaName`, `rubricDesc`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(4, 'DISCIPLINE', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'DISCIPLINE', '3', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'DISCIPLINE', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'DISCIPLINE', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(4, 'ATTENDANCE', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'ATTENDANCE', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'ATTENDANCE', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'ATTENDANCE', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(4, 'PUNCTUALITY', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'PUNCTUALITY', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'PUNCTUALITY', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'PUNCTUALITY', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(4, 'COMPLETION OF TRAINING', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'COMPLETION OF TRAINING', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'COMPLETION OF TRAINING', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'COMPLETION OF TRAINING', '', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(4, 'DISCIPLINE', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'DISCIPLINE', '3', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'DISCIPLINE', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(1, 'DISCIPLINE', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(4, 'ATTENDANCE', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'ATTENDANCE', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'ATTENDANCE', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(1, 'ATTENDANCE', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(4, 'PUNCTUALITY', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'PUNCTUALITY', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'PUNCTUALITY', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(1, 'PUNCTUALITY', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(4, 'COMPLETION OF TRAINING', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'COMPLETION OF TRAINING', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'COMPLETION OF TRAINING', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(1, 'COMPLETION OF TRAINING', '', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp4` (
  `id` int(11) NOT NULL,
  `p4_area` varchar(50) NOT NULL,
  `customerFeedback` text NOT NULL,
  `p4_score` int(11) NOT NULL,
  `p4_rating` varchar(11) NOT NULL,
  `answeredBy` varchar(80) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp4`
--

INSERT INTO `tbl_ans_eformp4` (`id`, `p4_area`, `customerFeedback`, `p4_score`, `p4_rating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'April - June', '', 4, '0.04', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'July - September', '', 4, '0.04', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'October - December', '', 4, '0.04', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(4, 'January - March', '', 4, '0.04', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(5, 'April - June', '', 2, '0.02', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(6, 'July - September', '', 2, '0.02', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(7, 'October - December', '', 1, '0.01', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(8, 'January - March', '', 1, '0.01', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_summary`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_summary` (
  `id` int(11) NOT NULL,
  `performance_rating` varchar(60) NOT NULL,
  `total_weighted_rating` float NOT NULL,
  `percentage_allocation` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_summary`
--

INSERT INTO `tbl_ans_summary` (`id`, `performance_rating`, `total_weighted_rating`, `percentage_allocation`, `subtotal`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 3.1, 40, 1.24, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, 'PART 2: COMPETENCIES', 3.3, 30, 1.13, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(3, 'PART 3: PROFESSIONALISM', 3.75, 30, 0.99, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(4, '0', 0, 0, 0.04, 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 4, 40, 1.6, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(2, 'PART 2: COMPETENCIES', 4, 30, 0.67, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(3, 'PART 3: PROFESSIONALISM', 2.25, 30, 1.2, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018'),
(4, '0', 0, 0, 0.02, 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_summary_comments_acknowledgement`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_summary_comments_acknowledgement` (
  `id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `recommendations` text NOT NULL,
  `approval` text NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_summary_comments_acknowledgement`
--

INSERT INTO `tbl_ans_summary_comments_acknowledgement` (`id`, `comments`, `recommendations`, `approval`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, '<p>Commentssssss for mr starkâ€ƒ</p>', '<p>Rec for stankkkk<br></p>', 'This is to Certify that Tony  Stark confirms this Evaluation for this Year.', 'Human Resource Department', 'SOG-004', '02/2018-03/2018'),
(2, '<p>Commentsssss<br></p>', '<p>rec<br></p>', 'This is to Certify that Veronica F Canlas confirms this Evaluation for this Year.', 'College of Information Technology Education', 'SOG-002', '02/2018-03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_competencyratingscale`
--

CREATE TABLE IF NOT EXISTS `tbl_competencyratingscale` (
  `id` int(11) NOT NULL,
  `competency_rating_value` int(1) NOT NULL,
  `competency_title` text NOT NULL,
  `competency_description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_competencyratingscale`
--

INSERT INTO `tbl_competencyratingscale` (`id`, `competency_rating_value`, `competency_title`, `competency_description`) VALUES
(1, 4, 'Expert/ Exemplary', 'Evaluation; Defines Framework Modes; Mentor, Self-Actualization, Consistently exceeds all of the acquired competency level descriptors'),
(2, 3, 'Advanced/ Accomplished', 'Analysis; Synthesis; Understands Framework Models, Exceeds most competency level descriptors'),
(3, 2, 'Intermediate / Developing', 'Unsupervised Application; Beginning Analysis, Meets most of the required competency level descriptors'),
(4, 1, 'Beginner', 'Comprehension; Beginning Application; Some Experience, Meets some of the competency level descriptors'),
(5, 0, 'NE', 'Early stages of application of the item; Behavior not yet present.'),
(6, 0, 'NA', 'The item does not apply to your position or performance of assigned tasks and responsibilities.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_current_date`
--

CREATE TABLE IF NOT EXISTS `tbl_current_date` (
  `id` int(11) NOT NULL,
  `date` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_current_date`
--

INSERT INTO `tbl_current_date` (`id`, `date`) VALUES
(1, '12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dateevaluation`
--

CREATE TABLE IF NOT EXISTS `tbl_dateevaluation` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dateevaluation`
--

INSERT INTO `tbl_dateevaluation` (`startDate`, `endDate`) VALUES
('02/2018', '03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_datekpi`
--

CREATE TABLE IF NOT EXISTS `tbl_datekpi` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_datekpi`
--

INSERT INTO `tbl_datekpi` (`startDate`, `endDate`) VALUES
('12/2017', '01/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departments`
--

CREATE TABLE IF NOT EXISTS `tbl_departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `initials` varchar(100) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL DEFAULT 'PENDING',
  `departmentOwner` varchar(100) NOT NULL,
  `monitor` varchar(6) NOT NULL DEFAULT 'unread',
  `activation_status` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_departments`
--

INSERT INTO `tbl_departments` (`id`, `department_name`, `initials`, `kpiStatus`, `departmentOwner`, `monitor`, `activation_status`) VALUES
(3, 'College of Information Technology Education', 'CITE', 'PENDING', 'admin', 'read', ''),
(4, 'College of Health and Sciences', 'CHS', 'PENDING', 'admin2', 'read', ''),
(5, 'Human Resource Department', 'HRD', 'PENDING', 'superadmin', 'unread', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dump_evaluation_period`
--

CREATE TABLE IF NOT EXISTS `tbl_dump_evaluation_period` (
  `id` int(100) NOT NULL,
  `start_date` varchar(100) NOT NULL,
  `end_date` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dump_evaluation_period`
--

INSERT INTO `tbl_dump_evaluation_period` (`id`, `start_date`, `end_date`) VALUES
(1, '02/2018', '03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp1` (
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeight` int(11) NOT NULL,
  `kpiOwner` varchar(60) NOT NULL,
  `kraCreatedFor` varchar(11) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp1`
--

INSERT INTO `tbl_eformp1` (`kraID`, `kpiTitle`, `kpiWeight`, `kpiOwner`, `kraCreatedFor`, `kpiStatus`) VALUES
(1, 'KRA1', 0, 'Human Resource Department', 'SOG-004', 'APPROVED'),
(2, 'KRA2', 0, 'Human Resource Department', 'SOG-004', 'APPROVED'),
(3, 'KRA3', 0, 'Human Resource Department', 'SOG-004', 'APPROVED'),
(4, 'KRA4', 0, 'Human Resource Department', 'SOG-004', 'APPROVED'),
(5, 'ANOTHER ONE', 0, 'College of Information Technology Education', 'SOG-002', 'APPROVED');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` text NOT NULL,
  `competencyDesc` text NOT NULL,
  `requiredProfLevel` varchar(50) NOT NULL,
  `weights` float NOT NULL,
  `rating` int(2) NOT NULL,
  `weightedRating` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2a`
--

INSERT INTO `tbl_eformp2a` (`id`, `competency`, `competencyDesc`, `requiredProfLevel`, `weights`, `rating`, `weightedRating`) VALUES
(1, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community. Able to adjust to changes and exhibits a positive attitude towards different situations.</p>\r\n', '', 10, 0, 0),
(2, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approaches to identify opportunities, implement solutions, measure impact and serve customers.</p>\r\n', '', 10, 0, 0),
(3, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', '', 20, 0, 0),
(4, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.&nbsp;</p>\r\n', '', 20, 0, 0),
(5, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession. &nbsp;Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', '', 10, 0, 0),
(6, 'Leadership Excellence', '', '', 10, 0, 0),
(7, 'Management Excellence', '', '', 10, 0, 0),
(8, 'People Development', '', '', 10, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2b`
--

INSERT INTO `tbl_eformp2b` (`id`, `competency`) VALUES
(1, 'Adaptability'),
(2, 'Creativity and Innovation'),
(3, 'Customer Focus'),
(4, 'Team Orientation'),
(5, 'Technical Knowledge'),
(6, 'Leadership Excellence'),
(7, 'Management Excellence'),
(8, 'People Development');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp3`
--

INSERT INTO `tbl_eformp3` (`id`, `areas`) VALUES
(1, 'DISCIPLINE'),
(2, 'ATTENDANCE'),
(3, 'PUNCTUALITY'),
(4, 'COMPLETION OF TRAINING');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp4` (
  `id` int(11) NOT NULL,
  `areas` varchar(30) NOT NULL,
  `customerFeedback` varchar(100) NOT NULL,
  `respondentScore` int(2) NOT NULL,
  `ratingFactor` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp4`
--

INSERT INTO `tbl_eformp4` (`id`, `areas`, `customerFeedback`, `respondentScore`, `ratingFactor`) VALUES
(1, 'April - June', '', 0, 0),
(2, 'July - September', '', 0, 0),
(3, 'October - December', '', 0, 0),
(4, 'January - March', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation_requests`
--

CREATE TABLE IF NOT EXISTS `tbl_evaluation_requests` (
  `id` int(11) NOT NULL,
  `admin_id` varchar(60) NOT NULL,
  `employee_missed` varchar(60) NOT NULL,
  `employee_department` varchar(60) NOT NULL,
  `reason` text NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `request_status` varchar(30) NOT NULL DEFAULT 'SENT'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation_results`
--

CREATE TABLE IF NOT EXISTS `tbl_evaluation_results` (
  `id` int(11) NOT NULL,
  `emp_id` varchar(60) NOT NULL,
  `employee_firstName` varchar(60) NOT NULL,
  `employee_middleName` varchar(60) NOT NULL,
  `employee_lastName` varchar(60) NOT NULL,
  `employee_department` varchar(60) NOT NULL,
  `employee_position` varchar(30) NOT NULL,
  `employee_job_level` varchar(30) NOT NULL,
  `employee_years_company` int(11) NOT NULL,
  `employee_years_position` int(11) NOT NULL,
  `evaluationStatus` varchar(30) NOT NULL DEFAULT 'NOT YET STARTED',
  `performance_rating` varchar(100) NOT NULL,
  `overall_rating` varchar(100) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `ev_sog_feedback` text NOT NULL,
  `late_status` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_evaluation_results`
--

INSERT INTO `tbl_evaluation_results` (`id`, `emp_id`, `employee_firstName`, `employee_middleName`, `employee_lastName`, `employee_department`, `employee_position`, `employee_job_level`, `employee_years_company`, `employee_years_position`, `evaluationStatus`, `performance_rating`, `overall_rating`, `answeredBy`, `performanceCycle`, `ev_sog_feedback`, `late_status`) VALUES
(5, 'SOG-001', 'John', 'F', 'Mayer', 'College of Information Technology Education', 'Secretary', 'S1', 1, 1, 'NOT YET STARTED', '', '', '', '02/2018-03/2018', '', ''),
(6, 'SOG-002', 'Veronica', 'F', 'Canlas', 'College of Information Technology Education', 'A Sec', 'S1', 7, 7, 'COMPLETED', '3.47', '3.49', 'College of Information Technology Education', '02/2018-03/2018', '', ''),
(7, 'SOG-003', 'Mark', 'F', 'Hoppus', 'College of Health and Sciences', 'Sec', 'S1', 5, 5, 'NOT YET STARTED', '', '', '', '02/2018-03/2018', '', ''),
(8, 'SOG-004', 'Tony', '', 'Stark', 'Human Resource Department', 'Secretary', 'S1', 2, 2, 'COMPLETED', '3.36', '3.4', 'Human Resource Department', '02/2018-03/2018', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kpiratingscale`
--

CREATE TABLE IF NOT EXISTS `tbl_kpiratingscale` (
  `id` int(11) NOT NULL,
  `rating_value` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `weighted_rating` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kpiratingscale`
--

INSERT INTO `tbl_kpiratingscale` (`id`, `rating_value`, `description`, `weighted_rating`) VALUES
(1, '4', 'Exceeds Expectations (Performs above standards all the time)', ''),
(2, '3', 'Meets Expectations (Solid, consistent performance; Generally meets standards', ''),
(3, '2', 'Needs Improvement (Frequently fails to meet standards)', ''),
(4, '1', 'Unacceptable (Performance is below job requirements and needs)', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kpi_requests`
--

CREATE TABLE IF NOT EXISTS `tbl_kpi_requests` (
  `id` int(11) NOT NULL,
  `reason` text NOT NULL,
  `sent_at` varchar(60) NOT NULL,
  `department` varchar(60) NOT NULL,
  `request_status` varchar(60) NOT NULL,
  `date_of_kpi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE IF NOT EXISTS `tbl_messages` (
  `id` int(11) NOT NULL,
  `subject` text NOT NULL,
  `content` text NOT NULL,
  `sender` varchar(50) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'unread',
  `checked` varchar(3) NOT NULL DEFAULT 'no',
  `sent_at` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `deleted_by_sender` varchar(100) NOT NULL DEFAULT 'no',
  `deleted_by_receiver` varchar(100) NOT NULL DEFAULT 'no',
  `trash_by_HR` varchar(100) NOT NULL DEFAULT 'no',
  `trash_by_Department` varchar(100) NOT NULL DEFAULT 'no',
  `revision` varchar(100) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `id_appeal` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE IF NOT EXISTS `tbl_notification` (
  `id` int(11) NOT NULL,
  `notification` text NOT NULL,
  `sendBy` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`id`, `notification`, `sendBy`, `status`, `timeStamp`) VALUES
(1, 'Human Resource Department SENT AN EVALUATION', 'Human Reso', 'read', '2017-12-10 13:37:45'),
(2, 'College of Information Technology Education Sent a KPI', 'admin', 'unread', '2017-12-10 13:42:36'),
(3, 'College of Information Technology Education SENT AN EVALUATION', 'College of', 'unread', '2017-12-10 13:54:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification_user`
--

CREATE TABLE IF NOT EXISTS `tbl_notification_user` (
  `id` int(11) NOT NULL,
  `notification` text NOT NULL,
  `sendBy` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `owner` varchar(100) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification_user`
--

INSERT INTO `tbl_notification_user` (`id`, `notification`, `sendBy`, `status`, `owner`, `timeStamp`) VALUES
(1, 'HR APPROVED YOUR KRA', 'Human Resource Department', 'unread', 'College of Information Technology Education', '2017-12-10 13:43:27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_p1kpi` (
  `kraID` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kpiDesc` text NOT NULL,
  `kpiOwner` varchar(100) NOT NULL,
  `kraCreatedFor` varchar(60) NOT NULL,
  `kpiStatus` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p1kpi`
--

INSERT INTO `tbl_p1kpi` (`kraID`, `kpiID`, `kpiDesc`, `kpiOwner`, `kraCreatedFor`, `kpiStatus`) VALUES
(1, 1, 'A', 'Human Resource Department', 'SOG-004', 'APPROVED'),
(1, 2, 'A2', 'Human Resource Department', 'SOG-004', 'APPROVED'),
(1, 3, 'A3', 'Human Resource Department', 'SOG-004', 'APPROVED'),
(1, 4, 'A4', 'Human Resource Department', 'SOG-004', 'APPROVED'),
(2, 1, 'B1', 'Human Resource Department', 'SOG-004', 'APPROVED'),
(3, 1, 'C1', 'Human Resource Department', 'SOG-004', 'APPROVED'),
(2, 2, 'B2', 'Human Resource Department', 'SOG-004', 'APPROVED'),
(2, 3, 'B3', 'Human Resource Department', 'SOG-004', 'APPROVED'),
(5, 1, '', 'College of Information Technology Education', 'SOG-002', 'APPROVED'),
(5, 2, 'Wawa', 'College of Information Technology Education', 'SOG-002', 'APPROVED');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p2bcritinc`
--

CREATE TABLE IF NOT EXISTS `tbl_p2bcritinc` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(60) NOT NULL,
  `criticalIncident` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p2bcritinc`
--

INSERT INTO `tbl_p2bcritinc` (`id`, `competencyName`, `criticalIncident`) VALUES
(1, 'Adaptability', 'Critical Incident'),
(2, 'Adaptability', 'Critical Incident'),
(3, 'Adaptability', 'Critical Incident'),
(1, 'Creativity and Innovation', 'Critical Incident'),
(2, 'Creativity and Innovation', 'Critical Incident'),
(3, 'Creativity and Innovation', 'Critical Incident'),
(1, 'Customer Focus', 'Critical Incident'),
(2, 'Customer Focus', 'Critical Incident'),
(3, 'Customer Focus', 'Critical Incident'),
(1, 'Team Orientation', 'Critical Incident'),
(2, 'Team Orientation', 'Critical Incident'),
(3, 'Team Orientation', 'Critical Incident'),
(1, 'Technical Knowledge', 'Critical Incident'),
(2, 'Technical Knowledge', 'Critical Incident'),
(3, 'Technical Knowledge', 'Critical Incident'),
(1, 'Leadership Excellence', 'Critical Incident'),
(2, 'Leadership Excellence', 'Critical Incident'),
(3, 'Leadership Excellence', 'Critical Incident'),
(1, 'Management Excellence', 'Critical Incident'),
(2, 'Management Excellence', 'Critical Incident'),
(3, 'Management Excellence', 'Critical Incident'),
(1, 'People Development', 'Critical Incident'),
(2, 'People Development', 'Critical Incident'),
(3, 'People Development', 'Critical Incident');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p3rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_p3rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p3rubric`
--

INSERT INTO `tbl_p3rubric` (`id`, `areaName`, `rubricDesc`) VALUES
(4, 'DISCIPLINE', ''),
(3, 'DISCIPLINE', '3'),
(2, 'DISCIPLINE', ''),
(1, 'DISCIPLINE', ''),
(4, 'ATTENDANCE', ''),
(4, 'PUNCTUALITY', ''),
(3, 'ATTENDANCE', ''),
(4, 'COMPLETION OF TRAINING', ''),
(3, 'COMPLETION OF TRAINING', ''),
(2, 'ATTENDANCE', ''),
(1, 'ATTENDANCE', ''),
(3, 'PUNCTUALITY', ''),
(2, 'PUNCTUALITY', ''),
(2, 'COMPLETION OF TRAINING', ''),
(1, 'PUNCTUALITY', ''),
(1, 'COMPLETION OF TRAINING', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_proficiency_level`
--

CREATE TABLE IF NOT EXISTS `tbl_proficiency_level` (
  `id` int(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `initials` text NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_proficiency_level`
--

INSERT INTO `tbl_proficiency_level` (`id`, `value`, `initials`, `title`, `content`) VALUES
(1, '4', 'A', 'Contributing Independently', 'Learner-Doer. Individual contributor. Follower, team-player. Customer touchpoint.'),
(2, '3', 'B', 'Contributing with Expertise', 'Specialist. Expert implementor. Customer advocate. Problem Solver.'),
(3, '2', 'C', 'Contribution through Teams/Peers', 'Project Leader. Idea Leader. Technical Troubleshooter. Coach.'),
(4, '1', 'D', 'Contributing through Strategy Building', 'Navigator. Idea Innovator. Leader Developer. Mission Advocate.'),
(5, '0', 'NE', 'Early stages of application of the item', 'Behavior not yet present.'),
(6, '0', 'NA', '', 'The item does not apply to your position or performance of assigned tasks and responsibilities.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating_execute`
--

CREATE TABLE IF NOT EXISTS `tbl_rating_execute` (
  `ID` int(11) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `performance_rating` varchar(100) NOT NULL,
  `overall_rating` varchar(100) NOT NULL,
  `performanceCycle` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rating_execute`
--

INSERT INTO `tbl_rating_execute` (`ID`, `emp_id`, `performance_rating`, `overall_rating`, `performanceCycle`) VALUES
(1, 'SOG-004', '3.36', '3.4', '02/2018-03/2018'),
(2, 'SOG-002', '3.47', '3.49', '02/2018-03/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp2a` (
  `criticalIncidentCount` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp2a`
--

INSERT INTO `tbl_settingsp2a` (`criticalIncidentCount`) VALUES
(3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp3`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp3` (
  `rubricCount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp3`
--

INSERT INTO `tbl_settingsp3` (`rubricCount`) VALUES
(4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sog_employee`
--

CREATE TABLE IF NOT EXISTS `tbl_sog_employee` (
  `emp_id` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `department_head` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_temp` varchar(100) NOT NULL,
  `email_employee` text NOT NULL,
  `paths` text NOT NULL,
  `kraStatus` varchar(15) NOT NULL DEFAULT 'NOT YET STARTED'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sog_employee`
--

INSERT INTO `tbl_sog_employee` (`emp_id`, `firstname`, `middlename`, `lastname`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `department_head`, `username`, `password`, `password_temp`, `email_employee`, `paths`, `kraStatus`) VALUES
('SOG-001', 'John', 'F', 'Mayer', 'College of Information Technology Education', '1', 'Secretary', 'S1', '2016', '1', '', 'SOG-001', '158ad40dc08910e8eafa45d46143413f', 'MAYER', 'johnfmayer@gmail.com', '../assets/uploaded_files/accounts/3rPeiTKt.png', 'NOT YET STARTED'),
('SOG-002', 'Veronica', 'F', 'Canlas', 'College of Information Technology Education', '7', 'A Sec', 'S1', '2010', '7', '', 'SOG-002', 'e60781268442acd2fba50e5d62658918', 'CANLAS', 'veronne@gmail.com', '../assets/uploaded_files/accounts/maamronne.jpg', 'APPROVED'),
('SOG-003', 'Mark', 'F', 'Hoppus', 'College of Health and Sciences', '5', 'Sec', 'S1', '2012', '5', '', 'SOG-003', 'f4e7ddfe616f4195075b90e8f7332810', 'HOPPUS', 'mrmarkhop@gmail.com', '../assets/uploaded_files/accounts/default.png', 'NOT YET STARTED'),
('SOG-004', 'Tony', '', 'Stark', 'Human Resource Department', '2', 'Secretary', 'S1', '2015', '2', '', 'SOG-004', 'a9106b6bc4ae581eb39418098a2891b4', 'STARK', 'tonyyyy@gmail.com', '../assets/uploaded_files/accounts/default.png', 'NOT YET STARTED');

-- --------------------------------------------------------

--
-- Table structure for table `z_logs_ev`
--

CREATE TABLE IF NOT EXISTS `z_logs_ev` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp2a`
--
ALTER TABLE `tbl_ans_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp2b`
--
ALTER TABLE `tbl_ans_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp3`
--
ALTER TABLE `tbl_ans_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp4`
--
ALTER TABLE `tbl_ans_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_summary_comments_acknowledgement`
--
ALTER TABLE `tbl_ans_summary_comments_acknowledgement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_competencyratingscale`
--
ALTER TABLE `tbl_competencyratingscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_current_date`
--
ALTER TABLE `tbl_current_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_dump_evaluation_period`
--
ALTER TABLE `tbl_dump_evaluation_period`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp1`
--
ALTER TABLE `tbl_eformp1`
  ADD PRIMARY KEY (`kraID`);

--
-- Indexes for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_evaluation_requests`
--
ALTER TABLE `tbl_evaluation_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_evaluation_results`
--
ALTER TABLE `tbl_evaluation_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kpiratingscale`
--
ALTER TABLE `tbl_kpiratingscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kpi_requests`
--
ALTER TABLE `tbl_kpi_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification_user`
--
ALTER TABLE `tbl_notification_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_rating_execute`
--
ALTER TABLE `tbl_rating_execute`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_sog_employee`
--
ALTER TABLE `tbl_sog_employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `z_logs_ev`
--
ALTER TABLE `z_logs_ev`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp2a`
--
ALTER TABLE `tbl_ans_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp2b`
--
ALTER TABLE `tbl_ans_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp3`
--
ALTER TABLE `tbl_ans_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp4`
--
ALTER TABLE `tbl_ans_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_ans_summary_comments_acknowledgement`
--
ALTER TABLE `tbl_ans_summary_comments_acknowledgement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_competencyratingscale`
--
ALTER TABLE `tbl_competencyratingscale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_current_date`
--
ALTER TABLE `tbl_current_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_dump_evaluation_period`
--
ALTER TABLE `tbl_dump_evaluation_period`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_evaluation_requests`
--
ALTER TABLE `tbl_evaluation_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_evaluation_results`
--
ALTER TABLE `tbl_evaluation_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_kpiratingscale`
--
ALTER TABLE `tbl_kpiratingscale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_kpi_requests`
--
ALTER TABLE `tbl_kpi_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_notification_user`
--
ALTER TABLE `tbl_notification_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_rating_execute`
--
ALTER TABLE `tbl_rating_execute`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `z_logs_ev`
--
ALTER TABLE `z_logs_ev`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
