<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Welcome to SOG Evaluation System</title>



    <script src="assets/bower_components/sweetalert2/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="assets/bower_components/sweetalert2/sweetalert2.min.css">


    <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <script src="assets/bower_components/jquery/dist/jquery.js"></script>

    <script src="assets/bower_components/jquery-ui-1.12.1/jquery-ui.min.js"></script>

    <!-- End Bootstrapjs  -->
    <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <link rel="icon" href="assets/images/upang.png" type="image/png">
    <style type="text/css">
        body{
            background-image: url('assets/images/bg.jpg');
            background-size: 100%;
            background-repeat: no-repeat;
        }
        .navi{
           width: 100%;
           background-color:#2b3819;
           height: 80px;
           color: white; 
        }
        .title{
          font-size:25px; 
          padding-top: 10px;  
        }
        .title2{
            font-size:30px; 
            margin-top:-30px;
        }
        .pads{
           padding-left:80px; 
        }
        #container {
            opacity: 0.8;
            background-color: #2b3819;
            position: absolute;
            border-radius: 10px;
            height: 420px;
            width: 80%;
        }
        #box {
            opacity: 1;
            position: absolute;
            height: 420px;
            width: 80%;
        }
        .lab{
            color:white; 
            font-size:1.2em;
            text-transform: uppercase;
        }
        .log{
            font-size:25px;
            color: white;
        }
        .inp{
            padding:0px 20px 10px 20px; 
        }
        .foot{
            background-color: #2b3819;
            border-color: #2b3819;
        }
        .hr{
            padding: 0px 27px 0px 27px
        }
        a{
            color:white;
        }
        a:hover{
            color:#2b3819;
            text-decoration: none;
        }
    </style>
</head>

<body>

	<link rel="stylesheet" href="assets/bower_components/font-awesome/css/font-awesome.min.css"/>


     <div>
        <div class="navi">
            <div class="col-sm-1">
                <img src="assets/images/logo.png" height="200px;">
            </div>
            <div class="col-sm-5 pads">
                <label class="title">PHINMA UNIVERSITY OF PANGASINAN</label><br>
                <label class="title2">HR DEPARTMENT</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-8"></div>
            <div class="col-sm-4">
                <div id="container"></div>
                <div id="box">
                    <form method="post" name="myform">
                    <div style="padding: 15px 20px 5px 20px;">
                        <span class="log">LOGIN</span>
                    </div>

                    <div class="hr">
                        <hr>
                    </div>

                   <div style="padding: 15px 20px 5px 20px;">
                        <label class="lab">Username</label>
                    </div>

                    <div class="input-group  inp">
                      <span class="input-group-addon" id="basic-addon1"> <span class="fa fa-user">&nbsp</span></span>
                      <input type="text" class="form-control" placeholder="0-0000-000" name="username" aria-label="username" aria-describedby="basic-addon1">
                    </div>

                    <div style="padding: 5px 20px 5px 20px;">
                        <label class="lab">Password</label>
                    </div>

                    <div class="input-group inp">
                      <span class="input-group-addon" id="basic-addon1"><span class="fa fa-key"></span></span>
                      <input type="password" class="form-control" placeholder="*******" name="password" aria-label="password" aria-describedby="basic-addon1">
                    </div>

                   
                    <div style="padding: 15px 20px 0px 20px;">
                        <span style="color:white;"> forgot password? <a href="#" data-toggle="modal" data-target="#myModal">CLICK HERE</a></span>
                    </div>

                     <br>

                    <div style="padding: 15px 20px 5px 20px; margin-top:-20px;">
                        <button type="submit"  name="btnLogin" class="btn btn-sample" style="width: 100%;"> LOGIN <span class="fa fa-sign-in"></span></button>
                    </div>

                 </div>
             </form>
            </div>
        </div>
          <div class="navbar navbar-default navbar-fixed-bottom foot" >
            <div class="container">
              <p class="navbar-text pull-left" style="color:white;">© 2017 SOG Evaluation</p>
            </div>
        </div>
    </div>
	

</body>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reset your Password by E-mail</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post">
                        <div class="row container-fluid"><p>Type your E-mail address below and click 'SEND RESET TOKEN'. The message will contain instructions to help you reset your password.</p></div>
                        <div class="col-sm-2"><label>Email:</label></div>
                        <div class="col-sm-10"><input type="email" class="form-control" name="email_user" placeholder="example@gmail.com" /></div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="col-sm-12"><button class="btn btn-block btn-info" name="btnResetPassword">SEND RESET TOKEN</button></div>
                </form>
            </div>
        </div>

    </div>
</div>

</html>


<?php
if(isset($_POST['btnLogin'])){
//connection
    include'connection.php';

//field variables
    $username=$_POST['username'];
    $password=md5($_POST['password']);

//fetch all records
    $query1="SELECT * from tbl_admins WHERE username='$username' AND password='$password'";
    $result1=mysql_query($query1);
    $record=mysql_num_rows($result1);

//second query
    if($record==1){
        $query2="SELECT * from tbl_admins WHERE username='$username' AND password='$password'";
        $result2=mysql_query($query1);
        //list of array
        while(list($id,$lastname,$firstname,$middlename,$department,$years_in_company,$position,$job_level, $year_applied, $years_in_position,$username, $password, $password_temp,$paths, $userlevel, $email)=mysql_fetch_array($result2))
        {
            $aid=$id;
            $alastname=$lastname;
            $afirstname=$firstname;
            $amiddlename=$middlename;
            $adepartment=$department;
            $ayears_in_company=$years_in_company;
            $aposition=$position;
            $ajob_level=$job_level;
            $ayear_applied=$year_applied;
            $ayears_in_position = $years_in_position;
            $ausername = $username;
            $apassword = $password;
            $apassword_temp = $password_temp;
            $auserlevel = $userlevel;
            $apaths = $paths;
            $aemail = $email;
        }

        $_SESSION['id']=$aid;
        $_SESSION['firstname']=$afirstname;
        $_SESSION['middlename']=$amiddlename;
        $_SESSION['lastname']=$alastname;
        $_SESSION['department']=$adepartment;
        $_SESSION['years_in_company']=$ayears_in_company;
        $_SESSION['position']=$aposition;
        $_SESSION['job_level']=$ajob_level;
        $_SESSION['year_applied']=$ayear_applied;
        $_SESSION['years_in_position']=$ayears_in_position;
        $_SESSION['username'] = $ausername;
        $_SESSION['password'] = $apassword;
        $_SESSION['password_temp'] = $apassword_temp;
        $_SESSION['userlevel']=$auserlevel;
        $_SESSION['paths']=$apaths;
        $_SESSION['email'] = $aemail;
        $_SESSION['id'];

        //open landing page
        echo"
            
			<script type='text/javascript'>
				swal({
                  title: 'SUCCESS!',
                  text: 'Login Successful',
                  type: \"success\",
                  timer: 1000,
                  allowOutsideClick: false,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"_superadmin/dashboard.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"_superadmin/dashboard.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'Invalid Login! Check your credentials',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"index.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"index.php\";
                    }
                  }
                )
			</script>
		";
    }
}


?>

<?php
if(isset($_POST['btnResetPassword']) && $_POST['email_user'])
{
    include 'connection.php';

    $email = $_POST['email_user'];
    $select_admin=mysql_query("select * from tbl_admins where email='$email'");
    $select_sog=mysql_query("select * from tbl_sog_employee where email_employee='$email'");

    if(mysql_num_rows($select_admin)==1)
    {
        while($row=mysql_fetch_array($select_admin))
        {
            $email=$row['email'];
            $pass=md5($row['password']);
        }
        $link="<a href='http://localhost/git/sog/index/reset_password_admin.php?key=".$email."&reset=".$pass."'>Click To Reset password</a>";
        require_once('assets/phpmailer/PHPMailerAutoload.php');
        $mail = new PHPMailer();
        $mail->CharSet =  "utf-8";
        $mail->IsSMTP();
        // enable SMTP authentication
        $mail->SMTPAuth = true;
        // GMAIL username
        $mail->Username = "es.macbeth.2017@gmail.com";
        // GMAIL password
        $mail->Password = "nopasswordno";
        $mail->SMTPSecure = "ssl";
        // sets GMAIL as the SMTP server
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        // set the SMTP port for the GMAIL server
        $mail->Port = "465";
        $mail->From='es.macbeth.2017@gmail.com';
        $mail->FromName='Mac Beth';
        $mail->AddAddress($email);
        $mail->Subject  =  'Reset Password';
        $mail->IsHTML(true);
        $mail->Body    = 'Click On This Link to Reset Password '.$link.'';
        if($mail->Send())
        {

            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'Check Your Email and Click on the link sent to your email',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"index.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"index.php\";
                    }
                  }
                )
			</script>
		";

        }
        else
        {
            echo "Mail Error - >".$mail->ErrorInfo;
        }
    }
    elseif(mysql_num_rows($select_sog)==1)
    {
        while($row=mysql_fetch_array($select_sog))
        {
            $email=$row['email'];
            $pass=md5($row['password']);
        }
        $link="<a href='http://localhost/git/sog/index/reset_password_sog.php?key=".$email."&reset=".$pass."'>Click To Reset password</a>";
        require_once('assets/phpmailer/PHPMailerAutoload.php');
        $mail = new PHPMailer();
        $mail->CharSet =  "utf-8";
        $mail->IsSMTP();
        // enable SMTP authentication
        $mail->SMTPAuth = true;
        // GMAIL username
        $mail->Username = "es.macbeth.2017@gmail.com";
        // GMAIL password
        $mail->Password = "nopasswordno";
        $mail->SMTPSecure = "ssl";
        // sets GMAIL as the SMTP server
        $mail->Host = "smtp.gmail.com";
        // set the SMTP port for the GMAIL server
        $mail->Port = "465";
        $mail->From='sirmacbeth.es@gmail.com';
        $mail->FromName='Mac Beth';
        /*$mail->AddAddress('aysonjustinmax@gmail.com', 'Justin');*/
        $mail->AddAddress($email);
        $mail->Subject  =  'Reset Password';
        $mail->IsHTML(true);
        $mail->Body    = 'Click On This Link to Reset Password '.$link.'';
        if($mail->Send())
        {
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'Check Your Email and Click on the link sent to your email',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"index.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"index.php\";
                    }
                  }
                )
			</script>
		";
        }
        else
        {
            echo "Mail Error - >".$mail->ErrorInfo;
        }
    }
    else{
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'Email not found',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"index.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"index.php\";
                    }
                  }
                )
			</script>
		";
    }
}
?>
