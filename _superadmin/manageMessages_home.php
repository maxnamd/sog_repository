<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 9/8/2017
 * Time: 9:21 PM
 */
?>
<?php include 'includes/header.php'; ?>
<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview4aa").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Mail
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <a href="manageMessages_compose.php" class="btn btn-primary btn-block margin-bottom">Compose</a>

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Folders</h3>

                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>

                     <?php 
                        $result= mysql_query("SELECT * FROM tbl_messages WHERE status = 'unread' AND receiver = 'Human Resource Department' AND deleted_by_receiver = 'no'");
                        $num_rows = mysql_num_rows($result);
                    ?>
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            <li class="active"><a href="#"><i class="fa fa-inbox"></i> Inbox
                                    <span class="label label-primary pull-right"><?php echo $num_rows?></span></a></li>
                            <li><a href="manageMessages_home_sent.php"><i class="fa fa-envelope-o"></i> Sent</a></li>
                            <li><a href="manageMessages_home_trash.php"><i class="fa fa-trash-o"></i> Trash</a></li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
          
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Inbox</h3>

                        <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <input type="text" class="form-control input-sm" placeholder="Search Mail">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="mailbox-controls">

                      
                            <!-- Check all button -->
                            <input type="checkbox" class="btn btn-default btn-sm checkbox-toggle" onClick="toggle(this)">
                           
                            <div class="btn-group">
                              <form method="post">
                                <button type="submit" class="btn btn-default btn-sm" name="btn_delete" id="btn_delete" onclick="confirmation();return false;"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                            </div>
                            <!-- /.btn-group -->
                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>

                            <div class="pull-right">
                                1-50/200
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                                </div>
                                <!-- /.btn-group -->
                            </div>
                            <!-- /.pull-right -->
                        </div>
                        <div class="table-responsive mailbox-messages">
                            <table class="table table-hover table-striped">
                                <tbody>
                                 <?php
                                 
                            $query = "SELECT * FROM tbl_messages WHERE receiver='Human Resource Department' AND deleted_by_receiver='no' ORDER BY id DESC";
                            // execute query 
                            $result = mysql_query($query) or die ("Error in query: $query. ".mysql_error()); 
                            // see if any rows were returned 
                            if (mysql_num_rows($result) == 0) 
                            { 
                              echo"<td colspan='7'><center><h4><b>Inbox is empty!</b></h4></center></td>";
                            }
                            else
                            {
                              $display_messages=mysql_query("SELECT * FROM tbl_messages WHERE receiver='Human Resource Department' AND deleted_by_receiver='no' ORDER BY id DESC") or die(mysql_error());
                                  
                                $counter = 0;
                                  while($row=mysql_fetch_array($display_messages)){ 
                                   $counter++;

                            ?>
                            <tr> 
                            
                                    <td><input type="checkbox" name="deleteCb[]" value="<?php echo $row['id']?>"></td>
                                   
                                    <td> 
                                    
                                    <input type="hidden" name="message_id" value="<?php echo $row['id']?>">
                                    <input type="hidden" name="message_subject" value="<?php echo $row['subject']?>">
                                    <input type="hidden" name="message_content" value="<?php echo $row['content']?>">
                                    <input type="hidden" name="message_sender" value="<?php echo $row['sender']?>">
                                    <input type="hidden" name="message_receiver" value="<?php echo $row['receiver']?>">
                                    <input type="hidden" name="message_status" value="<?php echo $row['status']?>">
                                    <input type="hidden" name="message_checked" value="<?php echo $row['checked']?>">
                                    <input type="hidden" name="message_sent_at" value="<?php echo $row['sent_at']?>">
                                    <input type="hidden" name="message_location" value="<?php echo $row['location']?>">
                                    </td>
                                    <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
                                    
                                   <td class="mailbox-name"><a href="#viewInbox<?php echo $row['id'];?>" data-toggle="modal"><?php echo $row['sender']?></a></td>
                                  
                                  <td class="mailbox-subject"><b><?php echo $row['subject'];?></b> - <?php echo $row['content'];?>   </td>
                                   <td class="mailbox-attachment"></td>
                                  <td class="mailbox-date"><?php echo $row['sent_at'];?></td>

<!-- VIEW MESSAGE MODAL-->
<div class="modal fade" id="viewInbox<?php echo $row['id']; ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">View Message</h4>
      </div>
        
      <div class="modal-body">


                <div class="form-group" id="">
                  <label class="col-sm-3 control-label">From: </label>
                  <div class="col-sm-8">
                    <input type="text" name="sender" id="" class="form-control" value="<?php echo $row['sender']; ?>" disabled>
                  </div>
                </div>

                <br/>
                <br/>

                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Subject: </label>
                  <div class="col-sm-8">
                    <input type="text" name="subject" id="" class="form-control" value="<?php echo $row['subject']?>" disabled>
                  </div>
                </div>

                <br/>
                <br/>
                
                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Content: </label>
                  <div class="col-sm-8">
                    <textarea name="content" class="form-control" style="width: 100%; height: 300px; resize: none" disabled> <?php echo $row['content']?> </textarea>
                  </div>
                </div>

               
    </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="modal-footer">
                      <?php

                            if($row['revision'] == 'YES')
                            {
                                echo "<a href='#'' class='btn btn-success btn-flat' data-toggle='modal' data-target='#Reply' data-dismiss='modal'>Reply</a>";
                            }
                    ?>
                        <input type="button" class="btn btn-default" value="Close" data-dismiss="modal" style="margin-right: 7em;"/>
                    </div>
                </div>
            </div>
              
          
      </div>
    </div>
  </div>
</div>
<!-- VIEW MESSAGE MODAL-->

<!-- VIEW REPLY MODAL-->
<div class="modal fade" id="Reply" tabindex="-1" role="dialog">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Reply Message</h4>
      </div>
        
      <div class="modal-body">

                <div class="form-group" id="">
                  <label class="col-sm-3 control-label">To: </label>
                  <div class="col-sm-8">
                    <input type="text" name="reply_sender" id="" class="form-control" value="<?php echo $row['sender']; ?>" readonly>
                    <input type="hidden" name="reply_receiver" id="" class="form-control" value="<?php echo $row['receiver']; ?>" >
                  </div>
                </div>

                <br/>
                <br/>


                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Subject: </label>
                  <div class="col-sm-8">
                    <input type="text" name="reply_subject" id="" class="form-control" value="<?php echo $row['subject']?>" readonly>
                  </div>
                </div>

                <br/>
                <br/>

                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Content: </label>
                  <div class="col-sm-8">
                    <textarea name="reply_content[]" class="form-control" style="width: 100%; height: 300px; resize: none"></textarea>
                  </div>
                </div>

               
    </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="modal-footer">
                            <input type='submit' name='btn_send' value='SEND' class="btn btn-success">
                            <input type="reset" class="btn btn-default" value="Clear" style="margin-right: 7em;"/>
                    </div>
                </div>
            </div>
              
      </div>
    </div>
  </div>
</div>
<!-- VIEW REPLY MODAL-->
                                 

                                  <?php }} ?>
                               
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->
                     </form>
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>


<?php
if(isset($_POST['btn_delete']))
{
        $message_subject = $_POST['message_subject'];
        $message_content = $_POST['message_content'];
        $message_sender = $_POST['message_sender'];
        $message_receiver = $_POST['message_receiver'];
        $message_status = $_POST['message_status'];
        $message_checked = $_POST['message_checked'];
        $message_sent_at = $_POST['message_sent_at'];
         $message_location = $_POST['message_location'];

        $deleteCb = $_REQUEST['deleteCb'];
        for($i=0;$i<count($deleteCb);$i++)
        {
            $news_id = $deleteCb[$i];
            $q = "UPDATE tbl_messages SET deleted_by_receiver='YES' where id= ".$news_id;
            mysql_query($q);

            echo "<script type='text/javascript'>
          open('manageMessages_home.php','_self');
          </script>";      
        }
        
    }
?>

<?php
if(isset($_POST['btn_send']))
{
      $reply_sender = $_POST['reply_receiver'];
      $reply_receiver = $_POST['reply_sender'];
      $reply_subject = $_POST['reply_subject'];
      $reply_content = $_POST['reply_content'];

      $reply_content = $_REQUEST['reply_content'];
        for($i=0;$i<count($reply_content);$i++)
        {
            $reply_laman = $reply_content[$i];

            if($reply_laman == "")
            {

            }

            else{
            $query="INSERT INTO tbl_messages (subject,content,sender,receiver,location,revision) VALUES ('".addslashes($reply_subject)."','".addslashes($reply_laman)."','".addslashes($reply_sender)."','".addslashes($reply_receiver)."','sentbox','YES')";
            mysql_query($query) or die(mysql_error());

         echo "<script type='text/javascript'>
         alert('Message Sent!');
          open('manageMessages_home.php','_self');
          </script>";
          }          
        }

      
        
    }
?>

<script language="Javascript">
function toggle(source) {
  checkboxes = document.getElementsByName('deleteCb[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

 <script type="text/javascript">
                      function confirmation()
                      {
                        if(confirm('Are you sure to delete?')==1)
                        {
                          document.getElementById('btn_delete').submit();
                        }
                      
                      }
                    </script>