
<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 11/10/2017
 * Time: 3:26 PM
 */
include 'includes/header.php';
?>
<!-- Setting the treeview active -->
<script type="text/javascript">
    $('#treeview5').removeClass("active menu-open");
    $('#treeview5').addClass('active menu-open');
</script>
<!-- End Setting the treeview active -->
<!-- Content Wrapper. Contains page content -->
<script>
    $(document).ready(function() {
        $('#tbl_missed').DataTable();
    } );
    $(document).ready(function() {
        $('#tbl_requests').DataTable();
    } );
</script>


<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

        <!--DIV OF MISSED EVALUATIONS-->
        <div class="box box-info" style="border-color: green">
            <div class="box-header with-border">
                <h3 class="box-title">Late Evaluation Passers</h3>

               <div class="pull-right" style="margin-right: 1em">
                    <input type="button" class="btn btn-success" onClick="printing();" value="PRINT"/>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin" id="tbl_missed">
                        <thead>
                            <tr>
                                <th class="col-sm-3">Department Head Name</th>
                                <th class="col-sm-2">Employee Name</th>
                                <th class="col-sm-2">Performance Cycle</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php

                            $queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
                            $dateEVRow = mysql_fetch_array($queryDateEV);
                            $startDateEV = $dateEVRow['startDate'];
                            $endDateEV = $dateEVRow['endDate'];
                            $performanceCycle = $startDateEV . '-' . $endDateEV;

                            $display_missed_ev=mysql_query("SELECT * FROM tbl_evaluation_results WHERE late_status = 'YES' AND performanceCycle != '$performanceCycle' ORDER BY employee_department") or die(mysql_error());

                            while($row=mysql_fetch_array($display_missed_ev)) {
                                $employee_department = $row['employee_department'];
                                $employee_firstname = $row['employee_firstName'];
                                $employee_middlename = $row['employee_middleName'];
                                $employee_lastname = $row['employee_lastName'];
                                $employee_fullName = $employee_firstname . ' ' . $employee_lastname;
                                $tbl_performanceCycle = $row['performanceCycle'];
                                $evaluation_stat = $row['evaluationStatus'];

                                $search_admin = mysql_query("SELECT * FROM tbl_admins WHERE department = '$employee_department'");
                                while($row_admin=mysql_fetch_array($search_admin)) {

                                    $head_firstname = $row_admin['firstname'];
                                    $head_middlename = $row_admin['middlename'];
                                    $head_lastname = $row_admin['lastname'];

                                    $head_fullname = $head_firstname . ' ' . $head_lastname;

                                }


                            ?>
                            <tr>
                                <td><?php echo $head_fullname?></td>
                                <td><?php echo $employee_fullName?></td>
                                <td><?php echo $tbl_performanceCycle?></td>
                            </tr>

                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!--END DIV OF MISSED EVALUATIONS-->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>

<script type="text/javascript">

    function printing()
    {
         var myWindow=window.open('','','width=1280,height=720');
         var divtoprint = document.getElementById('tbl_missed');
         var htmlToPrint = '' +
        '<style type="text/css">' +
        'table th, table td {' +
        'border:0.5px solid black;' +
        'border-collapse: collapse;' +
        'padding: 0.5em;' +
        'margin: 0px' +
        '}' + 'select {' + '-webkit-appearance: none !important;' + '-moz-appearance: none !important;' +
        ' appearance: none !important;' + 'border: none !important;' + 'background: none !important;' +
        '</style>';

        /*myWindow.document.write('<center><table><tr><td id="repLogo"><img src="../assets/images/main/logo.png" style="width:100px;height:100px"/></td><td><center><span style="font-family:Segoe UI Light;font-size:11pt;">CAFFEINE HUB<br>LINGAYEN<br>Contact No.(075)09028 82787<br>Sales Report</span></center></td></tr></table></center>');*/
        myWindow.document.write('');
        myWindow.document.write('<div style="margin-left: 5em"><img src="../assets/login/img/upang.png" style="width:100px;height:100px"/> </div> <div style="margin-left: 15em; margin-top: -100px"> PHINMA - UNIVERSITY OF PANGASINAN <br/> <div style="margin-left: 52px;"> Arellano St., Dagupan City </div> <br/> <div style="margin-left: -180px; margin-top: 10px;"> <center> <strong> LATE EVALUATION PASSERS </strong> </center> </div> </div>');
        htmlToPrint += divtoprint.outerHTML;
        myWindow.document.write('<br/><br/>');
        myWindow.document.write(htmlToPrint);
        //myWindow.document.write(document.getElementById('tbl_EV_t10').outerHTML);
        myWindow.document.write('<footer style="position: absolute;right: 0;bottom: 0;left: 0;padding: 1rem;background-color: #efefef;text-align: right;"> <b>TOP 10 Report</b> <br/>Prepared by: Human Resource Department</footer>');
        myWindow.document.close();
        myWindow.focus();
        myWindow.print();
        myWindow.close();
    }
</script>