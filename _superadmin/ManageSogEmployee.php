<?php include 'includes/header.php'; ?>


<?php 
 
?>
<style type="text/css">
.control-label{
  text-align: left !important;
}
</style>
<script>
    $(document).ready(function() {
        $('#tbl_sog').DataTable();
    } );
</script>

<script>
    function getJobLevel(val) {
        $.ajax({
            type: "POST",
            url: "phpfunctions/s_getJobLevel.php",
            data:'department='+val,
            success: function(data){
                $("#job_level_this").html(data);
            }
        });
    }
</script>

<script>
    function getJobLevel_edit(val) {
        $.ajax({
            type: "POST",
            url: "phpfunctions/s_getJobLevel_edit.php",
            data:'department_edit='+val,
            success: function(data){
                $("#job_level_edit").html(data);
            }
        });
    }
</script>

 <!-- Keypress -->
    <script type="text/javascript"> 
        function letter(e) 
        { 
            var key; var keychar; 
                if (window.event) 
                    key = window.event.keyCode; 
                else if (e)   
                    key = e.which; 
                else return true; 
                keychar = String.fromCharCode(key); 
                keychar = keychar.toLowerCase(); 
                if ((("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ").indexOf(keychar) > -1)) 
                    return true;
                else 
                    return false; 
        }
        function number(e) 
        { 
            var key; var keychar; 
                if (window.event) 
                    key = window.event.keyCode; 
                else if (e) 
                    key = e.which; 
                else return true; 
                keychar = String.fromCharCode(key); 
                keychar = keychar.toLowerCase(); 
                if ((("0123456789").indexOf(keychar) > -1))
                    return true; 
                else 
                    return false; 
        }
        function number2(e) 
        { 
            var key; var keychar; 
                if (window.event) 
                    key = window.event.keyCode; 
                else if (e) 
                    key = e.which; 
                else return true; 
                keychar = String.fromCharCode(key); 
                keychar = keychar.toLowerCase(); 
                if ((("0123456789-").indexOf(keychar) > -1))
                    return true; 
                else 
                    return false; 
        }   
        function lenum(e) 
        { 
            var key; var keychar; 
                if (window.event) 
                    key = window.event.keyCode; 
                else if (e) 
                    key = e.which; 
                else return true; 
                keychar = String.fromCharCode(key); 
                keychar = keychar.toLowerCase(); 
                if ((("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789").indexOf(keychar) > -1))
                    return true; 
                else 
                    return false; 
        } 
        function lenum2(e) 
        { 
            var key; var keychar; 
                if (window.event) 
                    key = window.event.keyCode; 
                else if (e) 
                    key = e.which; 
                else return true; 
                keychar = String.fromCharCode(key); 
                keychar = keychar.toLowerCase(); 
                if ((("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789,.").indexOf(keychar) > -1))
                    return true; 
                else 
                    return false; 
        } 

    </script>
    <!-- End of Keypress -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <?php
    $result_dept = mysql_query("SELECT * FROM tbl_departments ");
    $rows_dept = mysql_num_rows($result_dept);

    $result_admin = mysql_query("SELECT * FROM tbl_admins ");
    $rows_admin = mysql_num_rows($result_admin);

    if($rows_dept==0)
    {
        echo "
            <script>
                bootbox.alert('SETUP THE DEPARTMENTS FIRST',
                function() {
                   setTimeout('window.location.replace(\'manageSystemSettings.php\')',600);
                });
            </script>
            ";
    }
    elseif ($rows_admin==0)
    {
        echo "
            <script>
                bootbox.alert('SETUP THE DEPARTMENT HEADS ACCOUNT FIRST,
                function() {
                   setTimeout('window.location.replace(\'manageAdmin.php\')',600);
                });
            </script>
            ";
    }
    else{

    ?>



  <!-- Main content -->
  <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading" style="background: black"><h4><b style="color:white;">SOG Employee Management</b></h4></div>



              <div class="container-fluid" style="margin-top: 1em;">
             
                  <div class="col-sm-2">
                      <a href="#" class="btn btn-success" style="background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7)" data-toggle="modal" data-target="#addEmployee"> &nbsp;&nbsp;<i class="fa fa-plus"></i> NEW EMPLOYEE &nbsp;</a>
                  </div>

                   <div class="col-sm-7"></div>

                   <div class="col-sm-2">
                                        <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_sog" class="btn btn-success" value="Add" style=" width:100px;" />
                                        </form>
                                    </div>
              </div>
            <hr>

            <div class="panel-body">
                <table class="table table-bordered" id="tbl_sog">
                      <thead class="">
                        <tr>
                          <th>Name</th>
                          <th>Department</th>
                          <th>Years In Company</th>
                          <th>Position</th>
                          <th>Job Level</th>
                          <th>Years in Position</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                        <?php
                            $query = "SELECT * FROM tbl_sog_employee";
                            // execute query 
                            $result = mysql_query($query) or die ("Error in query: $query. ".mysql_error()); 
                            // see if any rows were returned 
                            if (mysql_num_rows($result) == 0) 
                            { 
                              echo"<td colspan='7'><center><h4><b>There are no SOG Employees yet.</b></h4></center></td>";
                            }
                            else
                            {
                                $count = 1;
                                $counter=0;
                              $display_sog_employee=mysql_query("SELECT * FROM tbl_sog_employee") or die(mysql_error());
                                  
                                
                                  while($row=mysql_fetch_array($display_sog_employee)){ 
                                    $counter++;
                            ?>
                            <tr> 
                                  <td><b><?php echo $row['firstname'] . ' ' . $row['middlename'].' '. $row['lastname'];?></td>
                                  <td><?php echo $row['department'];?> </td>
                                  <td><?php echo $row['years_in_company'];?> </td>
                                  <td><?php echo $row['position'];?> </td>
                                  <td><?php echo $row['job_level'];?> </td>
                                  <td><?php echo $row['years_in_position'];?> </td>
                                  <td>
                                      <!--view-->
                                      <div class="col-sm-2"> <a href="#viewEmployee-<?php echo $row['emp_id'];?>" class="btn btn-default" title="View" data-toggle="modal"> <i class="fa fa-search"></i></a> </div>
                                        </div> 

                                        <div class="col-sm-2"> </div>
                                       <!--edit--> 
                                          <div class="col-sm-2"> 
                                          <a href="#editEmployee-<?php echo $row['emp_id'];?>" class="btn btn-default" title="Edit" data-toggle="modal"> <i class="fa fa-edit"></i></a> 
                                      </div>
                                      <!--delete-->
                                      <div width="10"></div>
                                      <div class="col-sm-2">
                                        <form method="post"> 
                                            <input type="hidden" name="hiddenID" value="<?php echo $row['emp_id']?>"/>
                                        </form>
                                      </div>

                                      <div width="30"> </div>
                            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#approveModal_<?php echo $counter?>" title="Delete"><i class="fa fa-trash" ></i></a>

                            <!-- Modal -->
                            <div id="approveModal_<?php echo $counter?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header" style="background-color: #2E7D32 !important;border-color: #2E7D32 !important;color: white">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Confirm</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Are you sure you want to Delete This Employee?</h5>
                                        </div>
                                        <div class="modal-footer">
                                            <form method="post">
                                                <input type="hidden" name="admin_id" value="<?php echo $row['emp_id']?>"/>
                                            <button type="submit" class="btn btn-success btn-md" name="btnApprove">Yes</button>
                                            <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">No</button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

<!-- VIEW EMPLOYEE MODAL-->
<div class="modal fade" id="viewEmployee-<?php echo $row['emp_id']; ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">View Employee</h4>
      </div>
        
      <div class="modal-body">

          <table class="table table-bordered">
          <form method="post" class="form-horizontal">

           <tr>
                <td>

                </td>

                 <td>
                       <img src="<?php echo $row['paths'];?>" class="img-responsive img-circle" width="100" height="100">
                  </td>
              </tr>

              <tr>
                <td>
                  <label class="control-label">Employee No: </label>
                </td>

                <td>
                    <input type="text" name="employee_id" id="" class="form-control" style="width: 350px;" value="<?php echo $row['emp_id']; ?>" disabled>
                </td>
              </tr>

              <tr>
                 <td>
                  <label class="control-label">First Name: </label>
                </td>

                  <td>
                    <input type="text" name="firstname" id="" class="form-control" style="width: 350px;" value="<?php echo $row['firstname']?>" disabled>
                  </td>
              </tr>

              <tr>
                 <td>
                  <label class="control-label">Middle Name: </label>
                  </td>

                  <td>
                    <input type="text" name="middlename" id="" class="form-control" style="width: 350px;" value="<?php echo $row['middlename']?>" disabled>
                  </td>
              </tr>

              <tr>
                <td>
                  <label class="control-label">Last Name: </label>
                </td>

                <td>
                    <input type="text" name="lastname" id="" class="form-control" style="width: 350px;" value="<?php echo $row['lastname']?>" disabled>
                  </td>
              </tr>

               <tr>
                <td>
                  <label class="control-label">E-mail Address: </label>
                </td>

                <td>
                    <input type="email" name="email_employee" id="" class="form-control" style="width: 350px;" value="<?php echo $row['email_employee']?>" disabled>
                  </td>
              </tr>
                
                <tr>
                   <td>
                      <label class="control-label">Years in Company: </label>
                   </td>

                   <td>
                      <input type="text" name="years_in_company" id="" class="form-control" style="width: 350px;" value="<?php echo $row['years_in_company']?>" disabled>
                   </td>
                </tr>

                <tr>
                   <td>
                      <label class="control-label">Position: </label>
                    </td>

                    <td>
                      <input type="text" name="position" id="" class="form-control" style="width: 350px;" value="<?php echo $row['position']?>" disabled>
                    </td>
                </tr>

                <tr>
                  <td>
                    <label class="control-label">Job Level: </label>
                  </td>

                  <td>
                    <input type="text" name="job_level" id="" class="form-control" style="width: 350px;" value="<?php echo $row['job_level']?>" disabled>
                  </td>
                </tr>

                <tr>
                  <td>
                  <label class="control-label">Years in Position: </label>
                  </td>

                  <td>
                    <input type="text" name="years_in_position" id="" class="form-control" style="width: 350px;" value="<?php echo $row['years_in_position']?>" disabled>
                  </td>
                </tr>

                <tr>
                  <td>
                    <label class="control-label">Department: </label>
                  </td>

                  <td>
                    <input type="text" name="department" id="" class="form-control" style="width: 350px;" value="<?php echo $row['department']?>" disabled>
                  </td>
                </tr>
               
              </table>
    </div>

                <div class="modal-footer">
                
                   
                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- VIEW EMPLOYEE MODAL-->

                                    


<!-- EDIT EMPLOYEE MODAL-->
<div class="modal fade" id="editEmployee-<?php echo $row['emp_id']; ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Employee</h4>
      </div>
        
      <div class="modal-body">

          <table class="table table-bordered">
          <form method="post" class="form-horizontal" enctype="multipart/form-data">
           <tr>
                <td>

                </td>

                 <td>
                       <img src="<?php echo $row['paths'];?>" class="img-responsive img-circle" width="100" height="100">
                  </td>
              </tr>
            <tr>
                <td>
                  <label class="control-label">Employee No: </label>
                </td>

                <td>
                    <input type="text" name="employee_id" id="" class="form-control" style="width: 350px;" value="<?php echo $row['emp_id']; ?>" disabled>
                </td>
              </tr>

              <tr>
                 <td>
                  <label class="control-label">First Name: </label>
                </td>

                <td>
                    <input type="text" name="firstname" id="" class="form-control" style="width: 350px;" value="<?php echo $row['firstname']?>" onkeypress="return letter(event)" required>
                    <input type="hidden" name="id" id="" class="form-control" value="<?php echo $row['emp_id']?>">
                </td>
              </tr>

              <tr>
                 <td>
                  <label class="control-label">Middle Name: </label>
                  </td>

                  <td>
                    <input type="text" name="middlename" id="" class="form-control" style="width: 350px;" value="<?php echo $row['middlename']?>" onkeypress="return letter(event)">
                 </td>
              </tr>

              <tr>
                <td>
                  <label class="control-label">Last Name: </label>
                </td>

                <td>
                    <input type="text" name="lastname" id="" class="form-control" style="width: 350px;" value="<?php echo $row['lastname']?>" onkeypress="return letter(event)" required>
                </td>
              </tr>

               <tr>
                <td>
                  <label class="control-label">E-mail Address: </label>
                </td>

                <td>
                    <input type="email" name="email_employee" id="" class="form-control" style="width: 350px;" value="<?php echo $row['email_employee']?>" required>
                </td>
              </tr>
                
                <tr>
                  <td>
                    <label class="control-label">Years in Company: </label>
                  </td>

                  <td>
                    <input type="text" name="years_in_company" id="" class="form-control" style="width: 350px;" value="<?php echo $row['years_in_company']?>" readonly>
                  </td>
                </tr>

                <tr>
                  <td>
                    <label class="control-label">Position: </label>
                  </td>

                  <td>
                   <!--  <input type="text" name="position" id="" class="form-control" style="width: 350px;" value="<?php echo $row['position']?>" required> -->
                   <select name="position" class="form-control" style="width: 350px;">
                   <option></option>
                                 <?php
                                    $query1=mysql_query("SELECT * FROM tbl_job_level") or die (mysql_error());
                                    while($row=mysql_fetch_array($query1)){
                                  ?>
                                   <option value="<?php echo $row['position']?>">
                                     <?php echo $row['position']?>
                                 </option>
                                 <?php } ?>
                   </select>
                 </td>
                </tr>



                <tr>
                  <td>
                    <label class="control-label">Years in Position: </label>
                  </td>
                   <?php
                            $query2=mysql_query("SELECT * FROM tbl_sog_employee") or die (mysql_error());
                            while($row=mysql_fetch_array($query2)){
                              $yip=$row['years_in_position'];
                            }
                          ?>

                  <td>
                    <input type="text" name="years_in_position" id="" class="form-control" style="width: 350px;" value="<?php echo $yip;?>" readonly>
                  </td>
                </tr>

                <tr>
                  <td>
                  <label class="control-label">Department: </label>
                  </td>

                   <?php
                            $query3=mysql_query("SELECT * FROM tbl_sog_employee") or die (mysql_error());
                            while($row=mysql_fetch_array($query3)){
                              $dep=$row['department'];
                            }
                          ?>

                  <td>
                     <select name="department" class="form-control" style="width: 350px;" id="department_edit" onChange="getJobLevel_edit(this.value);" required>
                         <option><?php echo $dep;?></option>
                         <option disabled>--------------</option>
                         <?php
                            $query=mysql_query("SELECT * FROM tbl_departments WHERE department_name NOT IN (SELECT department FROM tbl_admins)") or die (mysql_error());
                            while($row=mysql_fetch_array($query)){
                          ?>
                         <option value="<?php echo $row['department_name']?>">
                             <?php echo $row['department_name']?>
                         </option>
                         <?php } ?>
                    </select>
                  </td>
                </tr>

                <tr>
                  <td>
                  <label class="control-label">Job Level: </label>
                  </td>

                  <td>
                    <!-- <input type="text" name="job_level" id="" class="form-control" style="width: 350px;" value="<?php echo $row['job_level']?>" required> -->
                    <select name="job_level" id="job_level_edit" class="form-control" style="width: 350px;">
                      <option value="">SELECT JOB LEVEL</option>
                    </select>
                  </td>
                </tr>

                <tr>
                  <td>
                    <label class="control-label">Employee Image: </label>
                  </td>

                  <td>
                            <label class="btn btn-primary" for="my-file-selector-<?php echo $count; ?>">
                                <input id="my-file-selector-<?php echo $count; ?>" type="file" name="file" style="display:none"
                                onchange="$('#upload-file-info-<?php echo $count; ?>').html(this.files[0].name)"/>
                                Select Image
                            </label>
                            <span class='label label-info' id="upload-file-info-<?php echo $count; ?>"></span>
                   </td>
                  </tr>

               </table>
    </div>

                <div class="modal-footer">
                
                    <input type="submit" class="btn btn-success" name="edit_employee" value="UPDATE EMPLOYEE"  />
                    <input type="reset" class="btn btn-default" value="Clear"/>

                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- EDIT EMPLOYEE MODAL-->

                                    
                                  </td>
                            </tr>
                            <?php $count++;}} ?>
                      </tbody>
                </table>
            </div>
        </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->



<!-- ADD NEW EMPLOYEE MODAL-->
<div class="modal fade" id="addEmployee" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add New Employee</h4>
      </div>
        
      <div class="modal-body">

          <form method="post" class="form-horizontal" enctype="multipart/form-data">
                  <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Employee ID: </label>
                  <div class="col-sm-8">
                    <input type="text" name="employee_id" id="" class="form-control" placeholder="Employee ID"/>
                  </div>
                </div>


                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">First Name: </label>
                  <div class="col-sm-8">
                    <input type="text" name="firstname" id="" class="form-control" placeholder="First Name" onkeypress="return letter(event)" required/>
                  </div>
                </div>

                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Middle Name: </label>
                  <div class="col-sm-8">
                    <input type="text" name="middlename" id="" class="form-control" placeholder="Middle Name" onkeypress="return letter(event)"/>
                  </div>
                </div>

                <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Last Name: </label>
                  <div class="col-sm-8">
                    <input type="text" name="lastname" id="" class="form-control" placeholder="Last Name" onkeypress="return letter(event)" required>
                  </div>
                </div>

                <div class="form-group" id="">
                  <label class="col-sm-3 control-label">E-mail Address: </label>
                  <div class="col-sm-8">
                    <input type="email" name="email_employee" id="" class="form-control" placeholder="E-mail Address" required>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">Year Applied: </label>
                  <div class="col-sm-8">
                     <select name="year_applied" class="form-control" required>
             			<option></option>
	             		<?php
	             			$timezone = "Asia/Manila";
	             			if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
	             			$year = date("Y");
	             			$yy = $year - 50;

	             			for($z=$year;$z>=$yy;$z--){
	             				$y = $z + 1;
	             				$sy = $z. " - ". $y;
	             			echo "<option value='$z'>".$z."</option>";	
	             			}
	             		?>
             		</select>
                  </div>
                </div>

                


                  <div class="form-group" id="">
                          <label class="col-sm-3 control-label">Department: </label>
                          <div class="col-sm-8">
                             <select name="department" id="department" class="form-control" onChange="getJobLevel(this.value);" required>
                                 <option></option>
                                 <?php
                                    $query=mysql_query("SELECT * FROM tbl_departments") or die (mysql_error());
                                    while($row=mysql_fetch_array($query)){
                                  ?>
                                 <option value="<?php echo $row['department_name']?>">
                                     <?php echo $row['department_name']?>
                                 </option>
                                 <?php } ?>
                            </select>
                          </div>
                    </div>

                    <div class="form-group" id="">
                      <label class="col-sm-3 control-label">Job Level: </label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="job_level" id="" class="form-control" placeholder="Job Level" required /> -->
                        <select name="job_level" class="form-control" id="job_level_this" onChange="getPosition(this.value);" required>
                        <option value="">SELECT JOB LEVEL</option>
                        </select>
                      </div>
                    </div>

                    <!-- <div class="form-group" id="" class="hidden">
                      <label class="col-sm-3 control-label">Position: </label>
                      <div class="col-sm-8">
                        <input type="text" name="position" id="" class="form-control" placeholder="Position" required />
                        <input type="hidden" name="position" id="position" readonly>
                      </div>
                    </div> -->

                    <input type="hidden" name="position" id="position" readonly>

                    <div class="form-group" id="">
                    <label class="col-sm-3 control-label">Employee Image: </label>


                    <div class="col-sm-8">
                            <label class="btn btn-primary" for="my-file-selector">
                                <input id="my-file-selector" type="file" name="file" style="display:none"
                                onchange="$('#upload-file-info').html(this.files[0].name)">
                                Select Image
                            </label>
                            <span class='label label-info' id="upload-file-info"></span>
                    </div>
                  </div>
               
    </div>

                <div class="modal-footer">
                
                    <input type="submit" class="btn btn-success" name="add_employee" value="ADD EMPLOYEE"  />
                    <input type="reset" class="btn btn-default" value="Clear"/>

                </div>
              
          </form>
      </div>
    </div>
  </div>

  <?php
if(isset($_POST['btnApprove'])){
    $admin_id = $_POST['admin_id'];
    mysql_query("DELETE FROM tbl_sog_employee WHERE emp_id = '$admin_id'") or die (mysql_error());
    echo"
      <script type='text/javascript'>
        
        swal({
                  title: 'SUCCESS!',
                  text: 'The Employee Record has been Deleted!',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"ManageSogEmployee.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"ManageSogEmployee.php\";
                    }
                  }
                )
      </script>
    ";
}
?>
<!-- ADD NEW EMPLOYEE MODAL-->

<?php include 'phpfunctions/addEmployee-CRUD.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php } ?>






