<?php include 'includes/header.php'; ?>
<?php
    $selected_emp_id = $_SESSION['selected_emp_id'];
    $selected_emp_department =  $_SESSION['selected_emp_department'];
?>

<?php 
  $results = mysql_query("SELECT * FROM tbl_sog_employee WHERE emp_id='$selected_emp_id'") or die (mysql_error());
  while ($results_row = mysql_fetch_array($results)){
    $kpiSog=$results_row['firstname'].' '.$results_row['middlename'].' '.$results_row['lastname'];
  }
?>

<!-- Setting the treeview active -->
<script type="text/javascript">
document.getElementById("treeview2").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->


<script>
    function approve_confirmation(){
        if(confirm("Are you sure?")==1)
        {
            document.getElementById('btnApprove').submit();
        }
    }
</script>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        KPI Management
        <!-- <small>Version 2.0</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

    <!-- TABLE: KRA -->
    <div class="box box-info" style="border-color: green">
      <div class="box-header with-border">
        <h3 class="box-title">KPI of <strong><?php echo $kpiSog;?></strong> </h3>
          <?php
            $count = 1;
            $display_stat=mysql_query("SELECT * FROM tbl_departments WHERE departmentOwner = '$selected_emp_department' AND (kpiStatus = 'PENDING' OR kpiStatus = 'APPROVED' OR kpiStatus = 'UPDATE')") or die(mysql_error());
            while($rowStat=mysql_fetch_array($display_stat)){
                $fetchStat = $rowStat['kpiStatus'];

                if ($fetchStat=='PENDING') {
                  # code...
                  echo "<span style='color:red;float:right;'><b>*THIS DEPARTMENT IS CURRENTLY CREATING HIS/HER KPI</b></span>";
                }
                else{
                  echo "";
                }
            }
          ?>

        <!-- <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <div class="row" style="margin-bottom: 1em;">
              <div class="pull-right container-fluid">
                  <?php
                  $checkIfExist_0=mysql_query("SELECT * FROM tbl_sog_employee WHERE department = '$selected_emp_department' AND (kraStatus = 'APPROVED' OR kraStatus = 'UPDATE' or kraStatus = 'SENT')") or die(mysql_error());
                  $checked_0 = mysql_num_rows($checkIfExist_0);
                  if($checked_0==1){
                      echo "<a href=\"#\" class=\"btn btn-success btn-block\"
                     style=\"background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7)\"
                     data-toggle=\"modal\" data-target=\"#sendNoteModal\"> &nbsp;&nbsp;<i class=\"fa fa-sticky-note\"></i> SEND NOTE
                      &nbsp;</a>";
                  }
                  ?>

              </div>

          </div>
          <div class="row container-fluid">

                <table class="table table-bordered" id="">


                  <thead>
                    <th class="col-sm-1">KRA</th>
                    <th class="col-sm-4">KEY PERFORMANCE INDICATORS</th>
                  </thead>

                  <tbody>

                    <?php
                    $checkIfExist=mysql_query("SELECT * FROM tbl_eformp1 WHERE kraCreatedFor = '$selected_emp_id'") or die(mysql_error());
                    $checked = mysql_num_rows($checkIfExist);
                    if($checked>=1){

                      $count = 1;
                      $display_ep1=mysql_query("SELECT * FROM tbl_eformp1 WHERE kpiOwner = '$selected_emp_department' AND kraCreatedFor = '$selected_emp_id'") or die(mysql_error());
                                $result = mysql_num_rows($display_ep1);
                                while($row=mysql_fetch_array($display_ep1)){
                                  $mainID = $row['kraID'];
                                  $kpiTitle=$row['kpiTitle'];
                    ?>
                    <tr>
                      <td><b><?php echo $kpiTitle;?></b></td>
                      <td>
                          <?php
                          $display_kpi=mysql_query("SELECT * FROM tbl_p1KPI WHERE kraID ='$mainID' AND kpiOwner = '$selected_emp_department' AND kraCreatedFor = '$selected_emp_id' ORDER BY kpiID ASC") or die(mysql_error());
                          while($row2=mysql_fetch_array($display_kpi)){
                              $kranewID = $row2['kraID'];
                              $kpinewID = $row2['kpiID'];
                              $kpiDesc = $row2['kpiDesc'];

                          ?>
                          <div class="row" style="margin-bottom: 1em">
                            <div class="col-sm-12"><?php echo $kpiDesc;?> </div>
                          </div>
                          <?php }?>
                      </td>
                    </tr>

                          <?php $count++;}}
                    else{
                        echo"<tr><td colspan='2'><h3><b class=''><center>NO KRA YET</center></b></h3></td></tr>";
                    }
                    ?>
                  </tbody>
                </table>
          </div>



      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <form method="post" id="form_this">


          <input type="hidden" name="KRA_counter" value="<?php echo $result;?>">
          <input type="hidden" name="emp_id" value="<?php echo $selected_emp_id;?>" /> <!--WAIT-->
          <input type="hidden" name="dept_name" value="<?php echo $selected_emp_department;?>" />

            <?php
            $count = 1;
            $display_stat=mysql_query("SELECT * FROM tbl_sog_employee WHERE department = '$selected_emp_department' AND (kraStatus = 'APPROVED' OR kraStatus = 'UPDATE' or kraStatus = 'SENT')") or die(mysql_error());
            while($rowStat=mysql_fetch_array($display_stat)){
                $fetchStat = $rowStat['kraStatus'];

                if ($fetchStat=='SENT') {
                    # code...
                    ?>
                    <button class="btn btn-sm btn-info btn-flat pull-right" name="btnApprove" id="btnApprove" onclick="approve_confirmation(this); return false;">APPROVE</button>
                <?php
                }
                elseif ($fetchStat=='UPDATE') {
                    # code...
                    ?>
                    <button class="btn btn-sm btn-info btn-flat pull-right" name="btnApprove" id="btnApprove" onclick="approve_confirmation(this); return false;">APPROVE</button>
                    <?php
                }
                else{
                    echo "";
                }
            }
            ?>
        </form>
      </div>
      <!-- /.box-footer -->
    </div>


        <!-- SEND NOTE MODAL-->
        <div class="modal fade" id="sendNoteModal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Send a note to <?php echo $selected_emp_department?></h4>
                    </div>

                    <div class="modal-body">

                        <form method="post" class="form-horizontal">
                            <input type="hidden" name="sent_at" value="<?php date_default_timezone_set('Asia/Manila'); echo date('F j, Y g:i:a');?>">
                            <div class="form-group container-fluid" > <!-- Message field -->
                                <label class="control-label " for="message">Note:</label>

                                <textarea class="form-control ckeditor" id="note_msg" name="note_msg">
                                <script type="text/javascript">
                                    var textarea = document.getElementById('note_msg');

                                    CKEDITOR.replace( 'textarea',
                                        {
                                            toolbar : 'Basic', /* this does the magic */
                                            uiColor : '#9AB8F3'
                                        });

                                </script>
                            </textarea>
                            </div>

                            <div class="form-group container-fluid checkbox" > <!-- Message field -->
                                <label class="control-label " for="message"><input type="checkbox" value="ACTIVATED" name="activation_status"/>Open the KRA Creation/Update Module? </label>


                            </div>





                    </div>

                    <div class="modal-footer">

                        <input type="submit" class="btn btn-success" name="btnSubmitNote" value="SUBMIT"  />
                        <input type="reset" class="btn btn-default" value="Clear"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SEND NOTE MODAL-->
 
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>
<?php include 'phpfunctions/approveKRA.php'; ?>
<?php
//UPDATE THE STATUS OF THE MONITORING THE KRA
mysql_query("UPDATE tbl_departments SET monitor = 'read' WHERE department_name = '$selected_emp_department' AND (kpiStatus = 'SENT' OR kpiStatus = 'PENDING' OR kpiStatus ='UPDATE')") or die(mysql_error());
?>
<?php
if(isset($_POST['btnSubmitNote'])){
    $note_message = $_POST['note_msg'];
    $activation_status = $_POST['activation_status'];
    $sent_at_dept = $_POST['sent_at'];
    $revision_for = $selected_emp_id;
    if($activation_status=="ACTIVATED"){
        mysql_query("UPDATE tbl_departments SET activation_status = 'ACTIVATED' WHERE department_name = '$kpiDept' AND (kpiStatus = 'SENT' OR kpiStatus = 'PENDING')") or die(mysql_error());
        mysql_query("INSERT INTO tbl_notification_user (notification,sendBy,status,owner) VALUES ('HR SENT A NOTE TO YOUR KRA MODULE','$department_name_session','unread','$kpiDept')") or die(mysql_error());
        mysql_query("INSERT INTO tbl_messages (subject,content,sender,receiver,sent_at,location,revision,id_appeal)VALUES ('HR SENT A NOTE TO YOUR KRA MODULE','$note_message','$department_name_session','$selected_emp_department','$sent_at_dept','sentbox','','$revision_for')") or die (mysql_error());


        $rewrew=mysql_query("SELECT * FROM tbl_admins WHERE department='$selected_emp_department'") or die (mysql_error());
       while ($rew = mysql_fetch_array($rewrew)){
        $email_receiving = $rew['email'];
       }



   require '../assets/phpmailer/PHPMailerAutoload.php';

      $mail = new PHPMailer;
     $body .= $note_message;

      //$mail->SMTPDebug = 3;                               // Enable verbose debug output
      //$mail->SMTPDebug = 1;
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = "smtp.gmail.com";    // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = "es.macbeth.2017@gmail.com";
      $mail->Password = "nopasswordno";                          // SMTP password
      $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 465;                                    // TCP port to connect to

      $mail->setFrom('from@example.com', 'UPang - Human Resource Department');
      $mail->addAddress($email_receiving, 'Joe User');          // Add a recipient
      $mail->addAddress('ellen@example.com');               // Name is optional
      $mail->addReplyTo('info@example.com', 'Information');
      $mail->addCC('cc@example.com');
      $mail->addBCC('bcc@example.com');

      $mail->isHTML(true);   
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ));
                                           // Set email format to HTML

      $mail->Subject = 'PHINMA UPang SOG EVALUATION SYSTEM -- SUBJECT FOR KRA REVISION for '. $kpiSog;
      $mail->Body    = $body;
      
     if(!$mail->send()) {
        echo "<script type='text/javascript'>
        
        swal({
                  title: 'ERROR!',
                  text: 'Oops! Something went wrong!',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageKRA_s2.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageKRA_s2.php\";
                    }
                  }
                )
      </script>";
  } else 
    {
          echo "<script type='text/javascript'>
        swal({
                  title: 'SUCCESS!',
                  text: 'Note has been sent to $selected_emp_department',
                  type: \"success\",
                  timer: 1000,
                  allowOutsideClick: false,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageKRA_s2.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageKRA_s2.php\";
                    }
                  }
                )
      </script>";
    }
    }
    else{
        mysql_query("INSERT INTO tbl_notification_user (notification,sendBy,status,owner) VALUES ('HR SENT A NOTE TO YOUR KRA MODULE','$department_name_session','unread','$kpiDept')") or die(mysql_error());
        mysql_query("INSERT INTO tbl_messages (subject,content,sender,receiver,sent_at,location,revision,id_appeal)VALUES ('HR SENT A NOTE TO YOUR KRA MODULE','$note_message','$department_name_session','$selected_emp_department','$sent_at_dept','sentbox','','$revision_for')") or die (mysql_error());

        $rewrew=mysql_query("SELECT * FROM tbl_admins WHERE department='$selected_emp_department'") or die (mysql_error());
       while ($rew = mysql_fetch_array($rewrew)){
        $email_receiving = $rew['email'];
       }



   require '../assets/phpmailer/PHPMailerAutoload.php';

      $mail = new PHPMailer;
     $body .= $note_message;

      //$mail->SMTPDebug = 3;                               // Enable verbose debug output
      //$mail->SMTPDebug = 1;
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = "smtp.gmail.com";    // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = "es.macbeth.2017@gmail.com";
      $mail->Password = "nopasswordno";                          // SMTP password
      $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 465;                                    // TCP port to connect to

      $mail->setFrom('from@example.com', 'UPang - Human Resource Department');
      $mail->addAddress($email_receiving, 'Joe User');          // Add a recipient
      $mail->addAddress('ellen@example.com');               // Name is optional
      $mail->addReplyTo('info@example.com', 'Information');
      $mail->addCC('cc@example.com');
      $mail->addBCC('bcc@example.com');

      $mail->isHTML(true);   
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ));
                                           // Set email format to HTML

      $mail->Subject = 'PHINMA UPang SOG EVALUATION SYSTEM -- SUBJECT FOR KRA REVISION for '. $kpiSog;
      $mail->Body    = $body;
      
     if(!$mail->send()) {
        echo "<script type='text/javascript'>
        
        swal({
                  title: 'ERROR!',
                  text: 'Oops! Something went wrong!',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageKRA_s2.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageKRA_s2.php\";
                    }
                  }
                )
      </script>";
  } else 
    {
          echo "<script type='text/javascript'>
        swal({
                  title: 'SUCCESS!',
                  text: 'Note has been sent to $selected_emp_department',
                  type: \"success\",
                  timer: 1000,
                  allowOutsideClick: false,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageKRA_s2.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageKRA_s2.php\";
                    }
                  }
                )
      </script>";
    }
    }
  //       echo"
		// 	<script type='text/javascript'>
				
		// 		swal({
  //                 title: 'SUCCESS!',
  //                 text: 'The note has been sent',
  //                 type: \"success\",
  //                 timer: 10000,
  //               }).then(
  //                 function() {
  //               // Redirect the user
  //               window.location.href = \"manageKRA_s3.php\";
  //               console.log('The Ok Button was clicked.');
  //               },
  //                 // handling the promise rejection
  //                 function (dismiss) {
  //                   if (dismiss === 'timer') {
  //                      window.location.href = \"manageKRA_s3.php\";
  //                   }
  //                 }
  //               )
		// 	</script>
		// ";
    }





?>

