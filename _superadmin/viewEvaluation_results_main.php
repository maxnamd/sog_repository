

<?php include 'includes/header.php'; ?>

<?php 
$firstname = $_SESSION['employee_firstName'];
$middlename = $_SESSION['employee_middleName'];
$lastname = $_SESSION['employee_lastName'];
$employee_performance_cycle = $_SESSION['employee_performance_cycle'];
$employee_id= $_SESSION['employee_id'];
?>

<?php
$get_details=mysql_query("SELECT * FROM tbl_evaluation_results WHERE performanceCycle='$employee_performance_cycle' AND emp_id ='$employee_id'") or die(mysql_error());

while($get_row=mysql_fetch_array($get_details)) {

    $emp_id = $get_row['emp_id'];
    $employee_firstName = $get_row['employee_firstName'];
    $employee_middleName = $get_row['employee_middleName'];
    $employee_lastName = $get_row['employee_lastName'];
    $employee_department = $get_row['employee_department'];
    $employee_position = $get_row['employee_position'];
    $employee_job_level = $get_row['employee_job_level'];
    $employee_years_company = $get_row['employee_years_company'];
    $employee_years_position = $get_row['employee_years_position'];
    $evaluationStatus = $get_row['evaluationStatus'];
    $pCycle = $get_row['performanceCycle'];
}
?>
<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview5").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->
    <script>
        $(document).ready(function() {
            $('#tbl_EV').DataTable();
        } );
    </script>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--HEADER-->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- TABLE: STATUS KRA -->
        <div class="box box-info" style="border-color: green">
            <div class="box-header with-border">
                <h3 class="box-title">Evaluation Results of <b><?php echo $employee_firstName . ' ' .  $employee_middleName . ' ' . $employee_lastName ; ?></b>
                    <br><span class="small"><b><i><?php echo $employee_department?></i></b></span>
                </h3>

                <!--
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">P1 - KPI</a></li>
                        <li><a href="#tabs-2a">P2A - Competencies</a></li>
                        <li><a href="#tabs-2b">P2B - Competency Details</a></li>
                        <li><a href="#tabs-3">P3 - Professionalism</a></li>
                        <li><a href="#tabs-4">P4 - Customer Service</a></li>
                        <li><a href="#tabs-5">Summary</a></li>
                    </ul>


                    <div id="tabs-1">
                        <?php include 'evaluation_forms/form_p1_evaluation_results.php';?>
                    </div>

                    <div id="tabs-2a">
                        <?php include 'evaluation_forms/form_p2a_evaluation_results.php';?>
                    </div>

                    <div id="tabs-2b">
                        <?php include 'evaluation_forms/form_p2b_evaluation_results.php';?>
                    </div>

                    <div id="tabs-3">
                        <?php include 'evaluation_forms/form_p3_evaluation_results.php';?>
                    </div>

                    <div id="tabs-4">
                        <?php include 'evaluation_forms/form_p4_evaluation_results.php';?>
                    </div>

                    <div id="tabs-5">
                        <?php include 'evaluation_forms/form_summary_evaluation_results.php';?>
                    </div>


                </div>


            </div>
            <!-- /.box-body -->


            <script type="text/javascript">
                callOnLoadCompute();
                function callOnLoadCompute(){
                    var p1_sum = 0;

                    $('.compute_p1_weights').each(function() {
                        p1_sum += Number($(this).val());
                    });

                    document.getElementById('total_weight_p1').value=p1_sum;

                    var p1b_sum = 0;

                    $('.compute_p1_weighted_rating').each(function() {
                        p1b_sum += Number($(this).val());
                    });
                    document.getElementById('total_weighted_rating').value=parseFloat(p1b_sum).toFixed(2);


                    var p2_sum = 0;
                    $('.compute_weights_p2').each(function() {
                        p2_sum += Number($(this).val());
                    });
                    document.getElementById('total_weights_p2').value=p2_sum;

                    var p2_sum_twrp2 = 0;

                    $('.compute_p2_weighted_rating').each(function() {
                        p2_sum_twrp2 += Number($(this).val());
                    });

                    document.getElementById('total_weighted_rating_p2').value=parseFloat(p2_sum_twrp2).toFixed(2);


                    p3_comp();


                    var p4_sum = 0;

                    $('.factor_rating_p4').each(function() {

                        p4_sum += Number($(this).val());
                    });

                    document.getElementById('total_cs_rating').value=parseFloat(p4_sum/4).toFixed(2);

                    document.getElementById('cs_factor_rating').value=parseFloat(p4_sum/4).toFixed(2);

                }
            </script>
<?php include 'includes/footer.php'; ?>




