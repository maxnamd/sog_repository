<?php include 'includes/header.php'; ?>
<!-- Setting the treeview active -->
<script type="text/javascript">
document.getElementById("treeview2").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->
<script>
    $(document).ready(function() {
        $('table').DataTable();
    });
</script>

  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        KPI Management
        <!-- <small>Version 2.0</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- TABLE: STATUS KRA -->
        <div class="box box-info" style="border-color: green">
          <div class="box-header with-border">
            <h3 class="box-title">KPI Status / Deparment</h3>

            <!-- <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div> -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Deparment Name</th>
                    <th>Status</th>
                    <th>Read</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                    $display_status=mysql_query("SELECT * FROM tbl_departments WHERE department_name != '$department_name_session'") or die(mysql_error());

                    while($row=mysql_fetch_array($display_status)){

                        $departmentName = $row['department_name'];
                        $shortCut = $row['initials'];
                        $departmentOwner = $row['departmentOwner'];
                        $kpiStatus = $row['kpiStatus'];
                        $monitor = $row['monitor'];
                        echo "<tr>";
                        echo "<td>$departmentName</td>";

                        if ($kpiStatus=='SENT') {
                          # code...
                          echo "<td><span class='col-sm-5 label label-success'>$kpiStatus</span></td>";
                        }
                        elseif($kpiStatus=='APPROVED'){
                          echo "<td><span class='col-sm-5 label label-info'>$kpiStatus</span></td>";
                        }
                        else{
                          echo "<td><span class='col-sm-5 label label-warning'>$kpiStatus</span></td>";
                        }

                        if ($monitor=='read') {
                          # code...
                          echo "<td><span class='col-sm-5 label label-success'>YES</span></td>";
                        }
                        else{
                          echo "<td><span class='col-sm-5 label label-danger'>NO</span></td>";
                        }


                        ?>
                      <td>
                        <form method="POST">
                            <input type="hidden" name="kraDepartment" value="<?php echo $departmentName?>"/>


                            <button class="btn btn-info btn-sm" name="btnPass">
                              <i class="fa fa-eye"></i>
                            </button>
                        </form>

                      </td>


                      <?php
                      }
                    ?>



                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">

            <button class="btn btn-sm btn-default btn-flat pull-right" disabled="">ACTIVATE</button>
          </div>
          <!-- /.box-footer -->
        </div>

        <div class="box box-danger" style="border-color: orangered">
            <div class="box-header with-border">
                <h3 class="box-title">Missed KRA SUBMISSION</h3>

                 <div class="pull-right" style="margin-right: 1em">
                    <form action="viewKRA_missed_report.php">
                        <input type="submit" class="btn btn-success" value="REPORT"/>
                    </form>
                </div>

                <!-- <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                       <thead>
                            <th class="col-sm-5">Department Name</th>
                            <th class="col-sm-1">Messages</th>
                            <th class="col-sm-1">Reason</th>
                            <th class="col-sm-1">Request Status</th>
                            <th class="col-sm-1">Action</th>
                       </thead>
                        <tbody>
                        <?php
                        $counter = 1;
                        $display_request_kpi=mysql_query("SELECT * FROM tbl_kpi_requests WHERE request_status = 'SENT' OR request_status = 'APPEALED' OR request_status = 'DECLINED'") or die(mysql_error());

                        while($row=mysql_fetch_array($display_request_kpi)){

                            $departmentName = $row['department'];
                            $reason_msg = $row['reason'];
                            $request_status = $row['request_status'];
                            ?>
                            <tr>
                            <td><?php echo $departmentName;?></td>
                            <td>
                                <div class="col-sm-12">
                                    <div>
                                        <!--//MESSAGES-->
                                        <a href='#' class='btn btn-info btn-sm' data-toggle='modal' data-target="#viewMessages_modal<?php echo $counter?>">VIEW MESSAGES</a>

                                        <!-- Messages modal -->
                                        <div id="viewMessages_modal<?php echo $counter;?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Messages for this Request</h4>
                                                    </div>
                                                    <div class="modal-body" style="max-height: calc(100vh - 212px) !important;overflow-y: auto;padding: 1em;">

                                                        <style type="text/css">
                                                            .chat
                                                            {
                                                                list-style: none;
                                                                margin: 0;
                                                                padding: 0;
                                                            }

                                                            .chat li
                                                            {
                                                                margin-bottom: 10px;
                                                                padding-bottom: 5px;
                                                                border-bottom: 1px dotted #B3A9A9;
                                                            }

                                                            .chat li.left .chat-body
                                                            {
                                                                margin-left: 60px;
                                                            }

                                                            .chat li.right .chat-body
                                                            {
                                                                margin-right: 60px;
                                                            }


                                                            .chat li .chat-body p
                                                            {
                                                                margin: 0;
                                                                color: #777777;
                                                            }
                                                            ::-webkit-scrollbar-track
                                                            {
                                                                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                                                                background-color: #F5F5F5;
                                                            }

                                                            ::-webkit-scrollbar
                                                            {
                                                                width: 12px;
                                                                background-color: #F5F5F5;
                                                            }

                                                            ::-webkit-scrollbar-thumb
                                                            {
                                                                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
                                                                background-color: #555;
                                                            }

                                                        </style>

                                                        <ul class="chat">
                                                            <?php
                                                            $display_my_messages=mysql_query("SELECT * FROM tbl_messages WHERE id_appeal = '$departmentName' ORDER BY sent_at ASC") or die(mysql_error());
                                                            $check_num_rows = mysql_num_rows($display_my_messages);

                                                            if ($check_num_rows > 0) {
                                                                while($row_messages=mysql_fetch_array($display_my_messages)){
                                                                    $sender = $row_messages['sender'];
                                                                    $receiver = $row_messages['receiver'];
                                                                    $content = $row_messages['content'];
                                                                    $time = $row_messages['sent_at'];


                                                                    if($sender==$department_name_session){
                                                                        ?>
                                                                        <li class="right clearfix">
                                                                    <span class="chat-img pull-right">
                                                                        <img src="http://placehold.it/50/55C1E7/fff&text=HR" alt="User Avatar" class="img-circle" />
                                                                    </span>
                                                                            <div class="chat-body clearfix">
                                                                                <div class="header">
                                                                                    <strong class="primary-font"><?php echo $department_name_session?></strong> <small class="pull-right text-muted">
                                                                                        <span class="glyphicon glyphicon-time"></span><?php echo $time?></small>
                                                                                </div>
                                                                                <p>
                                                                                    <?php
                                                                                    echo $content;
                                                                                    ?>
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                    <?php }
                                                                    else{
                                                                        ?>
                                                                        <li class="left clearfix">

                                                        <span class="chat-img pull-left">
                                                            <img src="http://placehold.it/50/FA6F57/fff&text=DH" alt="User Avatar" class="img-circle" />
                                                        </span>
                                                                            <div class="chat-body clearfix">
                                                                                <div class="header">
                                                                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?php echo $time?></small>
                                                                                    <strong class="pull-right primary-font"><?php echo $sender;?></strong>
                                                                                </div>
                                                                                <p>
                                                                                    <?php
                                                                                    echo $content;
                                                                                    ?>
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                        <?php
                                                                    }

                                                                }}
                                                            else{?>
                                                                NO MESSAGES YET <?php }?>


                                                        </ul>

                                                    </div>
                                                    <div class="modal-footer">

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <!-- END Messages modal -->
                                    </div>
                                </div>
                            </td>
                            <td><div class="col-sm-2">
                                    <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#viewReasonModal_<?php echo $counter;?>" title="VIEW REASON"><i class="fa fa-eye" ></i></a>

                                    <!-- Modal -->
                                    <div id="viewReasonModal_<?php echo $counter;?>" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Reason</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <ul class="list-group">
                                                        <?php echo $reason_msg?>
                                                    </ul>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </td>

                            <td>
                                <?php
                                if ($request_status=="APPEALED") {
                                    # code...
                                    echo "<span class='col-sm-5 label label-danger'>$kpiStatus</span>";
                                }
                                elseif($request_status=='APPROVED'){
                                    echo "<span class='col-sm-5 label label-success'>$kpiStatus</span>";
                                }
                                else{
                                    echo "<span class='col-sm-5 label label-warning'>$kpiStatus</span>";
                                }
                                ?>
                            </td>
                            <td>

                                <?php
                                if($request_status != 'APPROVED'){
                                    ?>
                                    <div class="col-sm-2">
                                        <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#approveModal_<?php echo $counter?>" title="APPROVE"><i class="fa fa-check" ></i></a>

                                        <!-- Modal -->
                                        <div id="approveModal_<?php echo $counter?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header" style="background-color: #2E7D32 !important;border-color: #2E7D32 !important;color: white">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Confirm</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h5>Are you sure to approve this request of <?php echo $departmentName;?>?</h5>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <form method="post">
                                                            <input type="hidden" name="dept_id" value="<?php echo $departmentName?>"/>
                                                            <button type="submit" class="btn btn-success btn-md" name="btnApprove">Yes</button>
                                                            <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">No</button>
                                                        </form>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-1"></div>

                                    <div class="col-sm-2">
                                        <a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#denyModal_<?php echo $counter?>" title="DENY"><i class="fa fa-close" ></i></a>

                                        <!-- Modal -->
                                        <div id="denyModal_<?php echo $counter?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header" style="background-color: #2E7D32 !important;border-color: #2E7D32 !important;color: white">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Confirm</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h5>To deny the request of <b><?php echo $departmentName;?></b> you must state a reason below</h5>
                                                        <form method="post">
                                                            <input type="hidden" name="sent_at_dept" value="<?php date_default_timezone_set('Asia/Manila'); echo date('F j, Y g:i:a');?>">
                                                            <textarea class="form-control ckeditor" id="deny_reason_<?php echo $counter;?>" name="deny_reason">
                                                            <script type="text/javascript">
                                                                var textarea = document.getElementById('deny_reason_<?php echo $counter;?>');

                                                                CKEDITOR.replace( 'textarea',
                                                                    {
                                                                        toolbar : 'Basic', /* this does the magic */
                                                                        uiColor : '#9AB8F3'
                                                                    });

                                                            </script>
                                                </textarea>
                                                    </div>
                                                    <div class="modal-footer">

                                                        <input type="text" name="dept_id" value="<?php echo $departmentName?>"/>
                                                        <button type="submit" class="btn btn-success btn-md" name="btnDeny">Yes</button>
                                                        <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">No</button>
                                                        </form>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                <?php }
                                else {echo"<span class=\"col-sm-12 label label-success\">APPROVED</span>";}?>
                            </td>
                            </tr>
                        <?php $counter++;}?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

                <button class="btn btn-sm btn-default btn-flat pull-right" disabled="">ACTIVATE</button>
            </div>
            <!-- /.box-footer -->
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>
<?php
if (isset($_POST['btnPass'])){

    $kraDepartment = $_POST['kraDepartment'];
    $_SESSION['kraDepartment'] = $kraDepartment;


    echo "<script>location.href='manageKRA_s2.php';</script>";

}
?>

<?php
if(isset($_POST['btnApprove'])){
    $dept_id = $_POST['dept_id'];

    mysql_query("UPDATE tbl_kpi_requests SET request_status = 'APPROVED' WHERE department = '$dept_id'")or die(mysql_error());;
    mysql_query("INSERT INTO tbl_notification_user (notification,sendBy,status,owner) VALUES ('HR APPROVED YOUR REQUEST TO OPEN THE CREATION OF KRA','$department_name_session','unread','$dept_id')") or die(mysql_error());

    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'The request has been approved',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageKRA_s1.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageKRA_s1.php\";
                    }
                  }
                )
			</script>
		";
}
if(isset($_POST['btnDeny'])){
    $dept_id = $_POST['dept_id'];
    $sent_at_dept = $_POST['sent_at_dept'];
    $reason_hr = $_POST['deny_reason'];
    mysql_query("UPDATE tbl_kpi_requests SET request_status = 'DECLINED' WHERE department = '$dept_id'")or die(mysql_error());;
    mysql_query("INSERT INTO tbl_notification_user (notification,sendBy,status,owner) VALUES ('HR DENIED YOUR REQUEST TO OPEN THE CREATION OF KRA','$department_name_session','unread','$dept_id')") or die(mysql_error());
    mysql_query("INSERT INTO tbl_messages (subject,content,sender,receiver,sent_at,location,revision,id_appeal)VALUES ('DENIED REQUEST TO REOPEN THE CREATION OF KRA','$reason_hr','$department_name_session','$departmentName','$sent_at_dept','sentbox','','$departmentName')");
    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'The request has been declined',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageKRA_s1.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageKRA_s1.php\";
                    }
                  }
                )
			</script>
		";
}
?>
