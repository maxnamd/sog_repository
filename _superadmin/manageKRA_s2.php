<?php include 'includes/header.php'; ?>
<!-- Setting the treeview active -->
<script type="text/javascript">
document.getElementById("treeview2").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->
<script>
    $(document).ready(function() {
        $('#tbl_select_sog').DataTable();
    } );
</script>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading" style="background: black"><h4><b style="color:white;">Manage KRA - Select SOG Employee</b></h4>
            </div>

            <hr>

            <div class="panel-body">
                <table class="table table-hovered table-responsive" id="tbl_select_sog">
                    <thead class="">

                    <tr>
                        <th class="col-sm-1">SOG LASTNAME</th>
                        <th class="col-sm-1">SOG FIRSTNAME</th>
                        <!--<th class="col-sm-2">WEIGHTS</th>-->
                        <th class="col-sm-1">SOG POSITION</th>
                        <th class="col-sm-1">KRA COUNT</th>
                        <th class="col-sm-1">ACTION</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php
                    $selected_department = $_SESSION['kraDepartment'];
                    $display_sog_employee=mysql_query("SELECT * FROM tbl_sog_employee WHERE department = '$selected_department'") or die(mysql_error());
                    while($row=mysql_fetch_array($display_sog_employee)){
                        $kraStatus = $row['kraStatus'];
                        $employee_id = $row['emp_id']
                        ?>
                        <tr>
                            <td class="col-sm-1"><b><?php echo $row['lastname'];?></td>
                            <td class="col-sm-1"><?php echo $row['firstname'];?> </td>
                            <td class="col-sm-1"><?php echo $row['position'];?> </td>
                            <td class="col-sm-1">
                                <?php

                                $myKRA_dept=mysql_query("SELECT * FROM tbl_eformp1 WHERE kpiOwner = '$selected_department' AND kraCreatedFor ='$employee_id'") or die(mysql_error());
                                $countMyKra = mysql_num_rows($myKRA_dept);
                                echo "<span class='col-sm-5 label label-default font-weight-bold'>$countMyKra</span>";

                                ?>
                            </td>

                            <td class="col-sm-1">
                                <form method="post">
                                    <input type="hidden" name="emp_id" value="<?php echo $row['emp_id'];?>"/>

                                    <button name="btnPass" class="btn btn-sm btn-info">SELECT</button>
                                </form>
                            </td>
                        </tr>
                    <?php }?>

                    </tbody>

                </table>
            </div>

        </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>
<?php
if (isset($_POST['btnPass'])){


    $selected_emp_id = $_POST['emp_id'];
    $selected_emp_department = $_SESSION['kraDepartment'];

    $_SESSION['selected_emp_id'] = $selected_emp_id;
    $_SESSION['selected_emp_department'] = $selected_emp_department;


    echo "<script>location.href='manageKRA_S3.php';</script>";

}
?>

<?php
if(isset($_POST['btnApprove'])){
    $dept_id = $_POST['dept_id'];

    mysql_query("UPDATE tbl_kpi_requests SET request_status = 'APPROVED' WHERE department = '$dept_id'")or die(mysql_error());;
    mysql_query("INSERT INTO tbl_notification_user (notification,sendBy,status,owner) VALUES ('HR APPROVED YOUR REQUEST TO OPEN THE CREATION OF KRA','$department_name_session','unread','$dept_id')") or die(mysql_error());

    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'The request has been approved',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageKPI.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageKPI.php\";
                    }
                  }
                )
			</script>
		";
}
if(isset($_POST['btnDeny'])){
    $dept_id = $_POST['dept_id'];
    $sent_at_dept = $_POST['sent_at_dept'];
    $reason_hr = $_POST['deny_reason'];
    mysql_query("UPDATE tbl_kpi_requests SET request_status = 'DECLINED' WHERE department = '$dept_id'")or die(mysql_error());;
    mysql_query("INSERT INTO tbl_notification_user (notification,sendBy,status,owner) VALUES ('HR DENIED YOUR REQUEST TO OPEN THE CREATION OF KRA','$department_name_session','unread','$dept_id')") or die(mysql_error());
    mysql_query("INSERT INTO tbl_messages (subject,content,sender,receiver,sent_at,location,revision,id_appeal)VALUES ('DENIED REQUEST TO REOPEN THE CREATION OF KRA','$reason_hr','$department_name_session','$departmentName','$sent_at_dept','sentbox','','$departmentName')");
    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'The request has been declined',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageKPI.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageKPI.php\";
                    }
                  }
                )
			</script>
		";
}
?>
