<?php include 'includes/header.php'; ?>
<!-- Setting the treeview active -->
<script type="text/javascript">
document.getElementById("treeview2").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->

  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        KPI Management
        <!-- <small>Version 2.0</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- TABLE: STATUS KRA -->


        <div class="box box-danger" style="border-color: orangered">
            <div class="box-header with-border">
                <h3 class="box-title">Missed KRA SUBMISSION</h3>

                 <div class="pull-right" style="margin-right: 1em">
                    <form action="viewKRA_missed_report.php">
                        <input type="submit" class="btn btn-success" onclick="printing2();"value="PRINT"/>
                    </form>
                </div>

                <!-- <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin" id="tbl_late_kpi">
                       <thead>
                            <th class="col-sm-5">Department Name</th>
                            <th class="col-sm-5">Missed KPI Cycle Date</th>
                            <th class="col-sm-5">Request Status</th>
                       </thead>
                        <tbody>
                        <?php 
                            $display_date_kpi=mysql_query("SELECT * FROM tbl_datekpi") or die(mysql_error());
                            while($row2=mysql_fetch_array($display_date_kpi)){
                            $start = $row2['startDate'];
                            $end = $row2['endDate'];

                            $final = $start. '-' .$end;
                        ?>
                        
                        <?php } ?>

                        <?php
                        $counter = 1;
                        $display_request_kpi=mysql_query("SELECT * FROM tbl_kpi_requests") or die(mysql_error());

                        while($row=mysql_fetch_array($display_request_kpi)){

                            $departmentName = $row['department'];
                            $date_kpi = $row['date_of_kpi'];
                            $status = $row['request_status'];
                            ?>
                            <tr>
                            <td><?php echo $departmentName;?></td>
                            <td><?php echo $date_kpi;?></td>
                            <td><?php echo $status;?></td>
                            </tr>
                        <?php $counter++;}?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

                <button class="btn btn-sm btn-default btn-flat pull-right" disabled="">ACTIVATE</button>
            </div>
            <!-- /.box-footer -->
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>
<?php
if (isset($_POST['btnPass'])){


    $kpiOwner = $_POST['kpiOwner'];
    $kpiDept = $_POST['kpiDept'];

    $_SESSION['kpiOwner'] = $kpiOwner;
    $_SESSION['kpiDept'] = $kpiDept;


    echo "<script>location.href='viewKPI.php';</script>";

}
?>

<?php
if(isset($_POST['btnApprove'])){
    $dept_id = $_POST['dept_id'];

    mysql_query("UPDATE tbl_kpi_requests SET request_status = 'APPROVED' WHERE department = '$dept_id'")or die(mysql_error());;
    mysql_query("INSERT INTO tbl_notification_user (notification,sendBy,status,owner) VALUES ('HR APPROVED YOUR REQUEST TO OPEN THE CREATION OF KRA','$department_name_session','unread','$dept_id')") or die(mysql_error());

    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'The request has been approved',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageKPI.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageKPI.php\";
                    }
                  }
                )
			</script>
		";
}
if(isset($_POST['btnDeny'])){
    $dept_id = $_POST['dept_id'];
    $sent_at_dept = $_POST['sent_at_dept'];
    $reason_hr = $_POST['deny_reason'];
    mysql_query("UPDATE tbl_kpi_requests SET request_status = 'DECLINED' WHERE department = '$dept_id'")or die(mysql_error());;
    mysql_query("INSERT INTO tbl_notification_user (notification,sendBy,status,owner) VALUES ('HR DENIED YOUR REQUEST TO OPEN THE CREATION OF KRA','$department_name_session','unread','$dept_id')") or die(mysql_error());
    mysql_query("INSERT INTO tbl_messages (subject,content,sender,receiver,sent_at,location,revision,id_appeal)VALUES ('DENIED REQUEST TO REOPEN THE CREATION OF KRA','$reason_hr','$department_name_session','$departmentName','$sent_at_dept','sentbox','','$departmentName')");
    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'The request has been declined',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageKPI.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageKPI.php\";
                    }
                  }
                )
			</script>
		";
}
?>

<script type="text/javascript">

    function printing2()
    {


         var myWindow=window.open('','','width=1280,height=720');
         var divtoprint = document.getElementById('tbl_late_kpi');
         var htmlToPrint = '' +
        '<style type="text/css">' +
        'table th, table td {' +
        'border:0.5px solid black;' +
        'border-collapse: collapse;' +
        'padding: 0.5em;' +
        'margin: 0px' +
        '}' + 'select {' + '-webkit-appearance: none !important;' + '-moz-appearance: none !important;' +
        ' appearance: none !important;' + 'border: none !important;' + 'background: none !important;' +
        '</style>';

        /*myWindow.document.write('<center><table><tr><td id="repLogo"><img src="../assets/images/main/logo.png" style="width:100px;height:100px"/></td><td><center><span style="font-family:Segoe UI Light;font-size:11pt;">CAFFEINE HUB<br>LINGAYEN<br>Contact No.(075)09028 82787<br>Sales Report</span></center></td></tr></table></center>');*/
        myWindow.document.write('');
        myWindow.document.write('<div style="margin-left: 5em"><img src="../assets/login/img/upang.png" style="width:100px;height:100px"/> </div> <div style="margin-left: 15em; margin-top: -100px"> PHINMA - UNIVERSITY OF PANGASINAN <br/> <div style="margin-left: 52px;"> Arellano St., Dagupan City </div> <br/> <div style="margin-left: -180px; margin-top: 10px;"> <center> <strong> LATE KPI PASSERS </strong> </center> </div> </div>');
        htmlToPrint += divtoprint.outerHTML;
        myWindow.document.write('<br/><br/>');
         myWindow.document.write('<center>');
        myWindow.document.write(htmlToPrint);
        myWindow.document.write('</center>');
        //myWindow.document.write(document.getElementById('tbl_EV_t10').outerHTML);
        myWindow.document.write('<footer style="position: absolute;right: 0;bottom: 0;left: 0;padding: 1rem;background-color: #efefef;text-align: right;"> <b>TOP 10 Report</b> <br/>Prepared by: Human Resource Department</footer>');
        myWindow.document.close();
        myWindow.focus();
        myWindow.print();
        myWindow.close();
    }
</script>