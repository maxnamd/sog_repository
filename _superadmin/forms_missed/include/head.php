<?php
$employee_missed_performance_cycle = $_SESSION['employee_missed_performance_cycle'];
?>
<div class="col-sm-12">
	<div class="row">
		<div class="col-sm-1">
			<label class="control-label">Name: </label>
		</div>

		<div class="col-sm-5">
			<?php echo $_SESSION['employee_firstName_evaluation'] . ' ' .  $_SESSION['employee_middleName_evaluation'] . ' ' .$_SESSION['employee_lastName_evaluation'] ; ?>
		</div>	

		<div class="col-sm-2">
			<label class="control-label">Period Covered: </label>
		</div>

		<div class="col-sm-4">
			<?php echo $employee_missed_performance_cycle; ?>
		</div>
	</div>


	<div class="row">
		<div class="col-sm-1">
			<label class="control-label">Department: </label>
		</div>

		<div class="col-sm-5">
			<?php echo $department_name_session; ?>
		</div>

		<div class="col-sm-2">
			<label class="control-label">Position: </label>
		</div>

		<div class="col-sm-4">
			<?php echo $_SESSION['employee_position_evaluation']; ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-1">
			<label class="control-label">Section: </label>
		</div>

		<div class="col-sm-5">
			<?php echo ""; ?>
		</div>

		<div class="col-sm-2">
			<label class="control-label">Job Level: </label>
		</div>

		<div class="col-sm-4">
			<?php echo $_SESSION['employee_job_level_evaluation']; ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			<label class="control-label">Years in the Company: </label>
		</div>

		<div class="col-sm-1">
			<?php echo $_SESSION['employee_year_in_company_evaluation']; ?>
		</div>

        <div class="col-sm-3">
        </div>

		<div class="col-sm-2">
			<label class="control-label">Years in the Position: </label>
		</div>

		<div class="col-sm-4">
			<?php echo $_SESSION['employee_year_in_position_evaluation']; ?>
		</div>
	</div>

	<!-- forms -->

		<input type="hidden" name="lastName" value="<?php echo $lastname?>"/>
	    <input type="hidden" name="firstName" value="<?php echo $firstname?>"/>
	    <input type="hidden" name="periodCovered" value="<?php echo $employee_missed_performance_cycle;?>"/>
	    <input type="hidden" name="department" value="<?php echo $adepartment?>"/>
	    <input type="hidden" name="position" value="<?php echo $position?>"/>
	    <input type="hidden" name="section" value="<?php echo ''?>"/>
	    <input type="hidden" name="jobLevel" value="<?php echo '$jobLevel'?>"/>
	    <input type="hidden" name="years_in_company" value="<?php echo '$years_in_company'?>"/>
	    <input type="hidden" name="years_in_position" value="<?php echo '$years_in_position'?>"/>
    <!-- forms -->
</div>

