<?php error_reporting(0);?>
<?php 
    $rowSQL = mysql_query( "SELECT MAX(performanceCycle) AS max FROM tbl_evaluation_results;" );
    $row = mysql_fetch_array( $rowSQL );
    $largestNumber = $row['max'];
?>
<link rel="stylesheet" href="../assets/plugins/js-datatables/css/bootstrap-table.css">
<link rel="stylesheet" href="../assets/plugins/js-datatables/css/bootstrap-table-filter-control.css">
<link rel="stylesheet" href="../assets/plugins/js-datatables/css/datatable.css">
<script type="text/javascript" src="../assets/plugins/js-datatables/js/bootstrap-table.js"></script>
<script type="text/javascript" src="../assets/plugins/js-datatables/js/bootstrap-table-filter-control.js"></script>
<script type="text/javascript" src="../assets/plugins/js-datatables/js/Chart.bundle.js"></script>
<script type="text/javascript" src="../assets/plugins/js-datatables/js/js-yaml.js"></script>
<script type="text/javascript" src="../assets/plugins/js-datatables/js/datatable.js"></script>

    <link rel="stylesheet" media="print" href="print.css">
<div class="row">

</div>



 <div class="box box-info" style="border-color: green" id="printDIV">
            <div class="box-header with-border">
                <h3 class="box-title">Top 10 This Performance Cycle (<?php echo $largestNumber?>)</h3>

                <div class="pull-right" style="margin-right: 1em">
                    <input type="button" class="btn btn-success" onclick="openWinPrint();" value="PRINT"/>
                </div>

                <div class="pull-right" style="margin-right: 1em">
                	<form method="post">
                    <div class="pull-left">
                      <input type="email" name="email_receiving" class="form-control" placeholder="example@gmail.com" required>
                    </div>
                    	<input type="submit" class="btn btn-warning pull-right" name="send_to_email" value="SEND TO E-MAIL">
                    </form>
                </div>



                <!-- <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <div class="col-xs-6 pull-right">
                        <!--<div class="col-xs-5">
                            <label style="text-align: right;" class="pull-right">Search by Department: </label>
                        </div>
                        
                        <div class="col-xs-7">
                            <select id="tbl_EV_t10_select" class="form-control" name="select_dept">
                                <option></option>
                                <?php
/*                                $disp_dept=mysql_query("SELECT * FROM tbl_departments") or die(mysql_error());
                                $counter = 0;
                                while($d_row=mysql_fetch_array($disp_dept)){
                                        $f_dept = $d_row['department_name'];
                                    */?>
                                    <option value="<?php /*echo $f_dept*/?>"><?php /*echo $f_dept*/?></option>
                                 <?php /*  } */?>
                            </select>
                        </div>-->
                        
                    </div>
                    <?php
                    $return_arr = array();

                    $fetch = mysql_query("SELECT * FROM tbl_evaluation_results WHERE performanceCycle= '$largestNumber' AND evaluationStatus = 'COMPLETED' ORDER BY performance_rating DESC LIMIT 10");

                    while ($row = mysql_fetch_array($fetch, MYSQL_ASSOC)) {
                        $row_array['emp_id'] = $row['emp_id'];
                        $row_array['employee_fullName'] = $row['employee_firstName'] . ' ' . $row['employee_middleName'] . ' ' . $row['employee_lastName'];
                        $row_array['employee_firstName'] = $row['employee_firstName'];
                        $row_array['employee_middleName'] = $row['employee_middleName'];
                        $row_array['employee_lastName'] = $row['employee_lastName'];
                        $row_array['employee_department'] = $row['employee_department'];
                        $row_array['performance_rating'] = $row['performance_rating'];
                        $row_array['overall_rating'] = $row['overall_rating'];

                        array_push($return_arr,$row_array);
                    }

                    $json_data = json_encode($return_arr);
                    file_put_contents('ajax/data_this_pc.json', $json_data);



                    require_once '../assets/phpexcel/PHPExcel.php';

                    $objPHPExcel = new PHPExcel();

                    $query = "SELECT * FROM tbl_evaluation_results WHERE performanceCycle= '$largestNumber' ORDER BY performance_rating DESC LIMIT 10";

                    $result = mysql_query($query) or die(mysql_error());

                    $objPHPExcel = new PHPExcel();

                    $rowCount = 2;


                    while($row = mysql_fetch_array($result)){


                        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $rowCount-1);
                        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $row['employee_lastName']. ' ' .$row['employee_firstName'] );
                        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $row['employee_department']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $row['performance_rating']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $row['overall_rating']);
                        $rowCount++;
                    }
                    $objPHPExcel->getActiveSheet()->setTitle('TOP 10 PERFORMERS');

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

                    $objPHPExcel->getActiveSheet()->getStyle("A1:E1")->applyFromArray(array("font" => array("bold" => true)));

                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'RANK');
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'EMPLOYEE NAME');
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'DEPARTMENT');
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'PERFORMANCE RATING');
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'OVERALL RATING');





                    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
                    //$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
                    $filename ='SOG-Performers Of The Year.xlsx';
                    $objWriter->save(__DIR__ . '/data/' . $filename);

                    ?>

                    <table id="tbl_EV_t10" class="table datatable" data-json="ajax/data_this_pc.json"
                           data-id-field="name" data-rank-mode="grouped_muted"
                           data-sort-name="performance_rating"
                           data-sort-order="desc"
                           data-pagination="true"
                           data-show-pagination-switch="false"
                           data-filter-control="true">


                        <thead>
                        <tr>
                            <th data-rank="true" data-sortable="true">TOP</th>
                            <th data-field="employee_fullName" data-sortable="true">Employee Name</th>
                            <th data-field="employee_department" data-sortable="true" data-filter-control="select">Department Name</th>
                            <th data-field="performance_rating" data-sortable="true">Performance Rating</th>
                            <th data-field="overall_rating" data-sortable="true">Overall Rating</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            
            <!-- /.box-footer -->
</div>
<script>
     /*$(document).ready(function() {
         var t = $('#tbl_EV_t10').DataTable( {
             "order": [[ 1, 'asc' ]],
             "sPaginationType": "full_numbers",
             "bFilter": true,
             "sDom":"lrtip",
             "columnDefs": [ {
                 "searchable": false,
                 "orderable": false,
                 "targets": 0
             } ],
         } );

         var oTable;
         oTable = $('#tbl_EV_t10').dataTable();

         $('#tbl_EV_t10_select').change( function() {
             oTable.fnFilter( $(this).val() );
         });
     } );*/
</script>
<script type="text/javascript">

    function openWinPrint()
    {


         var myWindow=window.open('','','width=1280,height=720');
         var divtoprint = document.getElementById('tbl_EV_t10');
         var htmlToPrint = '' +
        '<style type="text/css">' +
        'table th, table td {' +
        'border:0.5px solid black;' +
        'border-collapse: collapse;' +
        'padding: 0.5em;' +
        'margin: 0px' +
        '}' + 'select {' + '-webkit-appearance: none !important;' + '-moz-appearance: none !important;' +
        ' appearance: none !important;' + 'border: none !important;' + 'background: none !important;' +
        '</style>';

        /*myWindow.document.write('<center><table><tr><td id="repLogo"><img src="../assets/images/main/logo.png" style="width:100px;height:100px"/></td><td><center><span style="font-family:Segoe UI Light;font-size:11pt;">CAFFEINE HUB<br>LINGAYEN<br>Contact No.(075)09028 82787<br>Sales Report</span></center></td></tr></table></center>');*/
        myWindow.document.write('');
        myWindow.document.write('<div style="margin-left: 5em"><img src="../assets/login/img/upang.png" style="width:100px;height:100px"/> </div> <div style="margin-left: 15em; margin-top: -100px"> PHINMA - UNIVERSITY OF PANGASINAN <br/> <div style="margin-left: 52px;"> Arellano St., Dagupan City </div> <br/> <div style="margin-left: -180px; margin-top: 10px;"> <center> <strong> TOP 10 PERFORMERS [<?php echo $largestNumber;?>] </strong> </center> </div> </div>');
        htmlToPrint += divtoprint.outerHTML;
        myWindow.document.write('<br/><br/>');
        myWindow.document.write(htmlToPrint);
        //myWindow.document.write(document.getElementById('tbl_EV_t10').outerHTML);
        myWindow.document.write('<footer style="position: absolute;right: 0;bottom: 0;left: 0;padding: 1rem;background-color: #efefef;text-align: right;"> <b>TOP 10 Report</b> <br/>Prepared by: Human Resource Department</footer>');
        myWindow.document.close();
        myWindow.focus();
        myWindow.print();
        myWindow.close();
    }
</script>

<?php
         
    if(isset($_POST['send_to_email']))
    {
      $email_receiving = $_POST['email_receiving'];

      $counter = 0;
     
   require '../assets/phpmailer/PHPMailerAutoload.php';

      $mail = new PHPMailer;

        $query = "SELECT * FROM tbl_evaluation_results WHERE performanceCycle= '$largestNumber' ORDER BY performance_rating DESC LIMIT 10";

		 $result = mysql_query($query) or die(mysql_error());

		 $body .= '<center><strong>TOP 10 PERFORMERS</strong> <br/> <br/> <table style="border: 1px solid black; border-collapse: collapse"><tr><td style="border: 1px solid black;"><strong>RANK</strong></td><td style="border: 1px solid black;"><strong>EMPLOYEE NAME</strong></td><td style="border: 1px solid black;"><strong>EMPLOYEE DEPARTMENT</strong></td><td style="border: 1px solid black;"><strong>PERFORMANCE RATING</strong></td><td style="border: 1px solid black;"><strong>OVERALL RATING</strong></td></tr>';
		 while($row2 = mysql_fetch_array($result))
		 {
      $counter++;
		 	$body .= '<tr><td style="border: 1px solid black;">'.$counter.'</td><td style="border: 1px solid black;">' .$row2['employee_firstName']. ' '.$row2['employee_middleName']. ' '.$row2['employee_lastName']. '</td><td style="border: 1px solid black;">' .$row2['employee_department']. '</td><td style="border: 1px solid black;">' .$row2['performance_rating']. '</td><td style="border: 1px solid black;">' .$row2['overall_rating']. '</td></tr>';
		 }
		 $body .= '</table></center>';


      //$mail->SMTPDebug = 3;                               // Enable verbose debug output
      //$mail->SMTPDebug = 1;
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = "smtp.gmail.com";    // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = "es.macbeth.2017@gmail.com";
      $mail->Password = "nopasswordno";                          // SMTP password
      $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 465;                                    // TCP port to connect to

      $mail->setFrom('from@example.com', 'UPang - Human Resource Department');
      $mail->addAddress($email_receiving, 'Joe User');     			// Add a recipient
      $mail->addAddress('ellen@example.com');               // Name is optional
      $mail->addReplyTo('info@example.com', 'Information');
      $mail->addCC('cc@example.com');
      $mail->addBCC('bcc@example.com');

      $mail->addAttachment('data/SOG-Performers Of The Year.xlsx', 'Performers of the Year.xlsx');

      $mail->isHTML(true);   
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ));
                                           // Set email format to HTML

      $mail->Subject = 'University of Pangasinan - PHINMA Top 10 Performers';
      $mail->Body    = $body.'<br/><footer style="position: absolute;right: 0;bottom: 0;left: 0;padding: 1rem;background-color: #efefef;text-align: right;"> <b>TOP 10 Report</b> <br/>Prepared by: Human Resource Department</footer>';
      $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
      
     if(!$mail->send()) {
  			echo "<script type='text/javascript'>
        
        swal({
                  title: 'ERROR!',
                  text: 'Oops! Something went wrong!',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"viewResults.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"viewResults.php\";
                    }
                  }
                )
      </script>";
	} else 
		{
	  			echo "<script type='text/javascript'>
        swal({
                  title: 'SUCCESS!',
                  text: 'E-mail has been sent to PEN',
                  type: \"success\",
                  timer: 1000,
                  allowOutsideClick: false,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"viewResults.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"viewResults.php\";
                    }
                  }
                )
      </script>";
		}
    }
  ?>