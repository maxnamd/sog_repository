<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 9/4/2017
 * Time: 8:51 PM
 */
?>

<?php include 'includes/header.php'; ?>
<?php
$pass_data_emp_id = $_SESSION['employee_username'];
$pass_data_pCycle = $_SESSION['performanceCycle_EV'];
?>


<?php
$get_details=mysql_query("SELECT * FROM tbl_evaluation_results WHERE performanceCycle='$pass_data_pCycle' AND emp_id ='$pass_data_emp_id'") or die(mysql_error());

while($get_row=mysql_fetch_array($get_details)) {

    $emp_id = $get_row['emp_id'];
    $employee_firstName = $get_row['employee_firstName'];
    $employee_middleName = $get_row['employee_middleName'];
    $employee_lastName = $get_row['employee_lastName'];
    $employee_department = $get_row['employee_department'];
    $employee_position = $get_row['employee_position'];
    $employee_job_level = $get_row['employee_job_level'];
    $employee_years_company = $get_row['employee_years_company'];
    $employee_years_position = $get_row['employee_years_position'];
    $evaluationStatus = $get_row['evaluationStatus'];
    $pCycle = $get_row['performanceCycle'];
    $admin_answered = $get_row['answeredBy'];
    //$employee_email = $get_row['email_employee'];

    $_SESSION['fullName_emp'] = $employee_firstName . ' '. $employee_middleName . ' ' . $employee_lastName;
    $_SESSION['admin_answered'] = $admin_answered;
    $_SESSION['pc_answered'] = $get_row['performanceCycle'];
    $_SESSION['dept_emp'] = $employee_department;
    //$_SESSION['email_employee'] = $employee_email;
}
?>
<?php
$get_fname=mysql_query("SELECT * FROM tbl_sog_employee WHERE emp_id ='$pass_data_emp_id'") or die(mysql_error());
while($get_rowName=mysql_fetch_array($get_fname)){
    $employee_email = $get_rowName['email_employee'];
    $_SESSION['email_employee'] = $employee_email;
}

?>

<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview4a").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->
<script>
    $(document).ready(function() {
        $('#tbl_EV').DataTable();
    } );
</script>

<!--<script>
    alert('<?php /*echo $_SESSION['email'];*/?>');
</script>-->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--HEADER-->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- TABLE: STATUS KRA -->
        <div class="box box-info" style="border-color: green">
            <div class="box-header with-border">
                <h3 class="box-title">Ongoing Evaluation of <b><?php echo $employee_firstName . ' ' .  $employee_middleName . ' ' . $employee_lastName ; ?></b>
                    <br><span class="small"><b><i><?php echo $employee_department?></i></b></span>
                </h3>

                <!-- <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->



                <!-- GET ADMIN ID-->
                <?php
                $admin_receiver = mysql_query("SELECT * FROM tbl_departments WHERE department_name = '$employee_department'");
                $ar_row=mysql_fetch_array($admin_receiver);
                $admin_receiver = $ar_row['departmentOwner'];
                $admin_dept = $ar_row['department_name'];
                ?>
                <!-- END GET ADMIN ID-->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">P1 - KPI</a></li>
                        <li><a href="#tabs-2a">P2A - Competencies</a></li>
                        <li><a href="#tabs-2b">P2B - Competency Details</a></li>
                        <li><a href="#tabs-3">P3 - Professionalism</a></li>
                        <li><a href="#tabs-4">P4 - Customer Service</a></li>
                        <li><a href="#tabs-5">Summary</a></li>
                    </ul>


                    <div id="tabs-1">
                        <?php include 'evaluation_forms/form_p1.php';?>
                    </div>

                    <div id="tabs-2a">
                        <?php include 'evaluation_forms/form_p2a.php';?>
                    </div>

                    <div id="tabs-2b">
                        <?php include 'evaluation_forms/form_p2b.php';?>
                    </div>

                    <div id="tabs-3">
                        <?php include 'evaluation_forms/form_p3.php';?>
                    </div>

                    <div id="tabs-4">
                        <?php include 'evaluation_forms/form_p4.php';?>
                    </div>

                    <div id="tabs-5">
                        <?php include 'evaluation_forms/form_summary.php';?>
                    </div>

                </div>


            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <div class="row" style="margin-bottom: 0">
                    <span class="small label label-danger" id="notify_client"></span>
                    <div class="col-sm-8"></div>
                    <div class="col-sm-2"><a href="#" class="btn btn-warning btn-block" data-toggle="modal" data-target="#reviseEV" id="revise_ev">SEND A NOTE</a></div>
                    <!--<div class="col-sm-2"><input type="button" class="btn btn-success btn-block" id="approveEV" value="APPROVE"/></div>-->



                    <!--<script>
                        alert("<?php /*echo $emp_id;*/?>");
                    </script>-->
                    <div class="col-sm-2">
                        <a class="btn btn-block btn-success" id="approve_ev" data-id="<?php echo $emp_id; ?>" href="javascript:void(0)">APPROVE</a>
                    </div>

                    <script type="text/javascript">

                        $(document).ready(function(){


                            $(document).on('click', '#approve_ev', function(e){
                                <?php
                                $fullName_emp = 'HR approved the evaluation for ' .$_SESSION['fullName_emp'];
                                $admin_ev = $department_name_session;
                                $new_admin_id = $employee_department;
                                $unread = 'unread';
                                ?>
                                var evaluationID = $(this).attr("data-id");
                                var dateID = '<?php echo $pass_data_pCycle;?>';

                                var fullName_emp = '<?php echo $fullName_emp;?>';
                                var admin_ev = '<?php echo $admin_ev;?>';
                                var new_admin_id = '<?php echo $new_admin_id;?>';
                                var unread = '<?php echo $unread;?>';
                                SwalDelete(evaluationID,dateID,fullName_emp,admin_ev,unread,new_admin_id);
                                e.preventDefault();
                            });

                        });

                        function SwalDelete(evaluationID,dateID,fullName_emp,admin_ev,unread,new_admin_id){
                            swal({
                                title: 'Are you sure?',
                                text: "You won't be able to revert this!",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, approve it!',
                                showLoaderOnConfirm: true,

                                preConfirm: function() {
                                    return new Promise(function(resolve) {
                                        $.ajax({
                                            url: 'phpfunctions/approve_evaluation.php',
                                            type: 'POST',
                                            data: 'approve='+evaluationID + '&date='+dateID + '&fullName_emp='+fullName_emp + '&admin_ev='+admin_ev + '&unread='+unread + '&new_admin_id='+new_admin_id,
                                            dataType: 'json'
                                        })
                                        .done(function(response){

                                            swal('APPROVED! ', response.message, response.status);
                                            $.ajax({
                                                type: "POST",
                                                url: "phpfunctions/send_status_approval.php",
                                                success: function() {
                                                    window.setTimeout(function(){
                                                        window.location.href = "viewEvaluations_department.php";
                                                    }, 5000);
                                                }
                                            });

                                        })
                                        .fail(function(){
                                            swal('Oops...', 'Something went wrong with ajax !', 'error');

                                        });
                                    });
                                },
                                allowOutsideClick: false
                            });
                        }

                    </script>
                </div>
            </div>
            <!-- /.box-footer -->
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="reviseEV" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">SEND A NOTE</h4>


            </div>

            <div class="modal-body">
                <div class="container-fluid">
                <form method="post" class="form-horizontal">
                    <div class="row" style="margin-bottom: 1em;margin-top: 1em;">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                        </div>
                    </div>



                    <div class="col-sm-2">
                        <label>
                            Note:
                        </label>
                    </div>


                    <div class="col-sm-10">

                        <textarea class="form-control" id="sendREV" name="message">
                                Please justify your ratings for <?php echo $employee_firstName . ' ' . $employee_lastName?>


                        </textarea>
                        <script type="text/javascript">
                            $('#sendREV').wysihtml5();
                        </script>
                    </div>

                </div>




                    <div class="modal-footer">
                        <div class="col-sm-8" style="margin-top: 2em;">
                            <span style="color: red" class="small">*This note will notify the Department Head to justify his/her evaluation for Mr/Ms <?php echo $employee_lastName?></span>
                            <input type="hidden" name="receiving" value="<?php echo $employee_department;?>">
                              <input type="hidden" name="sent_at" value="<?php
                                                                          date_default_timezone_set('Asia/Manila');
                                                                          echo date('F j, Y g:i:a  ');
                              ?>">
                        </div>

                        <div class="col-sm-4" style="margin-top: 2em;">
                            <input type="submit" class="btn btn-success" name="btnJustify" value="SEND"  />
                            <input type="reset" class="btn btn-default" value="Clear"/>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


    if('<?php echo "$evaluationStatus";?>' == 'COMPLETED'){
        document.getElementById("approve_ev").style.visibility = 'hidden';
        document.getElementById("revise_ev").style.visibility = 'hidden';

        document.getElementById('notify_client').innerHTML = "THIS EVALUATION HAS BEEN APPROVED";
    }
    else{
        document.getElementById("approve_ev").style.visibility = 'visible';
        document.getElementById("revise_ev").style.visibility = 'visible';
    }
</script>
<script type="text/javascript">
    callOnLoadCompute();
    function callOnLoadCompute(){
        var p1_sum = 0;

        $('.compute_p1_weights').each(function() {
            p1_sum += Number($(this).val());
        });

        document.getElementById('total_weight_p1').value=p1_sum;

        var p1b_sum = 0;

        $('.compute_p1_weighted_rating').each(function() {
            p1b_sum += Number($(this).val());
        });
        document.getElementById('total_weighted_rating').value=parseFloat(p1b_sum).toFixed(2);


        var p2_sum = 0;
        $('.compute_weights_p2').each(function() {
            p2_sum += Number($(this).val());
        });
        document.getElementById('total_weights_p2').value=p2_sum;

        var p2_sum_twrp2 = 0;

        $('.compute_p2_weighted_rating').each(function() {
            p2_sum_twrp2 += Number($(this).val());
        });

        document.getElementById('total_weighted_rating_p2').value=parseFloat(p2_sum_twrp2).toFixed(2);


        p3_comp();


        var p4_sum = 0;

        $('.factor_rating_p4').each(function() {

            p4_sum += Number($(this).val());
        });

        document.getElementById('total_cs_rating').value=parseFloat(p4_sum/4).toFixed(2);

        document.getElementById('cs_factor_rating').value=parseFloat(p4_sum/4).toFixed(2);


    }
</script>

<?php include 'includes/footer.php'; ?>

<?php
if (isset($_POST['btnJustify'])){
    $sent_at = $_POST['sent_at'];
    $note_subject = "JUSTIFY EVALUATION FOR $employee_firstName $employee_lastName";
    $note_message = $_POST['message'];
    $receiving = $_POST['receiving'];

     $query_DO=mysql_query("SELECT * FROM tbl_departments WHERE department_name = '$receiving'") or die (mysql_error());
         while($row=mysql_fetch_array($query_DO)){
            $department_owner = $row['department_name'];
            }

    mysql_query("INSERT INTO tbl_messages (subject,content,sender,receiver,revision,location,sent_at) VALUES ('$note_subject','$note_message','Human Resource Department','$receiving','YES','sentbox','$sent_at')") or die(mysql_error());
    mysql_query("INSERT INTO tbl_notification_user (notification,sendBy,status,owner) VALUES ('Human Resource Department sent a message','$department_name_session','unread','$department_owner')") or die(mysql_error());
   

    $rewrew=mysql_query("SELECT * FROM tbl_admins WHERE department='$receiving'") or die (mysql_error());
       while ($rew = mysql_fetch_array($rewrew)){
        $email_receiving = $rew['email'];
       }

   require '../assets/phpmailer/PHPMailerAutoload.php';

      $mail = new PHPMailer;
     $body .= $note_message;

      //$mail->SMTPDebug = 3;                               // Enable verbose debug output
      //$mail->SMTPDebug = 1;
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = "smtp.gmail.com";    // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = "es.macbeth.2017@gmail.com";
      $mail->Password = "nopasswordno";                          // SMTP password
      $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 465;                                    // TCP port to connect to

      $mail->setFrom('from@example.com', 'UPang - Human Resource Department');
      $mail->addAddress($email_receiving, 'Joe User');          // Add a recipient
      $mail->addAddress('ellen@example.com');               // Name is optional
      $mail->addReplyTo('info@example.com', 'Information');
      $mail->addCC('cc@example.com');
      $mail->addBCC('bcc@example.com');

      $mail->isHTML(true);   
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ));
                                           // Set email format to HTML

      $mail->Subject = 'PHINMA UPang SOG EVALUATION SYSTEM -- Evaluation for '. $employee_firstName. ' '.$employee_lastName. 'needs justification';
      $mail->Body    = $body. '<br/> Please reply to HR Department using the SOG Evaluation System';
      
     if(!$mail->send()) {
        echo "<script type='text/javascript'>
        
        swal({
                  title: 'ERROR!',
                  text: 'Oops! Something went wrong!',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"viewEvaluations_processing.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"viewEvaluations_processing.php\";
                    }
                  }
                )
      </script>";
  } else 
    {
          echo "<script type='text/javascript'>
        swal({
                  title: 'SUCCESS!',
                  text: 'You have sent your note to $receiving',
                  type: \"success\",
                  timer: 1000,
                  allowOutsideClick: false,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"viewEvaluations_processing.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"viewEvaluations_processing.php\";
                    }
                  }
                )
      </script>";
    }
}
?>


