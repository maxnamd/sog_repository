<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 9/4/2017
 * Time: 8:51 PM
 */
?>

<?php include 'includes/header.php'; ?>

<?php 
    $rowSQL = mysql_query( "SELECT MAX(performanceCycle) AS max FROM tbl_evaluation_results;" );
    $row = mysql_fetch_array( $rowSQL );
    $largestNumber = $row['max'];
?>

<?php
/*departmentEV
departmentOwner*/
if (isset($_POST['btnPassToEVresults'])){
    $_SESSION['departmentEV'] = $_POST['departmentEV'];
    $_SESSION['departmentOwner'] = $_POST['departmentOwner'];
    echo "
    <script type='text/javascript'>
    document.location.href='viewEvaluation_results.php';
    </script>";
}
?>

<?php
/*departmentEV
departmentOwner*/
if (isset($_POST['btn_link_to_history'])){
   
    echo "
    <script type='text/javascript'>
    document.location.href='link_to_history.php';
    </script>";
}
?>
<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview5").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->


<link rel="stylesheet" href="../assets/plugins/js-datatables/css/bootstrap-table.css">
<link rel="stylesheet" href="../assets/plugins/js-datatables/css/bootstrap-table-filter-control.css">
<link rel="stylesheet" href="../assets/plugins/js-datatables/css/datatable.css">
<script type="text/javascript" src="../assets/plugins/js-datatables/js/bootstrap-table.js"></script>
<script type="text/javascript" src="../assets/plugins/js-datatables/js/bootstrap-table-filter-control.js"></script>
<script type="text/javascript" src="../assets/plugins/js-datatables/js/Chart.bundle.js"></script>
<script type="text/javascript" src="../assets/plugins/js-datatables/js/js-yaml.js"></script>
<script type="text/javascript" src="../assets/plugins/js-datatables/js/datatable.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Evaluations Results
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- TABLE: STATUS KRA -->
        <div class="box box-info" style="border-color: green">
            <div class="box-header with-border">
                <h3 class="box-title">This Performance Cycle (<?php echo $largestNumber?>)</h3>

                <div class="pull-right" style="margin-right: 1em">
                    <input type="button" class="btn btn-success" onclick="openWinPrint();" value="PRINT"/>
                </div>

                <!-- <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <div class="col-sm-6 pull-right">
                        <!--<div class="col-sm-5">
                            <label style="text-align: right;" class="pull-right">Search by Department: </label>
                        </div>
                        
                        <div class="col-sm-7">
                            <select id="tbl_see_all_list_this_year_select" class="form-control" name="select_dept">
                                <option></option>
                                <?php
/*                                $disp_dept=mysql_query("SELECT * FROM tbl_departments") or die(mysql_error());
                                $counter = 0;
                                while($d_row=mysql_fetch_array($disp_dept)){
                                        $f_dept = $d_row['department_name'];
                                    */?>
                                    <option value="<?php /*echo $f_dept*/?>"><?php /*echo $f_dept*/?></option>
                                 <?php /*  } */?>
                            </select>
                        </div>-->
                        
                    </div>

                    <?php
                    $return_arr = array();

                    $fetch = mysql_query("SELECT * FROM tbl_evaluation_results WHERE performanceCycle= '$largestNumber' ORDER BY performance_rating DESC");

                    while ($row = mysql_fetch_array($fetch, MYSQL_ASSOC)) {
                        $row_array['emp_id'] = $row['emp_id'];
                        $row_array['employee_fullName'] = $row['employee_firstName'] . ' ' . $row['employee_middleName'] . ' ' . $row['employee_lastName'];
                        $row_array['employee_firstName'] = $row['employee_firstName'];
                        $row_array['employee_middleName'] = $row['employee_middleName'];
                        $row_array['employee_lastName'] = $row['employee_lastName'];
                        $row_array['employee_department'] = $row['employee_department'];
                        $row_array['performance_rating'] = $row['performance_rating'];
                        $row_array['overall_rating'] = $row['overall_rating'];

                        array_push($return_arr,$row_array);
                    }

                    $json_data = json_encode($return_arr);
                    file_put_contents('ajax/data_this_pc_all.json', $json_data);
                    ?>

                    <table id="tbl_see_all_list_this_year" class="table datatable" data-json="ajax/data_this_pc.json"
                           data-id-field="name" data-rank-mode="grouped_muted"
                           data-sort-name="performance_rating"
                           data-sort-order="desc"
                           data-pagination="true"
                           data-show-pagination-switch="false"
                           data-filter-control="true">
                        <thead>
                        <tr>
                            <th data-rank="true" data-sortable="true">TOP</th>
                            <th data-field="employee_fullName" data-sortable="true">Employee Name</th>
                            <th data-field="employee_department" data-sortable="true" data-filter-control="select">Department Name</th>
                            <th data-field="performance_rating" data-sortable="true">Performance Rating</th>
                            <th data-field="overall_rating" data-sortable="true">Overall Rating</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
           
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>




<script type="text/javascript">
    function openWinPrint()
    {
         var myWindow=window.open('','','width=1280,height=720');
         var divtoprint = document.getElementById('tbl_see_all_list_this_year');
         var htmlToPrint = '' +
        '<style type="text/css">' +
        'table th, table td {' +
        'border:0.5px solid black;' +
        'border-collapse: collapse;' +
        'padding: 0.5em;' +
        'margin: 0px' +
        '}' + 'select {' + '-webkit-appearance: none !important;' + '-moz-appearance: none !important;' +
        ' appearance: none !important;' + 'border: none !important;' + 'background: none !important;' +
        '</style>';
        /*myWindow.document.write('<center><table><tr><td id="repLogo"><img src="../assets/images/main/logo.png" style="width:100px;height:100px"/></td><td><center><span style="font-family:Segoe UI Light;font-size:11pt;">CAFFEINE HUB<br>LINGAYEN<br>Contact No.(075)09028 82787<br>Sales Report</span></center></td></tr></table></center>');*/
        myWindow.document.write('');
       myWindow.document.write('<div style="margin-left: 5em"><img src="../assets/login/img/upang.png" style="width:100px;height:100px"/> </div> <div style="margin-left: 15em; margin-top: -100px"> PHINMA - UNIVERSITY OF PANGASINAN <br/> <div style="margin-left: 52px;"> Arellano St., Dagupan City </div> <br/> <div style="margin-left: -180px; margin-top: 10px;"> <center> <strong> TOP 10 PERFORMERS [<?php echo $largestNumber;?>] </strong> </center> </div> </div>');
        htmlToPrint += divtoprint.outerHTML;
        myWindow.document.write('<br/><br/>');
        myWindow.document.write(htmlToPrint);
        //myWindow.document.write(document.getElementById('tbl_EV_t10').outerHTML);
        myWindow.document.write('<footer style="position: absolute;right: 0;bottom: 0;left: 0;padding: 1rem;background-color: #efefef;text-align: right;"> <b>TOP 10 Report</b> <br/>Prepared by: Human Resource Department</footer>');
        myWindow.document.close();
        myWindow.focus();
        myWindow.print();
        myWindow.close();
    }
</script>