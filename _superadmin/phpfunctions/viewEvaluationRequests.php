<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 11/10/2017
 * Time: 4:57 PM
 */
?>
<!--DIV OF REQUESTS-->
<div class="box box-info" style="border-color: green">
    <div class="box-header with-border">
        <h3 class="box-title">Requests Table</h3>

        <!--<div class="pull-right" style="margin-right: 1em">

            <input type="button" class="btn btn-success" value="REPORT"/>
        </div>-->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin" id="tbl_requests">
                <thead>
                <tr>
                    <th class="col-sm-2">Department Name</th>
                    <th class="col-sm-2">Department Head</th>
                    <th class="col-sm-2">Employee Name</th>
                    <th class="col-sm-2">Performance Cycle</th>
                    <th class="col-sm-1">Messages</th>
                    <th class="col-sm-1">Reason</th>
                    <th class="col-sm-1">Action</th>
                </tr>
                </thead>

                <tbody>
                    <?php
                    $counter = 1;
                    $display_request=mysql_query("SELECT * FROM tbl_evaluation_requests WHERE request_status = 'SENT' OR request_status = 'APPEALED' OR request_status = 'APPROVED'") or die(mysql_error());
                    while($row_req=mysql_fetch_array($display_request)) {
                        $request_status = $row_req['request_status'];
                        $head_department = $row_req['employee_department'];
                        $admin_id = $row_req['admin_id'];
                        $employee_id_missed = $row_req['employee_missed'];
                        $performanceCycle_missed = $row_req['performanceCycle'];
                        $reason_msg = $row_req['reason'];
                        $request_id = $row_req['id'];
                        $fetch_head_name=mysql_query("SELECT * FROM tbl_admins WHERE admin_id = '$admin_id'") or die(mysql_error());
                        while($row_fhead=mysql_fetch_array($fetch_head_name)) {
                            $head_firstName = $row_fhead['firstname'];
                            $head_middleName = $row_fhead['middlename'];
                            $head_lastName = $row_fhead['lastname'];

                            $head_fullname = $head_firstName . ' ' . $head_lastName;
                        }

                        $fetch_sog_name=mysql_query("SELECT * FROM tbl_sog_employee WHERE emp_id = '$employee_id_missed'") or die(mysql_error());
                        while($row_fsog=mysql_fetch_array($fetch_sog_name)) {
                            $sog_firstName = $row_fsog['firstname'];
                            $sog_middleName = $row_fsog['middlename'];
                            $sog_lastName = $row_fsog['lastname'];

                            $sog_fullname = $sog_firstName . ' ' . $sog_lastName;
                        }






                    ?>
                <tr>
                    <td>    <?php echo $head_department?>   </td>
                    <td>    <?php echo $head_fullname?>     </td>
                    <td>    <?php echo $sog_fullname;?>     </td>
                    <td>    <?php echo $performanceCycle_missed?></td>
                    <td>
                        <div class="col-sm-12">
                            <div>
                                <!--//MESSAGES-->
                                <a href='#' class='btn btn-info btn-sm' data-toggle='modal' data-target="#viewMessages_modal<?php echo $counter?>">VIEW MESSAGES</a>

                                <!-- Messages modal -->
                                <div id="viewMessages_modal<?php echo $counter;?>" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Messages for this Request</h4>
                                            </div>
                                            <div class="modal-body" style="max-height: calc(100vh - 212px) !important;overflow-y: auto;padding: 1em;">

                                                <style type="text/css">
                                                    .chat
                                                    {
                                                        list-style: none;
                                                        margin: 0;
                                                        padding: 0;
                                                    }

                                                    .chat li
                                                    {
                                                        margin-bottom: 10px;
                                                        padding-bottom: 5px;
                                                        border-bottom: 1px dotted #B3A9A9;
                                                    }

                                                    .chat li.left .chat-body
                                                    {
                                                        margin-left: 60px;
                                                    }

                                                    .chat li.right .chat-body
                                                    {
                                                        margin-right: 60px;
                                                    }


                                                    .chat li .chat-body p
                                                    {
                                                        margin: 0;
                                                        color: #777777;
                                                    }
                                                    ::-webkit-scrollbar-track
                                                    {
                                                        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                                                        background-color: #F5F5F5;
                                                    }

                                                    ::-webkit-scrollbar
                                                    {
                                                        width: 12px;
                                                        background-color: #F5F5F5;
                                                    }

                                                    ::-webkit-scrollbar-thumb
                                                    {
                                                        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
                                                        background-color: #555;
                                                    }

                                                </style>

                                                <ul class="chat">
                                                    <?php
                                                    $display_my_messages=mysql_query("SELECT * FROM tbl_messages WHERE performanceCycle = '$performanceCycle_missed' AND id_appeal = '$employee_id_missed' ORDER BY sent_at ASC") or die(mysql_error());
                                                    $check_num_rows = mysql_num_rows($display_my_messages);

                                                    if ($check_num_rows > 0) {
                                                        while($row_messages=mysql_fetch_array($display_my_messages)){
                                                            $sender = $row_messages['sender'];
                                                            $receiver = $row_messages['receiver'];
                                                            $content = $row_messages['content'];
                                                            $time = $row_messages['sent_at'];


                                                            if($sender==$department_name_session){
                                                                ?>
                                                                <li class="right clearfix">
                                                                    <span class="chat-img pull-right">
                                                                        <img src="http://placehold.it/50/55C1E7/fff&text=HR" alt="User Avatar" class="img-circle" />
                                                                    </span>
                                                                    <div class="chat-body clearfix">
                                                                        <div class="header">
                                                                            <strong class="primary-font"><?php echo $department_name_session?></strong> <small class="pull-right text-muted">
                                                                                <span class="glyphicon glyphicon-time"></span><?php echo $time?></small>
                                                                        </div>
                                                                        <p>
                                                                            <?php
                                                                            echo $content;
                                                                            ?>
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                            <?php }
                                                            else{
                                                                /*$display_name_admin=mysql_query("SELECT * FROM tbl_admins WHERE department = '$sender'") or die(mysql_error());
                                                                while($row_name_admin=mysql_fetch_array($display_name_admin)) {
                                                                    $fullname_admin = $row_name_admin['firstname'] . ' ' . $row_name_admin['lastname'];
                                                                }*/
                                                                ?>
                                                                <li class="left clearfix">

                                                        <span class="chat-img pull-left">
                                                            <img src="http://placehold.it/50/FA6F57/fff&text=DH" alt="User Avatar" class="img-circle" />
                                                        </span>
                                                                    <div class="chat-body clearfix">
                                                                        <div class="header">
                                                                            <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?php echo $time?></small>
                                                                            <strong class="pull-right primary-font"><?php echo $head_department;?></strong>
                                                                        </div>
                                                                        <p>
                                                                            <?php
                                                                            echo $content;
                                                                            ?>
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <?php
                                                            }

                                                        }}
                                                    else{?>
                                                        NO MESSAGES YET <?php }?>


                                                </ul>

                                            </div>
                                            <div class="modal-footer">

                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <!-- END Messages modal -->
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="col-sm-2">
                            <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#viewReasonModal_<?php echo $counter;?>" title="VIEW REASON"><i class="fa fa-eye" ></i></a>

                            <!-- Modal -->
                            <div id="viewReasonModal_<?php echo $counter;?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Reason</h4>
                                        </div>
                                        <div class="modal-body">
                                            <ul class="list-group">
                                                <?php echo $reason_msg?>
                                            </ul>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </td>

                    <td>

                        <?php
                        if($request_status != 'APPROVED'){
                        ?>
                        <div class="col-sm-2">
                            <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#approveModal_<?php echo $counter?>" title="APPROVE"><i class="fa fa-check" ></i></a>

                            <!-- Modal -->
                            <div id="approveModal_<?php echo $counter?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header" style="background-color: #2E7D32 !important;border-color: #2E7D32 !important;color: white">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Confirm</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Are you sure to approve this request of <?php echo $head_fullname;?> for <?php echo $sog_fullname?> ?</h5>
                                        </div>
                                        <div class="modal-footer">
                                            <form method="post">
                                                <input type="hidden" name="admin_id" value="<?php echo $head_department?>"/>
                                                <input type="hidden" name="employee_missed" value="<?php echo $employee_id_missed?>"/>
                                                <input type="hidden" name="missed_pc" value="<?php echo $performanceCycle_missed?>"/>
                                            <input type="hidden" name="request_id" value="<?php echo $request_id?>"/>
                                            <button type="submit" class="btn btn-success btn-md" name="btnApprove">Yes</button>
                                            <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">No</button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-1"></div>

                        <div class="col-sm-2">
                            <a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#denyModal_<?php echo $counter?>" title="DENY"><i class="fa fa-close" ></i></a>

                            <!-- Modal -->
                            <div id="denyModal_<?php echo $counter?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header" style="background-color: #2E7D32 !important;border-color: #2E7D32 !important;color: white">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Confirm</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h5>To deny the request of <b><?php echo $head_fullname;?></b> for <b><i><?php echo $sog_fullname?></i></b> you must state a reason below</h5>
                                            <form method="post">
                                             <input type="hidden" name="sent_at_dept" value="<?php date_default_timezone_set('Asia/Manila'); echo date('F j, Y g:i:a');?>">
                                                <textarea class="form-control ckeditor" id="deny_reason_<?php echo $emp_count;?>" name="deny_reason">
                                                    <script type="text/javascript">
                                                        var textarea = document.getElementById('deny_reason_<?php echo $emp_count;?>');

                                                        CKEDITOR.replace( 'textarea',
                                                            {
                                                                toolbar : 'Basic', /* this does the magic */
                                                                uiColor : '#9AB8F3'
                                                            });

                                                    </script>
                                                </textarea>
                                        </div>
                                        <div class="modal-footer">

                                                <input type="text" name="admin_id" value="<?php echo $head_department?>"/>
                                                <input type="hidden" name="employee_missed" value="<?php echo $employee_id_missed?>"/>
                                                <input type="hidden" name="missed_pc" value="<?php echo $performanceCycle_missed?>"/>
                                               
                                                <input type="hidden" name="request_id" value="<?php echo $request_id?>"/>
                                                <button type="submit" class="btn btn-success btn-md" name="btnDeny">Yes</button>
                                                <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">No</button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                            <?php }
                            else {echo"<span class=\"col-sm-12 label label-success\">APPROVED</span>";}?>
                    </td>


                </tr>
                <?php $counter++;}?>
                </tbody>
            </table>
        </div>
    </div>

</div>
<!--END DIV OF MISSED EVALUATIONS-->
<?php
if(isset($_POST['btnApprove'])){
    $post_admin_id = $_POST['admin_id'];
    $missed_emp = $_POST['employee_missed'];
    $missed_pc = $_POST['missed_pc'];
    $post_request_id = $_POST['request_id'];
    mysql_query("UPDATE tbl_evaluation_requests SET request_status = 'APPROVED' WHERE id = '$post_request_id'")or die(mysql_error());;
    mysql_query("INSERT INTO tbl_notification_user (notification,sendBy,status,owner) VALUES ('HR APPROVED YOUR REQUEST TO OPEN THE EVALUATION FOR $missed_emp|$missed_pc','$department_name_session','unread','$admin_id')") or die(mysql_error());

    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'The request has been approved',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"viewEvaluations_missed.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"viewEvaluations_missed.php\";
                    }
                  }
                )
			</script>
		";
}
if(isset($_POST['btnDeny'])){
    $post_admin_id = $_POST['admin_id'];
    $missed_emp = $_POST['employee_missed'];
    $missed_pc = $_POST['missed_pc'];
    $post_request_id = $_POST['request_id'];
    $reason_hr = $_POST['deny_reason'];
    $link_to = '';
    $sent_at_dept = $_POST['sent_at_dept'];
    mysql_query("UPDATE tbl_evaluation_requests SET request_status = 'DECLINED' WHERE id = '$post_request_id'")or die(mysql_error());;
    mysql_query("INSERT INTO tbl_notification_user (notification,sendBy,status,owner) VALUES ('HR DENIED YOUR REQUEST TO OPEN THE EVALUATION FOR $missed_emp($missed_pc)','$post_admin_id','unread','$admin_id')") or die(mysql_error());
    mysql_query("INSERT INTO tbl_messages (subject,content,sender,receiver,sent_at,location,revision,performanceCycle,id_appeal)VALUES ('DENIED REQUEST TO REOPEN THE EVALUATION','$reason_hr','Human Resource Department','$post_admin_id','$sent_at_dept','sentbox','','$missed_pc','$missed_emp')");
    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'The request has been declined',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"viewEvaluations_missed.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"viewEvaluations_missed.php\";
                    }
                  }
                )
			</script>
		";
}
?>