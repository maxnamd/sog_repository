<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 8/23/2017
 * Time: 11:32 PM
 */

if(isset($_POST['btnSubmitEV']))
{




    $performanceCycle = $_SESSION['employee_missed_performance_cycle'];



    /* P1 POST CONTROLS*/
    $mainKraID = $_POST['mainKraID'];
    $kpiID = $_POST['kpiID'];
    $rating = $_POST['kraRating'];
    $weightedRating = $_POST['kraWeightedRating'];
    $weight = $_POST['kpiWeights'];
    $kraTitle = $_POST['kraTitle'];
   
    $kpiAchievement = $_POST['kpiAchievement'];
    $employee_fullName = $_SESSION['employee_firstName_evaluation'] . ' ' . $_SESSION['employee_middleName_evaluation'] . ' ' . $_SESSION['employee_lastName_evaluation'];

    $employee_lastName = $_SESSION['employee_lastName_evaluation'];
    $employee_firstName = $_SESSION['employee_firstName_evaluation'];
    $employee_middleName = $_SESSION['employee_middleName_evaluation'];
    $employee_username = $_SESSION['employee_username_evaluation'];
    $employee_years_company = $_SESSION['employee_year_in_company_evaluation'];
    $employee_years_position = $_SESSION['employee_year_in_position_evaluation'];
    $employee_job_level = $_SESSION['employee_job_level_evaluation'];
    $employee_position = $_SESSION['employee_position_evaluation'];
    /* END P1 POST CONTROLS*/

    /* P2 POST CONTROLS*/
    $p2a_competencyName = $_POST['p2a_competencyName'];
    $competencyDesc = $_POST['competencyDesc'];
    $requiredProfLevel = $_POST['requiredProficiencyLevel'];
    $p2_weightedRating = $_POST['p2_weightedRating'];
    $p2_weights = $_POST['p2_weights'];
    $p2_rating = $_POST['p2_rating'];
    $p2_rating_hid = $_POST['p2_rating_hid'];

    $c_id = $_POST['c_id'];
    $p2b_competencyName = $_POST['p2b_competencyName'];
    /* END P2 POST CONTROLS*/

    /* P3 POST CONTROLS*/
    $p3_id = $_POST['p3_id'];
    $p3_area = $_POST['p3_area'];
    $p3_area_inside = $_POST['p3_area_inside'];
    $p3_rubric = $_POST['p3_rubric'];
    $p3_score = $_POST['p3_score'];
    /* END P3 POST CONTROLS*/

    /* P4 POST CONTROLS*/
    $p4_area = $_POST['areaP4'];
    $p4_csFeedback = $_POST['feedbackP4'];
    $p4_score = $_POST['scoreP4'];
    $p4_rating = $_POST['factorRatingP4'];
    /* END P4 POST CONTROLS*/






    $isP1Complete = 0;
    $isP2AComplete = 0;
    $isP3Complete = 0;
    $isCommentComplete = 0;
    $isRecommendationsComplete = 0;
    $isApprovalComplete = 0;
    
    for($i = 0; $i<count($rating); $i++) {

        if($rating[$i] == ""){
            $isP1Complete += 1;
        }
    }

    for($i = 0; $i<count($p2a_competencyName); $i++) {
        if($p2_rating_hid[$i] == ""){
            $isP2AComplete += 1;
        }
        if($requiredProfLevel[$i] == ""){
            $isP2AComplete += 1;
        }
    }

    for($i = 0; $i<count($p3_score); $i++) {
        if($p3_score[$i] == ""){
            $isP3Complete += 1;
        }
    }

    $test_comments = $_POST['comments'];
    $test_recommendations = $_POST['recommendations'];
    $test_approval = $_POST['approval'];

    if($test_comments == ""){
        $isCommentComplete += 1;
    }

    if($test_recommendations == ""){
        $isRecommendationsComplete += 1;
    }

    if($test_approval == ""){
        $isApprovalComplete += 1;
    }



    if($isP1Complete > 0){
        $rating = $_POST['kraRating'];
        $weightedRating = $_POST['kraWeightedRating'];
        $weight = $_POST['kpiWeights'];

        echo "<script type='text/javascript'> callOnRescueCompute();</script>";
        echo "<script>
        swal(
          'Oops...',
          'Please complete form P1 first!',
          'error'
        )
        
        $('#tabs').tabs({active: 0});
        </script>";



    }
    elseif ($isP2AComplete > 0){
        echo "<script type='text/javascript'> callOnRescueCompute();</script>";
        echo "<script>
        swal(
          'Oops...',
          'Please complete form P2 first!',
          'error'
        )
        $('#tabs').tabs({active: 1});
        </script>";
    }
    elseif ($isP3Complete > 0){
        echo "<script type='text/javascript'> callOnRescueCompute();</script>";
        echo "<script>
        swal(
          'Oops...',
          'Please complete form P3 first!',
          'error'
        )
        $('#tabs').tabs({active: 3});
        </script>";
    }
    elseif ($isCommentComplete > 0){
        echo "<script type='text/javascript'> callOnRescueCompute();</script>";
        echo "<script>
        swal(
          'Oops...',
          'Please insert comments first!',
          'error'
        )
        $('#tabs').tabs({active: 4});
        </script>";
    }
    elseif ($isRecommendationsComplete > 0){
        echo "<script type='text/javascript'> callOnRescueCompute();</script>";
        echo "<script>
        swal(
          'Oops...',
          'Please insert recommendations first!',
          'error'
        )
        $('#tabs').tabs({active: 4});
        </script>";
    }
    elseif ($isApprovalComplete > 0){
        echo "<script type='text/javascript'> callOnRescueCompute();</script>";
        echo "<script>
        swal(
          'Oops...',
          'Please put your approval first!',
          'error'
        )
        $('#tabs').tabs({active: 4});
        </script>";
    }
    else{

        /*       START PART 1 EVALUATION FORM        */
        for($i = 0; $i<count($rating); $i++){
            mysql_query("INSERT INTO tbl_ans_eformP1 (kraID,kpiTitle,kpiWeights,p1_rating,p1_weightedRating,answeredBy,answeredFor,performanceCycle) VALUES ('".addslashes($mainKraID[$i])."','".addslashes($kraTitle[$i])."','".addslashes($weight[$i])."','".addslashes($rating[$i])."','".addslashes($weightedRating[$i])."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')") or die(mysql_error());
        }

        for($aa = 0; $aa<count($rating); $aa++){
            for($a = 0; $a<count($_POST['kpiDescription'][$aa]); $a++){
                $kpiDescription = $_POST['kpiDescription'][$aa][$a];
                $mainKraID = $_POST['kraInsideID'][$aa][$a];
                mysql_query("INSERT INTO tbl_ans_eformp1kpi (kpiID,kraID,kpiDesc,answeredBy,answeredFor,performanceCycle) VALUES ('".addslashes($kpiID[$a])."','".addslashes($mainKraID)."','".addslashes($kpiDescription)."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')") or die(mysql_error());
            }
        }

        for($aa = 0; $aa<count($rating); $aa++){
            for($a = 0; $a<count($_POST['kpiAchievement'][$aa]); $a++){
                $kpiAchievement = $_POST['kpiAchievement'][$aa][$a];
                $kraID = $_POST['kraID'][$aa][$a];
                mysql_query("INSERT INTO tbl_ans_eformP1_achievements (kraID,kpiAchievement,answeredBy,answeredFor,performanceCycle) VALUES('".addslashes($kraID)."','".addslashes($kpiAchievement)."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')");
            }
        }


        /*       END PART 1 EVALUATION FORM        */

        /*       START PART 2 EVALUATION FORM        */


        /* P2A START FIRST PART */
        for($w = 0; $w<count($p2_rating_hid); $w++){
            mysql_query("INSERT INTO tbl_ans_eformp2a (competency,competencyDescription,proficiencyLevel,p2_weights,p2_rating,p2_weightedRating,answeredBy,answeredFor,performanceCycle) VALUES ('".addslashes($p2a_competencyName[$w])."','".addslashes($competencyDesc[$w])."','".addslashes($requiredProfLevel[$w])."','".addslashes($p2_weights[$w])."','".addslashes($p2_rating_hid[$w])."','".addslashes($p2_weightedRating[$w])."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')") or die(mysql_error());
        }

        /* P2A END FIRST PART */

        /* P2B START SECOND PART */
        for($c = 0; $c<count($p2b_competencyName); $c++){
            mysql_query("INSERT INTO tbl_ans_eformp2b (competency,answeredBy,answeredFor,performanceCycle) VALUES ('".addslashes($p2b_competencyName[$c])."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')") or die(mysql_error());

        }

        for($cc = 0; $cc<count($p2b_competencyName); $cc++){
            for($c = 0; $c<count($_POST['criticalIncident'][$cc]); $c++){

                $criticalIncident = $_POST['criticalIncident'][$cc][$c];
                $p2b_inside_competencyName = $_POST['p2b_inside_competencyName'][$cc][$c];
                $inside_cid = $_POST['inside_cid'][$cc][$c];

                mysql_query("INSERT INTO tbl_ans_eformp2b_critical_incident (id,competencyName,criticalIncident,answeredBy,answeredFor,performanceCycle) VALUES ('".addslashes($inside_cid)."','".addslashes($p2b_inside_competencyName)."','".addslashes($criticalIncident)."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')") or die(mysql_error());
            }
        }

        /* P2B END SECOND PART */
        /*       END PART 2 EVALUATION FORM        */

        /*       START PART 3 EVALUATION FORM      */
        for($i = 0; $i<count($p3_score); $i++){
            mysql_query("INSERT INTO tbl_ans_eformP3 (areas,score,answeredBy,answeredFor,performanceCycle) VALUES ('$p3_area[$i]','$p3_score[$i]','$department_name_session','$employee_username','$performanceCycle')") or die(mysql_error());
        }

        for($c = 0; $c<count($p3_rubric); $c++){
            mysql_query("INSERT INTO tbl_ans_eformp3_rubric (id,areaName,rubricDesc,answeredBy,answeredFor,performanceCycle) VALUES ('".addslashes($p3_id[$c])."','".addslashes($p3_area_inside[$c])."','".addslashes($p3_rubric[$c])."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')") or die(mysql_error());
        }



        /*       END PART 3 EVALUATION FORM        */

        /*       START PART 4 EVALUATION FORM      */
        for($i = 0; $i<count($p4_area); $i++){
            mysql_query("INSERT INTO tbl_ans_eformp4 (p4_area,customerFeedback,p4_score,p4_rating,answeredBy,answeredFor,performanceCycle) VALUES ('".addslashes($p4_area[$i])."','".addslashes($p4_csFeedback[$i])."','".addslashes($p4_score[$i])."','".addslashes($p4_rating[$i])."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')") or die(mysql_error());
        }
        /*       END PART 4 EVALUATION FORM        */



        /* SUMMARY FORM POST CONTROLS*/
        $p1_performance_rating = $_POST['p1_performance_rating'];
        $p1_summary_twr = $_POST['p1_summary_twr'];
        $p1_summary_percentage_allo = $_POST['p1_summary_percentage_allo'];
        $p1_summary_subtotal = $_POST['p1_summary_subtotal'];

        $p2_performance_rating = $_POST['p2_performance_rating'];
        $p2_summary_twr = $_POST['p2_summary_twr'];
        $p2_summary_percentage_allo = $_POST['p2_summary_percentage_allo'];
        $p2_summary_subtotal = $_POST['p2_summary_subtotal'];

        $p3_performance_rating = $_POST['p3_performance_rating'];
        $p3_summary_twr = $_POST['p3_summary_twr'];
        $p3_summary_percentage_allo = $_POST['p3_summary_percentage_allo'];
        $p3_summary_subtotal = $_POST['p3_summary_subtotal'];

        $p4_summary_subtotal = $_POST['cs_factor_rating'];

        $performance_rating_total = $p1_summary_subtotal + $p2_summary_subtotal + $p3_summary_subtotal;
        $overall_rating_total = $p1_summary_subtotal + $p2_summary_subtotal + $p3_summary_subtotal + $p4_summary_subtotal;


        $summary_comments = $_POST['comments'];
        $summary_recommendations = $_POST['recommendations'];
        $summary_approval = $_POST['approval'];


        /* SUMMARY FORM POST CONTROLS*/


        /*START SUMMARY FORM*/
            /*P1 MANUAL PART INSERTION*/
            mysql_query("INSERT INTO tbl_ans_summary (id,performance_rating,total_weighted_rating,percentage_allocation,subtotal,answeredBy,answeredFor,performanceCycle) VALUES ('1','".addslashes($p1_performance_rating)."','".addslashes($p1_summary_twr)."','".addslashes($p1_summary_percentage_allo)."','".addslashes($p1_summary_subtotal)."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')") or die(mysql_error());
            /*END P1 MANUAL PART INSERTION*/

            /*P2 MANUAL PART INSERTION*/
            mysql_query("INSERT INTO tbl_ans_summary (id,performance_rating,total_weighted_rating,percentage_allocation,subtotal,answeredBy,answeredFor,performanceCycle) VALUES ('2','".addslashes($p2_performance_rating)."','".addslashes($p2_summary_twr)."','".addslashes($p2_summary_percentage_allo)."','".addslashes($p2_summary_subtotal)."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')") or die(mysql_error());
            /*END P2 MANUAL PART INSERTION*/

            /*P3 MANUAL PART INSERTION*/
            mysql_query("INSERT INTO tbl_ans_summary (id,performance_rating,total_weighted_rating,percentage_allocation,subtotal,answeredBy,answeredFor,performanceCycle) VALUES ('3','".addslashes($p3_performance_rating)."','".addslashes($p3_summary_twr)."','".addslashes($p3_summary_percentage_allo)."','".addslashes($p3_summary_subtotal)."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')") or die(mysql_error());
            /*END P3 MANUAL PART INSERTION*/
            /*P4 MANUAL PART INSERTION*/
            mysql_query("INSERT INTO tbl_ans_summary (id,performance_rating,total_weighted_rating,percentage_allocation,subtotal,answeredBy,answeredFor,performanceCycle) VALUES ('4','0','0','0','".addslashes($p4_summary_subtotal)."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')") or die(mysql_error());
            /*END P4 MANUAL PART INSERTION*/

            /*PART IV.  COMMENTS AND ACKNOWLEDGEMENT INSERTION*/
            mysql_query("INSERT INTO tbl_ans_summary_comments_acknowledgement (comments,recommendations,approval,answeredBy,answeredFor,performanceCycle) VALUES ('".addslashes($summary_comments)."','".addslashes($summary_recommendations)."','".addslashes($summary_approval)."','".addslashes($department_name_session)."','".addslashes($employee_username)."','".addslashes($performanceCycle)."')") or die(mysql_error());
            /*END PART IV.  COMMENTS AND ACKNOWLEDGEMENT INSERTION*/

            mysql_query("INSERT INTO tbl_rating_execute (emp_id, performance_rating, overall_rating, performanceCycle) VALUES ('".addslashes($employee_username)."','".addslashes($performance_rating_total)."','".addslashes($overall_rating_total)."','".addslashes($performanceCycle)."')") or die (mysql_error());

        /*END SUMMARY FORM*/


        /*       INSERT EVALUATION LOG        */

      mysql_query("UPDATE tbl_evaluation_results SET performance_rating = '".addslashes($performance_rating_total)."',overall_rating ='".addslashes($overall_rating_total)."',evaluationStatus ='COMPLETED',answeredBy='".addslashes($department_name_session)."' WHERE emp_id ='$employee_username' AND evaluationStatus = 'NOT YET STARTED' AND performanceCycle = '$performanceCycle'") or die(mysql_error());
        //mysql_query("INSERT INTO tbl_evaluation_results (emp_id,employee_firstName,employee_middleName,employee_lastName,employee_department,employee_position,employee_job_level,employee_years_company,employee_years_position,evaluationStatus,performance_rating,overall_rating,answeredBy,performanceCycle) VALUES ('$employee_username','$employee_firstName','$employee_middleName','$employee_lastName','$adepartment','$employee_position','$employee_job_level','$employee_years_company','$employee_years_position','PROCESSING','$performance_rating_total','$overall_rating_total','$admin_id','$performanceCycle')") or die(mysql_error());
        mysql_query("INSERT INTO tbl_notification (notification,sendBy,status) VALUES ('$department_name_session SENT AN EVALUATION','".addslashes($department_name_session)."','unread')") or die(mysql_error());





        $rewrew=mysql_query("SELECT * FROM tbl_admins WHERE department='Human Resource Department'") or die (mysql_error());
       while ($rew = mysql_fetch_array($rewrew)){
        $email_receiving = $rew['email'];
       }


   require '../assets/phpmailer/PHPMailerAutoload.php';

      $mail = new PHPMailer;
     $body .= $department_name_session.' has sent an evaluation!';

      //$mail->SMTPDebug = 3;                               // Enable verbose debug output
      //$mail->SMTPDebug = 1;
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = "smtp.gmail.com";    // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = "es.macbeth.2017@gmail.com";
      $mail->Password = "nopasswordno";                          // SMTP password
      $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 465;                                    // TCP port to connect to

      $mail->setFrom('from@example.com', 'UPang - Human Resource Department');
      $mail->addAddress($email_receiving, 'Joe User');          // Add a recipient
      $mail->addAddress('ellen@example.com');               // Name is optional
      $mail->addReplyTo('info@example.com', 'Information');
      $mail->addCC('cc@example.com');
      $mail->addBCC('bcc@example.com');

      $mail->isHTML(true);   
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ));
                                           // Set email format to HTML

      $mail->Subject = 'PHINMA UPang SOG EVALUATION SYSTEM -- '. $department_name_session.' sent an evaluation';
      $mail->Body    = $body;

       if($mail->Send())
        {

            echo"
            <script type='text/javascript'>
                
                swal({
                  title: 'SUCCESS!',
                  text: 'Your completed the evaluation!',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"myEvaluations_A.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"myEvaluations_A.php\";
                    }
                  }
                )
            </script>
        ";

        }
        else
        {
            echo "<script type='text/javascript'>
        
        swal({
                  title: 'ERROR!',
                  text: 'Oops! Something went wrong!',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"myEvaluations_A.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"myEvaluations_A.php\";
                    }
                  }
                )
      </script>";
        }
      
    }

}

        // echo "<script>
        //     bootbox.alert('You sent your Evaluation form', 
        //         function() {
        //         setTimeout('window.location.replace(\'viewQuestions_A.php\')',600);
        //         });
        //     </script>";

        /*       END INSERT EVALUATION LOG    */

    /*function FormP1Validate($weight_val, $rating_val){
        if ($weight_val == "" || $rating_val == "") {
            return false;
        } else {
            return true;
        }
    }
    function FormP2AValidate($rating_val){
        if ($rating_val  == "" ) {
            return false;
        } else {
            return true;
        }
    }
    function FormP3Validate($score_val){
        if ($score_val  == "") {
            return false;
        } else {
            return true;
        }
    }*/

?>