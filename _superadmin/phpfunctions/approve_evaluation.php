<?php
header('Content-type: application/json; charset=UTF-8');
$response = array();

if ($_POST['approve']) {

     $DBhost = "localhost";
     $DBuser = "root";
     $DBpass = "";
     $DBname = "es_db";

     try {
         $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname",$DBuser,$DBpass);
         $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     } catch(PDOException $ex){
         die($ex->getMessage());
     }
    $ev_id = $_POST['approve'];
    $date_id = $_POST['date'];


    $query = "UPDATE tbl_evaluation_results SET evaluationStatus = 'COMPLETED' WHERE emp_id = :ev_id AND performanceCycle = :date_id";
    $stmt = $DBcon->prepare( $query );
    $stmt->execute(array(':ev_id'=>$ev_id,':date_id'=>$date_id));


    $sql = "INSERT INTO tbl_notification_user (notification,sendBy,status,owner)VALUES ('".$_POST["fullName_emp"]."','".$_POST["admin_ev"]."','".$_POST["unread"]."','".$_POST["new_admin_id"]."')";
    $DBcon->query($sql);

    if ($stmt) {
        $response['status']  = 'success';
        $response['message'] = 'Evaluation Approved Successfully ...';
    } else {
        $response['status']  = 'error';
        $response['message'] = 'Unable to approve the evaluation ...';
    }
    echo json_encode($response);

    
}
?>