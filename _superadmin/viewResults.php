<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 9/4/2017
 * Time: 8:51 PM
 */
?>

<?php include 'includes/header.php'; ?>



<?php
/*departmentEV
departmentOwner*/
if (isset($_POST['btnPassToEVresults'])){
    $_SESSION['departmentEV'] = $_POST['departmentEV'];
    $_SESSION['departmentOwner'] = $_POST['departmentOwner'];
    echo "
    <script type='text/javascript'>
    document.location.href='viewEvaluation_results.php';
    </script>";
}
?>

<?php
/*departmentEV
departmentOwner*/
if (isset($_POST['btn_link_to_history'])){
   
    echo "
    <script type='text/javascript'>
    document.location.href='link_to_history.php';
    </script>";
}
?>
<!-- Setting the treeview active -->
<script type="text/javascript">
    $('#treeview5').removeClass("active menu-open");
    $('#treeview5').addClass('active menu-open');
</script>
<!-- End Setting the treeview active -->
    <script>
        $(document).ready(function() {
            $('#tbl_EV').DataTable();
        } );
    </script>
    <link rel="stylesheet" href="../assets/plugins/js-datatables/css/bootstrap-table.css">
    <link rel="stylesheet" href="../assets/plugins/js-datatables/css/bootstrap-table-filter-control.css">
    <link rel="stylesheet" href="../assets/plugins/js-datatables/css/datatable.css">
    <script type="text/javascript" src="../assets/plugins/js-datatables/js/bootstrap-table.js"></script>
    <script type="text/javascript" src="../assets/plugins/js-datatables/js/bootstrap-table-filter-control.js"></script>
    <script type="text/javascript" src="../assets/plugins/js-datatables/js/Chart.bundle.js"></script>
    <script type="text/javascript" src="../assets/plugins/js-datatables/js/js-yaml.js"></script>
    <script type="text/javascript" src="../assets/plugins/js-datatables/js/datatable.js"></script>
    



    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Evaluations Results
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- TABLE: STATUS KRA -->
        <div class="box box-info" style="border-color: green">
            <div class="box-header with-border">
                <h3 class="box-title">Department Evaluation Results</h3>

                <!-- <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin" id="tbl_EV">
                        <thead>
                        <tr>
                            <th class="col-sm-8">Department Name</th>
                            <th class="col-sm-2">Number of Evaluations</th>
                            <th class="col-sm-2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
                        $dateEVRow = mysql_fetch_array($queryDateEV);
                        $startDateEV = $dateEVRow['startDate'];
                        $endDateEV = $dateEVRow['endDate'];
                        $top_performanceCycle = $startDateEV . '-' . $endDateEV;
                        ?>

                        <?php
                        $display_status=mysql_query("SELECT * FROM tbl_departments") or die(mysql_error());

                        while($row=mysql_fetch_array($display_status)){

                            $departmentName = $row['department_name'];
                            $departmentOwner = $row['departmentOwner'];
                            echo "<tr>";
                            echo "<td>$departmentName</td>";
                            ?>

                            <?php
                            $display_count_pass_completed=mysql_query("SELECT COUNT(employee_department) FROM tbl_evaluation_results WHERE employee_department = '$departmentName' AND evaluationStatus = 'COMPLETED'") or die(mysql_error());
                            while($c_row=mysql_fetch_array($display_count_pass_completed)){
                                $passer_count_completed = $c_row['COUNT(employee_department)'];
                            }
                            ?>
                            <td> <span class="col-sm-8 label label-success"><?php echo $passer_count_completed;?></span></td>
                            <td>
                                <form method="POST">
                                    <input type="hidden" name="departmentEV" value="<?php echo $departmentName?>"/>
                                    <input type="hidden" name="departmentOwner" value="<?php echo $departmentOwner?>"/>

                                     <input type="submit" class="btn btn-info btn-sm" name="btnPassToEVresults" value="PASS"/>
                                </form>

                            </td>

                            </tr>

                            <?php
                        }
                        ?>



                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
            </div>
            <!-- /.box-footer -->
        </div>
        
        <?php include 'top_10_this_performance_cycle.php';?>

        <div class="row">
            <div class="pull-right" style="margin-right: 1em;margin-left: 1em;">
            <form method="post">
            <input type="submit" name="btn_link_to_history" class="btn btn-success btn-block" value="Link To History">
            </form>
            </div>


            <div class="pull-right">
                <a href="see_all_list_this_year.php" class="btn btn-success">See All List</a>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>