<?php include 'includes/header.php'; ?>
<?php require_once 'phpfunctions/p3Functions.php';?>


<script type="text/javascript">
  function del_confirmation()
  {
    if(confirm("Are you sure?")==1)
    {
      document.getElementById('btnDelete_area').submit();
    }
  }
</script>
<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview_qm").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <?php
    $p3Count = mysql_query("SELECT * FROM tbl_settingsp3");
    $p3Row = mysql_fetch_array($p3Count);
    $rubricCount = $p3Row['rubricCount'];

    if($rubricCount==0){
        echo "
                <script>
                    bootbox.alert('SETUP THE NUMBER OF CRITICAL INCIDENT FIRST AT THE SYSTEM SETTINGS',
                    function() {
                       setTimeout('window.location.replace(\'manageSystemSettings.php\')',600);
                    });
                </script>
                ";
    }
    else{
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Questionnaire Management
        <!-- <small>Version 2.0</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="panel panel-default">
              <div class="panel-heading" style="background: black"><h4><b style="color:white;">Performance Evaluation Form for SOG - P3</b></h4></div>



                <div class="container-fluid" style="margin-top: 1em;">
                  <div class="col-sm-2">
                      <a href="#" class="btn btn-success" style="background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7)" data-toggle="modal" data-target="#addArea"> &nbsp;&nbsp;<i class="fa fa-plus"></i> NEW QUESTION &nbsp;</a>
                  </div>

                  <div class="col-sm-2">
                                        <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_p3_areas" class="btn btn-success" value="Import Areas" style=" width:100px;" />
                                        </form>
                                    </div>

                        <div class="col-sm-2"> </div>
                   <div class="col-sm-2">
                                        <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_p3" class="btn btn-success" value="Import Rubrics" style=" width:100px;" />
                                        </form>
                                    </div>
                </div>
              <hr>

              <div class="panel-body container-fluid">
                  <table class="table table-bordered">
                        <thead class="">
                          <tr>
                            <th width="2">AREAS</th>
                            <th width="20">Rubric</th>
                            <th width="10">Action</th>
                          </tr>
                        </thead>


                        <tbody>
                          <?php
                              $query = "SELECT * FROM tbl_eformp3";
                              // execute query 
                              $result = mysql_query($query) or die ("Error in query: $query. ".mysql_error()); 
                              // see if any rows were returned 
                              if (mysql_num_rows($result) == 0) 
                              { 
                                echo"<td colspan='4'><center><h4><b>There are no questions yet.</b></h4></center></td>";
                              }
                              else
                              {
                                $display_ep3=mysql_query("SELECT * FROM tbl_eformp3") or die(mysql_error());
                                    $myCount=1;
                                    while($row=mysql_fetch_array($display_ep3)){ 
                                        $areaID = $row['id'];
                                        $mainArea = $row['areas'];
                              ?>
                              <tr> 
                                    <td  width="2"><?php echo $row['areas'];?> </td>
                                    <td width="20">
<div class="">
    <div class="container-fluid">
      <div class="col-sm-2" style="float:right;margin-right: 3em">
          <a href="#" class="btn btn-success" style="background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7);margin-bottom: 1em;" data-toggle="modal" data-target="#addRubric<?php echo $areaID;?>"> &nbsp;&nbsp;<i class="fa fa-plus"></i> NEW RUBRIC &nbsp;</a>
      </div>


      <div class="col-sm-12">
          <table class="table table-responsive">
            <tr>
                <?php 
                $display_rubric=mysql_query("SELECT * FROM tbl_p3rubric WHERE areaName ='$mainArea' ORDER BY id DESC") or die(mysql_error());  
             
                while($row=mysql_fetch_array($display_rubric)){ 
                    $rubricID = $row['id'];
                    $areaName = $row['areaName'];
                    $rubricDesc = $row['rubricDesc'];

                    $str=preg_replace('/\s+/', '', $areaName);
                ?>
                <div class="row" style="margin-bottom: 1em">

                  <div class="col-sm-10"><?php echo $rubricID . ': '.$rubricDesc;?> </div>
                  <div class="col-sm-2"> 
                    <div class="col-sm-2"> <a href="#editRubric<?php echo $rubricID.$str?>" class="btn btn-default btn-sm" title="Edit Rubric" data-toggle="modal"> <i class="fa fa-edit"></i></a> </div>
                    
                  </div>
                </div>

 <!-- EDIT RUBRIC MODAL-->
      <div class="modal fade" id="editRubric<?php echo $rubricID.$str?>" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Rubric</h4>
            </div>
              
            <div class="modal-body">


                <form method="post" class="form-horizontal">
                       <div class="form-group" id="">
                        <label class="col-sm-3 control-label">Rubric: </label>
                        <div class="col-sm-8">
                          <input type="text" name="rubricDesc" id="" class="form-control" value="<?php echo $row['rubricDesc']; ?>">
                        </div>
                      </div>

                      
                          <input type="hidden" name="xArea" id="" class="form-control" value="<?php echo $row['areaName']; ?>">
                          <input type="hidden" name="rubric_id" id="" class="form-control" value="<?php echo $row['id']; ?>">
                      
                     
          </div>

                      <div class="modal-footer">
                      
                          <input type="submit" class="btn btn-success" name="edit_rubric" value="UPDATE RUBRIC"  />
                          <input type="reset" class="btn btn-default" value="Clear"/>

                      </div>
                    
                </form>
            </div>
          </div>
        </div>
      </div>
<!-- EDIT RUBRIC MODAL--> 
                  

                <?php
                  }
                ?>
            </tr>
          </table>


      </div>
    </div>



</div> 


<!-- ADD NEW RUBRIC MODAL-->
<div class="modal fade" id="addRubric<?php echo $areaID;?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add New Rubric to <?php echo $mainArea;?></h4>
      </div>
        
      <div class="modal-body">

          <form method="post" class="form-horizontal">
              <div class="form-group" id="">
                <label class="col-sm-4 control-label">Rubric: </label>
                <div class="col-sm-6">
                  <input type="text" name="rubricDesc" id="" class="form-control" placeholder="Rubric Description: "/>
                </div>
              </div>

              
              

              <div class="modal-footer">
                  <input type="hidden" name="xArea" value="<?php echo $mainArea?>">
                  <input type="submit" class="btn btn-success" name="add_rubric" value="ADD RUBRIC"  />
                  <input type="reset" class="btn btn-default" value="Clear"/>

              </div>
          </form>
      </div>
    </div>
  </div>
</div>
<!-- ADD NEW RUBRIC MODAL-->




                                    </td>
                                    <td width="8"> 
                                          
                                         <div class="col-sm-3"> </div>
                                      <div class="col-sm-2"> <a href="#editArea<?php echo $areaID?>" class="btn btn-default btn-sm" title="Edit Area" data-toggle="modal"> <i class="fa fa-edit"></i></a> </div>

                                      <div class="col-sm-1"></div>
                                          <!--delete-->
                                      <div class="col-sm-2">
                                        <form method="post"> 
                                            <input type="hidden" name="hiddenID_area" value="<?php echo $areaID?>"/>
                                            <button type="submit" class="btn btn-default btn-sm" title="Delete Area" name="btnDelete_area" id="btnDelete_area" onclick="del_confirmation(); return false;"> <i class="fa fa-trash"></i>
                                            </button> 
                                        </form>
                                      </div>
                                    </td>
                                    <!-- EDIT RUBRIC MODAL-->
      <div class="modal fade" id="editArea<?php echo $areaID;?>" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Area</h4>
            </div>
              
            <div class="modal-body">


                <form method="post" class="form-horizontal">
                       <div class="form-group" id="">
                        <label class="col-sm-3 control-label">Area: </label>
                        <div class="col-sm-8">
                          <input type="hidden" name="originArea" value="<?php echo $mainArea; ?>" />
                          <input type="text" name="mainArea" id="" class="form-control" value="<?php echo $mainArea; ?>">
                        </div>
                      </div>

                      
                          <input type="hidden" name="areaID" id="" class="form-control" value="<?php echo $areaID; ?>">
                      
                     
          </div>

                      <div class="modal-footer">
                      
                          <input type="submit" class="btn btn-success" name="edit_area" value="UPDATE AREA"  />
                          <input type="reset" class="btn btn-default" value="Clear"/>

                      </div>
                    
                </form>
            </div>
          </div>
        </div>
      </div>
<!-- EDIT RUBRIC MODAL--> 

                              </tr>
                              <?php $myCount++;}}?>
                        </tbody>
                  </table>
              </div>
          </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- ADD NEW AREA MODAL-->
<div class="modal fade" id="addArea" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add New Area</h4>
      </div>
        
      <div class="modal-body">

          <form method="post" class="form-horizontal">
              <div class="form-group" id="">
                <label class="col-sm-4 control-label">Area: </label>
                <div class="col-sm-6">
                  <input type="text" name="areaName" id="" class="form-control" placeholder="Area Name: "/>
                </div>
              </div>

              
              

              <div class="modal-footer">
              
                  <input type="submit" class="btn btn-success" name="add_area" value="ADD AREA"  />
                  <input type="reset" class="btn btn-default" value="Clear"/>

              </div>
          </form>
      </div>
    </div>
  </div>
</div>
<!-- ADD NEW AREA MODAL-->


<?php include 'includes/footer.php'; ?>
<?php }?>
