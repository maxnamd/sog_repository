
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <!--<b>Version</b> 2.4.0-->
    </div>
    <strong>Copyright &copy; SOG Evaluation 2017 <a href="https://adminlte.io">SP5 Web Devs</a>.</strong> All rights
    reserved.
</footer>
</div>
<!-- ./wrapper -->




<!-- AdminLTE App -->
<script src="../assets/dist/js/adminlte.min.js"></script>


</body>
</html>


<!--  
DEVS:
  Ayson, Justin Louise
  Clauna, Levi
  Capstone Project 2017


  Timeframe: 
  7/26/2017 
 -->