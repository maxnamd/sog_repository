<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 9/4/2017
 * Time: 8:51 PM
 */
?>

<?php include 'includes/header.php'; ?>



<?php
    $performanceCycle = $_SESSION['performanceCycle'];
?>
<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview5").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->
    <script>
        $(document).ready(function() {
            $('#tbl_EV').DataTable();
        } );
    </script>

    <script>
        $(document).ready(function() {
            $('#tbl_EV1').DataTable();
        } );
    </script>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Evaluations Results
        </h1>
    </section>



    <!-- Main content -->
    <section class="content">
        <!-- TABLE: STATUS KRA -->
        <!--PRINT-->
            <div class="row">
                <div class="pull-right" style="margin-right: 1em">
                    <iframe src="print_top_10_history.php" style="display:none;" name="frame"></iframe>
                    <input type="button" class="btn btn-success" onclick="frames['frame'].print()" value="PRINT">
                </div>
            </div>
        <!--PRINT-->

        <div class="box box-info" style="border-color: green">
            <div class="box-header with-border">
                <h3 class="box-title">Top 10 Performance Cycle For Year (<?php echo $performanceCycle;?>)</h3>
                <div class="pull-right">
                        <a href="see_all_list_history.php">See All List</a>
                    </div>
                <!-- <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin" id="tbl_EV">
                        <thead>
                        <tr>
                            <th>TOP</th>
                            <th>Full Name</th>
                            <th>Department</th>
                            <th>Performance Rating</th>
                            <th>Overall Rating</th>
                        </tr>
                        </thead>


                        <tbody>
            

                        <?php
                        $display_status=mysql_query("SELECT * FROM tbl_rating_execute WHERE performanceCycle = '$performanceCycle' ORDER BY overall_rating DESC LIMIT 10") or die(mysql_error());

                        $counter = 0;
                        while($row=mysql_fetch_array($display_status)){
                        $counter++;
                            $emp_id = $row['emp_id'];
                            $overall_rating = $row['overall_rating'];
                            $performance_rating = $row['performance_rating'];
                            echo "<tr>";
                            ?>
                            <?php
                        $display_status2=mysql_query("SELECT * FROM tbl_sog_employee WHERE emp_id= '$emp_id'") or die(mysql_error());
                        while($raw=mysql_fetch_array($display_status2)){
                            $fullname = $raw['firstname'].' '.$raw['middlename'].' '.$raw['lastname'];
                            $department = $raw['department'];
                         }  ?>

                           <tr> 
                                <td> <?php echo $counter;?></td>
                                <td> <?php echo $fullname;?> </td>
                                <td> <?php echo $department;?></td>
                                <td> <?php echo $performance_rating;?></td>
                                <td> <?php echo $overall_rating;?></td>

                           </tr>
                           <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
        
        </div>

    </section>
    <!-- /.content -->
</div>