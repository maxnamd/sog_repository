<?php include 'includes/header.php'; ?>
<?php require_once 'phpfunctions/p2afunctions.php';?>

<?php 
 
?>
<style type="text/css">
.control-label{
  text-align: left !important;
}
</style>

<script type="text/javascript">
  function del_confirmation()
  {
    if(confirm("Are you sure?")==1)
    {
      document.getElementById('btnDelete').submit();
    }
  }
</script>

<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview_qm").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <?php
    $p2Count = mysql_query("SELECT * FROM tbl_settingsp2a");
    $p2Row = mysql_fetch_array($p2Count);
    $criticalIncidentCount = $p2Row['criticalIncidentCount'];

    if($criticalIncidentCount==0){
        echo "
            <script>
                bootbox.alert('SETUP THE NUMBER OF CRITICAL INCIDENT FIRST AT THE SYSTEM SETTINGS',
                function() {
                   setTimeout('window.location.replace(\'manageSystemSettings.php\')',600);
                });
            </script>
            ";
    }
    else{
    ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Questionnaire Management
      <!-- <small>Version 2.0</small> -->
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading" style="background: black"><h4><b style="color:white;">Performance Evaluation Form for SOG - P2A</b></h4></div>



              <div class="container-fluid" style="margin-top: 1em;">
                  <div class="col-sm-2">
                      <a href="#" class="btn btn-success" style="background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7)" data-toggle="modal" data-target="#addQuestion"> &nbsp;&nbsp;<i class="fa fa-plus"></i> NEW QUESTION &nbsp;</a>
                  </div>

                     <div class="col-sm-2">
                                        <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_p2a" class="btn btn-success" value="Add" style=" width:100px;" />
                                        </form>
                                    </div>

                  <div class="pull-right" >
                    <label class="control-label">Weights Left: </label>
                        <?php 
                          $viewSum = mysql_query("SELECT SUM(weights) FROM tbl_eformp2a");
                          $rowSum = mysql_fetch_array($viewSum);
                          $weightLeft = $rowSum['SUM(weights)'];
                          $weightLeft = 100 - $weightLeft ;
                        ?>
                        <input type="text" name="" class="form-control" value="<?php echo $weightLeft?>" readonly/>
                    </div>
              </div>


            <hr>

            <div class="panel-body">
                <table class="table table-bordered">
                      <thead class="">
                        <tr>
                          <th width="300">Competency</th>
                          <th width="2">Weights</th>
                          <th width="10">Action</th>
                        </tr>
                      </thead>


                      <tbody>
                        <?php
                            $query = "SELECT * FROM tbl_eformp2a";
                            // execute query 
                            $result = mysql_query($query) or die ("Error in query: $query. ".mysql_error()); 
                            // see if any rows were returned 
                            if (mysql_num_rows($result) == 0) 
                            { 
                              echo"<td colspan='7'><center><h4><b>There are no questions yet.</b></h4></center></td>";
                            }
                            else
                            {
                              $display_ep2a=mysql_query("SELECT * FROM tbl_eformp2a") or die(mysql_error());
                                  
                                  $incID = 1;
                                  while($row=mysql_fetch_array($display_ep2a)){ 
                                    
                                    $cId=$row['id'];
                                    $competency=$row['competency'];
                                    $competencyDesc=$row['competencyDesc'];
                                    $requiredProfLevel=$row['requiredProfLevel'];
                                    $weights=$row['weights'];
                                    $weightedRating=$row['weightedRating'];

                            ?>
                            <tr> 
                                  <td width="3"><b><?php echo $row['competency'];?></b>:<?php echo $row['competencyDesc'];?></td>
                                  <td width="2"><?php echo $row['weights'];?> </td>
                                  <td width="10">
                                      <div class="col-sm-2"> <a href="#viewQuestion<?php echo$cId;?>" class="btn btn-default" title="View" data-toggle="modal"> <i class="fa fa-search"></i></a> 
<!-- VIEW QUESTION MODAL-->
<div class="modal fade" id="viewQuestion<?php echo$cId;?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">View Question</h4>
      </div>
        
      <div class="modal-body">

          <form method="post" class="form-horizontal">
              <div class="form-group" id="">
                <label class="col-sm-4 control-label">Competency: </label>
                <div class="col-sm-8">
                  <input type="text" name="competency_edit" id="" class="form-control" placeholder="Competency title: " value="<?php echo $competency;?>" readonly/>
                </div>
              </div>

              <div class="form-group" id="">
                <label class="col-sm-4 control-label">Competency Description:</label>
                <div class="col-sm-8">

                  <textarea name="competencyDesc_edit" id="viewEditor<?php echo$cId;?>" rows="10" cols="80" readonly style="resize: none;">
                    <?php echo $competencyDesc;?>
                  </textarea>
                  <script type="text/javascript">
                      // Replace the <textarea id="editor1"> with a CKEditor
                      // instance, using default configuration.
                      CKEDITOR.replace( 'viewEditor<?php echo$cId;?>' );

                  </script>
                </div>
              </div>


              <div class="form-group" id="">
                <label class="col-sm-4 control-label">Weights: </label>
                <div class="col-sm-8">
                  <input type="text" name="weights_edit" id="" class="form-control" placeholder="Weights: " value="<?php echo $weights;?>" readonly/>
                </div>
              </div>
              

          </form>
      </div>
    </div>
  </div>
</div>
<!-- END UPDATE QUESTION MODAL-->






                                      </div>

                                      <div class="col-sm-1"></div>
                                      
                                      <div class="col-sm-2"> 
                                        <a href="#editQuestion<?php echo$cId;?>" class="btn btn-default" title="Edit" data-toggle="modal"> <i class="fa fa-edit"></i></a> 
<!-- UPDATE QUESTION MODAL-->
<div class="modal fade" id="editQuestion<?php echo$cId;?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add New Question</h4>
      </div>
        
      <div class="modal-body">

          <form method="post" class="form-horizontal">
              <div class="form-group" id="">
                <label class="col-sm-4 control-label">Competency: </label>
                <div class="col-sm-8">
                  <input type="text" name="competency_edit" id="" class="form-control" placeholder="Competency title: " value="<?php echo $competency;?>" />
                </div>
              </div>

              <div class="form-group" id="">
                <label class="col-sm-4 control-label">Competency Description:</label>
                <div class="col-sm-8">

                  <textarea name="competencyDesc_edit" id="myEditor<?php echo$cId;?>" rows="10" cols="80">
                    <?php echo $competencyDesc;?>
                  </textarea>
                  <script type="text/javascript">
                      // Replace the <textarea id="editor1"> with a CKEditor
                      // instance, using default configuration.
                      CKEDITOR.replace( 'myEditor<?php echo$cId;?>' );
                  </script>
                </div>
              </div>

              <div class="form-group" id="">
                <label class="col-sm-4 control-label">Weights: </label>
                <div class="col-sm-8">
                  <input type="text" name="weights_edit" id="" class="form-control" placeholder="Weights: " value="<?php echo $weights;?>"/>
                </div>
              </div>
              

              <div class="modal-footer">
                  <input type="hidden" name="hiddenID" value="<?php echo $cId?>"/>
                  <input type="submit" class="btn btn-success" name="update_competency" value="UPDATE COMPETENCY"  />
                  <input type="reset" class="btn btn-default" value="Clear"/>

              </div>
          </form>
      </div>
    </div>
  </div>
</div>
<!-- END UPDATE QUESTION MODAL-->
                                      </div>

                                      <div class="col-sm-1"></div>

                                      <div class="col-sm-2">
                                        <form method="post"> 
                                            <input type="hidden" name="hiddenID" value="<?php echo $cId?>"/>
                                            <button type="submit" class="btn btn-default" title="Delete Competency" name="btnDelete" id="btnDelete" onclick="del_confirmation(this)"> <i class="fa fa-trash"></i> 
                                            </button> 
                                        </form>




                                      </div>
                                  </td>
                            </tr>
                            <?php $incID++; }}?>
                            <tr> 
                            	<td width="2"> <center> TOTAL </center> </td>
                            	
                            	 <?php
                                    $total_weights = 0;
                                    $query_weights=mysql_query("SELECT * FROM tbl_eformp2a") or die (mysql_error());
                                    while($row_total_weights=mysql_fetch_array($query_weights)){
                                        $weights = $row_total_weights['weights'];
                                            $total_weights += intval($weights);
                                }?>
                            	<td> <?php echo $total_weights;?></td>
                            	<td> </td>
                            </tr>
                      </tbody>
                </table>
            </div>
        </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->



<!-- ADD NEW QUESTION MODAL-->
<div class="modal fade" id="addQuestion" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add New Question</h4>

          
      </div>
        
      <div class="modal-body">

          <form method="post" class="form-horizontal">
              <div class="form-group" id="">
                <label class="col-sm-4 control-label">Competency: </label>
                <div class="col-sm-8">
                  <input type="text" name="competency" id="" class="form-control" placeholder="Competency title: "/>
                </div>
              </div>

              <div class="form-group" id="">
                <label class="col-sm-4 control-label">Competency Description:</label>
                <div class="col-sm-8">

                  <textarea name="competencyDesc" id="myEditor" rows="10" cols="80">
                                
                  </textarea>
                  <script type="text/javascript">
                      // Replace the <textarea id="editor1"> with a CKEditor
                      // instance, using default configuration.
                      CKEDITOR.replace( 'myEditor' );
                  </script>
                </div>
              </div>


              <div class="form-group" id="">
                <label class="col-sm-4 control-label">Weights: </label>
                <div class="col-sm-8">
                  <input type="text" name="weights" id="" class="form-control" placeholder="Weights: "/>
                </div>
              </div>
              

              <div class="modal-footer">
              
                  <input type="submit" class="btn btn-success" name="add_competency" value="ADD COMPETENCY"  />
                  <input type="reset" class="btn btn-default" value="Clear"/>

              </div>
          </form>
      </div>
    </div>
  </div>
</div>
<!-- ADD NEW QUESTION MODAL-->

<?php include 'includes/footer.php'; ?>
<?php }?>





