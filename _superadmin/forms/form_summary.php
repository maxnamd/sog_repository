<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 9/14/2017
 * Time: 5:10 PM
 */
?>
<div class="panel panel-success" style="border-color: #3A5F0B;">
    <div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>PERFORMANCE EVALUATION FORM FOR SOG - SUMMARY</b></h4></div>

    <div class="panel-body">
        <?php include 'include/head.php';?>

        <table class="table table-bordered">
            <thead>
                <th class="col-sm-2">PERFORMANCE RATING</th>
                <th class="col-sm-1">TOTAL WEIGHTED RATING</th>
                <th class="col-sm-1">PERCENTAGE ALLOCATION</th>
                <th class="col-sm-1">SUBTOTAL</th>
            </thead>


            <tbody>
                <tr>
                    <td>
                        <h5><b>PART 1: KEY PERFORMANCE INDICATORS</b></h5>
                        <input type="hidden" value="PART 1: KEY PERFORMANCE INDICATORS" name="p1_performance_rating"/>
                    </td>
                    <td><input type="text" name="p1_summary_twr" id="p1_summary_twr" class="form-control" value="0" readonly/></td>
                    <td><input type="text" name="p1_summary_percentage_allo" id="p1_summary_percentage_allo" class="form-control" value="40" readonly/></td>
                    <td><input type="text" name="p1_summary_subtotal" id="p1_summary_subtotal" class="form-control" value="0" readonly/></td>
                </tr>

                <tr>
                    <td>
                        <h5><b>PART 2: COMPETENCIES</b></h5>
                        <input type="hidden" value="PART 2: COMPETENCIES" name="p2_performance_rating"/>
                    </td>
                    <td><input type="text" name="p2_summary_twr" id="p2_summary_twr" class="form-control" value="0" readonly/></td>
                    <td><input type="text" name="p2_summary_percentage_allo" id="p2_summary_percentage_allo" class="form-control" value="30" readonly/></td>
                    <td><input type="text" name="p2_summary_subtotal" id="p2_summary_subtotal" class="form-control" value="0" readonly/></td>
                </tr>

                <tr>
                    <td>
                        <h5><b>PART 3: PROFESSIONALISM</b></h5>
                        <input type="hidden" value="PART 3: PROFESSIONALISM" name="p3_performance_rating"/>
                    </td>
                    <td><input type="text" name="p3_summary_twr" id="p3_summary_twr" class="form-control" value="0" readonly/></td>
                    <td><input type="text" name="p3_summary_percentage_allo" id="p3_summary_percentage_allo" class="form-control" value="30" readonly/></td>
                    <td><input type="text" name="p3_summary_subtotal" id="p3_summary_subtotal" class="form-control" value="0" readonly/></td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: #BDBDBD;color: white"><label class="pull-right">PERFORMANCE RATING</label></td>
                    <td class="totalCol">
                        <input type="text" name="performance_rating" class="form-control" id="performance_rating" value="0" readonly/>

                    </td>
                </tr>

                <tr>
                    <td><h5><b>PART 4: CUSTOMER SERVICE</b></h5></td>
                    <td colspan="2"><label class="pull-right">FACTOR RATING</label></td>
                    <td>
                        <input type="text" name="cs_factor_rating" id="cs_factor_rating" class="form-control" value="0" readonly/>
                    </td>
                </tr>


            </tbody>

            <tr class="totalColumn">

                <td colspan="3" style="background-color: #4b646f;color: white"><label class="pull-right">OVERALL RATING</label></td>
                <td class="totalCol">
                    <input type="text" name="total_overall_rating" class="form-control" id="total_overall_rating" value="0" readonly/>
                </td>

            </tr>
        </table>
    </div>

</div>


<div class="panel panel-success" style="border-color: #3A5F0B;">
    <div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>SUMMARY COMMENTS</b></h4></div>

    <div class="panel-body">
        <table class="table table-condensed">
            <thead>
                <th>Comments</th>
                <th>Recommendations</th>
                <th>Approval</th>
            </thead>

            <tbody>
                <tr>
                    <td>
                        <textarea class="form-control" id="comments" name="comments">
                            <?php if(isset($_POST['p2_rating_hid'][$p2a_arrayCount])){ echo $_POST['p2_rating_hid'][$p2a_arrayCount];}?>
                        </textarea>
                        <script type="text/javascript">
                            $('#comments').wysihtml5();
                        </script>
                    </td>
                    <td>
                        <textarea class="form-control" id="recommendations" name="recommendations">
                            <?php if(isset($_POST['p2_rating_hid'][$p2a_arrayCount])){ echo $_POST['p2_rating_hid'][$p2a_arrayCount];}?>
                        </textarea>
                        <script type="text/javascript">
                            $('#recommendations').wysihtml5();
                        </script>
                    </td>
                    <td>
                        <textarea class="form-control" id="approval" name="approval">This is to Certify that <?php echo $_SESSION['employee_firstName_evaluation'] . ' ' .  $_SESSION['employee_middleName_evaluation'] . ' ' .$_SESSION['employee_lastName_evaluation']?> confirms this Evaluation for this Year.</textarea>
                        <script type="text/javascript">
                            $('#approval').wysihtml5();
                        </script>
                    </td>

                </tr>
            </tbody>
        </table>
    </div>

</div>

<script type="text/javascript">
    function computeSummary(){

        //START GETTING THE TOTAL WEIGHTED RATING PER COLUMN
        var sum_p1 = document.getElementById('total_weighted_rating').value;
        document.getElementById('p1_summary_twr').value=sum_p1;

        var sum_p2 = document.getElementById('total_weighted_rating_p2').value;
        document.getElementById('p2_summary_twr').value=sum_p2;

        var sum_p3 = document.getElementById('total_score_p3').value;
        document.getElementById('p3_summary_twr').value=sum_p3;

        var sum_p4 = document.getElementById('total_cs_rating').value;
        document.getElementById('cs_factor_rating').value=sum_p4;
        //END GETTING THE TOTAL WEIGHTED RATING PER COLUMN

        //COMPUTING THE SUBTOTAL

        var x1 = document.getElementById('p1_summary_percentage_allo').value;
        var x2 = document.getElementById('p3_summary_percentage_allo').value;
        var x3 = document.getElementById('p2_summary_percentage_allo').value;

        var s1 = Number(x1/100).toLocaleString(undefined,{style: 'percent', minimumFractionDigits:2}); 
        var s2 = Number(x2/100).toLocaleString(undefined,{style: 'percent', minimumFractionDigits:2}); 
        var s3 = Number(x3/100).toLocaleString(undefined,{style: 'percent', minimumFractionDigits:2}); 

        var z1 = parseFloat(s1) / 100.0;
         var z2 = parseFloat(s2) / 100.0;
          var z3 = parseFloat(s3) / 100.0;

        console.log(s1);
        console.log(s2);
        console.log(s3);


        document.getElementById('p1_summary_subtotal').value=parseFloat(sum_p1*z1).toFixed(2);

        document.getElementById('p3_summary_subtotal').value=parseFloat(sum_p2*z2).toFixed(2);

        document.getElementById('p2_summary_subtotal').value=parseFloat(sum_p3*z3).toFixed(2);

        //END COMPUTING THE SUBTOTAL

        //COMPUTING THE PERFORMANCE RATING
        var hold_sub1 = parseFloat(sum_p1*z1).toFixed(2);
        var hold_sub2 = parseFloat(sum_p2*z2).toFixed(2);
        var hold_sub3 = parseFloat(sum_p3*z3).toFixed(2);

        document.getElementById('performance_rating').value = (parseFloat(hold_sub1)+parseFloat(hold_sub2)+parseFloat(hold_sub3)).toFixed(2);
        //END COMPUTING THE PERFORMANCE RATING


        var pf = parseFloat(document.getElementById('p1_summary_subtotal').value) + parseFloat(document.getElementById('p2_summary_subtotal').value) + parseFloat(document.getElementById('p3_summary_subtotal').value);
        //COMPUTING THE OVERALL RATING
        document.getElementById('total_overall_rating').value = (parseFloat(pf) + parseFloat(document.getElementById('cs_factor_rating').value)).toFixed(2) ;
        //END COMPUTING THE OVERALL RATING

    }
</script>

