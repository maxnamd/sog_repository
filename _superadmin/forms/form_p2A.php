

<div class="panel panel-success" style="border-color: #3A5F0B;">
    <div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>PERFORMANCE EVALUATION FORM FOR SOG - P2A</b></h4></div>

    <div class="panel-body">
        <?php include 'include/head.php';?>

        <table class="table-bordered">
            <thead>
                <th class="col-sm-6" colspan="3" style="background-color: #ffcc00">PROFECIENCY LEVELS</th>
            </thead>

            <tbody>
                <?php 
                    $count = 1;

                    $display_KpiRatingScale=mysql_query("SELECT * FROM tbl_proficiency_level ") or die(mysql_error());
                      while($row=mysql_fetch_array($display_KpiRatingScale)){ 
                        
                        $initials=$row['initials'];
                        $title = $row['title'];
                        $content=$row['content'];
                ?>
                <tr>
                    <td><?php echo $initials;?></td>
                    <td><?php echo '<strong>'.$row['title'].'</strong>'.' '.$content;?></td>
                </tr>

                <?php }?>
            </tbody>

        </table>


        <table class="table-bordered">
            <thead>
                <th class="col-sm-6" colspan="3" style="background-color: #ffcc00">COMPETENCY RATING SCALE</th>
            </thead>

            <tbody>
                <?php 
                    $count = 1;

                    $display_KpiRatingScale=mysql_query("SELECT * FROM tbl_competencyratingscale") or die(mysql_error());
                      while($row=mysql_fetch_array($display_KpiRatingScale)){ 
                        
                        $competency_rating_value=$row['competency_rating_value'];
                        $competency_title = $row['competency_title'];
                        $competency_description=$row['competency_description'];
                ?>
                <tr>
                    <td><?php echo $competency_rating_value;?></td>
                    <td><?php echo '<strong>'.$competency_title.'</strong>'.' '.$competency_description;?></td>
                </tr>

                <?php }?>
            </tbody>

        </table>




        <table class="table table-bordered" id="myTableId">
            <thead>
                <th class="col-sm-7">COMPETENCY</th>
                <th class="col-sm-1">REQUIRED PROFICIENCY LEVEL</th>
                <th class="col-sm-1">WEIGHTS</th>
                <th class="col-sm-1">RATING</th>
                <th class="col-sm-1">WEIGHTED RATING</th>
            </thead>

            <tbody>
                <?php
                    $p2counter = 1;
                    $p2a_arrayCount = 0;

                    $display_ep2a=mysql_query("SELECT * FROM tbl_eformp2a") or die(mysql_error());
                      while($row=mysql_fetch_array($display_ep2a)){ 
                        
                        $cId=$row['id'];
                        $competency=$row['competency'];
                        $competencyDesc=$row['competencyDesc'];
                        $weights=$row['weights'];
                        $rating=$row['rating'];
                        $weightedRating=$row['weightedRating'];
                ?>
                <tr>
                    <td><b><?php echo $row['competency'];?></b>.<?php echo $row['competencyDesc'];?></td>
                    <td>
                         <select name="requiredProficiencyLevel[]" class="form-control req_prof" data-weight-val="<?php echo $weights;?>" data-selected="false" data-id="myId_<?php echo $p2counter;?>" id="req_prof_<?php echo $p2counter?>">
                             <option value="<?php if(isset($_POST['requiredProficiencyLevel'][$p2a_arrayCount])){ echo $_POST['requiredProficiencyLevel'][$p2a_arrayCount];}?>"><?php if(isset($_POST['requiredProficiencyLevel'][$p2a_arrayCount])){ echo $_POST['requiredProficiencyLevel'][$p2a_arrayCount]; }else{echo"SELECT";}?></option>

                                <?php
                                $query=mysql_query("SELECT * FROM tbl_proficiency_level") or die (mysql_error());
                                while($row=mysql_fetch_array($query)){
                            ?>
                                <option value="<?php echo $row['initials'];?>">
                                    <?php echo $row['initials'].' ' .$row['title']. ' '. $row['content']; ?>
                                </option>
                            <?php } ?>
                        </select>     
                    </td>
                    <td><?php /*echo $weights;*/?>
                        <input type="text" name="p2_weights[]" id="weightsp2_<?php echo $p2counter;?>" value="<?php echo $weights;?>" class="form-control compute_weights_p2" data-weight-val="<?php echo $weights;?>" data-selected="false" data-id="myId_<?php echo $p2counter;?>" readonly/>
                    </td>
                    <td>
                        <select class="rating_trigger form-control" name="p2_rating[]" id="ratingsp2_<?php echo $p2counter;?>" data-id="myId_<?php echo $p2counter;?>">

                        <option value="<?php if(isset($_POST['p2_rating'][$p2a_arrayCount])){ echo $_POST['p2_rating'][$p2a_arrayCount];}?>"><?php if(isset($_POST['p2_rating'][$p2a_arrayCount])){ echo $_POST['p2_rating'][$p2a_arrayCount]; }else{echo"SELECT";}?></option>

                            <?php
                        $display_rating=mysql_query("SELECT * FROM tbl_kpiratingscale") or die (mysql_error());
                         while($row2 =mysql_fetch_array($display_rating)){

                         $Kpi = $row2['rating_value'];
                    ?>              
                   <option value="<?php echo $Kpi;?>">
                       <?php echo $Kpi;?>
                   </option>
                   <?php } ?>

                    </select>
                        <input type="hidden" data-id="myId_<?php echo $p2counter;?>" name="p2_rating_hid[]" class="hiddenRatingSel" id="ratingsp2_hid_<?php echo $p2counter;?>" value="<?php if(isset($_POST['p2_rating_hid'][$p2a_arrayCount])){ echo $_POST['p2_rating_hid'][$p2a_arrayCount];}?>"/>
                    </td>
                    <td>


                    <input type="text" name="p2_weightedRating[]" id="weighted_rating_idp2_<?php echo $p2counter;?>" class="compute_p2_weighted_rating form-control" value="<?php if(isset($_POST['p2_weightedRating'][$p2a_arrayCount])){ echo $_POST['p2_weightedRating'][$p2a_arrayCount];}?>" data-id="myId_<?php echo $p2counter;?>" readonly />
                    </td>
                    <!-- forms -->
                        <input type="hidden" name="p2a_competencyName[]" class="form-control" value="<?php if(isset($_POST['p2a_competencyName'][$p2a_arrayCount])){ echo $_POST['p2a_competencyName'][$p2a_arrayCount];}else{echo $competency;}?>"/>
                        <input type="hidden" name="competencyDesc[]" class="form-control" value="<?php echo $competencyDesc;?>" value="<?php if(isset($_POST['competencyDesc'][$p2a_arrayCount])){ echo $_POST['competencyDesc'][$p2a_arrayCount];}else{echo $competencyDesc;}?>"/>
                    <!-- forms -->
                </tr>

                <?php $p2counter++;$p2a_arrayCount++;}?>
            </tbody>

            <tr class="totalColumn">

                <td colspan="2" style="background-color: #4b646f;color: white"><center><label>TOTAL WEIGHT</label></center></td>
                <td class="totalCol"><input name="total_weights_p2" id="total_weights_p2" class="form-control" readonly /></td>
                <td colspan="1" style="background-color: #4b646f;color: white"><center><label>WEIGHTED RATING </label></center></td>
                <td class="totalCol"><input type="text" class="form-control" id="total_weighted_rating_p2" readonly /></td>

            </tr>
        </table>
    </div>

</div>
<script type="text/javascript">
    var sum = 0;
    $('.compute_weights_p2').each(function() {
        sum += Number($(this).val());
    });
    document.getElementById('total_weights_p2').value=sum;

    $('.rating_trigger').change(function () {
        var outValue = $(this).val();
        var outElement = $(this);
        var outDetail = outElement.attr("data-id");
        var weightsValue = 0;
        var ratingValue = 0;
        $('.hiddenRatingSel').each(function () {
            var inElement = $(this);
            var insideDetail = inElement.attr("data-id");
            var inValue = $(this).val();
            if(outDetail == insideDetail){

                inElement.attr("value",outValue);
                /*secret.attr("data-weight-val","0");*/
            }
        });

        $('.compute_weights_p2').each(function() {
            var weightCon = $(this);
            var wDetail = weightCon.attr("data-id");
            var w1 =  $(this).val();
            if(outDetail == wDetail){
                weightsValue = w1;
            }
        });

        /*var s = Number(outValue/100).toLocaleString(undefined,{style: 'percent', minimumFractionDigits:2});
        var last = parseFloat(weightsValue)*parseFloat(s);*/
        var last = ((parseFloat(weightsValue))/100)*outValue;
        last = parseFloat(last).toFixed(1);

        var sum_twrp2 = 0;
        $('.compute_p2_weighted_rating').each(function() {
            var elementP2 = $(this);
            var inDetail = elementP2.attr("data-id");
            if(outDetail == inDetail){
                elementP2.attr("value",last);
            }

            sum_twrp2 += Number($(this).val());



        });

        document.getElementById('total_weighted_rating_p2').value=parseFloat(sum_twrp2).toFixed(2);
        computeSummary();



    });


    var NoIsSelected = 0;
    var selectedWeight = 0;
    var containWeightVal = 0;
    var lastVal = 0;


    $(".req_prof").change(function(){
        var myStr = $(this).val();
        var element = $(this);

        if (myStr=="NA"){
            var weightVal = 0;
            var isSelected = element.attr("data-selected","true");
            var similarDet =  element.attr("data-id");

            $('.compute_weights_p2').each(function () {
                var secret = $(this);
                var similarDetInside = secret.attr("data-id");
                if(similarDet == similarDetInside){
                    secret.attr("data-selected","true"); //SETTING THE data-selected attribute same to the PROFICIENCY LEVEL
                    weightVal = secret.attr("value");
                    secret.attr("value","0");
                    /*secret.attr("data-weight-val","0");*/
                }
            });
            selectedWeight += parseFloat(weightVal);
            containWeightVal += parseFloat(weightVal);
            lastVal = parseFloat(weightVal);
            containWeightVal += parseFloat(weightVal);

            isSelected = element.attr("data-selected");
            console.log("Selected Weight Value: "+weightVal);
            console.log("test change: "+isSelected);
            console.log("last value of the selected weight"+lastVal);

            $('.hiddenRatingSel').each(function () {
                var secret = $(this);
                var similarDetInside = secret.attr("data-id");
                if(similarDet == similarDetInside){
                    secret.attr("value","0");
                    /*secret.attr("data-weight-val","0");*/
                }
            });

            $('.compute_p2_weighted_rating').each(function () {
                var myVar = $(this);
                var similarDetInside = myVar.attr("data-id");
                if(similarDet == similarDetInside){
                    //rowRating = myVar.attr("value");
                    myVar.attr("value","0");
                    /*secret.attr("data-weight-val","0");*/
                }
            });

            $('.rating_trigger').each(function () {
                var myVar = $(this);
                var similarDetInside = myVar.attr("data-id");
                if(similarDet == similarDetInside){

                    //rowRating = myVar.attr("value");
                    myVar.attr("disabled","disabled");
                    myVar.selectedIndex
                    /*secret.attr("data-weight-val","0");*/
                }
            });

            countMe();
            computeDist();
            test();
        }
        else if(myStr==""){
            console.log("last value of the selected weight"+lastVal);
            var isSelected = element.attr("data-selected","false");
            var weightVal = element.attr("data-weight-val");
            //selectedWeight -= containWeightVal;
            containWeightVal -= lastVal;
            // paste
            var similarDet =  element.attr("data-id");

            $('.compute_weights_p2').each(function () {
                var secret = $(this);
                var similarDetInside = secret.attr("data-id");
                if(similarDet == similarDetInside){
                    secret.attr("data-selected","false"); //SETTING THE data-selected attribute same to the PROFICIENCY LEVEL
                    weightVal = secret.attr("data-weight-val");

                    secret.attr("value",weightVal);
                    /*secret.attr("data-weight-val","0");*/
                }
            });
            //containWeightVal -= weightVal;


            $('.rating_trigger').each(function () {
                var myVar = $(this);
                var similarDetInside = myVar.attr("data-id");
                if(similarDet == similarDetInside){
                    //rowRating = myVar.attr("value");
                    myVar.removeAttr("disabled");
                    /*secret.attr("data-weight-val","0");*/
                }
            });

            countMe();
            returnDist();
            test();
        }
        else if(myStr =="A" || myStr == "B" || myStr == "C" || myStr == "D"){
             var similarDet =  element.attr("data-id");

            $('.compute_p2_weighted_rating').each(function () {
                var myVar = $(this);
                var similarDetInside = myVar.attr("data-id");
                if(similarDet == similarDetInside){
                    //rowRating = myVar.attr("value");
                    myVar.attr("value","0");
                    /*secret.attr("data-weight-val","0");*/
                }
            });

            $('.hiddenRatingSel').each(function () {
                var secret = $(this);
                var similarDetInside = secret.attr("data-id");
                if(similarDet == similarDetInside){
                    secret.attr("value","0");
                    /*secret.attr("data-weight-val","0");*/
                }
            });


        }
        else if(myStr=="NE"){
            //rating_trigger //rating dropdown
            //compute_p2_weighted_rating //weighted rating input
            var similarDet =  element.attr("data-id");

            $('.compute_p2_weighted_rating').each(function () {
                var myVar = $(this);
                var similarDetInside = myVar.attr("data-id");
                if(similarDet == similarDetInside){
                    //rowRating = myVar.attr("value");
                    myVar.attr("value","0");
                    /*secret.attr("data-weight-val","0");*/
                }
            });

            $('.rating_trigger').each(function () {
                var myVar = $(this);
                var similarDetInside = myVar.attr("data-id");
                if(similarDet == similarDetInside){

                    //rowRating = myVar.attr("value");
                    myVar.attr("disabled","disabled");
                    myVar.selectedIndex
                    /*secret.attr("data-weight-val","0");*/
                }
            });

            $('.hiddenRatingSel').each(function () {
                var secret = $(this);
                var similarDetInside = secret.attr("data-id");
                if(similarDet == similarDetInside){
                    secret.attr("value","0");
                    /*secret.attr("data-weight-val","0");*/
                }
            });

            /////////////////////////////////////////
            var weightVal = 0;
            var isSelected = element.attr("data-selected","true");

            $('.compute_weights_p2').each(function () {
                var secret = $(this);
                var similarDetInside = secret.attr("data-id");
                if(similarDet == similarDetInside){
                    secret.attr("data-selected","true"); //SETTING THE data-selected attribute same to the PROFICIENCY LEVEL
                    weightVal = secret.attr("value");
                    secret.attr("value","0");
                    /*secret.attr("data-weight-val","0");*/
                }
            });
            selectedWeight += parseFloat(weightVal);
            containWeightVal += parseFloat(weightVal);
            lastVal = parseFloat(weightVal);
            containWeightVal += parseFloat(weightVal);

            isSelected = element.attr("data-selected");
            console.log("Selected Weight Value: "+weightVal);
            console.log("test change: "+isSelected);
            console.log("last value of the selected weight"+lastVal);

            $('.hiddenRatingSel').each(function () {
                var secret = $(this);
                var similarDetInside = secret.attr("data-id");
                if(similarDet == similarDetInside){
                    secret.attr("value","0");
                    /*secret.attr("data-weight-val","0");*/
                }
            });

            $('.compute_p2_weighted_rating').each(function () {
                var myVar = $(this);
                var similarDetInside = myVar.attr("data-id");
                if(similarDet == similarDetInside){
                    //rowRating = myVar.attr("value");
                    myVar.attr("value","0");
                    /*secret.attr("data-weight-val","0");*/
                }
            });

            $('.rating_trigger').each(function () {
                var myVar = $(this);
                var similarDetInside = myVar.attr("data-id");
                if(similarDet == similarDetInside){

                    //rowRating = myVar.attr("value");
                    myVar.attr("disabled","disabled");
                    myVar.selectedIndex
                    /*secret.attr("data-weight-val","0");*/
                }
            });

            countMe();
            computeDist();
            test();
        }
        else{
            var similarDet =  element.attr("data-id");
            $('.rating_trigger').each(function () {
                var myVar = $(this);
                var similarDetInside = myVar.attr("data-id");
                if(similarDet == similarDetInside){

                    //rowRating = myVar.attr("value");
                    myVar.removeAttr("disabled");
                    /*secret.attr("data-weight-val","0");*/
                }
            });

            $('.hiddenRatingSel').each(function () {
                var secret = $(this);
                var similarDetInside = secret.attr("data-id");
                if(similarDet == similarDetInside){
                    secret.attr("value","");
                    /*secret.attr("data-weight-val","0");*/
                }
            });
        }

    });

    function countMe(){
        NoIsSelected = 0;
        $('.req_prof').each(function() {

            var element = $(this);
            var myCon = $(this).val();
            var testSelected = element.attr("data-selected");

            if(testSelected=="false"){
                NoIsSelected+=1;
            }
            console.log("Number of unselected: "+NoIsSelected);
        });
    }

    function computeDist(){
        var valDist = selectedWeight / NoIsSelected;

        console.log("Total Selected Weight Value: "+selectedWeight);
        console.log("Value to be distribute: "+valDist);
        console.log("rD Total Selected Weight Value: "+containWeightVal);

        $('.compute_weights_p2').each(function () {
            var myVal = $(this).val();
            var myVal_element = $(this);
            var testMyVal = myVal_element.attr("data-selected");
            if(testMyVal=="false"){
                myVal_element.attr("value",parseFloat(myVal)+parseFloat(valDist));
                selectedWeight = 0;
            }
        });
        NoIsSelected=0;
    }
    function returnDist(){
        $('.compute_weights_p2').each(function () {
            var myVal = $(this).val();
            var myVal_element = $(this);

            myVal_element.attr("value",myVal_element.attr("data-weight-val"));
            myVal_element.attr("data-selected","false");
        });

        $('.req_prof').each(function() {
            var selElement = $(this);
            selElement.prop('selectedIndex', 0);
            selElement.attr("data-selected","false");
        });
        /*var valDist = containWeightVal / NoIsSelected;
        console.log("rD Total Selected Weight Value: "+containWeightVal);
        console.log("Value to be distribute: "+valDist);


        $('.compute_weights_p2').each(function () {
            var myVal = $(this).val();
            var myVal_element = $(this);


            var testMyVal = myVal_element.attr("data-selected");
            var testTest = myVal_element.attr("data-weight-val");
            if(testMyVal=="false"){
                myVal_element.attr("value",parseFloat(testTest)+ parseFloat(valDist));
                selectedWeight = 0;
            }
        });
        NoIsSelected=0;*/
    }

    function test(){
        sum = 0;
        $('.compute_weights_p2').each(function() {
            sum += Number($(this).val());
        });
        document.getElementById('total_weights_p2').value=sum;
    }




</script>