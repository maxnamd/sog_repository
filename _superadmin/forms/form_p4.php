<div class="panel panel-success" style="border-color: #3A5F0B;">
    <div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>PERFORMANCE EVALUATION FORM FOR SOG - P4</b></h4></div>

    <div class="panel-body">
        <?php include 'include/head.php';?>

        <table class="table table-bordered">
            <thead>
            <th class="col-sm-2">AREAS</th>
            <th class="col-sm-8">Customer Feed Back</th>
            <th class="col-sm-1">Score from Respondents</th>
            <th class="col-sm-1">Factor Rating</th>
            </thead>


            <tbody>
            <?php
            $p4_arrayCount = 0;
            $display_ep3=mysql_query("SELECT * FROM tbl_eformp4") or die(mysql_error());
            $myCount=1;
            while($row=mysql_fetch_array($display_ep3)){
                $p4Area = $row['areas'];


                ?>
                <tr>
                    <td> <?php echo $p4Area;?>
                        <input type="hidden" name="areaP4[]" class="form-control" value="<?php echo $p4Area;?>"/>
                    </td>
                    <td>
                        <input type="text" name="feedbackP4[]" class="form-control" value="<?php if(isset($_POST['feedbackP4'][$p4_arrayCount])){ echo $_POST['feedbackP4'][$p4_arrayCount];}?>"onkeypress="return letter(event)">

                    </td>
                    <td> 
                    <!-- <input name="scoreP4[]" class="compute_factor_rating form-control" id="scoreP4-<?php echo $myCount;?>" onkeyup="p4_compute_row_factor_rating<?php echo $myCount;?>();"  value="<?php if(isset($_POST['scoreP4'][$p4_arrayCount])){ echo $_POST['scoreP4'][$p4_arrayCount];}?>"  onkeypress='return event.charCode >= 48 && event.charCode <= 52' minlength="1" maxlength="1"/>  -->
                    <select name="scoreP4[]" class="compute_factor_rating form-control" id="scoreP4-<?php echo $myCount;?>" onclick="p4_compute_row_factor_rating<?php echo $myCount;?>();" value="<?php if(isset($_POST['scoreP4'][$p4_arrayCount])){ echo $_POST['scoreP4'][$p4_arrayCount];}?>"  onkeypress='return event.charCode >= 48 && event.charCode <= 52' minlength="1" maxlength="1">
                        <option value="0"> </option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>

                    </td>
                    <td> <input type="text" name="factorRatingP4[]" class="form-control factor_rating_p4" readonly id="factorRatingP4-<?php echo $myCount;?>" value="<?php if(isset($_POST['factorRatingP4'][$p4_arrayCount])){ echo $_POST['factorRatingP4'][$p4_arrayCount];}?>"/></td>

                </tr>


                <script type="text/javascript">
                    function p4_compute_row_factor_rating<?php echo $myCount;?>()
                    {
                        var value1 = document.getElementById('scoreP4-<?php echo $myCount;?>').value;
                        value1+= '%';
                        pct = parseFloat(value1) / 100;
                        var formula1 =  pct*1.1;
                        var final = formula1.toFixed(2);
                        document.getElementById('factorRatingP4-<?php echo $myCount;?>').value=final;

                        computeSummary();
                    }
                </script>

            <?php $myCount++;$p4_arrayCount++;}?>
            </tbody>

            <tr class="totalColumn">

                <td colspan="3" style="background-color: #4b646f;color: white"><label class="pull-right">TOTAL CUSTOMER SERVICE RATING</label></td>
                <td class="totalCol"><input type="text" id="total_cs_rating" value="0" readonly/></td>

            </tr>
        </table>
    </div>

</div>

<script type="text/javascript">
    $('.compute_factor_rating').keyup(function () {

        var sum = 0;

        $('.factor_rating_p4').each(function() {

            sum += Number($(this).val());
        });


        document.getElementById('total_cs_rating').value=parseFloat(sum/4).toFixed(2);
        console.log(sum);

        computeSummary();
    });
</script>