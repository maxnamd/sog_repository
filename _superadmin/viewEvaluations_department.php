<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 9/4/2017
 * Time: 8:51 PM
 */
?>

<?php include 'includes/header.php'; ?>
<?php
/*departmentEV
departmentOwner*/
if (isset($_POST['btnPassProcessToProcessing'])){
    $_SESSION['departmentEV'] = $_POST['departmentEV'];
    $_SESSION['departmentOwner'] = $_POST['departmentOwner'];
    echo "
    <script type='text/javascript'>
    document.location.href='viewEvaluations_processing.php';
    </script>";
}
if (isset($_POST['btnPassProcessToProcessing_late'])){
    $_SESSION['departmentEV'] = $_POST['departmentEV'];
    $_SESSION['departmentOwner'] = $_POST['departmentOwner'];
    echo "
    <script type='text/javascript'>
    document.location.href='viewEvaluations_processing_late_ev.php';
    </script>";
}
?>
<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview4a").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->
    <script>
        $(document).ready(function() {
            $('#tbl_EV_og').DataTable();
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#tbl_EV_l').DataTable();
        });
    </script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Ongoing Evaluations
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- TABLE: STATUS KRA -->
        <div class="box box-info" style="border-color: green">
            <div class="box-header with-border">
                <h3 class="box-title">Ongoing Evaluation/Department</h3>

                <!-- <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin" id="tbl_EV_og">
                        <thead>
                            <tr>
                                <th class="col-sm-2">Department Name</th>
                                <th class="col-sm-3">
                                    <div class="col-sm-12">Evaluation Status</div>
                                    <div class="col-xs-4">COMPLETED</div>
                                    <div class="col-xs-4">PROCESSING</div>
                                    <div class="col-xs-4">NOT YET STARTED</div>
                                </th>
                                <th class="col-sm-1">Employee Total</th>
                                <th class="col-sm-1">Action</th>
                            </tr>
                        </thead>


                        <tbody>

                        <?php
                        $queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
                        $dateEVRow = mysql_fetch_array($queryDateEV);
                        $startDateEV = $dateEVRow['startDate'];
                        $endDateEV = $dateEVRow['endDate'];
                        $top_performanceCycle = $startDateEV . '-' . $endDateEV;
                        ?>

                        <?php
                        $display_status=mysql_query("SELECT * FROM tbl_departments WHERE department_name != '$department_name_session'") or die(mysql_error());

                        while($row=mysql_fetch_array($display_status)){


                            $departmentName = $row['department_name'];
                            $departmentOwner = $row['departmentOwner'];
                            ?>
                            <tr>
                                <td><?php echo $departmentName; ?></td>
                            <?php
                            $display_count_pass_completed=mysql_query("SELECT COUNT(employee_department) FROM tbl_evaluation_results WHERE employee_department = '$departmentName' AND performanceCycle = '$top_performanceCycle' AND evaluationStatus = 'COMPLETED'") or die(mysql_error());
                            while($c_row=mysql_fetch_array($display_count_pass_completed)){
                                $passer_count_completed = $c_row['COUNT(employee_department)'];
                            }
                            ?>

                            <?php
                            $display_count_pass_processing=mysql_query("SELECT COUNT(employee_department) FROM tbl_evaluation_results WHERE employee_department = '$departmentName' AND performanceCycle = '$top_performanceCycle' AND evaluationStatus = 'PROCESSING'") or die(mysql_error());
                            while($p_row=mysql_fetch_array($display_count_pass_processing)){
                                $passer_count_processing = $p_row['COUNT(employee_department)'];
                            }
                            ?>

                            <?php
                            $display_count_pass_nys=mysql_query("SELECT COUNT(employee_department) FROM tbl_evaluation_results WHERE employee_department = '$departmentName' AND performanceCycle = '$top_performanceCycle' AND evaluationStatus = 'NOT YET STARTED'") or die(mysql_error());
                            while($n_row=mysql_fetch_array($display_count_pass_nys)){
                                $passer_count_nys = $n_row['COUNT(employee_department)'];
                            }
                            ?>


                            <?php
                            $display_number_of_emp=mysql_query("SELECT COUNT(employee_department) FROM tbl_evaluation_results WHERE employee_department = '$departmentName' AND performanceCycle = '$top_performanceCycle'") or die(mysql_error());
                            while($e_row=mysql_fetch_array($display_number_of_emp)){
                                $emp_count = $e_row['COUNT(employee_department)'];
                            }
                            ?>
                            <td>
                                <div class="col-xs-4">
                                    <span class="col-sm-5 label label-success"><?php echo $passer_count_completed;?></span>
                                </div>
                                <div class="col-xs-4">
                                    <span class="col-sm-5 label label-warning"><?php echo $passer_count_processing;?></span>
                                </div>

                                <div class="col-xs-4">
                                    <span class="col-sm-5 label label-danger"><?php echo $passer_count_nys;?></span>
                                </div>

                            </td>

                            <td>
                                    <span class="col-sm-3 label label-default"><?php echo $emp_count;?></span>
                            </td>
                            <td>
                                <form method="POST">
                                    <input type="hidden" name="departmentEV" value="<?php echo $departmentName?>"/>
                                    <input type="hidden" name="departmentOwner" value="<?php echo $departmentOwner?>"/>

                                    <input type="submit" class="btn btn-info btn-sm" name="btnPassProcessToProcessing" value="VIEW"/>
                                </form>

                            </td>

                            </tr>
                            <?php
                        }
                        ?>



                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

            </div>
            <!-- /.box-footer -->
        </div>


        <!-- TABLE: MISSED EVALUATIONS KRA -->
        <div class="box box-danger" style="border-color: orange">
            <div class="box-header with-border">
                <h3 class="box-title">Ongoing Missed Evaluations/Department</h3>

                <!-- <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin" id="tbl_EV_l">
                        <thead>
                        <tr>
                            <th class="col-sm-2">Department Name</th>
                            <th class="col-sm-3">
                                <div class="col-sm-12">Evaluation Status</div>
                                <div class="col-xs-4">COMPLETED</div>
                                <div class="col-xs-4">PROCESSING</div>
                                <div class="col-xs-4">NOT YET STARTED</div>
                            </th>
                            <th class="col-sm-1">Action</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php
                        $queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
                        $dateEVRow = mysql_fetch_array($queryDateEV);
                        $startDateEV = $dateEVRow['startDate'];
                        $endDateEV = $dateEVRow['endDate'];
                        $top_performanceCycle = $startDateEV . '-' . $endDateEV;
                        ?>

                        <?php
                        $display_status=mysql_query("SELECT * FROM tbl_departments WHERE department_name != '$department_name_session'") or die(mysql_error());

                        while($row=mysql_fetch_array($display_status)){

                            $departmentName = $row['department_name'];
                            $departmentOwner = $row['departmentOwner'];
                            echo "<tr>";
                            echo "<td>$departmentName</td>";
                            ?>
                            <?php
                            $display_count_pass_completed=mysql_query("SELECT COUNT(employee_department) FROM tbl_evaluation_results WHERE employee_department = '$departmentName' AND performanceCycle != '$top_performanceCycle' AND evaluationStatus = 'COMPLETED' AND late_status = 'YES'") or die(mysql_error());
                            while($c_row=mysql_fetch_array($display_count_pass_completed)){
                                $passer_count_completed_missed = $c_row['COUNT(employee_department)'];
                            }
                            ?>

                            <?php
                            $display_count_pass_processing=mysql_query("SELECT COUNT(employee_department) FROM tbl_evaluation_results WHERE employee_department = '$departmentName' AND performanceCycle != '$top_performanceCycle' AND evaluationStatus = 'PROCESSING' AND late_status = 'YES'") or die(mysql_error());
                            while($p_row=mysql_fetch_array($display_count_pass_processing)){
                                $passer_count_processing_missed = $p_row['COUNT(employee_department)'];
                            }
                            ?>

                            <?php
                            $display_count_pass_not_yet=mysql_query("SELECT COUNT(employee_department) FROM tbl_evaluation_results WHERE employee_department = '$departmentName' AND performanceCycle != '$top_performanceCycle' AND evaluationStatus = 'NOT YET STARTED' AND late_status = 'YES'") or die(mysql_error());
                            while($p_row=mysql_fetch_array($display_count_pass_not_yet)){
                                $passer_count_not_yet_missed = $p_row['COUNT(employee_department)'];
                            }
                            ?>
                            <td>
                                <div class="col-xs-2">
                                    <span class="col-sm-5 label label-success"><?php echo $passer_count_completed_missed;?></span>
                                </div>

                                <div class="col-xs-2">
                                    <span class="col-sm-5 label label-warning"><?php echo $passer_count_processing_missed;?></span>
                                </div>

                                <div class="col-xs-2">
                                    <span class="col-sm-5 label label-danger"><?php echo $passer_count_not_yet_missed;?></span>
                                </div>

                            </td>
                            <td>
                                <form method="POST">
                                    <input type="hidden" name="departmentEV" value="<?php echo $departmentName?>"/>
                                    <input type="hidden" name="departmentOwner" value="<?php echo $departmentOwner?>"/>

                                    <input type="submit" class="btn btn-info btn-sm" name="btnPassProcessToProcessing_late" value="VIEW"/>
                                </form>

                            </td>
                            <?php
                            echo "</tr>";
                        }
                        ?>



                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

            </div>
            <!-- /.box-footer -->
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>

