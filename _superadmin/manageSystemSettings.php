<?php include 'includes/header.php'; ?>
 <link rel="stylesheet" href="../assets/plugins/Video/YouTubePopUp.css">

    <script type="text/javascript" src="../assets/plugins/Video/YouTubePopUp.jquery.js"></script>

<script type="text/javascript">
    jQuery(function(){
      jQuery("a.demo").YouTubePopUp();
    });
  </script>

<script type="text/javascript">
  function del_confirmation()
  {
    if(confirm("Are you sure?")==1)
    {
      document.getElementById('btnDelete').submit();
    }
  }
</script>


<script type="text/javascript">
  function del_confirmation_proficiency_level()
  {
    if(confirm("Are you sure?")==1)
    {
      document.getElementById('btnDelete').submit();
    }
  }
</script>

<script type="text/javascript">
  function del_confirmation_KpiRatingScale()
  {
    if(confirm("Are you sure?")==1)
    {
      document.getElementById('btnDelete').submit();
    }
  }
</script>

<script type="text/javascript">
  function del_confirmation_CompetencyRatingScale()
  {
    if(confirm("Are you sure?")==1)
    {
      document.getElementById('btnDelete').submit();
    }
  }
</script>

<!-- Setting the treeview active -->
<script type="text/javascript">
document.getElementById("treeview7").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->


<script type="text/javascript">
  $(function() {
      $('#startDate').datepicker( {
          changeMonth: true,
          changeYear: true,
          showButtonPanel: true,
          dateFormat: 'mm/yy',
          minDate: 0,
          onClose: function(dateText, inst) {
              $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
          }
      });

      $('#endDate').datepicker( {
          changeMonth: true,
          changeYear: true,
          showButtonPanel: true,
          dateFormat: 'mm/yy',
          minDate: 0,
          onClose: function(dateText, inst) {
              $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
          }
      });


  });
</script>

 <!-- Keypress -->
    <script type="text/javascript"> 
        function letter(e) 
        { 
            var key; var keychar; 
                if (window.event) 
                    key = window.event.keyCode; 
                else if (e)   
                    key = e.which; 
                else return true; 
                keychar = String.fromCharCode(key); 
                keychar = keychar.toLowerCase(); 
                if ((("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ").indexOf(keychar) > -1)) 
                    return true;
                else 
                    return false; 
        }
        function number(e) 
        { 
            var key; var keychar; 
                if (window.event) 
                    key = window.event.keyCode; 
                else if (e) 
                    key = e.which; 
                else return true; 
                keychar = String.fromCharCode(key); 
                keychar = keychar.toLowerCase(); 
                if ((("0123456789").indexOf(keychar) > -1))
                    return true; 
                else 
                    return false; 
        }
        function number2(e) 
        { 
            var key; var keychar; 
                if (window.event) 
                    key = window.event.keyCode; 
                else if (e) 
                    key = e.which; 
                else return true; 
                keychar = String.fromCharCode(key); 
                keychar = keychar.toLowerCase(); 
                if ((("0123456789-").indexOf(keychar) > -1))
                    return true; 
                else 
                    return false; 
        }   
        function lenum(e) 
        { 
            var key; var keychar; 
                if (window.event) 
                    key = window.event.keyCode; 
                else if (e) 
                    key = e.which; 
                else return true; 
                keychar = String.fromCharCode(key); 
                keychar = keychar.toLowerCase(); 
                if ((("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789").indexOf(keychar) > -1))
                    return true; 
                else 
                    return false; 
        } 
        function lenum2(e) 
        { 
            var key; var keychar; 
                if (window.event) 
                    key = window.event.keyCode; 
                else if (e) 
                    key = e.which; 
                else return true; 
                keychar = String.fromCharCode(key); 
                keychar = keychar.toLowerCase(); 
                if ((("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789,.").indexOf(keychar) > -1))
                    return true; 
                else 
                    return false; 
        } 

    </script>
    <!-- End of Keypress -->

  
<style>
.ui-datepicker-calendar {
    display: none;
  }
</style>
  
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-wrench"></i> System Settings
      <a class="demo" href="https://www.youtube.com/watch?v=EeWNs_p2Vc4" style="margin-left: 33em"><i class="fa fa-question"></i> Help</a>
    </h1>

  </section>

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <?php include 'systemsettings/setP2B.php';?>
    <?php include 'systemsettings/setP3.php';?>
    <?php include 'systemsettings/setDepartment.php';?>
    <?php include 'systemsettings/setRatingScale.php';?>
    <?php include 'systemsettings/setCompetencyRatingScale.php';?>
    <?php include 'systemsettings/setProficiencyLevel.php';?>
    <?php include 'systemsettings/setJobLevel.php';?>

    </div>
 </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php include 'includes/footer.php'; ?>

 



<?php include 'phpfunctions/addDepartment-CRUD.php'; ?>
<?php include 'phpfunctions/addProficiencyLevel-CRUD.php'; ?>
<?php include 'phpfunctions/addKpiRatingScale-CRUD.php'; ?>
<?php include 'phpfunctions/addCompetencyRatingScale-CRUD.php'; ?>
<?php include 'phpfunctions/addJobLevel-CRUD.php'; ?>

<!-- To be transfer sa isang file kapag working na -->
<?php 
  if (isset($_POST['btnSaveP2'])) {
    # code...
    $newQuantity = $_POST['newQuantityP2'];
    $criticalIncidentCount = $p2Row['criticalIncidentCount'];

    mysql_query("UPDATE tbl_settingsp2a SET criticalIncidentCount='$newQuantity'");

    if ($newQuantity>$criticalIncidentCount) {
      #Increase the number of Critical Incident
      $myLoop =$newQuantity-$criticalIncidentCount;

      while ($myLoop > 0) {
        # code...


        $max_id = mysql_query("SELECT MAX(id) FROM tbl_p2bcritinc");
        $row0 = mysql_fetch_array($max_id);
        $p2aCount = $row0['MAX(id)'];
        $testCount = $p2aCount + 1;

        mysql_query("CREATE TEMPORARY TABLE tmp SELECT * FROM tbl_p2bcritinc GROUP BY competencyName");
        mysql_query("UPDATE tmp SET id='$testCount',criticalIncident='Critical Incident'");
        mysql_query("INSERT INTO tbl_p2bcritinc SELECT * FROM tmp");
        mysql_query("DROP TABLE tmp");
        $myLoop--;
      }
        echo "<script>
            bootbox.alert('Success! You have updated the quantity of Critical Incident for P2B Form', 
                function() {
                setTimeout('window.location.replace(\'manageSystemSettings.php\')',600);
                });

            
            </script>";

    }



    elseif ($criticalIncidentCount>$newQuantity) {
      #Deacrease the number of Critical Incident



      $newCount0 = mysql_query("SELECT COUNT(competencyName) FROM tbl_p2bcritinc GROUP BY competencyName");
      $row1 = mysql_fetch_array($newCount0);
      $critCount = $row1['COUNT(competencyName)'];

      $myLoop = $critCount - $newQuantity;
      while ($myLoop > 0) {
        # code...
        $min_id = mysql_query("SELECT MAX(id) FROM tbl_p2bcritinc GROUP BY competencyName");
        $mRow = mysql_fetch_array($min_id);
        $testID = $mRow['MAX(id)'];

        mysql_query("DELETE FROM tbl_p2bcritinc WHERE id = '$testID'");
        $myLoop--;
          echo "<script>
            bootbox.alert('Success! You have updated the quantity of Critical Incident for P2B Form', 
                function() {
                setTimeout('window.location.replace(\'manageSystemSettings.php\')',600);
                });

            
            </script>";
      }

    }


    else{
      #walang mababago
        echo "<script>
      bootbox.alert('NO CHANGES FOUND', 
          function() {
          setTimeout('window.location.replace(\'manageSystemSettings.php\')',600);
          });

      
      </script>";



    }




  }

  if (isset($_POST['btnSaveP3'])) {
    # code...
    $newQuantity = $_POST['newQuantityP3'];
    $rubricCount = $p3Row['rubricCount'];

    if ($newQuantity <= 0 || $newQuantity == "1"){
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'Please input not less than 1 ',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageSystemSettings.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageSystemSettings.php\";
                    }
                  }
                )
			</script>
		";
    }
    elseif ($newQuantity > $rubricCount) {
      /*mysql_query("UPDATE tbl_settingsp3 SET rubricCount='$newQuantity'");*/

      /*# code...
      */

        $myLoop =$newQuantity-$rubricCount;

        while ($myLoop > 0) {
            # code...


            $max_id = mysql_query("SELECT MAX(id) FROM tbl_p3rubric");
            $row0 = mysql_fetch_array($max_id);
            $p2aCount = $row0['MAX(id)'];
            $testCount = $p2aCount + 1;

            mysql_query("CREATE TEMPORARY TABLE tmp SELECT * FROM tbl_p3rubric GROUP BY areaName");
            mysql_query("UPDATE tmp SET id='$testCount',rubricDesc='Enter your rubrics here'");
            mysql_query("INSERT INTO tbl_p3rubric SELECT * FROM tmp");
            mysql_query("DROP TABLE tmp");
            $myLoop--;
        }
        echo "<script>
            bootbox.alert('SUCCESS.', 
                function() {
                setTimeout('window.location.replace(\'manageSystemSettings.php\')',600);
                });
            </script>";
        mysql_query("UPDATE tbl_settingsp3 SET rubricCount='$newQuantity'");
    }
    elseif ($newQuantity < $rubricCount) {


        $newCount2 = mysql_query("SELECT COUNT(areaName) FROM tbl_p3rubric GROUP BY areaName");
        $row2 = mysql_fetch_array($newCount2);
        $rubricCount = $row2['COUNT(areaName)'];

        $myLoop = $rubricCount - $newQuantity;
        while ($myLoop > 0) {
          # code...
          $min_id = mysql_query("SELECT MAX(id) FROM tbl_p3rubric GROUP BY areaName");
          $mRow = mysql_fetch_array($min_id);
          $testID = $mRow['MAX(id)'];

          mysql_query("DELETE FROM tbl_p3rubric WHERE id = '$testID'");
          $myLoop--;

        }

        mysql_query("UPDATE tbl_settingsp3 SET rubricCount='$newQuantity'");
        echo "<script>
            bootbox.alert('SUCCESS', 
                function() {
                setTimeout('window.location.replace(\'manageSystemSettings.php\')',600);
                });

            
            </script>";
      
    }
    else{
        echo "<script>
      bootbox.alert('NO CHANGES FOUND', 
          function() {
          setTimeout('window.location.replace(\'manageSystemSettings.php\')',600);
          });

      
      </script>";
    }
  }
?>