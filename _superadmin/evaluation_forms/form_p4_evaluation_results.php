<?php 
$firstname = $_SESSION['employee_firstName'];
$middlename = $_SESSION['employee_middleName'];
$lastname = $_SESSION['employee_lastName'];
$employee_performance_cycle = $_SESSION['employee_performance_cycle'];
$employee_id= $_SESSION['employee_id'];
?>

<div class="panel panel-success" style="border-color: #3A5F0B;">
    <div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>PERFORMANCE EVALUATION FORM FOR SOG - P4</b></h4></div>

    <div class="panel-body">
        <?php include 'include/head_evaluation_results.php';?>

        <table class="table table-bordered">
            <thead>
            <th class="col-sm-2">AREAS</th>
            <th class="col-sm-8">Customer Feed Back</th>
            <th class="col-sm-1">Score from Respondents</th>
            <th class="col-sm-1">Factor Rating</th>
            </thead>


            <tbody>
            <?php
            $display_ep4=mysql_query("SELECT * FROM tbl_ans_eformp4 WHERE performanceCycle = '$employee_performance_cycle' AND answeredFor = '$employee_id'") or die(mysql_error());
            $myCount=1;
            while($row=mysql_fetch_array($display_ep4)){
                $p4Area = $row['p4_area'];
                $p4Feedback= $row['customerFeedback'];
                $p4Score= $row['p4_score'];
                $p4Rating = $row['p4_rating'];
                ?>
                <tr>
                    <td> <?php echo $p4Area;?></td>
                    <td>
                        <input type="text" value="<?php echo $p4Feedback?>" class="form-control" readonly/>

                    </td>
                    <td>  <input type="text" value="<?php echo $p4Score?>" class="form-control" readonly/></td>
                    <td> <input type="text" value="<?php echo $p4Rating?>" class="factor_rating_p4 form-control" readonly/></td>

                </tr>


            <?php }?>
            </tbody>

            <tr class="totalColumn">

                <td colspan="3" style="background-color: #4b646f;color: white"><label class="pull-right">TOTAL CUSTOMER SERVICE RATING</label></td>
                <td class="totalCol"><input type="text" id="total_cs_rating" readonly disabled/></td>

            </tr>
        </table>
    </div>

</div>