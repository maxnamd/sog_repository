<?php 
$firstname = $_POST['employee_firstName'];
$middlename = $_POST['employee_middleName'];
$lastname = $_POST['employee_lastName'];
$employee_performance_cycle = $_POST['employee_performance_cycle'];
$employee_id=$_POST['employee_id'];
?>

<div class="panel panel-success" style="border-color: #3A5F0B;">
    <div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>PERFORMANCE EVALUATION FORM FOR SOG - P4</b></h4></div>

    <div class="panel-body">
        <?php include 'include/head.php';?>

        <table class="table table-bordered">
            <thead>
            <th class="col-sm-2">AREAS</th>
            <th class="col-sm-8">Customer Feed Back</th>
            <th class="col-sm-1">Score from Respondents</th>
            <th class="col-sm-1">Factor Rating</th>
            </thead>


            <tbody>
            <?php
            $display_ep3=mysql_query("SELECT * FROM tbl_eformp4") or die(mysql_error());
            $myCount=1;
            while($row=mysql_fetch_array($display_ep3)){
                $p4Area = $row['areas'];


                ?>
                <tr>
                    <td> <?php echo $p4Area;?></td>
                    <td>
                        <input type="text" name="feedbackP4[]" class="form-control" />

                    </td>
                    <td> <input type="text" name="scoreP4[]" class="form-control" /> </td>
                    <td> <input type="text" name="factorRatingP4[]" class="form-control" disabled/ </td>

                </tr>


            <?php }?>
            </tbody>

            <tr class="totalColumn">

                <td colspan="3" style="background-color: #4b646f;color: white"><label class="pull-right">TOTAL CUSTOMER SERVICE RATING</label></td>
                <td class="totalCol"></td>

            </tr>
        </table>
    </div>

</div>