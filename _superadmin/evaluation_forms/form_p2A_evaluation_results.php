<?php 
$firstname = $_SESSION['employee_firstName'];
$middlename = $_SESSION['employee_middleName'];
$lastname = $_SESSION['employee_lastName'];
$employee_performance_cycle = $_SESSION['employee_performance_cycle'];
$employee_id= $_SESSION['employee_id'];
?>

<div class="panel panel-success" style="border-color: #3A5F0B;">
	<div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>PERFORMANCE EVALUATION FORM FOR SOG - P2A</b></h4></div>

	<div class="panel-body">
		<?php include 'include/head_evaluation_results.php';?>

		<table class="table-bordered">
			<thead>
				<th class="col-sm-6" colspan="3" style="background-color: #ffcc00">PROFECIENCY LEVELS</th>
			</thead>

			<tbody>
				<?php 
					$count = 1;
					$display_KpiRatingScale=mysql_query("SELECT * FROM tbl_proficiency_level") or die(mysql_error());
	                  while($row=mysql_fetch_array($display_KpiRatingScale)){ 
	                    
	                    $initials=$row['initials'];
	                    $title = $row['title'];
	                    $content=$row['content'];
				?>
				<tr>
					<td><?php echo $initials;?></td>
					<td><?php echo '<strong>'.$row['title'].'</strong>'.' '.$content;?></td>
				</tr>

				<?php }?>
			</tbody>

		</table>


		<table class="table-bordered">
			<thead>
				<th class="col-sm-6" colspan="3" style="background-color: #ffcc00">COMPETENCY RATING SCALE</th>
			</thead>

			<tbody>
				<?php 
					$count = 1;
					$display_KpiRatingScale=mysql_query("SELECT * FROM tbl_competencyratingscale") or die(mysql_error());
	                  while($row=mysql_fetch_array($display_KpiRatingScale)){ 
	                    
	                    $competency_rating_value=$row['competency_rating_value'];
	                    $competency_title = $row['competency_title'];
	                    $competency_description=$row['competency_description'];
				?>
				<tr>
					<td><?php echo $competency_rating_value;?></td>
					<td><?php echo '<strong>'.$competency_title.'</strong>'.' '.$competency_description;?></td>
				</tr>

				<?php }?>
			</tbody>

		</table>




		<table class="table table-bordered">
			<thead>
				<th class="col-sm-7">COMPETENCY</th>
				<th class="col-sm-2">REQUIRED PROFICIENCY LEVEL</th>
				<th class="col-sm-1">WEIGHTS</th>
				<th class="col-sm-1">RATING</th>
				<th class="col-sm-1">WEIGHTED RATING</th>
			</thead>

			<tbody>
				<?php
                    $p2counter = 1;
					$display_ep2a=mysql_query("SELECT * FROM tbl_ans_eformp2a WHERE answeredFor = '$employee_id' AND performanceCycle='$employee_performance_cycle'") or die(mysql_error());
                      while($row=mysql_fetch_array($display_ep2a)){ 
                        
                        $cId=$row['id'];
                        $competency=$row['competency'];
                        $competencyDesc=$row['competencyDescription'];
                        $requiredProfLevel=$row['proficiencyLevel'];
                        $weights=$row['p2_weights'];
                        $rating=$row['p2_rating'];
                        $weightedRating=$row['p2_weightedRating'];
				?>
				<tr>
					<td><b><?php echo $competency;?></b>.<?php echo $competencyDesc;?></td>
	              	<td>
	              		<?php echo $requiredProfLevel;?>
	              	</td>
	                <td><?php echo $weights;?>
                        <input type="hidden" class="compute_weights_p2" value="<?php echo $weights;?>"/>
	                </td>
	                <td>
                        <?php echo $rating;?>
	                </td>

                    <td><?php echo $weightedRating;?>
                        <input type="hidden" class="compute_p2_weighted_rating" value="<?php echo $weightedRating;?>"/>
	                </td>
                </tr>

                <?php $p2counter++;}?>
			</tbody>

            <tr class="totalColumn">

                <td colspan="2" style="background-color: #4b646f;color: white"><center><label>TOTAL WEIGHT</label></center></td>
                <td class="totalCol"><input name="total_weights_p2" id="total_weights_p2" class="form-control" disabled/></td>
                <td colspan="1" style="background-color: #4b646f;color: white"><center><label>WEIGHTED RATING </label></center></td>
                <td class="totalCol"><input type="text" class="form-control" id="total_weighted_rating_p2" disabled/></td>

            </tr>
		</table>
	</div>

</div>