<?php 
$firstname = $_SESSION['employee_firstName'];
$middlename = $_SESSION['employee_middleName'];
$lastname = $_SESSION['employee_lastName'];
$employee_performance_cycle = $_SESSION['employee_performance_cycle'];
$employee_id= $_SESSION['employee_id'];
?>
<div class="panel panel-success" style="border-color: #3A5F0B;">
	<div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>PERFORMANCE EVALUATION FORM FOR SOG - P3</b></h4></div>

	<div class="panel-body">
		<?php include 'include/head_evaluation_results.php';?>

		<table class="table table-bordered">
			<thead>
				<th class="col-sm-1">AREAS</th>
                <th class="col-sm-10">Rubric</th>
                <th class="col-sm-1">Score</th>
			</thead>


			<tbody>
				<?php 
				$display_ep3=mysql_query("SELECT * FROM tbl_ans_eformp3 WHERE answeredFor = '$employee_id' AND performanceCycle='$employee_performance_cycle'") or die(mysql_error());
                                    $myCount=1;
                                    while($row=mysql_fetch_array($display_ep3)){ 
                                        $mainArea = $row['areas'];
                                        $p3_score = $row['score'];

                ?>
                <?php
                    $_counting=mysql_query("SELECT COUNT(areas) as total FROM tbl_eformp3") or die (mysql_error());
                    $counting = mysql_fetch_assoc($_counting);
                    $display = $counting['total'];
                ?>
                <tr>
                	<td> <?php echo $mainArea;?></td>
                	<td> 
                		<?php 
		                $display_rubric=mysql_query("SELECT * FROM tbl_ans_eformp3_rubric WHERE areaName ='$mainArea' AND answeredFor = '$employee_id' AND performanceCycle='$employee_performance_cycle' ORDER BY id DESC") or die(mysql_error());
		             
		                while($row=mysql_fetch_array($display_rubric)){ 
		                    $rubricID = $row['id'];
		                    $areaName = $row['areaName'];
		                    $rubricDesc = $row['rubricDesc'];
		                ?>
		                <div class="row">
		                	<div class="col-sm-12"><?php echo $rubricID . ': '.$rubricDesc;}?> </div>
		                </div>
		                
                	</td>

                	<td>
                        <?php echo $p3_score?>
                        <input type="hidden" value="<?php echo $p3_score?>" class="compute_score_p3"/>
                    </td>
                </tr>


                <?php }?>
			</tbody>

            <tr class="totalColumn">

                <td colspan="2" style="background-color: #4b646f;color: white"><center><label>TOTAL PROFESSIONALISM RATING</label></center></td>
                <td class="totalCol">
                    <input type="text" id="total_score_p3" disabled/>
                    <input type="hidden" id="divided_by" value="<?php echo $display;?>">
                </td>

            </tr>
		</table>
	</div>

</div>
<script type="text/javascript">
function p3_comp(){
    var sum = 0;
    var divide = document.getElementById('divided_by').value;
    $('.compute_score_p3').each(function() {
        sum += Number($(this).val());
    });

    var final = (sum/divide);
    document.getElementById('total_score_p3').value=parseFloat(final).toFixed(2);
}
</script>