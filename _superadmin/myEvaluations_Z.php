<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 11/11/2017
 * Time: 10:49 PM
 */
?>
<?php include 'includes/header.php'; ?>

<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview2a").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->

<!-- Keypress -->
<script type="text/javascript">
    function letter(e)
    {
        var key; var keychar;
        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else return true;
        keychar = String.fromCharCode(key);
        keychar = keychar.toLowerCase();
        if ((("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ").indexOf(keychar) > -1))
            return true;
        else
            return false;
    }
    function number(e)
    {
        var key; var keychar;
        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else return true;
        keychar = String.fromCharCode(key);
        keychar = keychar.toLowerCase();
        if ((("0123456789").indexOf(keychar) > -1))
            return true;
        else
            return false;
    }
    function number2(e)
    {
        var key; var keychar;
        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else return true;
        keychar = String.fromCharCode(key);
        keychar = keychar.toLowerCase();
        if ((("0123456789-").indexOf(keychar) > -1))
            return true;
        else
            return false;
    }
    function lenum(e)
    {
        var key; var keychar;
        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else return true;
        keychar = String.fromCharCode(key);
        keychar = keychar.toLowerCase();
        if ((("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789").indexOf(keychar) > -1))
            return true;
        else
            return false;
    }
    function lenum2(e)
    {
        var key; var keychar;
        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else return true;
        keychar = String.fromCharCode(key);
        keychar = keychar.toLowerCase();
        if ((("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789,.").indexOf(keychar) > -1))
            return true;
        else
            return false;
    }

</script>
<!-- End of Keypress -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Evaluation Form
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">






        <form method="post">
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">P1 - KPI</a></li>
                    <li><a href="#tabs-2a">P2A - Competencies</a></li>
                    <li><a href="#tabs-2b">P2B - Competency Details</a></li>
                    <li><a href="#tabs-3">P3 - Professionalism</a></li>
                    <li><a href="#tabs-4">P4 - Customer Service</a></li>
                    <li><a href="#tabs-5">Summary</a></li>

                </ul>


                <div id="tabs-1">
                    <?php include 'forms_missed/form_p1.php';?>
                </div>

                <div id="tabs-2a">
                    <?php include 'forms_missed/form_p2a.php';?>
                </div>

                <div id="tabs-2b">
                    <?php include 'forms_missed/form_p2b.php';?>
                </div>

                <div id="tabs-3">
                    <?php include 'forms_missed/form_p3.php';?>
                </div>

                <div id="tabs-4">
                    <?php include 'forms_missed/form_p4.php';?>
                </div>

                <div id="tabs-5">
                    <?php include 'forms_missed/form_summary.php';?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-11"></div>
                <div class="col-lg-1">
                    <button type="submit" name="btnSubmitEV" id="btnSends" class="btn btn-success btn-block" style="margin-top: 1em;">
                        SUBMIT
                    </button>
                </div>

            </div>
        </form>





    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<?php include 'includes/footer.php'; ?>
<?php include 'phpfunctions/sendEvaluationMissed.php' ?>
