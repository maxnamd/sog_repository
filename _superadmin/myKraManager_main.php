<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 12/8/2017
 * Time: 1:10 AM
 */
?>
<?php include 'includes/header.php'; ?>

<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview2b").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->


<script type="text/javascript">
    function del_confirmation1()
    {
        if(confirm("Are you sure?")==1)
        {
            document.getElementById('btnDeleteKRA').submit();
        }
    }

    function del_confirmation2()
    {
        if(confirm("Are you sure?")==1)
        {
            document.getElementById('btnDeleteKPI').submit();
        }
    }
</script>




<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">


        <?php

        $emp_id = $_SESSION['emp_id'];
        $format = "m/Y";

        $queryDateKPI = mysql_query("SELECT * FROM tbl_datekpi");
        $datekpiRow = mysql_fetch_array($queryDateKPI);
        $startDateKPI = \DateTime::createFromFormat($format, $datekpiRow['startDate']);
        $endDateKPI = \DateTime::createFromFormat($format, $datekpiRow['endDate']);

        $queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
        $dateEVRow = mysql_fetch_array($queryDateEV);
        $startDateEV = \DateTime::createFromFormat($format, $dateEVRow['startDate']);
        $endDateEV =  \DateTime::createFromFormat($format, $dateEVRow['endDate']);

        $queryStat = mysql_query("SELECT * FROM tbl_departments WHERE department_name = '$department_name_session'");
        $statRow = mysql_fetch_array($queryStat);
        $kpiStat = $statRow['kpiStatus']; //DEPARTMENT KRA STATUS
        $activateStat = $statRow['activation_status'];

        $query_kraStatus_emp = mysql_query("SELECT * FROM tbl_sog_employee WHERE emp_id = '$emp_id'");
        $statusEmpRow = mysql_fetch_array($query_kraStatus_emp);
        $kraStatusEmp = $statusEmpRow['kraStatus']; //EMPLOYEE KRA STATUS


        $query_status_request = mysql_query("SELECT * FROM tbl_kpi_requests WHERE department = '$department_name_session'");
        $request_row = mysql_fetch_array($query_status_request);
        $status = $request_row['request_status'];

        $result = mysql_query("SELECT * FROM tbl_p1kpi WHERE kpiOwner = '$department_name_session' AND kraCreatedFor = '$emp_id'");
        $my_kra_count = mysql_num_rows($result);

        $fetch_hr = mysql_query("SELECT * FROM tbl_admins WHERE userlevel = 'SUPERADMIN'");
        $row_hr = mysql_fetch_array($fetch_hr);
        $hr_dept = $row_hr['department'];
        ?>

        <div class="panel panel-default">
            <div class="panel-heading" style="background: black"><h4><b style="color:white;">Manage KPI</b></h4>
            </div>


            <div class="container-fluid" style="margin-top: 1em;">


                <!-- // I T O   Y U N G   I  - A A C T I V A T E   M O -->

                <div class='col-sm-2'>
                    <a href='#' class='btn btn-success'
                       style='background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7)'
                       data-toggle='modal' data-target='#addKRA'> &nbsp;&nbsp;<i class='fa fa-plus'></i> NEW KRA
                        &nbsp;</a>
                </div>

                 <div class="col-sm-5"> </div>

                                     <div class="col-sm-2">
                                        <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_kpi_title" class="btn btn-success" value="Add KPI Title" style=" width:100px;" />
                                        </form>
                                    </div>

                                       <div class="col-sm-2">
                                        <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_kpi" class="btn btn-success" value="Add KPI" style=" width:100px;" />
                                        </form>
                                    </div>


            </div>

            <hr>

            <div class="panel-body">




                <table class="table table-bordered table-responsive">
                    <thead class="">

                    <tr>
                        <th class="col-sm-1">KRA's</th>
                        <th class="col-sm-8">KEY PERFORMANCE INDICATORS</th>
                        <!--<th class="col-sm-2">WEIGHTS</th>-->
                        <th class="col-sm-1">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php

                    $query = "SELECT * FROM tbl_eformp1 WHERE kpiOwner = '$department_name_session' AND kraCreatedFor = '$emp_id'";
                    // execute query
                    $result = mysql_query($query) or die ("Error in query: $query. " . mysql_error());
                    // see if any rows were returned
                    if (mysql_num_rows($result) == 0) {

                        echo "<td colspan='4'><center><h4><b>This SOG employee doesn't have any KPI yet <br> Add Now!</b></h4></center></td>";
                    } else {
                        $myCount = 1;

                        $display_ep1 = mysql_query("SELECT * FROM tbl_eformp1 WHERE kpiOwner = '$department_name_session' AND kraCreatedFor = '$emp_id'") or die(mysql_error());
                        while ($row = mysql_fetch_array($display_ep1)) {
                            $kraID = $row['kraID'];
                            $kpiTitle = $row['kpiTitle'];

                            ?>
                            <tr>
                                <td>KRA #<?php echo $myCount; ?> </td>
                                <td><?php echo $row['kpiTitle']; ?>
                                    <div class="">
                                        <div class="">
                                            <div class="col-sm-2" style="float: right">
                                                <a href="#" class="btn btn-success"
                                                   style="background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7);margin-bottom: 1em;"
                                                   data-toggle="modal"
                                                   data-target="#addKpi<?php echo $kraID; ?>"> &nbsp;&nbsp;<i
                                                            class="fa fa-plus"></i> NEW KPI &nbsp;</a>
                                            </div>




                                            <div class="col-sm-12">


                                                <div class="box box-info collapsed-box"
                                                     style="border-color: green">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">KPI Status / Deparment</h3>

                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool"
                                                                    data-widget="collapse"><i
                                                                        class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <div class="">
                                                            <table class="table table-responsive">
                                                                <?php

                                                                $query = "SELECT * FROM tbl_p1KPI WHERE kpiOwner = '$department_name_session' AND kraID = '$kraID'";
                                                                // execute query
                                                                $result = mysql_query($query) or die ("Error in query: $query. " . mysql_error());
                                                                // see if any rows were returned
                                                                if (mysql_num_rows($result) == 0)
                                                                {
                                                                    echo "<td colspan='4'><center><h4><b>There are no KPI yet.</b></h4></center></td>";
                                                                }
                                                                else
                                                                {
                                                                ?>
                                                                <tr>
                                                                    <?php
                                                                    $display_kpi = mysql_query("SELECT * FROM tbl_p1KPI WHERE kraID ='$kraID' AND kpiOwner = '$department_name_session' AND kraCreatedFor = '$emp_id' ORDER BY kpiID ASC") or die(mysql_error());

                                                                    while ($row2 = mysql_fetch_array($display_kpi)) {

                                                                        $kranewID = $row2['kraID'];
                                                                        $kpinewID = $row2['kpiID'];
                                                                        $kpiDesc = $row2['kpiDesc'];
                                                                        ?>


                                                                        <div class="row"
                                                                             style="margin-bottom: 1em">
                                                                            <div class="col-sm-10"><?php echo $kpiDesc; ?> </div>

                                                                            <div class="col-sm-2">
                                                                                <div class="col-sm-2"><a
                                                                                            href="#editKPIDesc<?php echo $kraID . $kpinewID; ?>"
                                                                                            class="btn btn-default btn-sm"
                                                                                            title="Edit KPI"
                                                                                            data-toggle="modal">
                                                                                        <i class="fa fa-edit"></i></a>
                                                                                </div>

                                                                                <div class="col-sm-2"></div>

                                                                                <div class="col-sm-2">
                                                                                    <form method="post">
                                                                                        <input type="hidden"
                                                                                               name="hiddenKRAID"
                                                                                               value="<?php echo $kranewID ?>"/>
                                                                                        <input type="hidden"
                                                                                               name="hiddenKPIID"
                                                                                               value="<?php echo $kpinewID ?>"/>
                                                                                        <button type="submit"
                                                                                                class="btn btn-default btn-sm"
                                                                                                title="Delete KPI"
                                                                                                name="btnDeleteKPI"
                                                                                                id="btnDeleteKPI"
                                                                                                onclick="del_confirmation2(this);return false">
                                                                                            <i class="fa fa-trash"></i>
                                                                                        </button>
                                                                                    </form>

                                                                                </div>

                                                                            </div>
                                                                        </div>


                                                                        <!-- EDIT KPI Description MODAL-->
                                                                        <div class="modal fade"
                                                                             id="editKPIDesc<?php echo $kraID . $kpinewID; ?>"
                                                                             tabindex="-1" role="dialog">
                                                                            <div class="modal-dialog">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header"
                                                                                         style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                                                                                        <button type="button"
                                                                                                class="close"
                                                                                                data-dismiss="modal"
                                                                                                aria-label="Close">
                                                                                            <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                        <h4 class="modal-title">
                                                                                            Edit
                                                                                            KPI <?php echo $kpiTitle; ?></h4>
                                                                                    </div>

                                                                                    <div class="modal-body">
                                                                                        <form method="post">
                                                                                            <div class="row"
                                                                                                 id="">
                                                                                                <label class="col-sm-4 control-label">Description: </label>
                                                                                                <div class="col-sm-8">
                                                                                                    <input type="text"
                                                                                                           name="editkpiDesc"
                                                                                                           class="form-control"
                                                                                                           placeholder="KPI Description: "
                                                                                                           value="<?php echo $kpiDesc; ?>"/>
                                                                                                </div>
                                                                                            </div>

                                                                                    </div>

                                                                                    <div class="modal-footer">
                                                                                        <input type="hidden"
                                                                                               name="xkranewID"
                                                                                               value="<?php echo $kranewID ?>"/>
                                                                                        <input type="hidden"
                                                                                               name="xkpinewID"
                                                                                               value="<?php echo $kpinewID ?>"/>
                                                                                        <input type="submit"
                                                                                               class="btn btn-success"
                                                                                               name="update_kpi"
                                                                                               value="UPDATE"/>
                                                                                        <input type="reset"
                                                                                               class="btn btn-default"
                                                                                               value="Clear"/>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        </form>
                                                                        <!-- END EDIT KPI Description MODAL-->


                                                                        <?php
                                                                    }
                                                                    }
                                                                    ?>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <!-- /.table-responsive -->
                                                    </div>
                                                    <!-- /.box-body -->
                                                    <!-- /.box-footer -->
                                                </div>

                                            </div>
                                        </div>


                                    </div>


                                    <!-- ADD KPI Description MODAL-->
                                    <div class="modal fade" id="addKpi<?php echo $kraID; ?>" tabindex="-1"
                                         role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header"
                                                     style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close"><span
                                                                aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Add New KPI
                                                        to <?php echo $kpiTitle; ?></h4>
                                                </div>

                                                <div class="modal-body">

                                                    <form method="post" class="form-horizontal">
                                                        <div class="form-group" id="">
                                                            <label class="col-sm-4 control-label">Description: </label>
                                                            <div class="col-sm-6">
                                                                <input type="text" name="kpiDesc" id=""
                                                                       class="form-control"
                                                                       placeholder="KPI Description: "/>
                                                            </div>
                                                        </div>

                                                </div>

                                                <div class="modal-footer">
                                                    <input type="hidden" name="xkpiTitle"
                                                           value="<?php echo $kpiTitle ?>"/>
                                                    <input type="hidden" name="xkraID"
                                                           value="<?php echo $kraID ?>"/>
                                                    <input type="submit" class="btn btn-success" name="add_kpi"
                                                           value="ADD KPI"/>
                                                    <input type="reset" class="btn btn-default" value="Clear"/>

                                                </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END ADD KPI Description MODAL-->

                                </td>
                                <!--<td>
                                        WEIGHTS
                                </td>-->
                                <td>
                                    <div class="col-sm-2"><a href="#editKRA<?php echo $kraID; ?>"
                                                             class="btn btn-default btn-sm" title="Edit KRA"
                                                             data-toggle="modal"> <i class="fa fa-edit"></i></a>
                                    </div>

                                    <div class="col-sm-2"></div>

                                    <div class="col-sm-2">
                                        <form method="post">
                                            <input type="hidden" name="hiddenID" value="<?php echo $kraID ?>"/>
                                            <button type="submit" class="btn btn-default btn-sm"
                                                    title="Delete Competency" name="btnDeleteKRA"
                                                    id="btnDeleteKRA"
                                                    onclick="del_confirmation1(this);return false"><i
                                                        class="fa fa-trash"></i>
                                            </button>
                                        </form>

                                    </div>

                                    <!-- EDIT KRA MODAL-->
                                    <div class="modal fade" id="editKRA<?php echo $kraID; ?>" tabindex="-1"
                                         role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header"
                                                     style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close"><span
                                                                aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Edit
                                                        KRA <?php echo $kpiTitle ?></h4>
                                                </div>

                                                <div class="modal-body">

                                                    <form method="post" class="form-horizontal">
                                                        <div class="form-group" id="">
                                                            <label class="col-sm-4 control-label">KRA
                                                                Title: </label>
                                                            <div class="col-sm-6">
                                                                <input type="text" name=" edit_kpiTitle"
                                                                       value="<?php echo $kpiTitle ?>" id=""
                                                                       class="form-control"
                                                                       placeholder="KRA Name: "/>
                                                            </div>
                                                        </div>


                                                        <div class="modal-footer">
                                                            <input type="hidden" name="hiddenID"
                                                                   value="<?php echo $kraID ?>">
                                                            <input type="submit" class="btn btn-success"
                                                                   name="update_kra" value="UPDATE KRA"/>
                                                            <input type="reset" class="btn btn-default"
                                                                   value="Clear"/>

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END EDIT KRA MODAL-->

                                </td>

                            </tr>
                            <?php $myCount++;
                        }
                    } ?>
                    </tbody>

                </table>

            </div>
        </div>



    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- REQUEST MODAL-->
<div class="modal fade" id="requestModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Request to REOPEN KRA CREATION</h4>
            </div>

            <div class="modal-body">

                <form method="post" class="form-horizontal">
                    <input type="hidden" name="employee_id" value="<?php echo $emp_id?>"/>
                    <input type="hidden" name="sent_at" value="<?php date_default_timezone_set('Asia/Manila'); echo date('F j, Y g:i:a');?>">

                    <div class="form-group container-fluid" > <!-- Message field -->
                        <label class="control-label " for="message">Request Reason</label>

                        <textarea class="form-control ckeditor" id="request_msg" name="request_msg">
                                <script type="text/javascript">
                                    var textarea = document.getElementById('request_msg');

                                    CKEDITOR.replace( 'textarea',
                                        {
                                            toolbar : 'Basic', /* this does the magic */
                                            uiColor : '#9AB8F3'
                                        });

                                </script>
                            </textarea>
                    </div>





            </div>

            <div class="modal-footer">

                <input type="submit" class="btn btn-success" name="btnSubmitRequest" value="SEND"  />
                <input type="reset" class="btn btn-default" value="Clear"/>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ADD NEW REQUEST MODAL-->

<!-- APPEAL MODAL-->
<div class="modal fade" id="appealModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Appeal to REOPEN KRA CREATION</h4>
            </div>

            <div class="modal-body">

                <form method="post" class="form-horizontal">
                    <input type="hidden" name="employee_id" value="<?php echo $emp_id?>"/>
                    <input type="hidden" name="sent_at" value="<?php date_default_timezone_set('Asia/Manila'); echo date('F j, Y g:i:a');?>">

                    <div class="form-group container-fluid" > <!-- Message field -->
                        <label class="control-label " for="message">Request Reason</label>

                        <textarea class="form-control ckeditor" id="request_msg" name="request_msg">
                                <script type="text/javascript">
                                    var textarea = document.getElementById('request_msg');

                                    CKEDITOR.replace( 'textarea',
                                        {
                                            toolbar : 'Basic', /* this does the magic */
                                            uiColor : '#9AB8F3'
                                        });

                                </script>
                            </textarea>
                    </div>





            </div>

            <div class="modal-footer">

                <input type="submit" class="btn btn-success" name="btnSubmitAppeal" value="SUBMIT"  />
                <input type="reset" class="btn btn-default" value="Clear"/>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ADD NEW REQUEST MODAL-->

<!-- ADD NEW KRA MODAL-->
<div class="modal fade" id="addKRA" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New KRA</h4>
            </div>

            <div class="modal-body">

                <form method="post" class="form-horizontal">
                    <div class="form-group" id="">
                        <label class="col-sm-4 control-label">KRA Title: </label>
                        <div class="col-sm-6">
                            <input type="text" name="kraTitle" id="" class="form-control" placeholder="KRA Name: "/>
                        </div>
                    </div>




                    <div class="modal-footer">

                        <input type="submit" class="btn btn-success" name="add_kra" value="ADD KRA"  />
                        <input type="reset" class="btn btn-default" value="Clear"/>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ADD NEW KRA MODAL-->
<?php include 'includes/footer.php'; ?>
<?php include 'phpfunctions/kra_manager/p1functions.php'; ?>

