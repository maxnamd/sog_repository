<?php include 'includes/header.php'; ?>
<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview_qm").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->

  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php
    $p2Count = mysql_query("SELECT * FROM tbl_settingsp2a");
    $p2Row = mysql_fetch_array($p2Count);
    $criticalIncidentCount = $p2Row['criticalIncidentCount'];

    if($criticalIncidentCount==0){
        echo "
                <script>
                    bootbox.alert('SETUP THE NUMBER OF CRITICAL INCIDENT FIRST AT THE SYSTEM SETTINGS',
                    function() {
                       setTimeout('window.location.replace(\'manageSystemSettings.php\')',600);
                    });
                </script>
                ";
    }
    else{
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Questionnaire Management
        <!-- <small>Version 2.0</small> -->
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="panel panel-default">
              <div class="panel-heading" style="background: black"><h4><b style="color:white;">Performance Evaluation Form for SOG - P2B</b></h4></div>
              
              <div class="panel-body">

                  <table class="table table-bordered">
                        <thead class="">
                          <tr>
                            <th class="col-sm-2">Competency</th>
                            <th class="col-sm-10">Critical Incident</th>
                          </tr>
                        </thead>


                        <tbody>
                          <?php
                              $query = "SELECT * FROM tbl_eformp2b";
                              // execute query 
                              $result = mysql_query($query) or die ("Error in query: $query. ".mysql_error()); 
                              // see if any rows were returned 
                              if (mysql_num_rows($result) == 0) 
                              { 
                                echo"<td colspan='5'><center><h4><b>There are no questions yet.</b></h4></center></td>";
                              }
                              else
                              {
                                $display_ep2a=mysql_query("SELECT * FROM tbl_eformp2b") or die(mysql_error());
                                    
                                    while($row=mysql_fetch_array($display_ep2a)){ 
                                      $incCompetency=$row['competency'];
                              ?>
                              <tr> 
                                    <td><?php echo $row['competency'];?> </td>
                                    <td>
                                      <?php
                                        $newCount = mysql_query("SELECT * FROM tbl_settingsp2a");
                                        $row = mysql_fetch_array($newCount);
                                        $criticalIncidentCount = $row['criticalIncidentCount'];

                                        $displayCritInc=mysql_query("SELECT tbl_p2bcritinc.criticalIncident,tbl_eformp2b.competency FROM tbl_p2bcritinc INNER JOIN tbl_eformp2b ON tbl_p2bcritinc.competencyName = tbl_eformp2b.competency LIMIT $criticalIncidentCount ") or die(mysql_error());
                                          $count=1;
                                          while($row2=mysql_fetch_array($displayCritInc)){ 
                                        ?>
                                          <?php echo $row2['criticalIncident']; echo " #".$count." :";?>  <br>
                                        <?php $count++;}?>
                                    </td>
                              </tr>
                          <?php }}?>
                        </tbody>
                  </table>

                  <div class="row">
                      <div class="col-sm-12 text-right text-danger">*to edit the number of critical incident please go to system settings</div>
                  </div>
              </div>
          </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>
<?php }?>
