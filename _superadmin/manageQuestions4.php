<?php include 'includes/header.php'; ?>
    <!-- Setting the treeview active -->
    <script type="text/javascript">
        document.getElementById("treeview_qm").className = "active menu-open"
    </script>
    <!-- End Setting the treeview active -->

  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Questionnaire Management
        <!-- <small>Version 2.0</small> -->
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="panel panel-default">
              <div class="panel-heading" style="background: black"><h4><b style="color:white;">Performance Evaluation Form for SOG - P4</b></h4></div>



                <div class="container-fluid" style="margin-top: 1em;">
                  <form method="post">
                    <a href="#" class="btn btn-success btn-md pull-right" data-toggle="modal" data-target="#addAreaModal"> <b> ADD AREA </b> </a>
                  </form>
                  <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_p4" class="btn btn-success" value="Add" style=" width:100px;" />
                                        </form>
                </div>
              <hr>

              <div class="panel-body">
                  <table class="table table-bordered">
                        <thead class="">
                          <tr>
                            <th class="col-sm-10">AREAS</th>
                            <th class="col-sm-1">FACTOR RATING</th>
                          </tr>
                        </thead>


                        <tbody>
                          <?php
                              $query_p4 = "SELECT * FROM tbl_eformp4";
                              // execute query 
                              $result_p4 = mysql_query($query_p4) or die ("Error in query: $query_p4. ".mysql_error());
                              // see if any rows were returned 
                              if (mysql_num_rows($result_p4) == 0)
                              { 
                                echo"<td colspan='5'><center><h4><b>There are no areas yet.</b></h4></center></td>";
                              }
                              else
                              {
                                $display_ep4=mysql_query("SELECT * FROM tbl_eformp4") or die(mysql_error());
                                    $counter = 1;
                                    while($row=mysql_fetch_array($display_ep4)){
                                        $r_id = $row['id'];
                                        $r_area = $row['areas'];
                              ?>
                              <tr> 
                                    <td><?php echo $row['areas'];?> </td>
                                    <td>
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <a href="#" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#updateAreaModal_<?php echo $r_id?>" title="edit area"><i class="fa fa-edit"></i></a>
                                            </div>

                                            <div class="col-sm-2">

                                            </div>
                                            <div class="col-sm-2">
                                                <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete_area_<?php echo $r_id?>" title="delete area"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </div>
                                        <!-- Add AREA modal -->
                                        <div id="updateAreaModal_<?php echo $r_id?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Update Area - <?php echo $r_area?></h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <form method="post">
                                                            <div class="form-group">
                                                                <label for="areaName">Area Name:</label>
                                                                <input type="hidden" name="area_id" value="<?php echo $r_id?>"/>
                                                                <input class="form-control" name="areaName" id="areaName" placeholder="Area Name" value="<?php echo $r_area?>"/>
                                                            </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary" name="btnUpdateArea">UPDATE</button>

                                                        </form>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <!-- END AREA modal -->

                                        <!-- Delete area Modal -->
                                        <div id="delete_area_<?php echo $r_id?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header" style="background-color: #2E7D32 !important;border-color: #2E7D32 !important;color: white">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Confirm</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h5>Are you sure to delete this area - <?php echo $r_area?> ?</h5>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <form method="post">
                                                            <input type="hidden" name="area_id" value="<?php echo $r_id?>"/>
                                                            <button type="submit" class="btn btn-success btn-md" name="btnDeleteArea">Yes</button>
                                                            <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">No</button>
                                                        </form>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- END Delete AREA modal -->
                                    </td>
                              </tr>
                              <?php $counter++;}}?>
                        </tbody>
                  </table>
              </div>
          </div>
    </section>
    <!-- /.content -->


        <!-- Add AREA modal -->
        <div id="addAreaModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Add Area</h4>
                  </div>
                  <div class="modal-body">

                      <form method="post">

                          <input type="hidden" name="employee_id" value="<?php echo $emp_id?>"/>

                          <div class="form-group">
                              <label for="areaName">Area Name:</label>
                              <input class="form-control" name="areaName" id="areaName" placeholder="Area Name"/>
                          </div>

                  </div>
                  <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" name="btnAddArea">ADD</button>

                      </form>
                  </div>

              </div>

          </div>
        </div>
        <!-- END AREA modal -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>
<?php
if(isset($_POST['btnAddArea'])){
    $areaName = $_POST['areaName'];
    mysql_query("INSERT INTO tbl_eformp4(areas) VALUES ('".addslashes($areaName)."')");
    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'The area has been added',
                  type: \"success\",
                  timer: 1000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageQuestions4.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageQuestions4.php\";
                    }
                  }
                )
			</script>
		";
}

if(isset($_POST['btnUpdateArea'])){
    $r_id = $_POST['area_id'];
    $areaName = $_POST['areaName'];
    mysql_query("UPDATE tbl_eformp4 SET areas = '".addslashes($areaName)."' WHERE id ='$r_id'");
    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'The area has been updated',
                  type: \"success\",
                  timer: 1000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageQuestions4.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageQuestions4.php\";
                    }
                  }
                )
			</script>
		";
}

if(isset($_POST['btnDeleteArea'])){
    $r_id = $_POST['area_id'];
    mysql_query("DELETE FROM tbl_eformp4 WHERE id = '$r_id'");
    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'The area has been deleted',
                  type: \"success\",
                  timer: 1000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageQuestions4.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageQuestions4.php\";
                    }
                  }
                )
			</script>
		";
}
?>

<?php 
if(isset($_POST["import_p4"])){ 

    //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            //parse data from csv file line by line
            while(($emapData = fgetcsv($csvFile, 10000, ",")) !== FALSE){
              
            $emapData0=ucfirst($emapData[0]);
            // $emapData1=ucfirst($emapData[1]);
            // $emapData2=$emapData[2];
            // $emapData1=strtoupper($emapData[1]);
            // $emapData2=strtoupper($emapData[2]);
            // $emapData3=strtoupper($emapData[3]);
            // $emapData4=strtoupper($emapData[4]);
            // $emapData5=strtoupper($emapData[5]);
            // $emapData6=strtoupper($emapData[6]);
            // $emapData7=strtoupper($emapData[7]);
            // $emapData8=strtoupper($emapData[8]);
            // $emapData9=strtoupper($emapData[9]);
            // $emapData10=strtoupper($emapData[10]);
            // $emapData11=strtoupper($emapData[11]); 
            // $emapData12=strtoupper($emapData[12]); 
            // $emapData13=strtoupper($emapData[13]); 

            
                    //insert member data into database
              //lastname firstname middlename specialization email contact
                    mysql_query("INSERT INTO tbl_eformp4 (areas) VALUES('$emapData0')") or die (mysql_error());
                     echo"
            <script type='text/javascript'>
                alert('Your file has been successfully imported.');
                open('manageQuestions4.php','_self');
            </script>
            ";
            }
            
            //close opened csv file
            fclose($csvFile);


           
        }
        else{
            echo"
            <script type='text/javascript'>
                alert('Error: Please check your format.');
                open('curriculum_importalllevel.php','_self');
            </script>
            ";
        }
      }
    }

?>
