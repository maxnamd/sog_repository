<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 11/10/2017
 * Time: 3:26 PM
 */
include 'includes/header.php';
?>
<!-- Setting the treeview active -->
<script type="text/javascript">
    $('#treeview5').removeClass("active menu-open");
    $('#treeview5').addClass('active menu-open');
</script>
<!-- End Setting the treeview active -->
<!-- Content Wrapper. Contains page content -->
<script>
    $(document).ready(function() {
        $('#tbl_missed').DataTable();
    } );
    $(document).ready(function() {
        $('#tbl_requests').DataTable();
    } );
</script>


<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

        <!--DIV OF MISSED EVALUATIONS-->
        <div class="box box-info" style="border-color: green">
            <div class="box-header with-border">
                <h3 class="box-title">Missed Evaluations</h3>

                <div class="pull-right" style="margin-right: 1em">
                <form action="viewEvaluations_missed_report.php">
                    <input type="submit" class="btn btn-success" value="REPORT"/>
                 </form>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin" id="tbl_missed">
                        <thead>
                            <tr>
                                <th class="col-sm-3">Department Head Name</th>
                                <th class="col-sm-2">Employee Name</th>
                                <th class="col-sm-2">Performance Cycle</th>
                                <th class="col-sm-2">Evaluation Status</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php

                            $queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
                            $dateEVRow = mysql_fetch_array($queryDateEV);
                            $startDateEV = $dateEVRow['startDate'];
                            $endDateEV = $dateEVRow['endDate'];
                            $performanceCycle = $startDateEV . '-' . $endDateEV;

                            $display_missed_ev=mysql_query("SELECT * FROM tbl_evaluation_results WHERE late_status = 'YES' AND performanceCycle != '$performanceCycle' ORDER BY employee_department") or die(mysql_error());

                            while($row=mysql_fetch_array($display_missed_ev)) {
                                $employee_department = $row['employee_department'];
                                $employee_firstname = $row['employee_firstName'];
                                $employee_middlename = $row['employee_middleName'];
                                $employee_lastname = $row['employee_lastName'];
                                $employee_fullName = $employee_firstname . ' ' . $employee_lastname;
                                $tbl_performanceCycle = $row['performanceCycle'];
                                $evaluation_stat = $row['evaluationStatus'];

                                $search_admin = mysql_query("SELECT * FROM tbl_admins WHERE department = '$employee_department'");
                                while($row_admin=mysql_fetch_array($search_admin)) {

                                    $head_firstname = $row_admin['firstname'];
                                    $head_middlename = $row_admin['middlename'];
                                    $head_lastname = $row_admin['lastname'];

                                    $head_fullname = $head_firstname . ' ' . $head_lastname;

                                }


                            ?>
                            <tr>
                                <td><?php echo $head_fullname?></td>
                                <td><?php echo $employee_fullName?></td>
                                <td><?php echo $tbl_performanceCycle?></td>

                                <td>
                                    <?php
                                    if($evaluation_stat == 'COMPLETED'){
                                        echo"<span class=\"col-sm-5 label label-success\">$evaluation_stat</span>";
                                    }
                                    else if($evaluation_stat == 'PROCESSING') {
                                        echo"<span class=\"col-sm-5 label label-warning\">$evaluation_stat</span>";
                                    }
                                    else {
                                        echo"<span class=\"col-sm-5 label label-danger\">$evaluation_stat</span>";

                                    }
                                    ?>




                                </td>
                            </tr>

                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!--END DIV OF MISSED EVALUATIONS-->

        <?php
        include 'phpfunctions/viewEvaluationRequests.php';
        ?>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>