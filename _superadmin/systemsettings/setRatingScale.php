<script>
    $(document).ready(function() {
        $('#tbl_kpi_rating').DataTable();
    } );
</script>

<div class="col-sm-12">
  		 <div class="col-sm-12">
        <div class="row">
                  <div class="panel panel-success">
                      <div class="panel-heading" style="background-color: #2b3819 !important;"> <label style="color: white !important;">KPI RATING SCALE </label></div>
                          <div class="panel-body">

                             <div class="container-fluid" style="margin-top: 1em;">
             
                                    <div class="col-sm-2">
                                        <a href="#" class="btn btn-success" style="background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7)" data-toggle="modal" data-target="#addKpiRatingScale"> &nbsp;&nbsp;<i class="fa fa-plus"></i> ADD KPI RATING SCALE &nbsp;</a>
                                    </div>
                                      <div class="col-sm-7"> </div>

                                     <div class="col-sm-2">
                                        <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_kpi_rating_scale" class="btn btn-success" value="Add" style=" width:100px;" />
                                        </form>
                                    </div>
                                </div>

                                <div class="panel-body">
                                  <table class="table table-bordered" id="_tbl_kpi_rating">
                                        <thead class="">
                                          <tr>
                                            <th colspan="2">Rating Value</th>
                                            <th colspan="1">Description</th>
                                            <th colspan="2">Action</th>
                                          </tr>
                                        </thead>

                                        <tbody>
                                             <?php
                                                $query = "SELECT * FROM tbl_KpiRatingScale";
                                                // execute query 
                                                $result = mysql_query($query) or die ("Error in query: $query. ".mysql_error()); 
                                                // see if any rows were returned 
                                                if (mysql_num_rows($result) == 0) 
                                                { 
                                                  echo"<td colspan='7'><center><h4><b>There are no KPI Ratings yet.</b></h4></center></td>";
                                                }
                                                else
                                                {
                                                  $display_KpiRatingScale=mysql_query("SELECT * FROM tbl_KpiRatingScale") or die(mysql_error());
                                                      
                                                      $counter=0;
                                                      while($row=mysql_fetch_array($display_KpiRatingScale)){ 
                                                      $counter++;
                                                ?>




                                                <tr> 
                                                    <td colspan="2"><b><?php echo $row['rating_value'];?></td>
                                                    <td><?php echo $row['description'];?> </td>
                                                    <td>
                                      <div class="col-sm-4"> <a href="#viewKpiRatingScale-<?php echo $row['id'];?>" class="btn btn-default" title="View" data-toggle="modal"> <i class="fa fa-search"></i></a> 

<!-- VIEW EMPLOYEE MODAL-->
<div class="modal fade" id="viewKpiRatingScale-<?php echo $row['id']; ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">View KPI Rating Scale</h4>
      </div>
        
      <div class="modal-body">

      <table class="table table-bordered">
          <form method="post" class="form-horizontal">
              <tr>
                  <td>
                    <label class="control-label">Rating Value: </label>
                  </td>

                  <td>
                    <input type="text" name="rating_value" id="" class="form-control" value="<?php echo $row['rating_value']?>" disabled>
                  </td>
                </tr>

                <tr>
                  <td>
                  <label class="control-label">Description: </label>
                  </td>

                  <td>
                    <input type="text" name="description" id="" class="form-control" value="<?php echo $row['description']?>" disabled>
                  </td>
               </tr>
               
    </div>

        </table>
                <div class="modal-footer">
                
                   
                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- VIEW EMPLOYEE MODAL-->

                                      </div> 

                                      <div class="col-sm-4"> 
                                        <a href="#editKpiRatingScale-<?php echo $row['id'];?>" class="btn btn-default" title="Edit" data-toggle="modal"> <i class="fa fa-edit"></i></a> 

                                      </div> 


<!-- EDIT EMPLOYEE MODAL-->
<div class="modal fade" id="editKpiRatingScale-<?php echo $row['id']; ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit KPI Rating Scale</h4>
      </div>
        
      <div class="modal-body">

        <table class="table table-bordered">

          <form method="post" class="form-horizontal">
                <tr>
                  <td>
                  <label class="control-label">Rating Value: </label>
                  </td>

                  <td>
                    <input type="text" name="rating_value" id="" class="form-control" value="<?php echo $row['rating_value']?>" onkeypress="return number(event)" maxlength="2">
                    <input type="hidden" name="id" id="" class="form-control" value="<?php echo $row['id']?>">
                  </td>
               </tr>

               <tr>
                 <td>
                  <label class="control-label">Description: </label>
                  </td>

                  <td>
                    <input type="text" name="kpi_description" id="" class="form-control" value="<?php echo $row['description']?>">
                 </td>
               </tr>
    </div>

        </table>
                <div class="modal-footer">
                
                    <input type="submit" class="btn btn-success" name="edit_KpiRatingScale" value="UPDATE RATING SCALE"  />
                    <input type="reset" class="btn btn-default" value="Clear"/>

                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- EDIT EMPLOYEE MODAL-->

                                      <div class="col-sm-2">
                                        <form method="post"> 
                                            <input type="hidden" name="hiddenID" value="<?php echo $row['id']?>"/>
                                        </form>


                                         <div class="col-sm-2">
                            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#approveModal_kpiRatingScale_<?php echo $counter?>" title="Delete"><i class="fa fa-trash" ></i></a>

                            <!-- Modal -->
                            <div id="approveModal_kpiRatingScale_<?php echo $counter?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header" style="background-color: #2E7D32 !important;border-color: #2E7D32 !important;color: white">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Confirm</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Are you sure you want to Delete This KPI Rating Scale?</h5>
                                        </div>
                                        <div class="modal-footer">
                                            <form method="post">
                                                <input type="hidden" name="admin_id_kpiRatingScale" value="<?php echo $row['id']?>"/>
                                            <button type="submit" class="btn btn-success btn-md" name="btnApprove_kpiRatingScale">Yes</button>
                                            <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">No</button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                                      </div>

                                  </td>
                            </tr>
                            <?php  }} ?>
                      </tbody>
                </table>
            </div>
        </div>


                                        </tbody>
                                    </table>

<!-- ADD NEW EMPLOYEE MODAL-->
<div class="modal fade" id="addKpiRatingScale" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add New KPI Rating Scale</h4>
      </div>
        
      <div class="modal-body">

          <form method="post" class="form-horizontal">
                  
                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Rating Value: </label>
                  <div class="col-sm-8">
                    <input type="text" name="rating_value" id="" class="form-control" placeholder="Rating Value" onkeypress="return number(event)" maxlength="2"/>
                  </div>
                </div>

                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Description: </label>
                  <div class="col-sm-8">
                    <input type="text" name="description" id="" class="form-control" placeholder="Description"/>
                  </div>
                </div>
    </div>

                <div class="modal-footer">
                
                    <input type="submit" class="btn btn-success" name="add_KpiRatingScale" value="ADD KPI RATING SCALE"  />
                    <input type="reset" class="btn btn-default" value="Clear"/>

                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- ADD NEW EMPLOYEE MODAL-->

                                        </tbody>
                                    </table>
                          </div>
                        </div>
                  </div>

<?php
if(isset($_POST['btnApprove_kpiRatingScale'])){
    $admin_id_kpiRatingScale = $_POST['admin_id_kpiRatingScale'];
    mysql_query("DELETE FROM tbl_kpiratingscale WHERE id = '$admin_id_kpiRatingScale'") or die (mysql_error());
    echo"
      <script type='text/javascript'>
        
        swal({
                  title: 'SUCCESS!',
                  text: 'The KPI Rating Scale has been Deleted!',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageSystemSettings.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageSystemSettings.php\";
                    }
                  }
                )
      </script>
    ";
}
?>