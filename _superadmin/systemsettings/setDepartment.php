<script>
    $(document).ready(function() {
        $('#tbl_departments').DataTable();
    } );
</script>

<div class="col-sm-12">
  		 <div class="col-sm-12">
        <div class="row">
                  <div class="panel panel-success">
                      <div class="panel-heading" style="background-color: #2b3819  !important;"> <label style="color: white !important;">DEPARTMENTS </label></div>
                          <div class="panel-body">

                             <div class="container-fluid" style="margin-top: 1em;">
             
                                    <div class="col-sm-2">
                                        <a href="#" class="btn btn-success" style="background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7)" data-toggle="modal" data-target="#addDepartment"> &nbsp;&nbsp;<i class="fa fa-plus"></i> ADD DEPARTMENT &nbsp;</a>
                                    </div>

                                    <div class="col-sm-7"> </div>

                                     <div class="col-sm-2">
                                        <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_department" class="btn btn-success" value="Add" style=" width:100px;" />
                                        </form>
                                    </div>
                                </div>

                                <div class="panel-body">
                                  <table class="table table-bordered" id="tbl_departments">
                                        <thead class="">
                                          <tr>
                                            <th colspan="2">Department Name</th>
                                            <th colspan="1">Initials</th>
                                            <th colspan="2">Action</th>
                                          </tr>
                                        </thead>

                                        <tbody>
                                             <?php
                                                $query = "SELECT * FROM tbl_departments";
                                                // execute query 
                                                $result = mysql_query($query) or die ("Error in query: $query. ".mysql_error()); 
                                                // see if any rows were returned 
                                                if (mysql_num_rows($result) == 0) 
                                                { 
                                                  echo"<td colspan='7'><center><h4><b>There are no Departments yet.</b></h4></center></td>";
                                                }
                                                else
                                                {
                                                  $display_departments=mysql_query("SELECT * FROM tbl_departments") or die(mysql_error());
                                                      
                                                      $counter=0;
                                                      while($row=mysql_fetch_array($display_departments)){ 
                                                      $counter++;
                                                ?>




                                                <tr> 
                                                    <td colspan="2"><b><?php echo $row['department_name'];?></td>
                                                    <td><?php echo $row['initials'];?> </td>
                                                    <td>
                                      <div class="col-sm-4"> <a href="#viewDepartment-<?php echo $row['id'];?>" class="btn btn-default" title="View" data-toggle="modal"> <i class="fa fa-search"></i></a> 

<!-- VIEW EMPLOYEE MODAL-->
<div class="modal fade" id="viewDepartment-<?php echo $row['id']; ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">View Department</h4>
      </div>
        
      <div class="modal-body">

      <table class="table table-bordered">

      <tr>
          <form method="post" class="form-horizontal">
                <td>
                  <label class="control-label">Department Name: </label>
                </td>
                <td>
                    <input type="text" name="department_name" id="" class="form-control" value="<?php echo $row['department_name']?>" style="width: 400px;" disabled>
                </td>
        </tr>
        <tr>

                <td>
                  <label class="control-label">Initials: </label>
                </td>
                <td>
                    <input type="text" name="initials" id="" class="form-control" value="<?php echo $row['initials']?>" style="width: 400px"; disabled>
                </td>         
    </div>
    </tr>
    </table>

                <div class="modal-footer">
                
                   
                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- VIEW EMPLOYEE MODAL-->

                                      </div> 

                                      <div class="col-sm-4"> 
                                        <a href="#editDepartment-<?php echo $row['id'];?>" class="btn btn-default" title="Edit" data-toggle="modal"> <i class="fa fa-edit"></i></a> 

                                      </div> 


<!-- EDIT EMPLOYEE MODAL-->
<div class="modal fade" id="editDepartment-<?php echo $row['id']; ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Employee</h4>
      </div>
        
      <div class="modal-body">

      <table class="table table-bordered">
          <form method="post" class="form-horizontal">
              <tr>
                <td>
                  <label class="control-label">Department Name: </label>
                </td>
                <td>
                    <input type="text" name="department_name" id="" class="form-control" value="<?php echo $row['department_name']?>" onkeypress="return letter(event)" style="width: 400px;">
                    <input type="hidden" name="id" id="" class="form-control" value="<?php echo $row['id']?>">
                </td>
            </tr>

            <tr>
                <td>
                  <label class="col-sm-3 control-label">Initials: </label>
                </td>
                <td>
                    <input type="text" name="initials" id="" class="form-control" value="<?php echo $row['initials']?>" onkeypress="return letter(event)" style="width: 400px;">
                </td>
            </tr>
               
    </div>
      </table>
                <div class="modal-footer">
                
                    <input type="submit" class="btn btn-success" name="edit_department" value="UPDATE DEPARTMENT"  />
                    <input type="reset" class="btn btn-default" value="Clear"/>

                </div>
              
          </form>
      
      </div>
    </div>
  </div>
</div>
<!-- EDIT EMPLOYEE MODAL-->

                                      <div class="col-sm-2">
                                        <form method="post"> 
                                            <input type="hidden" name="hiddenID" value="<?php echo $row['id']?>"/>
                                        </form>
                                        <a href="#" class="btn btn-default" data-toggle="modal" data-target="#approveModal_<?php echo $counter?>" title="Delete"><i class="fa fa-trash" ></i></a>

                                         <div id="approveModal_<?php echo $counter?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header" style="background-color: #2E7D32 !important;border-color: #2E7D32 !important;color: white">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Confirm</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Are you sure you want to Delete This Department?</h5>
                                        </div>
                                        <div class="modal-footer">
                                            <form method="post">
                                                <input type="hidden" name="admin_id" value="<?php echo $row['id']?>"/>
                                            <button type="submit" class="btn btn-success btn-md" name="btnApprove">Yes</button>
                                            <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">No</button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>


                                      </div>
                                  </td>
                            </tr>
                            <?php  }} ?>
                      </tbody>
                </table>
            </div>
        </div>


                                        </tbody>
                                    </table>

<!-- ADD NEW EMPLOYEE MODAL-->
<div class="modal fade" id="addDepartment" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add New Department</h4>
      </div>
        
      <div class="modal-body">

          <form method="post" class="form-horizontal">
                  
                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Department Name: </label>
                  <div class="col-sm-8">
                    <input type="text" name="department_name" id="" class="form-control" placeholder="Department Name" onkeypress="return letter(event)"/>
                  </div>
                </div>

                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Initials: </label>
                  <div class="col-sm-8">
                    <input type="text" name="initials" id="" class="form-control" placeholder="Initials" nkeypress="return letter(event)"/>
                  </div>
                </div>
               
    </div>

                <div class="modal-footer">
                
                    <input type="submit" class="btn btn-success" name="add_department" value="ADD DEPARTMENT"  />
                    <input type="reset" class="btn btn-default" value="Clear"/>

                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- ADD NEW EMPLOYEE MODAL-->

                                        </tbody>
                                    </table>
                          </div>
                        </div>
                  </div>

<?php
if(isset($_POST['btnApprove'])){
    $admin_id = $_POST['admin_id'];
    mysql_query("DELETE FROM tbl_departments WHERE id = '$admin_id'") or die (mysql_error());
    echo"
      <script type='text/javascript'>
        
        swal({
                  title: 'SUCCESS!',
                  text: 'The Employee Record has been Deleted!',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageSystemSettings.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageSystemSettings.php\";
                    }
                  }
                )
      </script>
    ";
}


?>