<div class="col-md-6">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h4 class="box-title"><b>Number of Rubrics per Area in P3 Form</b></h4>

        <div class="box-tools">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>

      <div class="box-body ">
        <form name="" method="POST">
          <?php 
            $p3Count = mysql_query("SELECT * FROM tbl_settingsp3");
            $p3Row = mysql_fetch_array($p3Count);
            $rubricCount = $p3Row['rubricCount'];
          ?>

          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-2"><label class="label-control">Quantity</label></div>

              <div class="col-sm-10"><input type="" name="newQuantityP3" class="form-control" value="<?php echo$rubricCount;?>" id="newQuantityP3"/></div>
                <input type="hidden" name="safe_newQuantityP3" class="form-control" value="<?php echo$rubricCount;?>" id="safe_newQuantityP3"/>
            </div>

            <div class="row" style="float: right">
                <input type="button" class="btn btn-md" style="margin-top: 1em;" value="EDIT" onclick="enableSaveButton_P3();" id="edit_cancel_P3"/>
                <input type="submit" class="btn btn-info btn-md" style="margin-top: 1em;" name="btnSaveP3" value="SAVE" id="btnSaveP3"/>
              
            </div>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
  </div>
</div>

<script type="text/javascript">
    $('#newQuantityP3').attr('disabled', true);
    $('#btnSaveP3').attr('disabled', true);

    $('#edit_cancel_P3').addClass('btn-warning');
    $('#btnSaveDateKPI_P3').addClass('btn-success');

    function enableSaveButton_P3(){
        var btn_edit_cancel_P3 = document.getElementById('edit_cancel_P3').value;

        if(btn_edit_cancel_P3=="EDIT"){
            document.getElementById('edit_cancel_P3').value = 'CANCEL'

            $('#newQuantityP3').attr('disabled', false);

            $('#btnSaveP3').attr('disabled', false);

        }
        else{
            document.getElementById('edit_cancel_P3').value = 'EDIT';
            document.getElementById('newQuantityP3').value = document.getElementById('safe_newQuantityP3').value;
            $('#newQuantityP3').attr('disabled', true);
            $('#btnSaveP3').attr('disabled', true);
        }


    }


</script>