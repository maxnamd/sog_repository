<div class="col-md-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h4 class="box-title"><b>Set the Date for Creation of KPI</b></h4>

        <div class="box-tools">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>

      <div class="box-body ">
          <?php
          $queryDateEV = mysql_query("SELECT * FROM tbl_datekpi");
          $dateEVRow = mysql_fetch_array($queryDateEV);
          $startDateEV = $dateEVRow['startDate'];
          $endDateEV = $dateEVRow['endDate'];

          ?>
        <form name="" method="POST">
          
          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-6">
                <div class="col-sm-4"><label class="label-control">Start Date</label></div>
                <div class="col-sm-8">
                  <input type="text" name="startDate" id="startDate" class="date-picker" class="form-control" value="<?php echo $startDateEV?>"/>
                    <input type="hidden" id="defaultStartDate" value="<?php echo $startDateEV?>"/>
                </div>

                
              </div>

              <div class="col-sm-6">
                <div class="col-sm-4"><label class="label-control">End Date</label></div>
                <div class="col-sm-8">
                  <input type="text" name="endDate" id="endDate" class="date-picker" class="form-control" value="<?php echo $endDateEV?>"/>
                    <input type="hidden" id="defaultEndDate" value="<?php echo $endDateEV?>"/>

                </div>
                  


                
              </div>
            </div>

            <div class="row" style="float: right">
                <input type="button" class="btn btn-md" style="margin-top: 1em;" value="EDIT" onclick="enableSaveButton_SD();" id="edit_cancel"/>

                <input type="submit" class="btn btn-md" style="margin-top: 1em;" name="btnSaveDateKPI" value="SAVE" id="btnSaveDateEV"/>
              
            </div>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
  </div>
</div>

<script type="text/javascript">
    $('#startDate').attr('disabled', true);
    $('#endDate').attr('disabled', true);
    $('#btnSaveDateEV').attr('disabled', true);


    document.getElementById("edit_cancel").classList.add('btn-warning');
    document.getElementById("btnSaveDateEV").classList.add('btn-success');

    function enableSaveButton_SD(){
        var btn_edit_cancel = document.getElementById('edit_cancel').value;

        if(btn_edit_cancel=="EDIT"){
            document.getElementById('edit_cancel').value = 'CANCEL'

            $('#startDate').attr('disabled', false);
            $('#endDate').attr('disabled', false);

            $('#btnSaveDateEV').attr('disabled', false);

        }
        else{
            document.getElementById('edit_cancel').value = 'EDIT'
            $('#startDate').attr('disabled', true);
            $('#endDate').attr('disabled', true);
            $('#btnSaveDateEV').attr('disabled', true);

            document.getElementById('endDate').value = document.getElementById('defaultEndDate').value;
            document.getElementById('startDate').value = document.getElementById('defaultStartDate').value;
        }


    }
</script>