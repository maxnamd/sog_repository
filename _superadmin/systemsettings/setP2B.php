<div class="col-md-6">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h4 class="box-title"><b>Number of Critical Incident per Competency in P2B Form</b></h4>

        <div class="box-tools">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>

      <div class="box-body ">
        <form name="" method="POST">
          <?php 
            $p2Count = mysql_query("SELECT * FROM tbl_settingsp2a");
            $p2Row = mysql_fetch_array($p2Count);
            $criticalIncidentCount = $p2Row['criticalIncidentCount'];
          ?>

          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-2"><label class="label-control">Quantity</label></div>

              <div class="col-sm-10"><input type="text" name="newQuantityP2" class="form-control" value="<?php echo$criticalIncidentCount;?>" id="newQuantityP2"/></div>
                <input type="hidden" name="safe_newQuantityP2" id="safe_newQuantityP2" value="<?php echo$criticalIncidentCount;?>"/>
            </div>

            <div class="row" style="float: right">
              <input type="button" class="btn btn-md" style="margin-top: 1em;" value="EDIT" onclick="enableSaveButton_P2B();" id="edit_cancel_P2B"/>
              <input type="submit" class="btn btn-info btn-md" style="margin-top: 1em;" name="btnSaveP2" value="SAVE" id="btnSaveP2"/>
              
            </div>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
  </div>
</div>

<script type="text/javascript">
    $('#newQuantityP2').attr('disabled', true);
    $('#btnSaveP2').attr('disabled', true);

    $('#edit_cancel_P2B').addClass('btn-warning');
    $('#btnSaveDateKPI_P2B').addClass('btn-success');



    function enableSaveButton_P2B(){
        var btn_edit_cancel_P2B = document.getElementById('edit_cancel_P2B').value;

        if(btn_edit_cancel_P2B=="EDIT"){
            document.getElementById('edit_cancel_P2B').value = 'CANCEL'

            $('#newQuantityP2').attr('disabled', false);

            $('#btnSaveP2').attr('disabled', false);

        }
        else{
            document.getElementById('edit_cancel_P2B').value = 'EDIT'
            document.getElementById('newQuantityP2').value = document.getElementById('safe_newQuantityP2').value;
            $('#newQuantityP2').attr('disabled', true);
            $('#btnSaveP2').attr('disabled', true);
        }


    }


</script>