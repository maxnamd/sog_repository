<!-- P R O F E C I E N C Y   L E V E L -->     
<script>
    $(document).ready(function() {
        $('#tbl_proficiency_level').DataTable();
    } );
</script>

         	<div class="col-sm-12">
  		 <div class="col-sm-12">
        <div class="row">
                  <div class="panel panel-success">
                      <div class="panel-heading" style="background-color: #2b3819 !important;"> <label style="color: white !important;">JOB LEVELS </label></div>
                          <div class="panel-body">

                             <div class="container-fluid" style="margin-top: 1em;">
             
                                    <div class="col-sm-2">
                                        <a href="#" class="btn btn-success" style="background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7)" data-toggle="modal" data-target="#addJobLevel"> &nbsp;&nbsp;<i class="fa fa-plus"></i> ADD JOB LEVEL &nbsp;</a>
                                    </div>
                                     <div class="col-sm-7"> </div>

                                     <div class="col-sm-2">
                                        <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_job_level" class="btn btn-success" value="Add" style=" width:100px;" />
                                        </form>
                                    </div>
                                </div>

                                <div class="panel-body">
                                  <table class="table table-bordered" id="_tbl_proficiency_level">
                                        <thead class="">
                                          <tr>
                                            <th colspan="2">Code</th>
                                            <th colspan="1">Position</th>
                                            <th colspan="2">Department</th>
                                            <th colspan="1">Level</th>
                                            <th colspan="2">Action</th>
                                          </tr>
                                        </thead>

                                        <tbody>
                                             <?php
                                                $query = "SELECT * FROM tbl_job_level";
                                                // execute query 
                                                $result = mysql_query($query) or die ("Error in query: $query. ".mysql_error()); 
                                                // see if any rows were returned 
                                                if (mysql_num_rows($result) == 0) 
                                                { 
                                                  echo"<td colspan='7'><center><h4><b>There are no Job Level(s) yet.</b></h4></center></td>";
                                                }
                                                else
                                                {
                                                  $display_job_level=mysql_query("SELECT * FROM tbl_job_level") or die(mysql_error());
                                                      
                                                      $counter=0;
                                                      while($row=mysql_fetch_array($display_job_level)){ 
                                                      $counter++;
                                                ?>




                                                <tr> 
                                                    <td colspan="2"><b><?php echo '<center>'. $row['code']. '</center>';?></td>
                                                    <td colspan="2"><b><?php echo '<center>'. $row['position']. '</center>';?></td>
                                                    <td><?php echo $row['department'];?> </td>
                                                    <td><?php echo $row['level'];?> </td>
                                                    <td>
                                      <div class="col-sm-4"> <a href="#viewJobLevel-<?php echo $row['id'];?>" class="btn btn-default" title="View" data-toggle="modal"> <i class="fa fa-search"></i></a> 

<!-- VIEW PROFICIENCY LEVEL MODAL-->
<div class="modal fade" id="viewJobLevel-<?php echo $row['id']; ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">View Job Level</h4>
      </div>
        
      <div class="modal-body">

          <table class="table table-bordered">
          <form method="post" class="form-horizontal">
               <tr>
                  <td>
                  <label class="control-label"> Code: </label>
                  </td>

                  <td>
                    <input type="text" name="value" id="" class="form-control" value="<?php echo $row['code']?>" disabled>
                  </td>
               </tr>

               <tr>
                 <td>
                  <label class="control-label"> Position: </label>
                </td>

                <td>
                    <input type="text" name="initials" id="" class="form-control" value="<?php echo $row['position']?>" disabled>
                </td>
                </tr>

                <tr>
                  <td>
                  <label class="control-label">Department: </label>
                  </td>

                  <td>
                    <input type="text" name="title" id="" class="form-control" value="<?php echo $row['department']?>" disabled>
                  </td>
                </tr>

                 <tr>
                  <td>
                  <label class="control-label">Level: </label>
                  </td>

                  <td>
                    <input type="text" name="title" id="" class="form-control" value="<?php echo $row['level']?>" disabled>
                  </td>
                </tr>
               
    </div>

      </table>
                <div class="modal-footer">
                
                   
                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- VIEW PROFICIENCY LEVEL MODAL-->

                                      </div> 

                                      <div class="col-sm-3"> 
                                        <a href="#ediJobLevel-<?php echo $row['id'];?>" class="btn btn-default" title="Edit" data-toggle="modal"> <i class="fa fa-edit"></i></a> 

                                      </div> 


<!-- EDIT PROFICIENCY LEVEL MODAL-->
<div class="modal fade" id="ediJobLevel-<?php echo $row['id']; ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Job Level</h4>
      </div>
        
      <div class="modal-body">

      <table class="table table-bordered">

          <form method="post" class="form-horizontal">
              <tr>
                <td>
                  <label class="control-label">Code: </label>
                 </td>

                 <td>
                    <input type="text" name="code" id="" class="form-control" value="<?php echo $row['code']?>" onkeypress="return number(event)" maxlength="2">
                    <input type="hidden" name="id" id="" class="form-control" value="<?php echo $row['id']?>">
                </td>
                </tr>

                <tr>
                  <td>
                  <label class="control-label">Position: </label>
                  </td>
                  <td>
                    <input type="text" name="position" id="" class="form-control" value="<?php echo $row['position']?>">
                    <input type="hidden" name="id" id="" class="form-control" value="<?php echo $row['id']?>">
                  </td>
                </tr>

                <tr>
                 <td>
                  <label class="control-label">Department: </label>
                  </td>

                  <td>
                    <input type="text" name="department" id="" class="form-control" value="<?php echo $row['department']?>">
                  </td>
               </tr>

                 <tr>
                 <td>
                  <label class="control-label">Level: </label>
                  </td>

                  <td>
                    <select name="level" class="form-control">
                    <option><?php echo $row['level'];?></option>
                    <option disabled>-----------------------</option>
                    <option>DEPARTMENT HEAD</option>
                    <option>SOG EMPLOYEE</option>
                    </select>
                  </td>
               </tr>
               
    </div>

      </table>
                <div class="modal-footer">
                
                    <input type="submit" class="btn btn-success" name="edit_job_level" value="UPDATE JOB LEVEL"  />
                    <input type="reset" class="btn btn-default" value="Clear"/>

                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- EDIT PROFICIENCY LEVEL MODAL-->

                                      <div class="col-sm-2">
                                        <form method="post"> 
                                            <input type="hidden" name="hiddenID" value="<?php echo $row['id']?>"/>
                                        </form>

                                           <div class="col-sm-2">
                            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#approveModal_jobLevel_<?php echo $counter?>" title="Delete"><i class="fa fa-trash" ></i></a>

                            <!-- Modal -->
                            <div id="approveModal_jobLevel_<?php echo $counter?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header" style="background-color: #2E7D32 !important;border-color: #2E7D32 !important;color: white">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Confirm</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Are you sure you want to Delete This Job Level?</h5>
                                        </div>
                                        <div class="modal-footer">
                                            <form method="post">
                                                <input type="hidden" name="admin_id_proficiencyLevel" value="<?php echo $row['id']?>"/>
                                            <button type="submit" class="btn btn-success btn-md" name="btnApprove_jobLevel">Yes</button>
                                            <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">No</button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                                      </div>
                                  </td>
                            </tr>
                            <?php  }} ?>
                      </tbody>
                </table>
            </div>
        </div>


                                        </tbody>
                                    </table>

<!-- ADD NEW PROFICIENCY LEVEL MODAL-->
<div class="modal fade" id="addJobLevel" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add New Job Level</h4>
      </div>
        
      <div class="modal-body">

          <form method="post" class="form-horizontal">
                  
                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Code: </label>
                  <div class="col-sm-8">
                    <input type="text" name="code" id="" class="form-control" placeholder="Code" />
                  </div>
                </div>

                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Position: </label>
                  <div class="col-sm-8">
                    <input type="text" name="position" id="" class="form-control" placeholder="Position"/>
                  </div>
                </div>

                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Department: </label>
                  <div class="col-sm-8">
                    <input type="text" name="department" id="" class="form-control" placeholder="Department"/>
                  </div>
                </div>

                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Level: </label>
                  <div class="col-sm-8">
                    <select name="level" class="form-control">
                    <option>DEPARTMENT HEAD</option>
                    <option>SOG EMPLOYEE</option>
                    </select>
                  </div>
                </div>
    </div>

                <div class="modal-footer">
                
                    <input type="submit" class="btn btn-success" name="add_job_level" value="ADD JOB LEVEL"  />
                    <input type="reset" class="btn btn-default" value="Clear"/>

                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- ADD NEW EMPLOYEE MODAL-->

                                        </tbody>
                                    </table>

                          </div>
                        </div>
                  </div>
        
      
<!--E N D  O F  P R O F E C I E N C Y   L E V E L -->

<?php
if(isset($_POST['btnApprove_jobLevel'])){
    $admin_id_proficiencyLevel = $_POST['admin_id_proficiencyLevel'];
    mysql_query("DELETE FROM tbl_job_level WHERE id = '$admin_id_proficiencyLevel'") or die (mysql_error());
    echo"
      <script type='text/javascript'>
        
        swal({
                  title: 'SUCCESS!',
                  text: 'The Job Level has been Deleted!',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageSystemSettings.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageSystemSettings.php\";
                    }
                  }
                )
      </script>
    ";
}
?>