<!-- P R O F E C I E N C Y   L E V E L -->     
<script>
    $(document).ready(function() {
        $('#tbl_proficiency_level').DataTable();
    } );
</script>

         	<div class="col-sm-12">
  		 <div class="col-sm-12">
        <div class="row">
                  <div class="panel panel-success">
                      <div class="panel-heading" style="background-color: #2b3819 !important;"> <label style="color: white !important;">PROFICIENCY LEVEL </label></div>
                          <div class="panel-body">

                             <div class="container-fluid" style="margin-top: 1em;">
             
                                    <div class="col-sm-2">
                                        <a href="#" class="btn btn-success" style="background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7)" data-toggle="modal" data-target="#addProficiencyLevel"> &nbsp;&nbsp;<i class="fa fa-plus"></i> ADD PROFICIENCY LEVEL &nbsp;</a>
                                    </div>
                                     <div class="col-sm-7"> </div>

                                     <div class="col-sm-2">
                                        <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_proficiency_level" class="btn btn-success" value="Add" style=" width:100px;" />
                                        </form>
                                    </div>
                                </div>

                                <div class="panel-body">
                                  <table class="table table-bordered" id="_tbl_proficiency_level">
                                        <thead class="">
                                          <tr>
                                            <th colspan="2">Values</th>
                                            <th colspan="1">Initials</th>
                                            <th colspan="2">Content</th>
                                            <th colspan="2">Action</th>
                                          </tr>
                                        </thead>

                                        <tbody>
                                             <?php
                                                $query = "SELECT * FROM tbl_proficiency_level";
                                                // execute query 
                                                $result = mysql_query($query) or die ("Error in query: $query. ".mysql_error()); 
                                                // see if any rows were returned 
                                                if (mysql_num_rows($result) == 0) 
                                                { 
                                                  echo"<td colspan='7'><center><h4><b>There are no Proficiency Level(s) yet.</b></h4></center></td>";
                                                }
                                                else
                                                {
                                                  $display_departments=mysql_query("SELECT * FROM tbl_proficiency_level") or die(mysql_error());
                                                      
                                                      $counter=0;
                                                      while($row=mysql_fetch_array($display_departments)){ 
                                                      $counter++;
                                                ?>




                                                <tr> 
                                                    <td colspan="2"><b><?php echo '<center>'. $row['value']. '</center>';?></td>
                                                    <td colspan="2"><b><?php echo '<center>'. $row['initials']. '</center>';?></td>
                                                    <td><?php echo '<strong>'.$row['title'].'</strong>'. ' ' .$row['content'];?> </td>
                                                    <td>
                                      <div class="col-sm-4"> <a href="#viewProficienlyLevel-<?php echo $row['id'];?>" class="btn btn-default" title="View" data-toggle="modal"> <i class="fa fa-search"></i></a> 

<!-- VIEW PROFICIENCY LEVEL MODAL-->
<div class="modal fade" id="viewProficienlyLevel-<?php echo $row['id']; ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">View Proficiency Level</h4>
      </div>
        
      <div class="modal-body">

          <table class="table table-bordered">
          <form method="post" class="form-horizontal">
               <tr>
                  <td>
                  <label class="control-label"> Value: </label>
                  </td>

                  <td>
                    <input type="text" name="value" id="" class="form-control" value="<?php echo $row['value']?>" disabled>
                  </td>
               </tr>

               <tr>
                 <td>
                  <label class="control-label"> Initial: </label>
                </td>

                <td>
                    <input type="text" name="initials" id="" class="form-control" value="<?php echo $row['initials']?>" disabled>
                </td>
                </tr>

                <tr>
                  <td>
                  <label class="control-label">Title: </label>
                  </td>

                  <td>
                    <input type="text" name="title" id="" class="form-control" value="<?php echo $row['title']?>" disabled>
                  </td>
                </tr>

                <tr>
                  <td>
                  <label class="control-label">Content: </label>
                  </td>

                  <td>
                    <input type="text" name="content" id="" class="form-control" value="<?php echo $row['content']?>" disabled>
                  </td>
                </tr>
               
    </div>

      </table>
                <div class="modal-footer">
                
                   
                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- VIEW PROFICIENCY LEVEL MODAL-->

                                      </div> 

                                      <div class="col-sm-3"> 
                                        <a href="#editProficiencyLevel-<?php echo $row['id'];?>" class="btn btn-default" title="Edit" data-toggle="modal"> <i class="fa fa-edit"></i></a> 

                                      </div> 


<!-- EDIT PROFICIENCY LEVEL MODAL-->
<div class="modal fade" id="editProficiencyLevel-<?php echo $row['id']; ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Proficiency Level</h4>
      </div>
        
      <div class="modal-body">

      <table class="table table-bordered">

          <form method="post" class="form-horizontal">
              <tr>
                <td>
                  <label class="control-label">Value: </label>
                 </td>

                 <td>
                    <input type="text" name="value" id="" class="form-control" value="<?php echo $row['value']?>" onkeypress="return number(event)" maxlength="2">
                    <input type="hidden" name="id" id="" class="form-control" value="<?php echo $row['id']?>">
                </td>
                </tr>

                <tr>
                  <td>
                  <label class="control-label">Initial: </label>
                  </td>
                  <td>
                    <input type="text" name="initials" id="" class="form-control" value="<?php echo $row['initials']?>">
                    <input type="hidden" name="id" id="" class="form-control" value="<?php echo $row['id']?>">
                  </td>
                </tr>

                <tr>
                 <td>
                  <label class="control-label">Title: </label>
                  </td>

                  <td>
                    <input type="text" name="title" id="" class="form-control" value="<?php echo $row['title']?>">
                  </td>
               </tr>

               <tr>
                <td>
                  <label class="control-label">Content: </label>
                </td>

                <td>
                    <input type="text" name="content" id="" class="form-control" value="<?php echo $row['content']?>">
                </td>
                </tr>
               
    </div>

      </table>
                <div class="modal-footer">
                
                    <input type="submit" class="btn btn-success" name="edit_proficiency_level" value="UPDATE PROFICIENCY LEVEL"  />
                    <input type="reset" class="btn btn-default" value="Clear"/>

                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- EDIT PROFICIENCY LEVEL MODAL-->

                                      <div class="col-sm-2">
                                        <form method="post"> 
                                            <input type="hidden" name="hiddenID" value="<?php echo $row['id']?>"/>
                                        </form>

                                           <div class="col-sm-2">
                            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#approveModal_proficiencyLevel_<?php echo $counter?>" title="Delete"><i class="fa fa-trash" ></i></a>

                            <!-- Modal -->
                            <div id="approveModal_proficiencyLevel_<?php echo $counter?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header" style="background-color: #2E7D32 !important;border-color: #2E7D32 !important;color: white">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Confirm</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Are you sure you want to Delete This Proficiency Level?</h5>
                                        </div>
                                        <div class="modal-footer">
                                            <form method="post">
                                                <input type="hidden" name="admin_id_proficiencyLevel" value="<?php echo $row['id']?>"/>
                                            <button type="submit" class="btn btn-success btn-md" name="btnApprove_proficiencyLevel">Yes</button>
                                            <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">No</button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                                      </div>
                                  </td>
                            </tr>
                            <?php  }} ?>
                      </tbody>
                </table>
            </div>
        </div>


                                        </tbody>
                                    </table>

<!-- ADD NEW PROFICIENCY LEVEL MODAL-->
<div class="modal fade" id="addProficiencyLevel" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add New Proficiency Level</h4>
      </div>
        
      <div class="modal-body">

          <form method="post" class="form-horizontal">
                  
                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Value: </label>
                  <div class="col-sm-8">
                    <input type="text" name="value" id="" class="form-control" placeholder="Value" onkeypress="return number(event)" maxlength="2" />
                  </div>
                </div>

                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Initial: </label>
                  <div class="col-sm-8">
                    <input type="text" name="initials" id="" class="form-control" placeholder="Initial"/>
                  </div>
                </div>

                 <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Title: </label>
                  <div class="col-sm-8">
                    <input type="text" name="title" id="" class="form-control" placeholder="Title"/>
                  </div>
                </div>

                <div class="form-group" id="">
                  <label class="col-sm-3 control-label">Content: </label>
                  <div class="col-sm-8">
                    <input type="text" name="content" id="" class="form-control" placeholder="Content"/>
                  </div>
                </div>
               
    </div>

                <div class="modal-footer">
                
                    <input type="submit" class="btn btn-success" name="add_proficiency_level" value="ADD PROFICIENCY LEVEL"  />
                    <input type="reset" class="btn btn-default" value="Clear"/>

                </div>
              
          </form>
      </div>
    </div>
  </div>
</div>
<!-- ADD NEW EMPLOYEE MODAL-->

                                        </tbody>
                                    </table>

                          </div>
                        </div>
                  </div>
        
      
<!--E N D  O F  P R O F E C I E N C Y   L E V E L -->

<?php
if(isset($_POST['btnApprove_proficiencyLevel'])){
    $admin_id_proficiencyLevel = $_POST['admin_id_proficiencyLevel'];
    mysql_query("DELETE FROM tbl_proficiency_level WHERE id = '$admin_id_proficiencyLevel'") or die (mysql_error());
    echo"
      <script type='text/javascript'>
        
        swal({
                  title: 'SUCCESS!',
                  text: 'The Proficiency Level has been Deleted!',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageSystemSettings.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageSystemSettings.php\";
                    }
                  }
                )
      </script>
    ";
}
?>