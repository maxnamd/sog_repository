
<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 9/4/2017
 * Time: 8:51 PM
 */
?>

<?php include 'includes/header.php'; ?>

<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview5").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->
    <script>
        $(document).ready(function() {
            $('#tbl_EV').DataTable();
        } );
    </script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Evaluations Results
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- TABLE: STATUS KRA -->
        <div class="box box-info" style="border-color: green">
            <div class="box-header with-border">
                <h3 class="box-title">History of Performance Cycle</h3>

                <!-- <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin" id="tbl_EV">
                        <thead>
                        <tr>
                            <th>Department Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
            

                        <?php
                        $display_status=mysql_query("SELECT * FROM tbl_rating_execute GROUP BY performanceCycle ORDER BY performanceCycle DESC ") or die(mysql_error());

                        while($row=mysql_fetch_array($display_status)){

                            $performanceCycle = $row['performanceCycle'];
                            //$end_date = $row['endDate'];
                            echo "<tr>";
                            ?>
                            
                                <td>
                                    <?php echo $performanceCycle;?>  
                                </td>
                                <form method="POST">
                                <td> 
                                    <input type="hidden" name="performanceCycle" value="<?php echo $performanceCycle?>"/> 
                                    <input type="submit" class="btn btn-info btn-sm" name="btn_pass_to_history" value="View"/>
                                </td>
                                </form>
                            </tr>
                            <?php } ?>



                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

                <button class="btn btn-sm btn-default btn-flat pull-right" disabled=""></button>
            </div>
            <!-- /.box-footer -->
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>

<?php
if (isset($_POST['btn_pass_to_history'])){
    $_SESSION['performanceCycle'] = $_POST['performanceCycle'];
    
    echo "
    <script type='text/javascript'>
    document.location.href='history_list_top_10.php';
    </script>";
}
?>