<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 9/4/2017
 * Time: 8:51 PM
 */
?>

<?php include 'includes/header.php'; ?>
<?php
$departmentEV = $_SESSION['departmentEV'];
$departmentOwner = $_SESSION['departmentOwner'];

echo "<script>alert('$departmentEV');</script>";
?>


<?php
/*departmentEV
departmentOwner*/
if (isset($_POST['btnPassToPerformanceCycle'])){
    $_SESSION['employee_username'] = $_POST['employee_username'];
    $_SESSION['performanceCycle_EV'] = $_POST['performanceCycle_EV'];
}
?>
<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview5").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->
    <script>
        $(document).ready(function() {
            $('#tbl_EV').DataTable();
        } );
    </script>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Evaluations Results
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- TABLE: STATUS KRA -->
        <div class="box box-info" style="border-color: green">
            <div class="box-header with-border">
                <h3 class="box-title">Evaluation Results of <b><?php echo $departmentEV?></b></h3>

                <!-- <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">

                    <table class="table no-margin" id="tbl_EV">
                        <thead>
                        <tr>
                            <th>Employee Last Name</th>
                            <th>Employee First Name</th>
                            <th>Employee Position</th>
                            <th>Number of Evaluations</th>
                            <th>ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
                        $dateEVRow = mysql_fetch_array($queryDateEV);
                        $startDateEV = $dateEVRow['startDate'];
                        $endDateEV = $dateEVRow['endDate'];
                        $top_performanceCycle = $startDateEV . '-' . $endDateEV;




                        ?>

                        <?php
                        $display_status=mysql_query("SELECT * FROM tbl_evaluation_results WHERE answeredBy = '$departmentEV' AND evaluationStatus = 'COMPLETED' GROUP BY emp_id ") or die(mysql_error());

                        while($row=mysql_fetch_array($display_status)){
                            $employee_lastName = $row['employee_lastName'];
                            $employee_firstName = $row['employee_firstName'];
                            $employee_position = $row['employee_position'];
                            $employee_username = $row['emp_id'];
                            $performanceCycle = $row['performanceCycle'];
                            $departmentName = $row['employee_department'];
                            ?>

                            <?php
                            $display_count_pass_completed=mysql_query("SELECT COUNT(employee_department) FROM tbl_evaluation_results WHERE emp_id = '$employee_username' AND employee_department = '$departmentName' AND evaluationStatus = 'COMPLETED'") or die(mysql_error());
                            while($c_row=mysql_fetch_array($display_count_pass_completed)){
                                $passer_count_completed = $c_row['COUNT(employee_department)'];
                            }
                            ?>
                            <tr>
                                    <td><label><?php echo $employee_lastName?></label></td>
                                    <td><label><?php echo $employee_firstName?></label></td>
                                    <td><label><?php echo $employee_position?></label></td>
                                    <td><span class="col-sm-5 label label-success"><?php echo $passer_count_completed;?></span></td>
                                    <td>
                                        <form method="POST">
                                            <input type="hidden" name="employee_username" value="<?php echo $employee_username?>"/>
                                            <input type="hidden" name="performanceCycle_EV" value="<?php echo $performanceCycle?>"/>

                                            <input type="submit" class="btn btn-info btn-sm" name="btnPassToPerformanceCycle" value="REVIEW"/>
                                        </form>

                                    </td>
                            </tr>

                            <?php
                        }
                        ?>



                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

            </div>
            <!-- /.box-footer -->
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>
