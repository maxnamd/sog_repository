-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 11, 2017 at 03:43 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `es_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sog_employee`
--

CREATE TABLE IF NOT EXISTS `tbl_sog_employee` (
  `id` int(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `department_head` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_temp` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sog_employee`
--

INSERT INTO `tbl_sog_employee` (`id`, `firstname`, `middlename`, `lastname`, `department`, `years_in_company`, `position`, `job_level`, `years_in_position`, `department_head`, `username`, `password`, `password_temp`) VALUES
(1, 'Mark', 'Dunno', 'Zuckerburg', 'College of Information Technology Education', '5', 'CEO', 'Regular', '3', '', 'Employee-1', 'Zuckerburg', ''),
(3, 'hello', 'hello', 'hi', 'College of Arts and Sciences', 'hello', 'hello', 'hello', 'hello', '', 'Employee-2', 'bf8c144140b15befb8ce662632a7b76e', 'HI');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_sog_employee`
--
ALTER TABLE `tbl_sog_employee`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_sog_employee`
--
ALTER TABLE `tbl_sog_employee`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
