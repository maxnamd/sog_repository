<?php include 'includes/header.php'; ?>
  <!-- Setting the treeview active -->
  <script type="text/javascript">
  document.getElementById("treeview1").className = "active menu-open"
  </script>
  <!-- End Setting the treeview active -->
    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard
          <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol>
      </section>

      <!-- Main content -->
          <section class="content">
        <!-- Info boxes -->
        <div class="row">
           <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-android-calendar"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Evaluation Period</span>
                        <span class="info-box-number">
                            <?php
                            $queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
                            $dateEVRow = mysql_fetch_array($queryDateEV);
                            $startDateEV = $dateEVRow['startDate'];
                            $endDateEV = $dateEVRow['endDate'];

                            echo $startDateEV . " - " . $endDateEV;
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
          
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-calendar"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">KRA Creation Period</span>
                        <span class="info-box-number">
                            <?php
                            $queryDateKPI = mysql_query("SELECT * FROM tbl_datekpi");
                            $dateKpiRow = mysql_fetch_array($queryDateKPI);
                            $startDateKPI = $dateKpiRow['startDate'];
                            $endDateKPI = $dateKpiRow['endDate'];

                            echo $startDateKPI . " - " . $endDateKPI;
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-ios-chatbubble-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Unread Notifications</span>
                        <span class="datacount info-box-number"></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            <!-- /.col -->

              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="ion ion-ios-pulse"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">CRITICAL INCIDENT NUMBER COUNT</span>
                        <span class="info-box-number">
                            <?php
                            $queryStatus= mysql_query("SELECT * FROM tbl_settingsp2a");
                            $statusRow = mysql_fetch_array($queryStatus);
                            $criticalIncidentCount = $statusRow['criticalIncidentCount'];

                            echo $criticalIncidentCount;
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </div>
        <!-- /.row -->


<!-- A N O T H E R   R O W -->

      <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="ion ion-ios-copy"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">RUBRICS PER AREA COUNT</span>
                        <span class="info-box-number">
                            <?php
                            $queryStatus= mysql_query("SELECT * FROM tbl_settingsp3");
                            $statusRow = mysql_fetch_array($queryStatus);
                            $rubricCount = $statusRow['rubricCount'];

                            echo $rubricCount;
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
          
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

           <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-podium"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">NUMBER OF DEPARTMENTS</span>
                        <span class="info-box-number">
                            <?php
                            $queryStatus= mysql_query("SELECT * FROM tbl_departments");
                            $statusRow = mysql_num_rows($queryStatus);

                            echo $statusRow;
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-people"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">NUMBER OF DEPARTMENT HEADS</span>
                        <span class="info-box-number">
                            <?php
                            $queryStatusa= mysql_query("SELECT * FROM tbl_admins");
                            $statusRowa = mysql_num_rows($queryStatusa);

                            echo $statusRowa;
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">NUMBER OF SOG EMPLOYEES</span>
                        <span class="info-box-number">
                            <?php
                            $queryStatusb= mysql_query("SELECT * FROM tbl_sog_employee");
                            $statusRowb = mysql_num_rows($queryStatusb);

                            echo $statusRowb;
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </div>


        <!-- A N O T H E R   R O W-->



 <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="ion ion-ios-book"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">NUMBER OF DEPARTMENTS SENT THEIR KRA</span>
                        <span class="info-box-number">
                            <?php
                            $queryStatusc = mysql_query("SELECT * FROM tbl_departments");
                            $statusRowc = mysql_num_rows($queryStatusc);

                            echo $statusRowc;
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
          
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

           <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-checkmark"></i></span>

                     <div class="info-box-content">
                        <span class="info-box-text">NUMBER OF APPROVED DEPARTMENT's KRA</span>
                        <span class="info-box-number">
                            <?php
                            $queryStatusd = mysql_query("SELECT * FROM tbl_departments WHERE kpiStatus = 'APPROVED'");
                            $statusRowd = mysql_num_rows($queryStatusd);

                            echo $statusRowd;
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <!-- /.col -->
        </div>


      
    </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>