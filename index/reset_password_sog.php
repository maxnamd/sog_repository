
<!DOCTYPE html>
<html>
<head>
    <title>Welcome to SOG Evaluation System</title>
    <script src="../assets/bower_components/sweetalert2/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="../assets/bower_components/sweetalert2/sweetalert2.min.css">


    <link rel="stylesheet" href="../assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <script src="../assets/bower_components/jquery/dist/jquery.js"></script>

    <script src="../assets/bower_components/jquery-ui-1.12.1/jquery-ui.min.js"></script>

    <!-- End Bootstrapjs  -->
    <script src="../assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../assets/bower_components/font-awesome/css/font-awesome.min.css"/>




</head>


<body>
<?php
if($_GET['key'] && $_GET['reset'])
{
    $email=$_GET['key'];
    $pass=$_GET['reset'];
    include '../connection.php';
    $select=mysql_query("select * from tbl_sog_employee where email_employee='$email' and md5(password)='$pass'");
    if(mysql_num_rows($select)==1)
    {
        ?>

        <div class="container">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3>RESET PASSWORD</h3>
                </div>

                <div class="panel-body">
                    <form method="post">
                        <input type="hidden" name="email" value="<?php echo $email;?>">
                        <div class="row">
                            <div class="col-sm-2"><label>New Password:</label></div>
                            <div class="col-sm-10 form-group" id="div_pass"><input type="password" class="form-control" name="password" id="password"/></div>
                        </div>

                        <div class="row" style="margin-top: 1em;">
                            <div class="col-sm-2"><label>Confirm Password:</label></div>
                            <div class="col-sm-10 form-group" id="div_confirm_pass"><input type="password" class="form-control" name="c_password" id="confirm_password"/></div>
                        </div>

                        <span id='message'></span>
                        <br>
                        <span id='message2'></span>


                </div>

                <div class="panel-footer">
                    <div class="row">
                        <div class=col-sm-2> <input type="submit" name="submit_password" class="btn btn-info btn-block" id="btnSubmit_password"/></div>
                    </div>
                    </form>
                </div>

            </div>



        </div>
        <?php
    }
}
?>

</body>

<script>
    $('#password, #confirm_password').on('keyup', function () {

        var password = $("#password").val();
        var confirmPassword = $("#confirm_password").val();

        if(password.length < 6 || password.length == 0){

            $('#message2').html('Password is empty or not more than 6 characters').css('color', 'red');
            $('#div_pass').addClass('has-error');
            $('#div_confirm_pass').addClass('has-error');
            $("#btnSubmit_password").prop("disabled", true);
        }

        else if (password != confirmPassword) {
            $('#div_pass').addClass('has-error');
            $('#div_confirm_pass').addClass('has-error');
            $("#btnSubmit_password").prop("disabled", true);
            $('#message').html('Not Matching').css('color', 'red');
            return false;
        } else {
            $('#div_pass').removeClass('has-error');
            $('#div_confirm_pass').removeClass('has-error');

            $('#div_pass').addClass('has-success');
            $('#div_confirm_pass').addClass('has-success');

            $("#btnSubmit_password").prop("disabled", false);
            $('#message').html('Matching').css('color', 'green');
            $('#message2').html('').css('color', 'red');
            return true;
        }


    });
</script>
</html>


<?php
if(isset($_POST['submit_password']) && $_POST['email'] && $_POST['password'])
{
    $test = $_POST['password'];
    /*echo "<script type='text/javascript'>console.log('$test');</script>";*/
    include '../connection.php';
    $email=$_POST['email'];
    $pass_inc=md5($_POST['password']);
    $pass=$_POST['password'];
    $select=mysql_query("UPDATE tbl_sog_employee SET password='$pass_inc',password_temp='$pass' where email_employee='$email'");
    echo"
            
			<script type='text/javascript'>
				swal({
                  title: 'SUCCESS!',
                  text: 'Password Reset Successful. You can now login to your account',
                  type: \"success\",
                  timer: 10000,
                  allowOutsideClick: false,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = '../index.php';
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = '../index.php';
                    }
                  }
                )
			</script>
		";
}
?>
