<div class="panel panel-success" style="border-color: #3A5F0B;">
	<div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>PERFORMANCE EVALUATION FORM FOR SOG - P3</b></h4></div>

	<div class="panel-body">
		<?php include 'include/head.php';?>

		<table class="table table-bordered">
			<thead>
				<th class="col-sm-1">AREAS</th>
                <th class="col-sm-10">Rubric</th>
                <th class="col-sm-1">Score</th>
			</thead>


			<tbody>

				<?php
                $p3_arrayCount = 0;
				$display_ep3=mysql_query("SELECT * FROM tbl_eformp3") or die(mysql_error());
                                    $myCount=1;
                                    while($row=mysql_fetch_array($display_ep3)){ 
                                        $mainArea = $row['areas'];
                ?>

                 <?php
                    $_counting=mysql_query("SELECT COUNT(areas) as total FROM tbl_eformp3") or die (mysql_error());
                    $counting = mysql_fetch_assoc($_counting);
                    $display = $counting['total'];
                ?>
                <input type="hidden" name="p3_area[]" class="form-control" value="<?php echo $mainArea?>"/>

                                        <tr>
                	<td> <?php echo $mainArea;?></td>
                	<td> 
                		<?php 
		                $display_rubric=mysql_query("SELECT * FROM tbl_p3rubric WHERE areaName ='$mainArea' ORDER BY id DESC") or die(mysql_error());  
		             
		                while($row=mysql_fetch_array($display_rubric)){ 
		                    $rubricID = $row['id'];
		                    $areaName = $row['areaName'];
		                    $rubricDesc = $row['rubricDesc'];
		                ?>
		                <div class="row">
                            <input type="hidden" name="p3_area_inside[]" class="form-control" value="<?php echo $areaName?>"/>
                            <input type="hidden" name="p3_id[]" class="form-control" value="<?php echo $rubricID?>"/>
		                	<input type="hidden" name="p3_rubric[]" class="form-control" value="<?php echo $rubricDesc?>" />
		                	<div class="col-sm-12"><?php echo $rubricID . ': '.$rubricDesc;}?> </div>
		                </div>
		                
                	</td>

                	<td> 
                        <select class="form-control compute_score_p3" name="p3_score[]">
                            <option value="<?php if(isset($_POST['p3_score'][$p3_arrayCount])){ echo $_POST['p3_score'][$p3_arrayCount];}?>"><?php if(isset($_POST['p3_score'][$p3_arrayCount])){ echo $_POST['p3_score'][$p3_arrayCount]; }else{echo"SELECT";}?></option>

                            <?php
                                $display_rating=mysql_query("SELECT * FROM tbl_kpiratingscale") or die (mysql_error());
                                 while($row2 =mysql_fetch_array($display_rating)){

                                 $Kpi = $row2['rating_value'];
                            ?>
                           <option value="<?php echo $Kpi;?>"> <?php echo $Kpi;?> </option>
                            <?php } ?>

                        </select>
                	</td>
                </tr>


                <?php $p3_arrayCount++;}?>
			</tbody>

            <tr class="totalColumn">

                <td colspan="2" style="background-color: #4b646f;color: white"><center><label>TOTAL PROFESSIONALISM RATING</label></center></td>
                <td class="totalCol">
                    <input type="text" id="total_score_p3" readonly />
                    <input type="hidden" id="divided_by" value="<?php echo $display;?>">
                </td>

            </tr>
		</table>
	</div>
</div>

<script type="text/javascript">
    $('.compute_score_p3').on('change', function() {
        var sum = 0;
        var divide = document.getElementById('divided_by').value;
        $('.compute_score_p3').each(function() {
            sum += Number($(this).val());
        });
         var final = (sum/divide);
        document.getElementById('total_score_p3').value=parseFloat(final).toFixed(2);
        console.log(sum);

        computeSummary();
    })
</script>

<script type="text/javascript">
    function p3_comp(){
        var sum = 0;
        var divide = document.getElementById('divided_by').value;
        $('.compute_score_p3').each(function() {
            sum += Number($(this).val());
        });

        var final = (sum/divide);
        document.getElementById('total_score_p3').value=parseFloat(final).toFixed(2);
    }
</script>