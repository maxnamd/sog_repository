<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 12/4/2017
 * Time: 7:25 PM
 */ ?>
<?php include 'includes/header.php'; ?>

<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview2b").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->
<script>
    $(document).ready(function() {
        $('#tbl_select_sog').DataTable();
    } );
</script>

<script type="text/javascript">
    function del_confirmation1()
    {
        if(confirm("Are you sure?")==1)
        {
            document.getElementById('btnDeleteKRA').submit();
        }
    }

    function del_confirmation2()
    {
        if(confirm("Are you sure?")==1)
        {
            document.getElementById('btnDeleteKPI').submit();
        }
    }
</script>




<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">


        <?php
        $format = "m/Y";

        $queryDateKPI = mysql_query("SELECT * FROM tbl_datekpi");
        $datekpiRow = mysql_fetch_array($queryDateKPI);
        $startDateKPI = \DateTime::createFromFormat($format, $datekpiRow['startDate']);
        $endDateKPI = \DateTime::createFromFormat($format, $datekpiRow['endDate']);

        $queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
        $dateEVRow = mysql_fetch_array($queryDateEV);
        $startDateEV = \DateTime::createFromFormat($format, $dateEVRow['startDate']);
        $endDateEV =  \DateTime::createFromFormat($format, $dateEVRow['endDate']);

        $queryStat = mysql_query("SELECT * FROM tbl_departments WHERE department_name = '$adepartment'");
        $statRow = mysql_fetch_array($queryStat);
        $kpiStat = $statRow['kpiStatus'];
        $activateStat = $statRow['activation_status'];


        $query_status_request = mysql_query("SELECT * FROM tbl_kpi_requests WHERE department = '$adepartment'");
        $request_row = mysql_fetch_array($query_status_request);
        $status = $request_row['request_status'];

        $result = mysql_query("SELECT * FROM tbl_p1kpi WHERE kpiOwner = '$adepartment'");
        $my_kra_count = mysql_num_rows($result);

        $fetch_hr = mysql_query("SELECT * FROM tbl_admins WHERE userlevel = 'SUPERADMIN'");
        $row_hr = mysql_fetch_array($fetch_hr);
        $hr_dept = $row_hr['department'];
                ?>
        <div class="panel panel-default">
                    <div class="panel-heading" style="background: black"><h4><b style="color:white;">Manage KRA - Select SOG Employee</b></h4>
                    </div>


                    <div class="container-fluid" style="margin-top: 1em;">


                        <div class='col-sm-1 pull-right'>
                            <marquee><span><b>CREATION OF KRA STARTED</b></span></marquee>
                        </div>


                    </div>

                    <hr>

                    <div class="panel-body">




                        <table class="table table-hovered table-responsive" id="tbl_select_sog">
                            <thead class="">

                                <tr>
                                    <th class="col-sm-1">SOG LASTNAME</th>
                                    <th class="col-sm-1">SOG FIRSTNAME</th>
                                    <!--<th class="col-sm-2">WEIGHTS</th>-->
                                    <th class="col-sm-1">SOG POSITION</th>
                                    <th class="col-sm-1">KRA COUNT</th>
                                    <th class="col-sm-1">ACTION</th>
                                </tr>
                            </thead>

                            <tbody>

                                <?php
                                $display_sog_employee=mysql_query("SELECT * FROM tbl_sog_employee WHERE department = '$adepartment'") or die(mysql_error());
                                while($row=mysql_fetch_array($display_sog_employee)){
                                    $kraStatus = $row['kraStatus'];
                                    $employee_id = $row['emp_id'];
                                ?>
                                <tr>
                                    <td class="col-sm-1"><b><?php echo $row['lastname'];?></td>
                                    <td class="col-sm-1"><?php echo $row['firstname'];?> </td>
                                    <td class="col-sm-1"><?php echo $row['position'];?> </td>
                                    <td class="col-sm-1">
                                        <?php

                                        $myKRA_dept=mysql_query("SELECT * FROM tbl_eformp1 WHERE kpiOwner = '$adepartment' AND kraCreatedFor ='$employee_id'") or die(mysql_error());
                                        $countMyKra = mysql_num_rows($myKRA_dept);
                                        echo "<span class='col-sm-5 label label-default font-weight-bold'>$countMyKra</span>";

                                        ?>
                                    </td>

                                    <td class="col-sm-1">
                                        <form method="post">
                                            <input type="hidden" name="emp_id" value="<?php echo $row['emp_id'];?>"/>
                                            <button name="btnSelectEmployee" class="btn btn-sm btn-info">SELECT</button>
                                        </form>
                                    </td>
                                </tr>
                                <?php }?>

                            </tbody>

                        </table>

                    </div>
                </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- REQUEST MODAL-->
<div class="modal fade" id="requestModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Request to REOPEN KRA CREATION</h4>
            </div>

            <div class="modal-body">

                <form method="post" class="form-horizontal">
                    <input type="hidden" name="employee_id" value="<?php echo $emp_id?>"/>
                    <input type="hidden" name="sent_at" value="<?php date_default_timezone_set('Asia/Manila'); echo date('F j, Y g:i:a');?>">

                    <div class="form-group container-fluid" > <!-- Message field -->
                        <label class="control-label " for="message">Request Reason</label>

                        <textarea class="form-control ckeditor" id="request_msg" name="request_msg">
                                <script type="text/javascript">
                                    var textarea = document.getElementById('request_msg');

                                    CKEDITOR.replace( 'textarea',
                                        {
                                            toolbar : 'Basic', /* this does the magic */
                                            uiColor : '#9AB8F3'
                                        });

                                </script>
                            </textarea>
                    </div>





            </div>

            <div class="modal-footer">

                <input type="submit" class="btn btn-success" name="btnSubmitRequest" value="SUBMIT"  />
                <input type="reset" class="btn btn-default" value="Clear"/>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ADD NEW REQUEST MODAL-->

<!-- APPEAL MODAL-->
<div class="modal fade" id="appealModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Appeal to REOPEN KRA CREATION</h4>
            </div>

            <div class="modal-body">

                <form method="post" class="form-horizontal">
                    <input type="hidden" name="employee_id" value="<?php echo $emp_id?>"/>
                    <input type="hidden" name="sent_at" value="<?php date_default_timezone_set('Asia/Manila'); echo date('F j, Y g:i:a');?>">

                    <div class="form-group container-fluid" > <!-- Message field -->
                        <label class="control-label " for="message">Request Reason</label>

                        <textarea class="form-control ckeditor" id="request_msg" name="request_msg">
                                <script type="text/javascript">
                                    var textarea = document.getElementById('request_msg');

                                    CKEDITOR.replace( 'textarea',
                                        {
                                            toolbar : 'Basic', /* this does the magic */
                                            uiColor : '#9AB8F3'
                                        });

                                </script>
                            </textarea>
                    </div>





            </div>

            <div class="modal-footer">

                <input type="submit" class="btn btn-success" name="btnSubmitAppeal" value="SUBMIT"  />
                <input type="reset" class="btn btn-default" value="Clear"/>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ADD NEW REQUEST MODAL-->

<!-- ADD NEW KRA MODAL-->
<div class="modal fade" id="addKRA" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New KRA</h4>
            </div>

            <div class="modal-body">

                <form method="post" class="form-horizontal">
                    <div class="form-group" id="">
                        <label class="col-sm-4 control-label">KRA Title: </label>
                        <div class="col-sm-6">
                            <input type="text" name="kraTitle" id="" class="form-control" placeholder="KRA Name: "/>
                        </div>
                    </div>




                    <div class="modal-footer">

                        <input type="submit" class="btn btn-success" name="add_kra" value="ADD KRA"  />
                        <input type="reset" class="btn btn-default" value="Clear"/>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ADD NEW KRA MODAL-->
<?php include 'includes/footer.php'; ?>

<?php include 'phpfunctions/request_opening_kpi.php'; ?>
<?php
if (isset($_POST['btnSelectEmployee'])){
    $emp_id = $_POST['emp_id'];
    $_SESSION['emp_id'] = $emp_id;

    echo "<script>location.href='manageKRA_main.php';</script>";

}
?>


