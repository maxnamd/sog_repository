<?php include 'includes/header.php'; ?>

    <!-- Setting the treeview active -->
    <script type="text/javascript">
        document.getElementById("treeview2b").className = "active menu-open"
    </script>
    <!-- End Setting the treeview active -->


    <script type="text/javascript">
        function del_confirmation1()
        {
            if(confirm("Are you sure?")==1)
            {
                document.getElementById('btnDeleteKRA').submit();
            }
        }

        function del_confirmation2()
        {
            if(confirm("Are you sure?")==1)
            {
                document.getElementById('btnDeleteKPI').submit();
            }
        }
    </script>




    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">


        <!-- Main content -->
        <section class="content">


            <?php

            $emp_id = $_SESSION['emp_id'];
            $format = "m/Y";

            $queryDateKPI = mysql_query("SELECT * FROM tbl_datekpi");
            $datekpiRow = mysql_fetch_array($queryDateKPI);
            $startDateKPI = \DateTime::createFromFormat($format, $datekpiRow['startDate']);
            $endDateKPI = \DateTime::createFromFormat($format, $datekpiRow['endDate']);

            $queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
            $dateEVRow = mysql_fetch_array($queryDateEV);
            $startDateEV = \DateTime::createFromFormat($format, $dateEVRow['startDate']);
            $endDateEV =  \DateTime::createFromFormat($format, $dateEVRow['endDate']);

            $queryStat = mysql_query("SELECT * FROM tbl_departments WHERE department_name = '$adepartment'");
            $statRow = mysql_fetch_array($queryStat);
            $kpiStat = $statRow['kpiStatus']; //DEPARTMENT KRA STATUS
            $activateStat = $statRow['activation_status'];

            $query_kraStatus_emp = mysql_query("SELECT * FROM tbl_sog_employee WHERE emp_id = '$emp_id'");
            $statusEmpRow = mysql_fetch_array($query_kraStatus_emp);
            $kraStatusEmp = $statusEmpRow['kraStatus']; //EMPLOYEE KRA STATUS
            $empFullName = $statusEmpRow['firstname'] . ' ' . $statusEmpRow['lastname'];


            $query_status_request = mysql_query("SELECT * FROM tbl_kpi_requests WHERE department = '$adepartment'");
            $request_row = mysql_fetch_array($query_status_request);
            $status = $request_row['request_status'];

            $result = mysql_query("SELECT * FROM tbl_p1kpi WHERE kpiOwner = '$adepartment' AND kraCreatedFor = '$emp_id'");
            $my_kra_count = mysql_num_rows($result);

            $fetch_hr = mysql_query("SELECT * FROM tbl_admins WHERE userlevel = 'SUPERADMIN'");
            $row_hr = mysql_fetch_array($fetch_hr);
            $hr_dept = $row_hr['department'];



            /*echo "<script>alert('$adepartment')</script>";*/
            /*$currentDate = date("m/Y");*/
            /*$currentDate = "03/2018";*/
            $currentDate = \DateTime::createFromFormat($format, $currentDate);
            /*$currentDate = "12/2017";*/
            ?>
            <div class="panel panel-default">
                <div class="panel-heading" style="background: black"><h4><b style="color:white;">Manage KPI</b> <i><b style="color: white;"><?php echo $empFullName?></b></i></h4></div>



                <div class="container-fluid" style="margin-top: 1em;">
                    <div class="col-sm-2">
                        <a href="#" class="btn btn-success" style="background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7)" data-toggle="modal" data-target="#addKRA"> &nbsp;&nbsp;<i class="fa fa-plus"></i> NEW KRA &nbsp;</a>
                    </div>

                 <div class="col-sm-5"> </div>

                                     <div class="col-sm-2">
                                        <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_kpi_title_admin" class="btn btn-success" value="Add KPI Title" style=" width:100px;" />
                                        </form>
                                    </div>

                                       <div class="col-sm-2">
                                        <form method="POST" enctype="multipart/form-data" role="form">
                                        <input type="file" name="file" id="file" required="" />
                                        <input type="submit" id="import_department" name="import_kpi_admin" class="btn btn-success" value="Add KPI" style=" width:100px;" />
                                        </form>
                                    </div>
                </div>



                <hr>

                <div class="panel-body">
                    <table class="table table-bordered table-responsive">
                        <thead class="">

                        <tr>
                            <th class="col-sm-1">KRA's</th>
                            <th class="col-sm-10">KEY PERFORMANCE INDICATORS</th>
                            <th class="col-sm-1">Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        $query = "SELECT * FROM tbl_eformp1 WHERE kpiOwner = '$adepartment' AND kraCreatedFor = '$emp_id'";
                        // execute query
                        $result = mysql_query($query) or die ("Error in query: $query. ".mysql_error());
                        // see if any rows were returned
                        if (mysql_num_rows($result) == 0)
                        {
                            echo"<td colspan='4'><center><h4><b>Creation of KPI started <br> Add your KPI now</b></h4></center></td>";
                        }
                        else
                        {
                            $myCount=1;
                            $display_ep1=mysql_query("SELECT * FROM tbl_eformp1 WHERE kpiOwner = '$adepartment'  AND kraCreatedFor = '$emp_id'") or die(mysql_error());
                            while($row=mysql_fetch_array($display_ep1)){
                                $kraID = $row['kraID'];
                                $kpiTitle = $row['kpiTitle'];

                                ?>
                                <tr>
                                    <td>KRA #<?php echo $myCount;?> </td>
                                    <td><?php echo $row['kpiTitle'];?>
                                        <div class="">
                                            <div class="">
                                                <div class="col-sm-2" style="float: right">
                                                    <a href="#" class="btn btn-success" style="background-color:rgba(62, 110, 0, 0.7);border-color:rgba(62, 110, 0, 0.7);margin-bottom: 1em;" data-toggle="modal" data-target="#addKpi<?php echo $kraID;?>"> &nbsp;&nbsp;<i class="fa fa-plus"></i> NEW KPI &nbsp;</a>
                                                </div>


                                                <div class="col-sm-12">


                                                    <div class="box box-info collapsed-box" style="border-color: green">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">KPI Status / Deparment</h3>

                                                            <div class="box-tools pull-right">
                                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                                            </div>
                                                        </div>
                                                        <!-- /.box-header -->
                                                        <div class="box-body">
                                                            <div class="">
                                                                <table class="table table-responsive">
                                                                    <?php

                                                                    $query = "SELECT * FROM tbl_p1KPI WHERE kpiOwner = '$adepartment' AND kraID = '$kraID' AND kraCreatedFor = '$emp_id'";
                                                                    // execute query
                                                                    $result = mysql_query($query) or die ("Error in query: $query. ".mysql_error());
                                                                    // see if any rows were returned
                                                                    if (mysql_num_rows($result) == 0)
                                                                    {
                                                                        echo"<td colspan='4'><center><h4><b>There are no KPI yet.</b></h4></center></td>";
                                                                    }
                                                                    else
                                                                    {?>
                                                                    <tr>
                                                                        <?php
                                                                        $display_kpi=mysql_query("SELECT * FROM tbl_p1KPI WHERE kraID ='$kraID' AND kpiOwner = '$adepartment' AND kraCreatedFor = '$emp_id' ORDER BY kpiID ASC") or die(mysql_error());

                                                                        while($row2=mysql_fetch_array($display_kpi)){

                                                                            $kranewID = $row2['kraID'];
                                                                            $kpinewID = $row2['kpiID'];
                                                                            $kpiDesc = $row2['kpiDesc'];
                                                                            ?>


                                                                            <div class="row" style="margin-bottom: 1em">
                                                                                <div class="col-sm-10"><?php echo $kpiDesc;?> </div>

                                                                                <div class="col-sm-2">
                                                                                    <div class="col-sm-2"> <a href="#editKPIDesc<?php echo $kraID.$kpinewID;?>" class="btn btn-default btn-sm" title="Edit KPI" data-toggle="modal"> <i class="fa fa-edit"></i></a> </div>

                                                                                    <div class="col-sm-2"> </div>

                                                                                    <div class="col-sm-2">
                                                                                        <form method="post">
                                                                                            <input type="hidden" name="hiddenKRAID" value="<?php echo $kranewID?>"/>
                                                                                            <input type="hidden" name="hiddenKPIID" value="<?php echo $kpinewID?>"/>
                                                                                            <button type="submit" class="btn btn-default btn-sm" title="Delete KPI" name="btnDeleteKPI" id="btnDeleteKPI" onclick="del_confirmation2(this);return false"> <i class="fa fa-trash"></i>
                                                                                            </button>
                                                                                        </form>

                                                                                    </div>

                                                                                </div>
                                                                            </div>


                                                                            <!-- EDIT KPI Description MODAL-->
                                                                            <div class="modal fade" id="editKPIDesc<?php echo $kraID.$kpinewID;?>" tabindex="-1" role="dialog">
                                                                                <div class="modal-dialog">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                            <h4 class="modal-title">Edit KPI <?php echo $kpiTitle;?></h4>
                                                                                        </div>

                                                                                        <div class="modal-body">
                                                                                            <form method="post">
                                                                                                <div class="row" id="">
                                                                                                    <label class="col-sm-4 control-label">Description: </label>
                                                                                                    <div class="col-sm-8">
                                                                                                        <input type="text" name="editkpiDesc" class="form-control" placeholder="KPI Description: " value="<?php echo $kpiDesc;?>" />
                                                                                                    </div>
                                                                                                </div>
                                                                                            </form>
                                                                                        </div>

                                                                                        <div class="modal-footer">
                                                                                            <input type="hidden" name="xkranewID" value="<?php echo $kranewID?>"/>
                                                                                            <input type="hidden" name="xkpinewID" value="<?php echo $kpinewID?>"/>
                                                                                            <input type="submit" class="btn btn-success" name="update_kpi" value="UPDATE"/>
                                                                                            <input type="reset" class="btn btn-default" value="Clear"/>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- END EDIT KPI Description MODAL-->




                                                                            <?php
                                                                        }}
                                                                        ?>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <!-- /.table-responsive -->
                                                        </div>
                                                        <!-- /.box-body -->
                                                        <!-- /.box-footer -->
                                                    </div>

                                                </div>
                                            </div>



                                        </div>


                                        <!-- ADD KPI Description MODAL-->
                                        <div class="modal fade" id="addKpi<?php echo $kraID;?>" tabindex="-1" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Add New KPI to <?php echo $kpiTitle;?></h4>
                                                    </div>

                                                    <div class="modal-body">

                                                        <form method="post" class="form-horizontal">
                                                            <div class="form-group" id="">
                                                                <label class="col-sm-4 control-label">Description: </label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" name="kpiDesc" id="" class="form-control" placeholder="KPI Description: "/>
                                                                </div>
                                                            </div>

                                                    </div>

                                                    <div class="modal-footer">
                                                        <input type="hidden" name="xkpiTitle" value="<?php echo $kpiTitle?>"/>
                                                        <input type="hidden" name="xkraID" value="<?php echo $kraID?>"/>
                                                        <input type="submit" class="btn btn-success" name="add_kpi" value="ADD KPI"  />
                                                        <input type="reset" class="btn btn-default" value="Clear"/>

                                                    </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END ADD KPI Description MODAL-->

                                    </td>
                                    <td>
                                        <div class="col-sm-2"> <a href="#editKRA<?php echo $kraID;?>" class="btn btn-default btn-sm" title="Edit KRA" data-toggle="modal"> <i class="fa fa-edit"></i></a> </div>

                                        <div class="col-sm-2">  </div>

                                        <div class="col-sm-2">
                                            <form method="post">
                                                <input type="hidden" name="hiddenID" value="<?php echo $kraID?>"/>
                                                <button type="submit" class="btn btn-default btn-sm" title="Delete Competency" name="btnDeleteKRA" id="btnDeleteKRA" onclick="del_confirmation1(this);return false"> <i class="fa fa-trash"></i>
                                                </button>
                                            </form>

                                        </div>

                                        <!-- EDIT KRA MODAL-->
                                        <div class="modal fade" id="editKRA<?php echo $kraID;?>" tabindex="-1" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Edit KRA <?php  echo $kpiTitle?></h4>
                                                    </div>

                                                    <div class="modal-body">

                                                        <form method="post" class="form-horizontal">
                                                            <div class="form-group" id="">
                                                                <label class="col-sm-4 control-label">KRA Title: </label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" name=" edit_kpiTitle" value="<?php echo$kpiTitle?>" id="" class="form-control" placeholder="KRA Name: "/>
                                                                </div>
                                                            </div>




                                                            <div class="modal-footer">
                                                                <input type="hidden" name="hiddenID" value="<?php echo $kraID?>">
                                                                <input type="submit" class="btn btn-success" name="update_kra" value="ADD KRA"  />
                                                                <input type="reset" class="btn btn-default" value="Clear"/>

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END EDIT KRA MODAL-->

                                    </td>

                                </tr>
                                <?php $myCount++;}}?>
                        </tbody>

                    </table>
                </div>
            </div>

            <div class="box box-info" style="border-color: green">
                <div class="box-header with-border">
                    <h3 class="box-title">Messages for this Revision</h3>

                    <div class="pull-right" style="margin-right: 1em">

                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="max-height: calc(100vh - 212px) !important;overflow-y: auto;padding: 1em;">
                    <style type="text/css">
                        .chat
                        {
                            list-style: none;
                            margin: 0;
                            padding: 0;
                        }

                        .chat li
                        {
                            margin-bottom: 10px;
                            padding-bottom: 5px;
                            border-bottom: 1px dotted #B3A9A9;
                        }

                        .chat li.left .chat-body
                        {
                            margin-left: 60px;
                        }

                        .chat li.right .chat-body
                        {
                            margin-right: 60px;
                        }


                        .chat li .chat-body p
                        {
                            margin: 0;
                            color: #777777;
                        }
                        ::-webkit-scrollbar-track
                        {
                            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                            background-color: #F5F5F5;
                        }

                        ::-webkit-scrollbar
                        {
                            width: 12px;
                            background-color: #F5F5F5;
                        }

                        ::-webkit-scrollbar-thumb
                        {
                            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
                            background-color: #555;
                        }

                    </style>

                    <ul class="chat">
                        <?php
                        $display_my_messages=mysql_query("SELECT * FROM tbl_messages WHERE id_appeal = '$emp_id' and receiver = '$adepartment' ORDER BY sent_at ASC") or die(mysql_error());
                        $check_num_rows = mysql_num_rows($display_my_messages);

                        if ($check_num_rows > 0) {

                            while($row_messages=mysql_fetch_array($display_my_messages)){
                                $sender = $row_messages['sender'];
                                $receiver = $row_messages['receiver'];
                                $content = $row_messages['content'];
                                $time = $row_messages['sent_at'];


                                if($sender==$hr_dept){
                                    ?>
                                    <li class="left clearfix">
                                            <span class="chat-img pull-left">
                                                <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                                            </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <strong class="primary-font"><?php echo $hr_dept?></strong> <small class="pull-right text-muted">
                                                    <span class="glyphicon glyphicon-time"></span><?php echo $time?></small>
                                            </div>
                                            <p>
                                                <?php
                                                echo $content;
                                                ?>
                                            </p>
                                        </div>
                                    </li>
                                <?php }
                                else{
                                    ?>
                                    <li class="right clearfix">

                                                        <span class="chat-img pull-right">
                                                            <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?php echo $time?></small>
                                                <strong class="pull-right primary-font"><?php echo $adepartment;?></strong>
                                            </div>
                                            <p>
                                                <?php
                                                echo $content;
                                                ?>
                                            </p>
                                        </div>
                                    </li>
                                    <?php
                                }

                            }}
                        else{?>
                            NO MESSAGES YET <?php }?>


                    </ul>
                </div>

            </div>



        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



<!-- ADD NEW KRA MODAL-->
<div class="modal fade" id="addKRA" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New KRA</h4>
            </div>

            <div class="modal-body">

                <form method="post" class="form-horizontal">
                    <div class="form-group" id="">
                        <label class="col-sm-4 control-label">KRA Title: </label>
                        <div class="col-sm-6">
                            <input type="text" name="kraTitle" id="" class="form-control" placeholder="KRA Name: "/>
                        </div>
                    </div>




                    <div class="modal-footer">

                        <input type="submit" class="btn btn-success" name="add_kra" value="ADD KRA"  />
                        <input type="reset" class="btn btn-default" value="Clear"/>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ADD NEW KRA MODAL-->
<?php include 'includes/footer.php'; ?>
<?php include 'phpfunctions/p1functions.php'; ?>
