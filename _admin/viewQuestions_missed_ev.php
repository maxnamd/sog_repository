<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 11/10/2017
 * Time: 1:07 AM
 */
include 'includes/header.php';
?>
<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview2a").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->
<!-- Content Wrapper. Contains page content -->
<script>
    $(document).ready(function() {
        $('#tbl_missed').DataTable();
    } );
    $(document).ready(function() {
        $('#tbl_requests').DataTable();
    } );
</script>


<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

        <!--DIV OF MISSED EVALUATIONS-->
        <div class="box box-info" style="border-color: green">
            <div class="box-header with-border">
                <h3 class="box-title">Missed Evaluations</h3>

                <div class="pull-right" style="margin-right: 1em">

                    <form>
                        <input type="submit" class="btn btn-success" onclick="printing2();" value="PRINT"/>
                    </form>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin" id="tbl_missed">
                        <thead>
                        <tr>
                            <th class="col-sm-1">Employee Last Name</th>
                            <th class="col-sm-1">Employee First Name</th>
                            <th class="col-sm-1">Position</th>
                            <th class="col-sm-1">Performance Cycle</th>
                            <th class="col-sm-1">Request Status</th>
                            <th class="col-sm-1">Evaluation Status</th>
                            <th class="col-sm-1">Messages</th>
                            <th class="col-sm-1">Action</th>

                        </tr>
                        </thead>

                        <tbody>
                            <?php
                            include 'phpfunctions/viewEmployee_evaluation_missed.php'
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!--END DIV OF MISSED EVALUATIONS-->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>
<?php
if (isset($_POST['btnSelectEmployee'])){
    $employee_firstName = $_POST['employee_firstName'];
    $employee_middleName = $_POST['employee_middleName'];
    $employee_lastName = $_POST['employee_lastName'];
    $employee_position = $_POST['employee_position'];
    $employee_year_in_position = $_POST['employee_year_in_position'];
    $employee_year_in_company = $_POST['employee_year_in_company'];
    $employee_job_level = $_POST['employee_job_level'];
    $employee_username = $_POST['employee_username'];
    $missed_performance_cycle = $_POST['missed_performance_cycle'];

    $_SESSION['employee_firstName_evaluation'] = $employee_firstName;
    $_SESSION['employee_middleName_evaluation'] = $employee_middleName;
    $_SESSION['employee_lastName_evaluation'] = $employee_lastName;
    $_SESSION['employee_position_evaluation'] = $employee_position;
    $_SESSION['employee_year_in_position_evaluation'] = $employee_year_in_position;
    $_SESSION['employee_year_in_company_evaluation'] = $employee_year_in_company;
    $_SESSION['employee_job_level_evaluation'] = $employee_job_level;
    $_SESSION['employee_username_evaluation'] = $employee_username;
    $_SESSION['employee_missed_performance_cycle'] = $missed_performance_cycle;

    echo "<script>location.href='viewQuestions_missed_main.php';</script>";

}
?>

<script type="text/javascript">

    function printing2()
    {


         var myWindow=window.open('','','width=1280,height=720');
         var divtoprint = document.getElementById('tbl_missed');
         var htmlToPrint = '' +
        '<style type="text/css">' +
        'table th, table td {' +
        'border:0.5px solid black;' +
        'border-collapse: collapse;' +
        'padding: 0.5em;' +
        'margin: 0px' +
        '}' + 'select {' + '-webkit-appearance: none !important;' + '-moz-appearance: none !important;' +
        ' appearance: none !important;' + 'border: none !important;' + 'background: none !important;' +
        '</style>';

        /*myWindow.document.write('<center><table><tr><td id="repLogo"><img src="../assets/images/main/logo.png" style="width:100px;height:100px"/></td><td><center><span style="font-family:Segoe UI Light;font-size:11pt;">CAFFEINE HUB<br>LINGAYEN<br>Contact No.(075)09028 82787<br>Sales Report</span></center></td></tr></table></center>');*/
        myWindow.document.write('');
        myWindow.document.write('<div style="margin-left: 5em"><img src="../assets/login/img/upang.png" style="width:100px;height:100px"/> </div> <div style="margin-left: 15em; margin-top: -100px"> PHINMA - UNIVERSITY OF PANGASINAN <br/> <div style="margin-left: 52px;"> Arellano St., Dagupan City </div> <br/> <div style="margin-left: -180px; margin-top: 10px;"> <center> <strong> MISSED EVALUATIONS </strong> </center> </div> </div>');
        htmlToPrint += divtoprint.outerHTML;
        myWindow.document.write('<br/><br/>');
         myWindow.document.write('<center>');
        myWindow.document.write(htmlToPrint);
        myWindow.document.write('</center>');
        //myWindow.document.write(document.getElementById('tbl_EV_t10').outerHTML);
        myWindow.document.write('<footer style="position: absolute;right: 0;bottom: 0;left: 0;padding: 1rem;background-color: #efefef;text-align: right;"> <b>TOP 10 Report</b> <br/>Prepared by: Human Resource Department</footer>');
        myWindow.document.close();
        myWindow.focus();
        myWindow.print();
        myWindow.close();
    }
</script>