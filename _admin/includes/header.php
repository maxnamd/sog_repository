<?php include '../connection.php';?>
<?php
    session_start();
    date_default_timezone_set('Asia/Manila');
    if(($_SESSION['id']) && ($_SESSION['userlevel'])==="ADMIN"){
    $firstname = $_SESSION['firstname'];
    $middlename = $_SESSION['middlename'];
    $lastname = $_SESSION['lastname'];
    $admin_id = $_SESSION['username'];
    $adepartment = $_SESSION['department'];
    $new_department = $_SESSION['department'];
    $fullname = $_SESSION['firstname'].' '.$_SESSION['middlename'].' '.$_SESSION['lastname'];
    $password = $_SESSION['password'];
    $position = $_SESSION['position'];
    $years_in_company = "";
    $years_in_position = "";
    $job_level = $_SESSION['job_level'];
    $id = $_SESSION['id'];
    $password_temp = $_SESSION['password_temp'];
    $userlevel = $_SESSION['userlevel'];
    }

    else if(($_SESSION['id']) && ($_SESSION['userlevel'])==="SUPERADMIN"){
        header("location:../_superadmin/dashboard.php");
    }

    else{
        header("location:../index.php");
    }

    $fetchCurDate=mysql_query("SELECT * FROM tbl_current_date") or die (mysql_error());
    $rowCurDate = mysql_fetch_assoc($fetchCurDate);
    $displayCurDate = $rowCurDate['date'];
    if($displayCurDate == ""){
        $currentDate = date("m/d/Y");
    }
    else{
        $currentDate = $displayCurDate;
    }
?>







<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SOG Evaluation System</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../assets/dist/css/skins/skin-green.css">
  <link rel="stylesheet" href="../assets/bower_components/jquery-ui-1.12.1/jquery-ui.min.css">
  <link rel="stylesheet" href="../assets/bower_components/sweetalert2/sweetalert2.min.css">

  <link rel="icon" href="../assets/images/upang.png" type="image/png">
  <!-- SCRIPTS-->
    <!-- SCRIPTS-->
      <!-- SCRIPTS-->
  
  <script src="../assets/bower_components/jquery/dist/jquery.js"></script>
  <script src="../assets/bower_components/jquery-ui-1.12.1/jquery-ui.min.js"></script>
  <!-- CLOCK rico -->
  <script src="../assets/plugins/clock/ricoClock.js"></script>
  <!-- End CLOCK rico -->


  <!-- CKEDITOR  -->
  <script src="../assets/bower_components/ckeditor/ckeditor.js"></script>
    <script src="../assets/bower_components/ckeditor/adapters/jquery.js"></script>
  <!-- End CKEDITOR  -->

  <!-- End Bootstrapjs  -->
  <script src="../assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Bootstrapjs  -->

  <!-- Bootbox  JS-->
  <script src="../assets/bower_components/bootbox/bootbox.min.js"></script>

  <!-- Bootbox  JS-->


  <!-- Notify  JS-->
  <script src="../assets/bower_components/notifyjs/notify.min.js"></script>
  <!-- End Notify  JS-->

  <script src="../assets/dist/js/adminlte.min.js"></script>

  <script>
    $( function() {
      $( "#tabs" ).tabs();
       /* $('#evaluation_form_link').removeAttr("onclick");*/
    } );
  </script>

    <script>
        $(function () {
            //Add text editor
            $("#compose-textarea").wysihtml5();
        });
    </script>

    <script type="text/javascript">

        $(document).ready(function(){

            $(".datacount").load("../_admin/phpfunctions/loadNotifications.php");



            setInterval(function(){
                $(".datacount").load('../_admin/phpfunctions/loadNotifications.php')
            }, 100);

        });


        $(document).on('click', '.noticlick', function () {

            $.ajax({
                type: "POST",
                url: "../_admin/phpfunctions/updateNotifications.php"
            });

        });

    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            $("#div1").load("../_admin/phpfunctions/popupNotifications.php");



            setInterval(function(){
                $("#div1").load('../_admin/phpfunctions/popupNotifications.php')
            }, 10000);

        });

    </script>





    <script src="../assets/bower_components/sweetalert2/sweetalert2.min.js"></script>
      <!-- SCRIPTS-->
    <!-- SCRIPTS-->
  <!-- SCRIPTS-->


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <link rel="stylesheet"
        href="../assets/bower_components/fonts/default.css">

    <!--START BOOTSTRAP WYSIHTML5-->
    <link rel="stylesheet" href="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
    <script src="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!--END BOOTSTRAP WYSIHTML5-->

    <!--START ALERTIFY-->
    <link rel="stylesheet" href="../assets/bower_components/alertifyjs/css/alertify.min.css">
    <link rel="stylesheet" href="../assets/bower_components/alertifyjs//css/themes/bootstrap.css">
    <!--END ALERTIFY-->

    <!--START BOOTSTRAP DATATABLES-->
    <script src="../assets/bower_components/datatables.net/js/jquery.dataTables.js"></script>
    <script src="../assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.js"></script>
    <link rel="stylesheet" href="../assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!--END BOOTSTRAP DATATABLES-->

    <!-- Morris chart -->
    <link rel="stylesheet" href="../assets/bower_components/morris.js/morris.css">
    <script src="../assets/bower_components/raphael/raphael.min.js"></script>
    <script src="../assets/bower_components/morris.js/morris.min.js"></script>





</head>
<body class="hold-transition sidebar-mini skin-green"> <!-- set dito yung name ng theme -->
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="dashboard.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SOG</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">SOG Evaluation System</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- Notifications: style can be found in dropdown.less -->
            <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle noticlick" data-toggle="dropdown" onclick="">
                    <i class="fa fa-bell-o"></i>
                    <span class="label label-warning datacount" id=""></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header" >You have <span class="datacount" id=""></span> notifications</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">


                            <?php
                            $user_query = mysql_query("SELECT * from tbl_notification_user WHERE (status = 'unread' OR status = 'read') AND owner='$adepartment' LIMIT 10") or die(mysql_error());
                            $i=1;


                            while ($row =mysql_fetch_array($user_query)) {
                                $nId = $row['id'];
                                $notification = $row['notification'];
                                $status = $row['status'];
                                ?>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-red"></i> <?php echo $row['notification'];?>
                                    </a>
                                </li>
                                <?php $i++;  } ?>
                        </ul>
                    </li>
                    <li class="footer"><a href="viewAllNotifications.php">View all</a></li>
                </ul>
            </li>
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $_SESSION['paths']?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $firstname. ' '. $lastname;?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $_SESSION['paths']?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $fullname?>
                  <small><?php echo $adepartment;?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <div class="col-sm-2">
                      <a href="#" class="btn btn-default btn-flat" data-toggle="modal" data-target="#manageProfile">Profile</a>
                  </div>
                </div>
                <div class="pull-right">
                  <a href="../logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>

    </nav>
  </header>
  <div class="pull-right" id="div1"></div>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $_SESSION['paths']?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $firstname. ' '. $lastname;?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->


      <div id="clockbox" style="font:12pt ; color:#ffffff;text-align: center;margin-bottom: 7px;padding: 10px"></div>


      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="" id="treeview1">
          <a href="dashboard.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
          
        </li>

          <li class="treeview" id="treeview2a">
              <a href="#"><i class="fa  fa-file-text-o"></i> <span>Evaluation Form</span>
                  <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="viewQuestions_A.php" id="evaluation_form_link">Ongoing Evaluations</a></li>
                  <li><a href="viewQuestions_missed_ev.php">Missed Evaluations</a></li>
              </ul>
          </li>


          <li class="" id="treeview2b">
          <a href="manageKRA_home.php">
            <i class="fa fa-eye"></i> <span>KRA Management</span>
          </a>
          
        </li>



        <li class="" id="treeview3">
          <a href="viewEvaluationResults_A.php">
            <i class="fa fa-bar-chart"></i> <span>Evaluation Results</span>
          </a>
          
        </li>

        <li class="" id="treeview4">
          <a href="mail_home.php">
              <?php
              $fetch_messages = mysql_query("select count(1) FROM tbl_messages WHERE status = 'unread' AND receiver = '$adepartment'");
              $row_unread = mysql_fetch_array($fetch_messages);
              $total_unread = $row_unread[0];
              ?>
              <i class="fa fa-envelope"></i> <span>Mail <sup class="badge label label-warning" id=""><?php echo $total_unread;?></sup></span>
          </a>
        </li>




        
        <!-- <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>



   <!-- MANAGE PROFILE MODAL-->
    <div class="modal fade" id="manageProfile" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Manage Profile</h4>
                </div>

                <div class="modal-body">

                    <form method="post" class="form-horizontal">
                        <div class="form-group" id="">
                            <div class="col-sm-6">
                                <input type="file" name="file" id="file" value=""/>
                            </div>
                        </div>

                        <div class="form-group" id="">
                            <div class="col-sm-12">
                                <label class="col-sm-4 control-label">Employee ID: </label>
                                <div class="col-sm-6">
                                 <input type="text" name="emp_id" id="" class="form-control col-sm-8" value="<?php echo $id?>" readonly>
                              </div>
                            </div>
                        </div>

                        <div class="form-group" id="">
                            <div class="col-sm-12">
                                <label class="col-sm-4 control-label">First Name: </label>
                                <div class="col-sm-6">
                                 <input type="text" name="firstname" id="" class="form-control col-sm-8" value="<?php echo $firstname?>" readonly>
                              </div>
                            </div>
                        </div>

                        <div class="form-group" id="">
                            <div class="col-sm-12">
                                <label class="col-sm-4 control-label">Middle Name: </label>
                                <div class="col-sm-6">
                                 <input type="text" name="middlename" id="" class="form-control col-sm-8" value="<?php echo $middlename?>" readonly>
                              </div>
                            </div>
                        </div>

                        <div class="form-group" id="">
                            <div class="col-sm-12">
                                <label class="col-sm-4 control-label">Last Name: </label>
                                <div class="col-sm-6">
                                 <input type="text" name="lastname" id="" class="form-control col-sm-8" value="<?php echo $lastname?>" readonly>
                              </div>
                            </div>
                        </div>





                        <div class="modal-footer">

                           <div class="pull-left">
                          <div class="col-sm-2">
                              <a href="#" class="btn btn-warning btn-flat" data-toggle="modal" data-target="#managePassword">Change Password</a>
                          </div>
                      </div>

                            <input type="submit" class="btn btn-success" name="update_profile" value="UPDATE PROFILE"  />
                            <input type="reset" class="btn btn-default" value="Clear"/>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MANAGE PROFILE MODAL-->


    <!-- MANAGE PASSWORD MODAL-->
    <div class="modal fade" id="managePassword" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style='background-color:rgba(62, 110, 0, 0.7);color: white;'>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Manage Password</h4>
                </div>

                <div class="modal-body">

                    <form method="post" class="form-horizontal">
                      
                    <input type="hidden" name="hidden_old_password" id="" class="form-control col-sm-8" value="<?php echo $password_temp;?>">

                    <input type="hidden" name="hidden_id" id="" class="form-control col-sm-8" value="<?php echo $id;?>">

                        <div class="form-group" id="">
                            <div class="col-sm-12">
                                <label class="col-sm-4 control-label">Old Password: </label>
                                <div class="col-sm-6">
                                 <input type="password" name="old_password" id="" class="form-control col-sm-8" placeholder="Old Password">
                              </div>
                            </div>
                        </div>

                         <div class="form-group" id="">
                            <div class="col-sm-12">
                                <label class="col-sm-4 control-label">New Password: </label>
                                <div class="col-sm-6">
                                 <input type="password" name="new_password" id="" class="form-control col-sm-8" placeholder="New Password">
                              </div>
                            </div>
                        </div>

                         <div class="form-group" id="">
                            <div class="col-sm-12">
                                <label class="col-sm-4 control-label">Comfirm New Password: </label>
                                <div class="col-sm-6">
                                 <input type="password" name="confirm_new_password" id="" class="form-control col-sm-8" placeholder="Confirm New Password">
                              </div>
                            </div>
                        </div>




                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" name="update_password" value="UPDATE PASSWORD"  />
                            <input type="reset" class="btn btn-default" value="Clear"/>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MANAGE PASSOWRD MODAL-->

<?php
//START- UPDATE PROFILE
    if(isset($_POST['update_profile']))
    {
      //ikaw na maglagay ng photo variable

       //ikaw na rin bahala sa query pre. kasi wala pang field yang photo sa db
     }
    
//END - UPDATE PROFILE


//START - UPDATE PASSWORD
 if(isset($_POST['update_password']))
    {
      $hidden_old_password=$_POST['hidden_old_password'];
      $old_password=$_POST['old_password'];
      $new_password=$_POST['new_password'];
      $new_password_md5=md5($_POST['new_password']);
      $confirm_new_password=$_POST['confirm_new_password'];

      $hidden_id=$_POST['hidden_id'];  

          if($hidden_old_password == $old_password)
            {
              if($new_password == $confirm_new_password)
                {
                    if(strlen($new_password) >= 8)
                      {
                         mysql_query("UPDATE tbl_admins SET password='$new_password_md5', password_temp='$new_password' WHERE admin_id='$hidden_id'") or die(mysql_error());
                         echo
                        "<script> 
                          alert('Password has been modified Successfully!');
                      </script>";
                      }
                    else
                      {
                        echo
                        "<script> 
                      alert('Your New Password Must be 8 Characters and Above!');
                    </script>";
                      }
                }
              else
                {
                    echo 
                  "<script> 
                    alert('Your Confirm New Password is Incorrect!');
                  </script>";
                }

            }
                  
      else
      {
        echo 
        "<script> 
          alert('Your Old Password is Incorrect!');
        </script>";
      }
    }
//END - UPDATE PASSWORD
?>

<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 11/9/2017
 * Time: 10:03 PM
 */
$queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
$dateEVRow = mysql_fetch_array($queryDateEV);
$startDateEV = $dateEVRow['startDate'];
$endDateEV = $dateEVRow['endDate'];
$performanceCycle = $startDateEV . '-' . $endDateEV;

/*
$search_exist=mysql_query("select * from tbl_evaluation_results WHERE performanceCycle = '$performanceCycle'");
if(mysql_num_rows($search_exist)<=0) {
    $select_sog=mysql_query("select * from tbl_sog_employee");
    while($row=mysql_fetch_array($select_sog))
    {
        $emp_id=$row['emp_id'];
        $employee_lastName = $row['lastname'];
        $employee_firstName = $row['firstname'];
        $employee_middleName = $row['middlename'];
        $employee_username = $row['emp_id'];
        $employee_years_company = $row['years_in_company'];
        $employee_years_position = $row['years_in_position'];
        $employee_job_level = $row['job_level'];
        $employee_position = $row['position'];
        $employee_department = $row['department'];
        $performance_rating_total = '';
        $overall_rating_total = '';
        $search_ev=mysql_query("select * from tbl_evaluation_results where emp_id='$emp_id' AND (evaluationStatus = 'COMPLETED' OR evaluationStatus = 'PROCESSING') AND performanceCycle = '$performanceCycle'");
        if(mysql_num_rows($search_ev)==0) {
            mysql_query("INSERT INTO tbl_evaluation_results (emp_id,employee_firstName,employee_middleName,employee_lastName,employee_department,employee_position,employee_job_level,employee_years_company,employee_years_position,evaluationStatus,performance_rating,overall_rating,answeredBy,performanceCycle) VALUES ('$employee_username','$employee_firstName','$employee_middleName','$employee_lastName','$employee_department','$employee_position','$employee_job_level','$employee_years_company','$employee_years_position','NOT YET STARTED','$performance_rating_total','$overall_rating_total','$admin_id','$performanceCycle')") or die(mysql_error());
        }
    }
}

$search_late=mysql_query("select * from tbl_evaluation_results WHERE performanceCycle != '$performanceCycle' AND evaluationStatus = 'NOT YET STARTED'");
if(mysql_num_rows($search_late)>0) {
    while($row=mysql_fetch_array($search_late))
    {
        $emp_id=$row['emp_id'];
        mysql_query("UPDATE tbl_evaluation_results SET late_status ='YES' WHERE emp_id = '$emp_id' AND evaluationStatus = 'NOT YET STARTED' AND performanceCycle != '$performanceCycle'");

    }
}*/
?>
<?php
$search_in_process=mysql_query("select * from tbl_sog_employee WHERE kraStatus != 'APPROVED' AND department = '$adepartment'");
if(mysql_num_rows($search_in_process)>0) {
    mysql_query("UPDATE tbl_departments SET kpiStatus ='PENDING' WHERE department_name = '$adepartment'");
}
else{
    mysql_query("UPDATE tbl_departments SET kpiStatus ='APPROVED' WHERE department_name = '$adepartment'");
}
?>