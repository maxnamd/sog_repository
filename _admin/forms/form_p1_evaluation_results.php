<?php 
$firstname = $_SESSION['_employee_firstName'];
$middlename = $_SESSION['_employee_middleName'];
$lastname = $_SESSION['_employee_lastName'];
$employee_performance_cycle = $_SESSION['_employee_performance_cycle'];
$employee_id=$_SESSION['_employee_id'];
?>
<script>


</script>
<div class="panel panel-success" style="border-color: #3A5F0B;">
	<div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>PERFORMANCE EVALUATION FORM FOR SOG - P1</b></h4></div>

	<div class="panel-body">
		<?php include 'include/head_evaluation_results.php';?>
	</div>

	<div class="container-fluid table-responsive">
		<table class="table-bordered table-responsive" style="margin-bottom: 1em">
			<thead>
				<th class="col-sm-6" colspan="3" style="background-color: #ffcc00">KPI RATING SCALE</th>
			</thead>

			<tbody>
				<?php 
					$count = 1;
					$display_KpiRatingScale=mysql_query("SELECT * FROM tbl_KpiRatingScale") or die(mysql_error());
	                  while($row=mysql_fetch_array($display_KpiRatingScale)){ 
	                    
	                    $rating_value=$row['rating_value'];
	                    $description=$row['description'];
				?>
				<tr>
					<td><?php echo $rating_value;?></td>
					<td><?php echo $description;?></td>
				</tr>

				<?php }?>
			</tbody>

		</table>



		<table class="table table-bordered table-responsive" id="sum_table">
			<thead class="titlerow">
				<th class="col-sm-6" colspan="3" style="background-color: #ffcc33">PERFORMANCE TARGETS</th>
				<th class="col-sm-5" colspan="2" style="background-color: #ffcc33">ACTUAL PERFORMANCE</th>
				<th class="col-sm-1" colspan="1"></th>
			</thead>

			<tr class="titlerow">
				<th class="col-sm-1">KRA</th>
				<th class="col-sm-4">KEY PERFORMANCE INDICATORS</th>
				<th class="col-sm-1">WEIGHTS</th>
				<th class="col-sm-4">ACHIEVEMENT/ACTUAL PERFORMANCE</th>
				<th class="col-sm-1">RATING</th>
				<th class="col-sm-1">WEIGHTED RATING</th>
			</tr>

			<tbody>
				<?php 
					$fCount = 1;
					$adminOwner = "";


					$display_ep1=mysql_query("SELECT * FROM tbl_ans_eformp1 WHERE answeredFor = '$employee_id' AND performanceCycle='$employee_performance_cycle'") or die(mysql_error());
					
	                  while($row=mysql_fetch_array($display_ep1)){ 
	                    
	                    $kpiTitle=$row['kpiTitle'];
	                    $kraID=$row['kraID'];
	                    $kpiWeights=$row['kpiWeights'];
	                    $p1_rating=$row['p1_rating'];
                        $p1_weightedRating=$row['p1_weightedRating'];
	                   

				?>
				<tr>
					<td><?php echo $fCount;?></td>
					<td>
						<div class="row" style="margin-bottom: 1em">
              				<div class="col-sm-10"> <b><?php echo $kpiTitle;?></b></div>
              			</div>

              			<hr>
						<?php 
							$count = 1;
							$display_p1KPI=mysql_query("SELECT * FROM tbl_ans_eformp1KPI WHERE answeredFor = '$employee_id' AND performanceCycle='$employee_performance_cycle' AND kraID = '$kraID' ") or die(mysql_error());
			                  while($row=mysql_fetch_array($display_p1KPI)){
			                    $kpiDesc=$row['kpiDesc'];

						?>		
						<div class="row" style="margin-bottom: 1em">
              				<div class="col-sm-12"><?php echo $kpiDesc;?> </div>
              			</div>

              			<?php }?>

					</td>

					<td class="rowDataSd">
                        <?php echo $kpiWeights;?>
                        <input class="compute_p1_weights" type="hidden" value="<?php echo $kpiWeights;?>"/>
					</td>
					<td>
						<div class="row" style="margin-bottom: 1em">
              				<div class="col-sm-10"> <b style="color: white"><?php echo $kpiTitle;?></b></div>
              			</div> 
              			<hr>
						<?php 
							$count = 1;
							$display_p1_achievements=mysql_query("SELECT * FROM tbl_ans_eformp1_achievements WHERE answeredBy = '$admin_id' AND kraID = ' $kraID' AND kpiAchievement != '' ") or die(mysql_error());
			                  while($a_row=mysql_fetch_array($display_p1_achievements)){
                                $kpiAchievement=$a_row['kpiAchievement'];
						?>
							<div class="row" style="margin-bottom: 1em">
	              				<div class="col-sm-10">
                                   <ul>
                                   		<li><?php echo  $kpiAchievement;?></li>
                                   </ul>
                                </div>
	              			</div>
						<?php }?>		
					</td>
					<td>
                        <?php echo $p1_rating?>
					</td>
					<td class="rowDataSd">
                        <?php echo $p1_weightedRating?>
                        <input class="compute_p1_weighted_rating" type="hidden" value="<?php echo $p1_weightedRating?>"/>

                    </td>


                    <!-- INVISIBLE FORMS -->
                        <input type="hidden" name="mainKraID[]" value="<?php echo $kraID?>"/>
                        <input type="hidden" name="kraTitle[]" value="<?php echo $kpiTitle?>"/>
                        <input type="hidden" name="kraWeight[]" value="<?php echo $kpiWeight?>"/>
                    <!-- INVISIBLE FORMS -->
	            </tr>



	            <?php $fCount++;}	?>
			</tbody>

            <tr class="totalColumn">

                <td colspan="2" style="background-color: #4b646f;color: white"><center><label>TOTAL WEIGHT</label></center></td>
                <td class="totalCol"><input type="text" id="total_weight_p1" disabled/></td>
                <td colspan="2" style="background-color: #4b646f;color: white"><center><label>WEIGHTED RATING </label></center></td>
                <td class="totalCol"><input type="text" id="total_weighted_rating" disabled/></td>

            </tr>
		</table>
	</div>

</div>