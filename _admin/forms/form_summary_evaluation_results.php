<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 9/14/2017
 * Time: 5:10 PM
 */
?>
<?php 
$firstname = $_SESSION['_employee_firstName'];
$middlename = $_SESSION['_employee_middleName'];
$lastname = $_SESSION['_employee_lastName'];
$employee_performance_cycle = $_SESSION['_employee_performance_cycle'];
$employee_id=$_SESSION['_employee_id'];
?>


<div class="panel panel-success" style="border-color: #3A5F0B;">
    <div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>PERFORMANCE EVALUATION FORM FOR SOG - SUMMARY</b></h4></div>

    <div class="panel-body">
        <?php include 'include/head_evaluation_results.php';?>

        <table class="table table-bordered">
            <thead>
                <th class="col-sm-2">PERFORMANCE RATING</th>
                <th class="col-sm-1">TOTAL WEIGHTED RATING</th>
                <th class="col-sm-1">PERCENTAGE ALLOCATION</th>
                <th class="col-sm-1">SUBTOTAL</th>
            </thead>
            

            <tbody>
                <tr>
                    <td>
                        <h5><b>PART 1: KEY PERFORMANCE INDICATORS</b></h5>
                    </td>

            <?php 
                        $display_summary_p1=mysql_query("SELECT * FROM tbl_ans_summary WHERE answeredFor = '$employee_id' AND performanceCycle = '$employee_performance_cycle' AND id=1 ") or die(mysql_error());
                          while($p1_row=mysql_fetch_array($display_summary_p1)){
                            $p1_total_weighted_rating=$p1_row['total_weighted_rating'];
                            $p1_percentage_allocation=$p1_row['percentage_allocation'];
                            $p1_subtotal=$p1_row['subtotal'];
                }
            ?>

            <?php 
                $display_summary_p2=mysql_query("SELECT * FROM tbl_ans_summary WHERE answeredFor = '$employee_id' AND performanceCycle = '$employee_performance_cycle' AND id=2 ") or die(mysql_error());
                  while($p2_row=mysql_fetch_array($display_summary_p2)){

                    $p2_total_weighted_rating=$p2_row['total_weighted_rating'];
                    $p2_percentage_allocation=$p2_row['percentage_allocation'];
                    $p2_subtotal=$p2_row['subtotal'];

            }?>

            <?php 
                $display_summary_p3=mysql_query("SELECT * FROM tbl_ans_summary WHERE answeredFor = '$employee_id' AND performanceCycle = '$employee_performance_cycle' AND id=3") or die(mysql_error());
                  while($p3_row=mysql_fetch_array($display_summary_p3)){
                    

                    $p3_total_weighted_rating=$p3_row['total_weighted_rating'];
                    $p3_percentage_allocation=$p3_row['percentage_allocation'];
                    $p3_subtotal=$p3_row['subtotal'];

            }?>
                    <td>



                    <input type="text" name="p1_summary_twr" id="p1_summary_twr" class="form-control" value="<?php echo $p1_total_weighted_rating?>" disabled/></td>

                    
                    <td><input type="text" name="p1_summary_percentage_allo" id="p1_summary_percentage_allo" class="form-control" value="<?php echo $p1_percentage_allocation?>" disabled/></td>
                    <td><input type="text" name="p1_summary_subtotal" id="p1_summary_subtotal" class="form-control" value="<?php echo $p1_subtotal?>" disabled/></td>
                </tr>

                <tr>
                    <td>
                        <h5><b>PART 2: COMPETENCIES</b></h5>
                    </td>
                    <td><input type="text" name="p2_summary_twr" id="p2_summary_twr" class="form-control" value="<?php echo $p2_total_weighted_rating?>" disabled/></td>
                    <td><input type="text" name="p2_summary_percentage_allo" id="p2_summary_percentage_allo" class="form-control" value="<?php echo $p2_percentage_allocation?>" disabled/></td>
                    <td><input type="text" name="p2_summary_subtotal" id="p2_summary_subtotal" class="form-control" value="<?php echo $p2_subtotal?>" disabled/></td>
                </tr>

                <tr>
                    <td>
                        <h5><b>PART 3: PROFESSIONALISM</b></h5>
                        <input type="hidden" value="PART 3: PROFESSIONALISM" name="p3_performance_rating"/>
                    </td>
                    <td><input type="text" name="p3_summary_twr" id="p3_summary_twr" class="form-control" value="<?php echo $p3_total_weighted_rating?>" disabled/></td>
                    <td><input type="text" name="p3_summary_percentage_allo" id="p3_summary_percentage_allo" class="form-control" value="<?php echo $p3_percentage_allocation?>" disabled/></td>
                    <td><input type="text" name="p3_summary_subtotal" id="p3_summary_subtotal" class="form-control" value="<?php echo $p3_subtotal?>" disabled/></td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: #BDBDBD;color: white"><label class="pull-right">PERFORMANCE RATING</label></td>
                    <td class="totalCol">
                        <input type="text" name="performance_rating" class="form-control" id="performance_rating" readonly/>

                    </td>
                </tr>

                <tr>
                    <td><h5><b>PART 4: CUSTOMER SERVICE</b></h5></td>
                    <td colspan="2"><label class="pull-right">FACTOR RATING</label></td>
                    <td>
                        <input type="text" name="cs_factor_rating" id="cs_factor_rating" class="form-control" readonly/>
                    </td>
                </tr>


            </tbody>

            <tr class="totalColumn">

                <td colspan="3" style="background-color: #4b646f;color: white"><label class="pull-right">OVERALL RATING</label></td>
                <td class="totalCol">
                    <input type="text" name="total_overall_rating" class="form-control" id="total_overall_rating" readonly/>
                </td>

            </tr>
        </table>
    </div>

</div>


<div class="panel panel-success" style="border-color: #3A5F0B;">
    <div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>SUMMARY COMMENTS</b></h4></div>

    <?php
            $display_comments=mysql_query("SELECT * FROM tbl_ans_summary_comments_acknowledgement WHERE answeredFor = '$emp_id' AND performanceCycle='$pCycle'") or die(mysql_error());
            while($summary_row=mysql_fetch_array($display_comments)){


                $comments=$summary_row['comments'];
                $recommendations=$summary_row['recommendations'];
                $approval=$summary_row['approval'];

            }?>

    <div class="panel-body">
        <table class="table table-condensed">
            <thead>
                <th>Comments</th>
                <th>Recommendations</th>
                <th>Approval</th>
            </thead>

            <tbody>
                <tr>
                    <td>
                        <div style="background-color: #e0e0e0; height: 200px; width: 100%">
                            <?php echo $comments;?>
                        </div>
                    </td>
                    <td>
                         <div style="background-color: #e0e0e0; height: 200px; width: 100%"">
                            <?php echo $recommendations;?>
                        </div>
                    </td>
                    <td>
                        <div style="background-color: #e0e0e0; height: 200px; width: 100%"">
                            <?php echo $approval;?>
                       </div>
                    </td>

                </tr>
            </tbody>
        </table>
                 <?php 
                $display_reaction=mysql_query("SELECT * FROM tbl_evaluation_results WHERE emp_id = '$employee_id' AND performanceCycle = '$employee_performance_cycle'") or die(mysql_error());
                  while($reaction_row=mysql_fetch_array($display_reaction)){
                    $reaction = $reaction_row['ev_sog_feedback'];

                     if($reaction == "")
                    {
                        echo "Feedback/Reaction: NO RESPONSE YET";
                    }
                    else
                        echo "Feedback/Reaction: $reaction";
                    }?>

                   
    </div>

</div>


<script type="text/javascript">
    //COMPUTING THE PERFORMANCE RATING
        var hold_sub1 = document.getElementById('p1_summary_subtotal').value;
        var hold_sub2 = document.getElementById('p2_summary_subtotal').value;
        var hold_sub3 = document.getElementById('p3_summary_subtotal').value;

        document.getElementById('performance_rating').value = parseFloat(hold_sub1)+parseFloat(hold_sub2)+parseFloat(hold_sub3);
        //END COMPUTING THE PERFORMANCE RATING


        var pf = parseFloat(document.getElementById('p1_summary_subtotal').value) + parseFloat(document.getElementById('p2_summary_subtotal').value) + parseFloat(document.getElementById('p3_summary_subtotal').value);
        //COMPUTING THE OVERALL RATING
        var p4_sum_recap = 0;

        $('.factor_rating_p4').each(function() {

            p4_sum_recap += Number($(this).val());
        });


        var x = parseFloat(pf).toFixed(2);
        var y = parseFloat(p4_sum_recap/4).toFixed(2);
        document.getElementById('total_overall_rating').value =  parseFloat(x) + parseFloat(y);
        //END COMPUTING THE OVERALL RATING
</script>