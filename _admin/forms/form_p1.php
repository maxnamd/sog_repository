

<input type="hidden" name="weights_counter" id="total" value=100>
<div>
	<div class="col-sm-2 pull-right">
		<label style="color: yellow">WEIGHTS REMAINING:</label>
		<input type="text" name="weights_remaining" id="remaining" class="form-control" value="100" readonly>
	</div>
</div>

<div class="panel panel-success" style="border-color: #3A5F0B;">
	<div class="panel-heading" style="background-color: #3A5F0B;color: white"><h4><b>PERFORMANCE EVALUATION FORM FOR SOG - P1</b></h4></div>

	<div class="panel-body">
		<?php include 'include/head.php';?>
	</div>

    <?php
    $myKRA_dept=mysql_query("SELECT * FROM tbl_eformp1 WHERE kpiOwner = '$adepartment' AND kraCreatedFor='$kraCreatedFor'") or die(mysql_error());
    $countMyKra = mysql_num_rows($myKRA_dept);


    if($countMyKra==0){
        ?>
        <!--<script type="text/javascript">
            $(window).on('load',function(){
                $('#shakeModal').modal('show');
            });
        </script>

        <div id="shakeModal" class="modal" data-easein="shake"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            ×
                        </button>
                        <h4 class="modal-title">
                            Ooooppsss
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            <h4>LOOKS LIKE YOU DONT HAVE ANY KPI ON YOUR ANY ASSIGNED SOG TO YOU!</h4>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">
                            Close
                        </button>
                        <button class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                </div>
            </div>
        </div>-->
        <script type="text/javascript">
            swal({
                title: "Oooopsss!",
                text: "This employee doesn't have any KRA! Please add his/her KRA before evaluating him/her",
                type: "error",
                allowOutsideClick: false
            }).then(function() {
                window.location.href = "manageKRA_home.php";
            });
        </script>
    <?php } ?>

	<div class="container-fluid table-responsive">
		<table class="table-bordered table-responsive" style="margin-bottom: 1em">
			<thead>
				<th class="col-sm-6" colspan="3" style="background-color: #ffcc00">KPI RATING SCALE</th>
			</thead>

			<tbody>
				<?php 
					$count = 1;
					$display_KpiRatingScale=mysql_query("SELECT * FROM tbl_KpiRatingScale") or die(mysql_error());
	                  while($row=mysql_fetch_array($display_KpiRatingScale)){ 
	                    
	                    $rating_value=$row['rating_value'];
	                    $description=$row['description'];
				?>
				<tr>
					<td><?php echo $rating_value;?></td>
					<td><?php echo $description;?></td>
				</tr>

				<?php }?>
			</tbody>

		</table>



		<table class="table table-bordered table-responsive" id="sum_table">
			<thead class="titlerow">
				<th class="col-sm-6" colspan="3" style="background-color: #ffcc33">PERFORMANCE TARGETS</th>
				<th class="col-sm-5" colspan="2" style="background-color: #ffcc33">ACTUAL PERFORMANCE</th>
				<th class="col-sm-1" colspan="1"></th>
			</thead>

			<tr class="titlerow">
				<th class="col-sm-1">KRA</th>
				<th class="col-sm-4">KEY PERFORMANCE INDICATORS</th>
				<th class="col-sm-1">WEIGHTS</th>
				<th class="col-sm-4">ACHIEVEMENT/ACTUAL PERFORMANCE</th>
				<th class="col-sm-1">RATING</th>
				<th class="col-sm-1">WEIGHTED RATING</th>
			</tr>

			<tbody>

				<?php 
					$fCount = 1;
					$arrayCount = 0;
					$display_ep1=mysql_query("SELECT * FROM tbl_eformp1 WHERE kpiOwner = '$adepartment' AND kraCreatedFor = '$kraCreatedFor'") or die(mysql_error());
					
	                  while($row=mysql_fetch_array($display_ep1)){ 
	                    
	                    $kpiTitle=$row['kpiTitle'];
	                    $kpiWeight=$row['kpiWeight'];
	                    $kraID=$row['kraID'];

	                   

				?>
				<tr>
					<td><?php echo $fCount;?></td>
					<td>
						<div class="row" style="margin-bottom: 1em">
              				<div class="col-sm-10"> <b><?php echo $kpiTitle;?></b></div>
              			</div>

              			<hr>
						<?php 
							$count = 1;
							$arrayCount_inside = 0;
							$display_p1KPI=mysql_query("SELECT * FROM tbl_p1KPI WHERE kpiOwner = '$adepartment' AND kpiStatus='APPROVED' AND kraID = ' $kraID' AND kraCreatedFor = '$kraCreatedFor'") or die(mysql_error());
			                  while($row=mysql_fetch_array($display_p1KPI)){
			                    $kpiDesc=$row['kpiDesc'];
						?>		
						<div class="row" style="margin-bottom: 1em">
              				<div class="col-sm-12"><?php echo $kpiDesc;?> </div>

              				<input type="hidden" name="kpiDescription[<?php echo $arrayCount;?>][<?php echo $arrayCount_inside; ?>]" value="<?php echo $kpiDesc;?>"/>

              				<input type="hidden" name="kraInsideID[<?php echo $arrayCount; ?>][<?php echo $arrayCount_inside; ?>]" value="<?php echo $kraID;?>"/>
              			</div>

              			<?php $arrayCount_inside++;}?>

					</td>

					<td class="rowDataSd">

					    <input type="text" name="kpiWeights[]" id="weights2<?php echo $fCount;?>" class="compute_p1_weights compute_weights_dynamic form-control"  value="<?php if(isset($_POST['kpiWeights'][$arrayCount])){ echo $_POST['kpiWeights'][$arrayCount];}?>" onkeypress="return number(event)" maxlength=3" onblur="detectHigh(this.id);"/>

                        <!--<script type="text/javascript">
                        function detectHigh(id){
                            var getRemaining = document.getElementById('remaining').value;
                            var getTotal =  document.getElementById('total_weight_p1').value;
                            if(getRemaining<0){
                                var current_num = parseInt(id.substr(8));

                                for (x=current_num+1;x<5;x++){
                                    var me = "weights2";
                                    var res = me.concat(x);
                                    document.getElementById(res).readOnly = true;
                                    /*console.log(res);*/

                                    swal(
                                        'Oops...',
                                        'Weights exceed the total weights',
                                        'error'
                                    )
                                }



                            }
                            else{

                                var current_num = parseInt(id.substr(8));
                                for (x=current_num+1;x<5;x++){
                                    var me = "weights2";
                                    var res = me.concat(x);
                                    document.getElementById(res).readOnly = false;
                                    /*console.log(res);*/
                                }
                            }
                            if(getTotal>100){
                                swal(
                                    'Oops...',
                                    'Weights exceed the total weights',
                                    'error'
                                )

                                 $('#btnSends').prop('disabled', true);
                            }

                            if(getTotal<100){
                                 $('#btnSends').prop('disabled', true);
                            }
                            if(getTotal==100)
                            {
                                  $('#btnSends').prop('disabled', false);
                            }
                        }
                        </script>-->
					</td>

					<td>
						<div class="row" style="margin-bottom: 1em">
              				<div class="col-sm-10"> <b style="color: white"><?php echo $kpiTitle;?></b></div>
              			</div>
              			<hr>
						<?php
							$count = 1;
							$arrayCount_inside = 0;
							$display_p1KPI=mysql_query("SELECT * FROM tbl_p1KPI WHERE kpiOwner = '$adepartment' AND kraID = ' $kraID' AND kraCreatedFor = '$kraCreatedFor'") or die(mysql_error());
			                  while($row=mysql_fetch_array($display_p1KPI)){
                                $kpiID=$row['kpiID'];
						?>
							<div class="row" style="margin-bottom: 1em">
	              				<div class="col-sm-10"> <input type="text" name="kpiAchievement[<?php echo $arrayCount;?>][<?php echo $arrayCount_inside?>]" class="form-control" value="<?php if(isset($_POST['kpiAchievement'][$arrayCount][$arrayCount_inside])){ echo $_POST['kpiAchievement'][$arrayCount][$arrayCount_inside];}?>" onkeypress="return letter(event)"> </div>
                                <input type="hidden" name="kraID[<?php echo $arrayCount;?>][<?php echo $arrayCount_inside?>]" value="<?php echo $kraID?>"/>


	              			</div>
                         <input type="hidden" name="kpiID[]" value="<?php echo $kpiID?>" />
						<?php $arrayCount_inside++;}?>
					</td>
					<td> 
					<select class="weightdd form-control" name="kraRating[]" id="ratings<?php echo $fCount;?>" onchange="p1_weighted_rating<?php echo $fCount;?>();">
					<option value="<?php if(isset($_POST['kraRating'][$arrayCount])){ echo $_POST['kraRating'][$arrayCount];}?>"><?php if(isset($_POST['kraRating'][$arrayCount])){ echo $_POST['kraRating'][$arrayCount]; }else{echo"SELECT";}?></option>
					<?php 
						$display_rating=mysql_query("SELECT * FROM tbl_kpiratingscale") or die (mysql_error());
						 while($row2 =mysql_fetch_array($display_rating)){

						 $Kpi = $row2['rating_value'];
					?>              
                   <option value="<?php echo $Kpi;?>">
                       <?php echo $Kpi;?>
                   </option>
                   <?php } ?>

					</select>
					</td>
					<td class="rowDataSd">
                        <script type="text/javascript">
                            function p1_weighted_rating<?php echo $fCount;?>()
                            {
                                var value1 = document.getElementById('weights2<?php echo $fCount;?>').value;
                                var value2 = document.getElementById('ratings<?php echo $fCount;?>').value;
                                var formula1 = value1*.01;
                                var final = formula1*value2;
                                var final = final.toFixed(2);
                                document.getElementById('kraWeightedRating_id<?php echo $fCount;?>').value=final;
                                computeSummary();
                            }
                        </script>


                         
                        <input type="text" name="kraWeightedRating[]" class="form-control compute_p1_weighted_rating" id="kraWeightedRating_id<?php echo $fCount;?>" value="<?php if(isset($_POST['kraWeightedRating'][$arrayCount])){ echo $_POST['kraWeightedRating'][$arrayCount];}?>" readonly/>

                    </td>


                    <!-- INVISIBLE FORMS -->
                        <input type="hidden" name="mainKraID[]" value="<?php echo $kraID?>"/>
                        <input type="hidden" name="kraTitle[]" value="<?php echo $kpiTitle?>"/>
                        <input type="hidden" name="kraWeight[]"/>
                    <!-- INVISIBLE FORMS -->
	            </tr>



	            <?php $fCount++;$arrayCount++;}	?>
			</tbody>

            <tr class="totalColumn">

                <td colspan="2" style="background-color: #4b646f;color: white"><center><label>TOTAL WEIGHT</label></center></td>
                <td class="totalCol"><input type="text" id="total_weight_p1" disabled /></td>
                <td colspan="2" style="background-color: #4b646f;color: white"><center><label>WEIGHTED RATING </label></center></td>
                <td class="totalCol"><input type="text" id="total_weighted_rating" disabled/></td>

            </tr>
		</table>
	</div>
    <span class="pull-left " style="margin-left: 0.5em;color: red;"><b>**MUST HAVE A TOTAL WEIGHT OF 100**</b></span>

</div>

<script type="text/javascript">
    // we used jQuery 'keyup' to trigger the computation as the user type
    var qtyKra = 0;

    $('.compute_p1_weights').each(function () {
        qtyKra+=1;
        console.log("KRA qty"+qtyKra);
    });
    $('.compute_p1_weights').keyup(function () {
        var sum = 0;
        var rem = document.getElementById('total').value;

        $('.compute_p1_weights').each(function() {
            sum += Number($(this).val());
        });



        rem -= sum;
        document.getElementById('remaining').value = rem;
        document.getElementById('total_weight_p1').value=sum;

        computeSummary();

        if(document.getElementById('total_weight_p1').value<100){
            $('#btnSends').prop('disabled', true);
        }
        else if(document.getElementById('total_weight_p1').value>100){
            $('#btnSends').prop('disabled', true);
            swal(
                'Oops...',
                'Weights exceed the total weights',
                'error'
            )
        }
        else if(document.getElementById('total_weight_p1').value > 0 && document.getElementById('total_weight_p1').value < 99){
            $('#btnSends').prop('disabled', true);
        }
        else {
            $('#btnSends').prop('disabled', false);

        }
    });


    $('.weightdd').change(function () {
        var sum = 0;

        $('.compute_p1_weighted_rating').each(function() {
            sum += Number($(this).val());
        });
        document.getElementById('total_weighted_rating').value=parseFloat(sum).toFixed(2);
        computeSummary();
    });
</script>

<script type="text/javascript">
    function callOnRescueCompute(){
        var p1_sum = 0;
        var rem = document.getElementById('total').value;

        $('.compute_p1_weights').each(function() {
            p1_sum += Number($(this).val());
        });

        rem -= p1_sum;
        document.getElementById('remaining').value = rem;
        document.getElementById('total_weight_p1').value=p1_sum;

        var p1b_sum = 0;

        $('.compute_p1_weighted_rating').each(function() {
            p1b_sum += Number($(this).val());
        });
        document.getElementById('total_weighted_rating').value=parseFloat(p1b_sum).toFixed(2);


        var p2_sum = 0;
        $('.compute_weights_p2').each(function() {
            p2_sum += Number($(this).val());
        });
        document.getElementById('total_weights_p2').value=p2_sum;

        var p2_sum_twrp2 = 0;

        $('.compute_p2_weighted_rating').each(function() {
            p2_sum_twrp2 += Number($(this).val());
        });

        document.getElementById('total_weighted_rating_p2').value=parseFloat(p2_sum_twrp2).toFixed(2);


        p3_comp();

        document.getElementById('total_score_p3').value=parseFloat(p3_sum).toFixed(2);


        var p4_sum = 0;

        $('.factor_rating_p4').each(function() {

            p4_sum += Number($(this).val());
        });


        document.getElementById('total_cs_rating').value=parseFloat(p4_sum/4).toFixed(2);

        computeSummary();
    }
</script>




