
<?php

$firstname = $_SESSION['_employee_firstName'];
$middlename = $_SESSION['_employee_middleName'];
$lastname = $_SESSION['_employee_lastName'];
$employee_performance_cycle = $_SESSION['_employee_performance_cycle'];
$employee_id=$_SESSION['_employee_id'];

$display_ongoing=mysql_query("SELECT * FROM tbl_evaluation_results WHERE performanceCycle='$employee_performance_cycle' AND emp_id ='$employee_id'") or die(mysql_error());

while($row=mysql_fetch_array($display_ongoing)) {

    $emp_id = $row['emp_id'];
    $employee_firstName = $row['employee_firstName'];
    $employee_middleName = $row['employee_middleName'];
    $employee_lastName = $row['employee_lastName'];
    $employee_department = $row['employee_department'];
    $employee_position = $row['employee_position'];
    $employee_job_level = $row['employee_job_level'];
    $employee_years_company = $row['employee_years_company'];
    $employee_years_position = $row['employee_years_position'];
    $evaluationStatus = $row['evaluationStatus'];
    $pCycle = $row['performanceCycle'];
}
?>
<div class="col-sm-12">
	<div class="row">
		<div class="col-sm-1">
			<label class="control-label">Name: </label>
		</div>

		<div class="col-sm-5">
			<?php echo $employee_firstName . ' ' .  $employee_middleName . ' ' . $employee_lastName ; ?>
		</div>	

		<div class="col-sm-2">
			<label class="control-label">Period Covered: </label>
		</div>

		<div class="col-sm-4">
			<?php echo $pCycle; ?>
		</div>
	</div>


	<div class="row">
		<div class="col-sm-1">
			<label class="control-label">Department: </label>
		</div>

		<div class="col-sm-5">
			<?php echo $employee_department; ?>
		</div>

		<div class="col-sm-2">
			<label class="control-label">Position: </label>
		</div>

		<div class="col-sm-4">
			<?php echo $employee_position; ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-1">
			<label class="control-label">Section: </label>
		</div>

		<div class="col-sm-5">
			<?php echo ""; ?>
		</div>

		<div class="col-sm-2">
			<label class="control-label">Job Level: </label>
		</div>

		<div class="col-sm-4">
			<?php echo $employee_job_level; ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			<label class="control-label">Years in the Company: </label>
		</div>

		<div class="col-sm-1">
			<?php echo $employee_years_company; ?>
		</div>

        <div class="col-sm-3">
        </div>

		<div class="col-sm-2">
			<label class="control-label">Years in the Position: </label>
		</div>

		<div class="col-sm-4">
			<?php echo $employee_years_position; ?>
		</div>
	</div>

</div>

