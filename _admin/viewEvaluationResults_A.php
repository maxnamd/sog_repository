<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 8/27/2017
 * Time: 9:43 PM
 */
 include 'includes/header.php'; ?>


    <!-- Setting the treeview active -->
    <script type="text/javascript">
        document.getElementById("treeview3").className = "active menu-open"
    </script>

    <script>
        $(document).ready(function() {
            $('#tbl_EVresults').DataTable();
        } );
    </script>
    <!-- End Setting the treeview active -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Evaluation Results
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box box-info" style="border-color: green">
                <div class="box-header with-border">
                    <h3 class="box-title">Select Evaluation Results</h3>

                    <!-- <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div> -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin" id="tbl_EVresults">
                            <thead>
                            <tr>
                                <th class="col-sm-3">Employee Last Name</th>
                                <th class="col-sm-3">Employee First Name</th>
                                <th>Position</th>
                                <th>Number of Evaluations</th>
                                <th>Action</th>
                            </tr>

                            </thead>

                            <tbody>

                            <?php include "phpfunctions/viewEmployee_results_A.php";?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>
<?php
if (isset($_POST['btnSelectEmployee'])){
    $employee_firstName = $_POST['employee_firstName'];
    $employee_middleName = $_POST['employee_middleName'];
    $employee_lastName = $_POST['employee_lastName'];
    $employee_position = $_POST['employee_position'];
    $employee_username = $_POST['employee_username'];


    $_SESSION['employee_firstName'] = $employee_firstName;
    $_SESSION['employee_middleName'] = $employee_middleName;
    $_SESSION['employee_lastName'] = $employee_lastName;
    $_SESSION['employee_position'] = $employee_position;
    $_SESSION['employee_username'] = $employee_username;

     echo "
    <script type='text/javascript'>
        document.location.href='viewEvaluationResults_B.php';
    </script>";
}
?>
