<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 8/26/2017
 * Time: 10:34 PM
 */
include 'includes/header.php';
?>
<!-- Setting the treeview active -->
<script type="text/javascript">
    document.getElementById("treeview2a").className = "active menu-open"
</script>
<!-- End Setting the treeview active -->

<!-- Setting the datatables per table -->
<script>
    $(document).ready(function() {
        $('#tbl_ev_emp').DataTable();
    } );
</script>
<!-- End Setting the datatables per table -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Evaluation Form
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">


        <?php
        $queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
        $dateEVRow = mysql_fetch_array($queryDateEV);
        $startDateEV = $dateEVRow['startDate'];
        $endDateEV = $dateEVRow['endDate'];



        /*$currentDate = date("02/2018");*/
        if ($startDateEV >= $currentDate || $currentDate <= $endDateEV){
            ?>

            <?php
            $myKRA_dept=mysql_query("SELECT * FROM tbl_eformp1 WHERE kpiOwner = '$adepartment'") or die(mysql_error());
            $countMyKra = mysql_num_rows($myKRA_dept);


            if($countMyKra==0){
            ?>
                <!--<script type="text/javascript">
                    $(window).on('load',function(){
                        $('#shakeModal').modal('show');
                    });
                </script>

                <div id="shakeModal" class="modal" data-easein="shake"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="false">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    ×
                                </button>
                                <h4 class="modal-title">
                                    Ooooppsss
                                </h4>
                            </div>
                            <div class="modal-body">
                                <p>
                                    <h4>LOOKS LIKE YOU DONT HAVE ANY KPI ON YOUR ANY ASSIGNED SOG TO YOU!</h4>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">
                                    Close
                                </button>
                                <button class="btn btn-primary">
                                    Save changes
                                </button>
                            </div>
                        </div>
                    </div>
                </div>-->
                <script type="text/javascript">
                    swal({
                        title: "Oooopsss!",
                        text: "All of the assigned SOG employees to you doesnt have any KPI, Please add KPI before evaluating one of your employees",
                        type: "warning",
                        allowOutsideClick: false
                    }).then(function() {
                        window.location.href = "manageKRA_home.php";
                    });
                </script>
            <?php } ?>
            <div class="box box-info" style="border-color: green">
                <div class="box-header with-border">
                    <h3 class="box-title">Evaluate Employee</h3>

                    <!-- <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div> -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin" id="tbl_ev_emp">
                            <thead>
                            <tr>
                                <th class="col-sm-3">Employee Last Name</th>
                                <th class="col-sm-3">Employee First Name</th>
                                <th>Position</th>
                                <th>KPI Count</th>
                                <th>Evaluation Status</th>
                                <th>Action</th>
                            </tr>

                            </thead>

                            <tbody>
                            <?php include "phpfunctions/viewEmployee_evaluation.php";?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        <?php }
        else{
            echo "
            <script>
                bootbox.alert('EVALUATION NOT YET STARTED',
                function() {
                   setTimeout('window.location.replace(\'dashboard.php\')',600);
                });
            </script>
            ";
        }
        ?>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>
<?php
if (isset($_POST['btnSelectEmployee'])){
    $employee_firstName = $_POST['employee_firstName'];
    $employee_middleName = $_POST['employee_middleName'];
    $employee_lastName = $_POST['employee_lastName'];
    $employee_position = $_POST['employee_position'];
    $employee_year_in_position = $_POST['employee_year_in_position'];
    $employee_year_in_company = $_POST['employee_year_in_company'];
    $employee_job_level = $_POST['employee_job_level'];
    $employee_username = $_POST['employee_username'];

    $_SESSION['employee_firstName_evaluation'] = $employee_firstName;
    $_SESSION['employee_middleName_evaluation'] = $employee_middleName;
    $_SESSION['employee_lastName_evaluation'] = $employee_lastName;
    $_SESSION['employee_position_evaluation'] = $employee_position;
    $_SESSION['employee_year_in_position_evaluation'] = $employee_year_in_position;
    $_SESSION['employee_year_in_company_evaluation'] = $employee_year_in_company;
    $_SESSION['employee_job_level_evaluation'] = $employee_job_level;
    $_SESSION['employee_username_evaluation'] = $employee_username;

    echo "<script>location.href='viewQuestions_B.php';</script>";

}
?>
