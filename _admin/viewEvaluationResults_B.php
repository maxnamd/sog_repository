<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 8/27/2017
 * Time: 9:43 PM
 */
include 'includes/header.php';
$employee_firstName = $_SESSION['employee_firstName'];
$employee_middleName = $_SESSION['employee_middleName'];
$employee_lastName = $_SESSION['employee_lastName'];
$employee_fullName = $employee_firstName . ' ' . $employee_lastName;
$employee_position = $_SESSION['employee_position'];
$emp_username = $_SESSION['employee_username'];
?>


    <!-- Setting the treeview active -->
    <script type="text/javascript">
        document.getElementById("treeview3").className = "active menu-open"
    </script>

      <script>
        $(document).ready(function() {
            $('#tbl_EVresultsB').DataTable();
        } );
    </script>
    <!-- End Setting the treeview active -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Evaluation Results
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box box-info" style="border-color: green">
                <div class="box-header with-border">
                    <h3 class="box-title">Evaluation Results of <b><i><?php echo $employee_fullName?></i></b></h3>

                    <!-- <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div> -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin" id="tbl_EVresultsB">
                            <thead>
                            <tr>
                                <th class="col-sm-3">Performance Rating</th>
                                <th class="col-sm-3">Overall Rating</th>
                                <th>Performance Cycle</th>
                                <th>Action</th>
                            </tr>

                            </thead>

                            <tbody>
                            <?php include "phpfunctions/viewEmployee_results_B.php";?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- Stats content -->
            <section class="content">
                <div class="box box-info" style="border-color: green">
                    <div class="box-header with-border">
                        <h3 class="box-title">Performance Statistics</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <div class="col-sm-12 text-center">
                                <label class="label label-success">Stats</label>

                                <div id="area-chart" >


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.Stats -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>
<?php
$display_emp=mysql_query("SELECT * FROM tbl_evaluation_results WHERE emp_id = '$emp_username' AND evaluationStatus = 'COMPLETED'") or die(mysql_error());
$chart_data = '';
while($row=mysql_fetch_array($display_emp)) {
    $id = $row['emp_id'];
    $firstName = $row['employee_firstName'];
    $middleName = $row['employee_middleName'];
    $lastName = $row['employee_lastName'];
    $performanceCycle = $row['performanceCycle'];
    $performanceRating = $row['performance_rating'];
    $overallRating = $row['overall_rating'];

    $chart_data .= "{ y:'".$row['performanceCycle']."', a:".$row['overall_rating']."}, ";

}

?>
<script type="text/javascript">


    $(function () {
        config = {
            data: [<?php echo $chart_data?>],
            xkey: 'y',
            ykeys: ['a'],
            labels: ['Overall Rating'],
            fillOpacity: 0.6,
            hideHover: 'auto',
            behaveLikeLine: true,
            resize: true,
            pointFillColors:['#ffffff'],
            pointStrokeColors: ['black'],
            lineColors:['gray','red'],
            parseTime:false
        };
        config.element = 'area-chart';

        window.m = Morris.Area(config);
        $(window).on("resize", function(){
            m.redraw();
        });
    });
</script>
