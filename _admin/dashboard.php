<?php include 'includes/header.php'; ?>
  <!-- Setting the treeview active -->
  <script type="text/javascript">
  document.getElementById("treeview1").className = "active menu-open"
  </script>
  <!-- End Setting the treeview active -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-ios-chatbubble-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Unread Notifications</span>
                        <span class="datacount info-box-number"></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="ion ion-ios-pulse"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">KRA STATUS</span>
                        <span class="info-box-number">
                            <?php
                            $queryStatus= mysql_query("SELECT * FROM tbl_departments WHERE departmentOwner = '$admin_id'");
                            $statusRow = mysql_fetch_array($queryStatus);
                            $finalStatus = $statusRow['kpiStatus'];

                            echo $finalStatus;
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-calendar"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">KRA Creation Period</span>
                        <span class="info-box-number">
                            <?php
                            $queryDateKPI = mysql_query("SELECT * FROM tbl_datekpi");
                            $dateKpiRow = mysql_fetch_array($queryDateKPI);
                            $startDateKPI = $dateKpiRow['startDate'];
                            $endDateKPI = $dateKpiRow['endDate'];

                            echo $startDateKPI . " - " . $endDateKPI;
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-android-calendar"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Evaluation Period</span>
                        <span class="info-box-number">
                            <?php
                            $queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
                            $dateEVRow = mysql_fetch_array($queryDateEV);
                            $startDateEV = $dateEVRow['startDate'];
                            $endDateEV = $dateEVRow['endDate'];

                            echo $startDateEV . " - " . $endDateEV;
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- TABLE: STATUS KRA -->
        <div class="box box-info" style="border-color: green">
            <div class="box-header with-border">
                <h3 class="box-title">KRA Status per Deparment</h3>

                <!-- <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th>Deparment Name</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $display_status=mysql_query("SELECT * FROM tbl_departments") or die(mysql_error());

                        while($row=mysql_fetch_array($display_status)){

                            $departmentName = $row['department_name'];
                            $shortCut = $row['initials'];
                            $departmentOwner = $row['departmentOwner'];
                            $kpiStatus = $row['kpiStatus'];
                            $monitor = $row['monitor'];
                            echo "<tr>";
                            echo "<td>$departmentName</td>";

                            if ($kpiStatus=='SENT') {
                                # code...
                                echo "<td><span class='col-sm-5 label label-success'>$kpiStatus</span></td>";
                            }
                            elseif($kpiStatus=='APPROVED'){
                                echo "<td><span class='col-sm-5 label label-info'>$kpiStatus</span></td>";
                            }
                            else{
                                echo "<td><span class='col-sm-5 label label-warning'>$kpiStatus</span></td>";
                            }
                            ?>


                            <?php
                        }
                        ?>



                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
            </div>
            <!-- /.box-footer -->
        </div>
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  

 




<?php include 'includes/footer.php'; ?>