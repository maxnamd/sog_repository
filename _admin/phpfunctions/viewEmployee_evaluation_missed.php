<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 11/10/2017
 * Time: 5:15 PM
 */

$emp_count = 1;
$queryDateEV = mysql_query("SELECT * FROM tbl_dateevaluation");
$dateEVRow = mysql_fetch_array($queryDateEV);
$startDateEV = $dateEVRow['startDate'];
$endDateEV = $dateEVRow['endDate'];
$performanceCycle = $startDateEV . '-' . $endDateEV;

$fetch_hr = mysql_query("SELECT * FROM tbl_admins WHERE userlevel = 'SUPERADMIN'");
$row_hr = mysql_fetch_array($fetch_hr);
$hr_id = $row_hr['admin_id'];
$hr_dept = $row_hr['department'];


$display_emp=mysql_query("SELECT * FROM tbl_evaluation_results WHERE employee_department = '$adepartment' AND performanceCycle != '$performanceCycle' AND (evaluationStatus = 'NOT YET STARTED' || evaluationStatus = 'PROCESSING') AND late_status = 'YES'") or die(mysql_error());
while($row=mysql_fetch_array($display_emp)){
    $firstName = $row['employee_firstName'];
    $middleName = $row['employee_middleName'];
    $lastName = $row['employee_lastName'];
    $position = $row['employee_position'];
    $years_in_position = $row['employee_years_position'];
    $years_in_company = $row['employee_years_company'];
    $employee_job_level = $row['employee_job_level'];
    $emp_id = $row['emp_id'];
    $performanceCycle_tbl = $row['performanceCycle'];
    $employee_department = $row['employee_department'];


    ?>
    <tr>
        <td><?php echo $lastName?></td>
        <td><?php echo $firstName?></td>
        <td><?php echo $position?></td>
        <td><?php echo $performanceCycle_tbl?></td>
        <?php
        $display_evaluation_status_1=mysql_query("SELECT * FROM tbl_evaluation_requests WHERE employee_missed = '$emp_id' AND performanceCycle = '$performanceCycle_tbl' GROUP BY employee_department") or die(mysql_error());
        if (mysql_num_rows($display_evaluation_status_1)){
            // Rows exist
            while($evaluation_status_row=mysql_fetch_array($display_evaluation_status_1)){
                $request_status = $evaluation_status_row['request_status'];
                if($request_status=='APPROVED'){
                    echo "<td><span class='col-sm-12 label label-success'>$request_status</span></td>";

                }
                else if($request_status=='DECLINED'){
                    echo "<td><span class='col-sm-12 label label-danger'>$request_status</span></td>";
                }
                else{

                    echo "<td><span class='col-sm-12 label label-warning'>$request_status</span></td>";
                    $request_status = '';
                }
            }
        }
        else{
            echo "<td><span class='col-sm-12 label label-warning'>NO REQUEST YET</span></td>";
            $request_status = '';
        }
        ?>
        <?php
        $display_evaluation_status_2=mysql_query("SELECT * FROM tbl_evaluation_results WHERE emp_id = '$emp_id' AND performanceCycle = '$performanceCycle_tbl' GROUP BY employee_department") or die(mysql_error());
        if (mysql_num_rows($display_evaluation_status_2)){
            // Rows exist
            while($evaluation_status_row=mysql_fetch_array($display_evaluation_status_2)){
                $evaluation_status = $evaluation_status_row['evaluationStatus'];
                if($evaluation_status=='COMPLETED'){
                    echo "<td><span class='col-sm-12 label label-success'>$evaluation_status</span></td>";
                }
                else if($evaluation_status=='NOT YET STARTED'){
                    echo "<td><span class='col-sm-12 label label-warning'>$evaluation_status</span></td>";
                }
                else{
                    echo "<td><span class='col-sm-12 label label-warning'>$evaluation_status</span></td>";
                }
            }
        }
        else{
            echo "<td><span class='col-sm-12 label label-danger'>NOT YET STARTED</span></td>";
        }
        ?>
        <td>
            <div class="col-sm-12" style="margin-left: -20px">
                <div>
            <!--//MESSAGES-->
            <a href='#' class='btn btn-info btn-sm' data-toggle='modal' data-target="#viewMessages_modal<?php echo $emp_count?>">VIEW MESSAGES</a>

                    <!-- Messages modal -->
                    <div id="viewMessages_modal<?php echo $emp_count;?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Messages for this Request</h4>
                                </div>
                                <div class="modal-body" style="max-height: calc(100vh - 212px) !important;overflow-y: auto;padding: 1em;">

                                    <style type="text/css">
                                        .chat
                                        {
                                            list-style: none;
                                            margin: 0;
                                            padding: 0;
                                        }

                                        .chat li
                                        {
                                            margin-bottom: 10px;
                                            padding-bottom: 5px;
                                            border-bottom: 1px dotted #B3A9A9;
                                        }

                                        .chat li.left .chat-body
                                        {
                                            margin-left: 60px;
                                        }

                                        .chat li.right .chat-body
                                        {
                                            margin-right: 60px;
                                        }


                                        .chat li .chat-body p
                                        {
                                            margin: 0;
                                            color: #777777;
                                        }
                                        ::-webkit-scrollbar-track
                                        {
                                            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                                            background-color: #F5F5F5;
                                        }

                                        ::-webkit-scrollbar
                                        {
                                            width: 12px;
                                            background-color: #F5F5F5;
                                        }

                                        ::-webkit-scrollbar-thumb
                                        {
                                            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
                                            background-color: #555;
                                        }

                                    </style>

                                    <ul class="chat">
                                    <?php
                                    $display_my_messages=mysql_query("SELECT * FROM tbl_messages WHERE performanceCycle = '$performanceCycle_tbl' AND id_appeal = '$emp_id' ORDER BY sent_at ASC") or die(mysql_error());
                                    $check_num_rows = mysql_num_rows($display_my_messages);

                                    if ($check_num_rows > 0) {




                                    while($row_messages=mysql_fetch_array($display_my_messages)){
                                            $sender = $row_messages['sender'];
                                            $receiver = $row_messages['receiver'];
                                            $content = $row_messages['content'];
                                            $time = $row_messages['sent_at'];


                                        if($sender==$hr_dept){
                                        ?>
                                        <li class="left clearfix">
                                            <span class="chat-img pull-left">
                                                <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                                            </span>
                                            <div class="chat-body clearfix">
                                                <div class="header">
                                                    <strong class="primary-font"><?php echo $hr_dept?></strong> <small class="pull-right text-muted">
                                                        <span class="glyphicon glyphicon-time"></span><?php echo $time?></small>
                                                </div>
                                                <p>
                                                    <?php
                                                    echo $content;
                                                    ?>
                                                </p>
                                            </div>
                                        </li>
                                    <?php }
                                        else{
                                            /*$display_name_admin=mysql_query("SELECT * FROM tbl_admins WHERE department = '$sender'") or die(mysql_error());
                                            while($row_name_admin=mysql_fetch_array($display_name_admin)) {
                                                $fullname_admin = $row_name_admin['firstname'] . ' ' . $row_name_admin['lastname'];
                                            }*/
                                            ?>
                                            <li class="right clearfix">

                                                        <span class="chat-img pull-right">
                                                            <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                                                        </span>
                                                <div class="chat-body clearfix">
                                                    <div class="header">
                                                        <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?php echo $time?></small>
                                                        <strong class="pull-right primary-font"><?php echo $adepartment;?></strong>
                                                    </div>
                                                    <p>
                                                        <?php
                                                        echo $content;
                                                        ?>
                                                    </p>
                                                </div>
                                            </li>
                                            <?php
                                        }

                                    }}
                                    else{?>
                                    NO MESSAGES YET <?php }?>


                                    </ul>

                                </div>
                                <div class="modal-footer">

                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- END Messages modal -->
                </div>
            </div>
        </td>
        <td>
            <div class="col-sm-12" style="margin-left: -60px">
                <div class="row">
                    <?php
                    $display_evaluation_status_3=mysql_query("SELECT * FROM tbl_evaluation_requests WHERE employee_missed = '$emp_id' AND performanceCycle = '$performanceCycle_tbl' GROUP BY employee_department") or die(mysql_error());
                    if (mysql_num_rows($display_evaluation_status_3)){
                        // Rows exist
                        while($evaluation_status_row=mysql_fetch_array($display_evaluation_status_3)){
                            $request_status = $evaluation_status_row['request_status'];
                            if($request_status=='APPROVED'){
                                echo "";
                            }
                            else if($request_status=='DECLINED'){
                                echo "<div class='col-sm-5'><a href='#' class='btn btn-info btn-sm' data-toggle='modal' data-target=\"#requestModal_appeal$emp_count\">REQUEST AGAIN</a></div>";
                            }

                            else if($request_status=='SENT'){

                                echo "";
                            }
                            else if($request_status=='APPEALED'){

                                echo "";
                            }
                            else{
                                $request_status = '';
                                echo "<div class='col-sm-3'><a href='#' class='btn btn-info btn-sm' data-toggle='modal' data-target=\"#requestModal_$emp_count\">REQUEST</a></div>";
                            }
                        }
                    }
                    else{
                        $request_status = '';
                        echo "<div class='col-sm-3'><a href='#' class='btn btn-info btn-sm' data-toggle='modal' data-target=\"#requestModal_$emp_count\">REQUEST</a></div>";

                    }
                    ?>
                    <!--<div class="col-sm-3">
                            <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#requestModal_<?php /*echo $emp_count;*/?>" id="requestBtn_<?php /*echo $emp_count;*/?>">REQUEST</a>
                    </div>-->


                    <div class="col-sm-5"></div>

                        <form method="POST">
                            <input type="hidden" name="employee_firstName" value="<?php echo $firstName?>"/>
                            <input type="hidden" name="employee_middleName" value="<?php echo $middleName?>"/>
                            <input type="hidden" name="employee_lastName" value="<?php echo $lastName?>"/>
                            <input type="hidden" name="employee_position" value="<?php echo $position?>"/>
                            <input type="hidden" name="employee_job_level" value="<?php echo $employee_job_level?>"/>
                            <input type="hidden" name="employee_year_in_position" value="<?php echo $years_in_position?>"/>
                            <input type="hidden" name="employee_year_in_company" value="<?php echo $years_in_company?>"/>
                            <input type="hidden" name="employee_username" value="<?php echo $emp_id?>"/>
                            <input type="hidden" name="missed_performance_cycle" value="<?php echo $performanceCycle_tbl?>"/>

                            <?php
                            //$display_evaluation_status=mysql_query("SELECT tbl_evaluation_results.evaluationStatus,tbl_evaluation_requests.request_status FROM tbl_evaluation_results INNER JOIN tbl_evaluation_requests WHERE tbl_evaluation_results.evaluationStatus ='NOT YET STARTED' GROUP BY tbl_evaluation_results.employee_department");
                            $display_evaluation_status=mysql_query("SELECT * FROM tbl_evaluation_results WHERE emp_id = '$emp_id' AND performanceCycle != '$performanceCycle' AND late_status = 'YES' GROUP BY employee_department") or die(mysql_error());
                            if (mysql_num_rows($display_evaluation_status)){
                                while($evaluation_status_row=mysql_fetch_array($display_evaluation_status)){
                                    $evaluation_status = $evaluation_status_row['evaluationStatus'];
                                    if($evaluation_status=='COMPLETED'){
                                        echo "<div class=\"col-sm-3\"><button type=\"submit\" name=\"btnSelectEmployee\" id=\"btnSelectEmployee$emp_count\" class=\"btn btn-info btn-sm\" disabled>EVALUATE</button></div>";

                                    }
                                    else if($evaluation_status=='NOT YET STARTED' && $request_status == 'APPROVED'){
                                        echo "<div class=\"col-sm-3\"><button type=\"submit\" name=\"btnSelectEmployee\" id=\"btnSelectEmployee$emp_count\" class=\"btn btn-info btn-sm\">EVALUATE</button></div>";
                                    }
                                    else{
                                        echo "<div class=\"col-sm-3\"><button type=\"submit\" name=\"btnSelectEmployee\" id=\"btnSelectEmployee$emp_count\" class=\"btn btn-info btn-sm\" disabled>EVALUATE</button></div>";
                                    }
                                }
                            }
                            else{
                                echo "<div class=\"col-sm-3\"><button type=\"submit\" name=\"btnSelectEmployee\" id=\"btnSelectEmployee$emp_count\" class=\"btn btn-info btn-sm\">EVALUATE</button></div>";
                            }
                            ?>

                        </form>


                </div>
            </div>

            <!-- Request modal -->
            <div id="requestModal_<?php echo $emp_count;?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Request for Opening</h4>
                        </div>
                        <div class="modal-body">

                            <form method="post">

                                <input type="hidden" name="employee_id" value="<?php echo $emp_id?>"/>
                                <input type="hidden" name="performanceCycle_missed" value="<?php echo $performanceCycle_tbl?>"/>
                                <div class="form-group"> <!-- Message field -->
                                    <label class="control-label " for="message">Request Reason</label>
                                    <input type="hidden" name="sent_at_req" value="<?php date_default_timezone_set('Asia/Manila'); echo date('F j, Y g:i:a');?>">

                                    <textarea class="form-control ckeditor" id="request_msg_<?php echo $emp_count;?>" name="request_msg">
                                        <script type="text/javascript">
                                            var textarea = document.getElementById('request_msg_<?php echo $emp_count;?>');

                                            CKEDITOR.replace( 'textarea',
                                                {
                                                    toolbar : 'Basic', /* this does the magic */
                                                    uiColor : '#9AB8F3'
                                                });

                                        </script>
                                    </textarea>


                                </div>


                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" name="btnSubmitRequest">Submit</button>

                            </form>
                        </div>

                    </div>

                </div>
            </div>
            <!-- END Request modal -->


            <!-- Appeal Request modal -->
            <div id="requestModal_appeal<?php echo $emp_count;?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Appeal to Request for Opening</h4>
                        </div>
                        <div class="modal-body">

                            <form method="post">

                                <input type="hidden" name="employee_id" value="<?php echo $emp_id?>"/>
                                <input type="hidden" name="performanceCycle_missed" value="<?php echo $performanceCycle_tbl?>"/>
                                <input type="hidden" name="sent_at_appeal" value="<?php date_default_timezone_set('Asia/Manila'); echo date('F j, Y g:i:a');?>">

                                <div class="form-group"> <!-- Message field -->
                                    <label class="control-label " for="message">Request Reason</label>

                                    <textarea class="form-control ckeditor" id="request_msg_<?php echo $emp_count;?>" name="request_msg">
                                        <script type="text/javascript">
                                            var textarea = document.getElementById('request_msg_<?php echo $emp_count;?>');

                                            CKEDITOR.replace( 'textarea',
                                                {
                                                    toolbar : 'Basic', /* this does the magic */
                                                    uiColor : '#9AB8F3'
                                                });

                                        </script>
                                    </textarea>


                                </div>


                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" name="btnSubmitAppeal">Submit</button>

                            </form>
                        </div>

                    </div>

                </div>
            </div>
            <!-- END Request modal -->
        </td>


    </tr>
    <?php $emp_count++;}?>
<?php
if(isset($_POST['btnSubmitRequest'])){
    $employee_missed = $_POST['employee_id'];
    $admin_id = $_SESSION['id'];
    $employee_department_ev = $employee_department;
    $reason = $_POST['request_msg'];
    $performanceCycle_missed = $performanceCycle_tbl;
    $sent_at_req = $_POST['sent_at_req'];

    $fetch_sog_name=mysql_query("SELECT * FROM tbl_sog_employee WHERE emp_id = '$employee_missed'") or die(mysql_error());
    while($row_fsog=mysql_fetch_array($fetch_sog_name)) {
        $sog_firstName = $row_fsog['firstname'];
        $sog_middleName = $row_fsog['middlename'];
        $sog_lastName = $row_fsog['lastname'];

        $sog_fullname = $sog_firstName . ' ' . $sog_lastName;
    }



    mysql_query("INSERT INTO tbl_evaluation_requests (admin_id,employee_missed,employee_department,reason,performanceCycle) VALUES ('$admin_id','$employee_missed','$employee_department_ev','<li class=\"list-group-item\"> $reason </li>','$performanceCycle_missed')") or die(mysql_error());
    mysql_query("INSERT INTO tbl_notification (notification,sendBy,status) VALUES ('$adepartment SUBMITTED A REQUEST TO OPEN AN EVALUATION FOR $sog_fullname | $performanceCycle_missed','$adepartment','unread')") or die(mysql_error());
    mysql_query("INSERT INTO tbl_messages (subject,content,sender,receiver,sent_at,location,revision,performanceCycle,id_appeal)VALUES ('REQUEST TO REOPEN THE EVALUATION for $sog_fullname ($performanceCycle_missed)','$reason','$employee_department_ev','Human Resource Department','$sent_at_req','sentbox','','$performanceCycle_missed','$employee_missed')");

    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'Please wait for approval of HR to your request',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"viewQuestions_missed_ev.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"viewQuestions_missed_ev.php\";
                    }
                  }
                )
			</script>
		";

}

if(isset($_POST['btnSubmitAppeal'])){
    $employee_missed = $_POST['employee_id'];
    $admin_id = $_SESSION['id'];
    $employee_department_ev = $employee_department;
    $reason = $_POST['request_msg'];
    $performanceCycle_missed = $performanceCycle_tbl;
    $sent_at_appeal = $_POST['sent_at_appeal'];


    $fetch_sog_name=mysql_query("SELECT * FROM tbl_sog_employee WHERE emp_id = '$employee_missed'") or die(mysql_error());
    while($row_fsog=mysql_fetch_array($fetch_sog_name)) {
        $sog_firstName = $row_fsog['firstname'];
        $sog_middleName = $row_fsog['middlename'];
        $sog_lastName = $row_fsog['lastname'];

        $sog_fullname = $sog_firstName . ' ' . $sog_lastName;
    }

    $fetch_reason=mysql_query("SELECT * FROM tbl_evaluation_requests WHERE employee_missed = '$employee_missed' AND performanceCycle = '$performanceCycle_missed'") or die(mysql_error());
    while($row_res=mysql_fetch_array($fetch_reason)) {
        $last_reason = $row_res['reason'];
    }


    mysql_query("UPDATE tbl_evaluation_requests SET request_status = 'APPEALED',reason = '$last_reason <li class=\"list-group-item\"> $reason </li>' WHERE employee_missed = '$employee_missed' AND performanceCycle = '$performanceCycle_missed'");
    //mysql_query("INSERT INTO tbl_evaluation_requests (admin_id,employee_missed,employee_department,reason,performanceCycle) VALUES ('$admin_id','$employee_missed','$employee_department_ev','$reason','$performanceCycle_missed')") or die(mysql_error());
    mysql_query("INSERT INTO tbl_notification (notification,sendBy,status) VALUES ('$adepartment SUBMITTED AN APPEAL REGARDING TO A DECLINED REQUEST TO OPEN AN EVALUATION FOR $sog_fullname | $performanceCycle_missed','$admin_id','unread')") or die(mysql_error());
    mysql_query("INSERT INTO tbl_messages (subject,content,sender,receiver,sent_at,location,revision,performanceCycle,id_appeal)VALUES ('APPEAL TO DENIED REQUEST TO REOPEN THE EVALUATION for $sog_fullname ($performanceCycle_missed)','$reason','$employee_department_ev','Human Resource Department','$sent_at_appeal','sentbox','','$performanceCycle_missed','$employee_missed')");

    echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'Please wait for approval of HR to your appeal',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"viewQuestions_missed_ev.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"viewQuestions_missed_ev.php\";
                    }
                  }
                )
			</script>
		";

}
?>


