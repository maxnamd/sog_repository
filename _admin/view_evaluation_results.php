<?php include 'includes/header.php'; ?>

  <!-- Setting the treeview active -->
  <script type="text/javascript">
  document.getElementById("treeview3").className = "active menu-open"
  </script>
  <!-- End Setting the treeview active -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Evaluation Form
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">






      <form method="post">
        <div id="tabs">
            <ul>
              <li><a href="#tabs-1">P1 - KPI</a></li>
              <li><a href="#tabs-2a">P2A - Competencies</a></li>
              <li><a href="#tabs-2b">P2B - Competency Details</a></li>
              <li><a href="#tabs-3">P3 - Professionalism</a></li>
              <li><a href="#tabs-4">P4 - Customer Service</a></li>
              <li><a href="#tabs-5">Summary</a></li>

            </ul>


            <div id="tabs-1">
                <?php include 'forms/form_p1_evaluation_results.php';?>
            </div>

            <div id="tabs-2a">
                <?php include 'forms/form_p2a_evaluation_results.php';?>
            </div>

            <div id="tabs-2b">
                <?php include 'forms/form_p2b_evaluation_results.php';?>
            </div>

            <div id="tabs-3">
                <?php include 'forms/form_p3_evaluation_results.php';?>
            </div>

            <div id="tabs-4">
                <?php include 'forms/form_p4_evaluation_results.php';?>
            </div>

            <div id="tabs-5">
                <?php include 'forms/form_summary_evaluation_results.php';?>
            </div>

        </div>
        <div class="row">
        </div> 
      </form>


      

      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script type="text/javascript">
    callOnLoadCompute();
    function callOnLoadCompute(){
        var p1_sum = 0;

        $('.compute_p1_weights').each(function() {
            p1_sum += Number($(this).val());
        });

        document.getElementById('total_weight_p1').value=p1_sum;

        var p1b_sum = 0;

        $('.compute_p1_weighted_rating').each(function() {
            p1b_sum += Number($(this).val());
        });
        document.getElementById('total_weighted_rating').value=parseFloat(p1b_sum).toFixed(2);


        var p2_sum = 0;
        $('.compute_weights_p2').each(function() {
            p2_sum += Number($(this).val());
        });
        document.getElementById('total_weights_p2').value=p2_sum;

        var p2_sum_twrp2 = 0;

        $('.compute_p2_weighted_rating').each(function() {
            p2_sum_twrp2 += Number($(this).val());
        });

        document.getElementById('total_weighted_rating_p2').value=parseFloat(p2_sum_twrp2).toFixed(2);


        var p3_sum = 0;

        $('.compute_score_p3').each(function() {
            p3_sum += Number($(this).val());
        });

        document.getElementById('total_score_p3').value=parseFloat(p3_sum).toFixed(2);


        var p4_sum = 0;

        $('.factor_rating_p4').each(function() {

            p4_sum += Number($(this).val());
        });

        document.getElementById('total_cs_rating').value=parseFloat(p4_sum/4).toFixed(2);

        document.getElementById('cs_factor_rating').value=parseFloat(p4_sum/4).toFixed(2);

    }
</script>

<?php include 'includes/footer.php'; ?>
<?php include 'phpfunctions/sendEvaluation.php' ?>


