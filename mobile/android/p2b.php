<?php 

	$sql = "SELECT * FROM tbl_ans_eformp2b_critical_incident";
	
	require_once('dbConnect.php');
	
	$r = mysqli_query($con,$sql);
	
	$result = array();
	
	while($row = mysqli_fetch_array($r)){
		array_push($result,array(
			'id'=>$row['id'],
			'competencyName'=>$row['competencyName'],
			'criticalIncident'=>$row['criticalIncident'],
			'answeredFor'=>$row['answeredFor'],
			'performanceCycle'=>$row['performanceCycle'],
		
		));
	}
	
	echo json_encode(array('result'=>$result));
	
	mysqli_close($con);