<?php 

	$sql = "SELECT * FROM tbl_ans_summary_comments_acknowledgement";
	
	require_once('dbConnect.php');
	
	$r = mysqli_query($con,$sql);
	
	$result = array();
	
	while($row = mysqli_fetch_array($r)){
		array_push($result,array(
			'comments'=>$row['comments'],			
			'recommendations'=>$row['recommendations'],
			'approval'=>$row['approval'],	
			'answeredFor'=>$row['answeredFor'],				
			'performanceCycle'=>$row['performanceCycle'],			
		));
	}
	
	echo json_encode(array('result'=>$result));
	
	mysqli_close($con);