<?php 

	$sql = "SELECT * FROM tbl_evaluation_results";
	
	require_once('dbConnect.php');
	
	$r = mysqli_query($con,$sql);
	
	$result = array();
	
	while($row = mysqli_fetch_array($r)){
		array_push($result,array(
			'emp_id'=>$row['emp_id'],
			'evaluationStatus'=>$row['evaluationStatus'],
			'performance_rating'=>$row['performance_rating'],
			'overall_rating'=>$row['overall_rating'],			
			'performanceCycle'=>$row['performanceCycle'],		
			'ev_sog_feedback'=>$row['ev_sog_feedback'],				
		));
	}
	
	echo json_encode(array('result'=>$result));
	
	mysqli_close($con);