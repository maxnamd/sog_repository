<?php 

	$sql = "SELECT * FROM tbl_ans_eformp2a";
	
	require_once('dbConnect.php');
	
	$r = mysqli_query($con,$sql);
	
	$result = array();
	
	while($row = mysqli_fetch_array($r)){
		array_push($result,array(
			'competency'=>$row['competency'],
			'proficiencyLevel'=>$row['proficiencyLevel'],
			'p2_weights'=>$row['p2_weights'],
			'p2_rating'=>$row['p2_rating'],
			'p2_weightedRating'=>$row['p2_weightedRating'],
			'answeredFor'=>$row['answeredFor'],
			'performanceCycle'=>$row['performanceCycle'],
		
		));
	}
	
	echo json_encode(array('result'=>$result));
	
	mysqli_close($con);