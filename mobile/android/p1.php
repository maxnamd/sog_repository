<?php 

	$sql = "SELECT * FROM tbl_ans_eformp1";
	
	require_once('dbConnect.php');
	
	$r = mysqli_query($con,$sql);
	
	$result = array();
	
	while($row = mysqli_fetch_array($r)){
		array_push($result,array(
			'kraID'=>$row['kraID'],
			'kpiWeights'=>$row['kpiWeights'],
			'p1_rating'=>$row['p1_rating'],
			'p1_weightedRating'=>$row['p1_weightedRating'],
			'answeredFor'=>$row['answeredFor'],
			'performanceCycle'=>$row['performanceCycle'],
		
		));
	}
	
	echo json_encode(array('result'=>$result));
	
	mysqli_close($con);