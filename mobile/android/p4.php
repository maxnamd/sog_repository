<?php 

	$sql = "SELECT * FROM tbl_ans_eformp4";
	
	require_once('dbConnect.php');
	
	$r = mysqli_query($con,$sql);
	
	$result = array();
	
	while($row = mysqli_fetch_array($r)){
		array_push($result,array(
			'p4_area'=>$row['p4_area'],
			'customerFeedback'=>$row['customerFeedback'],
			'p4_score'=>$row['p4_score'],
			'p4_rating'=>$row['p4_rating'],
			'answeredFor'=>$row['answeredFor'],
			'performanceCycle'=>$row['performanceCycle'],
		
		));
	}
	
	echo json_encode(array('result'=>$result));
	
	mysqli_close($con);