<?php 

	$sql = "SELECT * FROM tbl_ans_summary";
	
	require_once('dbConnect.php');
	
	$r = mysqli_query($con,$sql);
	
	$result = array();
	
	while($row = mysqli_fetch_array($r)){
		array_push($result,array(
			'id'=>$row['id'],
			'total_weighted_rating'=>$row['total_weighted_rating'],
			'percentage_allocation'=>$row['percentage_allocation'],			
			'subtotal'=>$row['subtotal'],
			'answeredFor'=>$row['answeredFor'],			
			'performanceCycle'=>$row['performanceCycle'],			
		));
	}
	
	echo json_encode(array('result'=>$result));
	
	mysqli_close($con);