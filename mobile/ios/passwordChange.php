<?php

    $conn=mysqli_connect("localhost","root","","es_db");
    
    if(mysqli_connect_errno()){
        echo "Failed to Connect to the database: " . mysqli_connect_errno();
    }
    if ($_SERVER['REQUEST_METHOD']=='POST'){

        $passwordChange = mysqli_real_escape_string($conn, $_POST['password_temp']);
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $resultValue = array();

        $sql = "UPDATE tbl_sog_employee SET password_temp = '$passwordChange' WHERE username = '$username'";

        $result = mysqli_query($conn, $sql);
        $row = mysqli_affected_rows($conn);

        if (!$row > 0){
            $resultValue['status'] = "Attention";
            $resultValue['message'] = "Password Update Failed!";
            echo json_encode($resultValue);
        }else {
            $resultValue['status'] = "Message";
            $resultValue['message'] = "Password successfully updated! Please login again!";
            echo json_encode($resultValue);
        }
    }
    mysqli_close($conn);
?>