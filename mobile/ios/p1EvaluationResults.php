<?php

    $conn=mysqli_connect("localhost","root","","es_db");
    
    if(mysqli_connect_errno()){
        echo "Failed to Connect to the database: " . mysqli_connect_errno();
    }
    // get data from post method
    if (isset($_GET['answeredFor'], $_GET['performanceCycle'])) {
        $emp_id = $_GET['answeredFor'];
        $pCycle = $_GET['performanceCycle'];
        $sql = "SELECT * FROM tbl_ans_eformp1 WHERE answeredFor = '$emp_id' AND performanceCycle = '$pCycle'";
        
        if ($result = mysqli_query($conn, $sql))
        {
            $tempArray = array();
            $resultsArray = array();

            while($row = $result->fetch_array()) 
            {
                $tempArray['kpiTitle'] = $row['kpiTitle'];
                $tempArray['kpiWeights'] = $row['kpiWeights'];
                $tempArray['p1_rating'] = $row['p1_rating'];
                $tempArray['p1_weightedRating'] = $row['p1_weightedRating'];

                array_push($resultsArray, $tempArray);
            }
            echo json_encode($resultsArray);
        }
    }
    mysqli_close($conn);
?>