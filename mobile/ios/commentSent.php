<?php

    $conn=mysqli_connect("localhost","root","","es_db");
    
    if(mysqli_connect_errno()){
        echo "Failed to Connect to the database: " . mysqli_connect_errno();
    }
    if ($_SERVER['REQUEST_METHOD']=='POST'){

        $emp_id = mysqli_real_escape_string($conn, $_POST['emp_id']);
        $evaluationStatus = mysqli_real_escape_string($conn, $_POST['evaluationStatus']);
        $performanceCycle = mysqli_real_escape_string($conn, $_POST['performanceCycle']);
        $resultValue = array();

        $sql = "SELECT * FROM tbl_evaluation_results WHERE emp_id = '$emp_id' AND evaluationStatus = '$evaluationStatus' AND performanceCycle = '$performanceCycle'";

        if($result = mysqli_query($conn, $sql)){

            $tempValue = array();

            while($row = $result->fetch_array()) {

                $tempValue = $row['ev_sog_feedback'];

                if ($tempValue == "NO RESPONSE YET" OR $tempValue == " ") {

                    $resultValue['status'] = "MessageUpdating";
                    $resultValue['message'] = "Send us your comment and/or suggestions!";
                    echo json_encode($resultValue);
                        
                } else {
                    $resultValue['status'] = "MessageUpdated";
                    $resultValue['message'] = "Sorry! but you can only send comment once per Performance Cycle!";
                    echo json_encode($resultValue);    
                }
            }
        }
    }
    mysqli_close($conn);
?>