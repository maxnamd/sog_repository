<?php

    $conn=mysqli_connect("localhost","root","","es_db");
    
    if(mysqli_connect_errno()){
        echo "Failed to Connect to the database: " . mysqli_connect_errno();
    }
    if ($_SERVER['REQUEST_METHOD']=='POST'){

        $updateFeedback = mysqli_real_escape_string($conn, $_POST['ev_sog_feedback']);
        $emp_id = mysqli_real_escape_string($conn, $_POST['emp_id']);
        $evaluationStatus = mysqli_real_escape_string($conn, $_POST['evaluationStatus']);
        $performanceCycle = mysqli_real_escape_string($conn, $_POST['performanceCycle']);
        $resultValue = array();

        $sql = "SELECT * FROM tbl_evaluation_results WHERE emp_id = '$emp_id' AND evaluationStatus = '$evaluationStatus' AND performanceCycle = '$performanceCycle'";

        if($result = mysqli_query($conn, $sql)){

            $tempValue = array();

            while($row = $result->fetch_array()) {

                $tempValue = $row['ev_sog_feedback'];

                if ($tempValue == "NO RESPONSE YET" OR $tempValue == " ") {
                    $sqlUpdate = "UPDATE tbl_evaluation_results SET ev_sog_feedback = '$updateFeedback' WHERE emp_id = '$emp_id' AND evaluationStatus = '$evaluationStatus' AND performanceCycle = '$performanceCycle'";
                    $resultUpdate = mysqli_query($conn, $sqlUpdate);
                    $rowUpdate = $result->fetch_array();

                    $resultValue['status'] = "MessageUpdating";
                    $resultValue['message'] = "This Feed back will be used as a reference for your next PERFORMANCE EVALUATION!";
                    echo json_encode($resultValue);
                        
                } else {
                    $resultValue['status'] = "MessageUpdated";
                    $resultValue['message'] = "Sorry! but you can only send comment once per Performance Cyle!";
                    echo json_encode($resultValue);    
                }
            }
        }
    }
    mysqli_close($conn);
?>