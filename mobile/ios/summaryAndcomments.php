<?php

    $conn=mysqli_connect("localhost","root","","es_db");
    
    if(mysqli_connect_errno()){
        echo "Failed to Connect to the database: " . mysqli_connect_errno();
    }
    // get data from post method
        $emp_id = $_GET['answeredFor'];
        $performanceCycle = $_GET['performanceCycle'];

        $sql = "SELECT * FROM tbl_ans_summary_comments_acknowledgement WHERE answeredFor = '$emp_id' AND performanceCycle = '$performanceCycle'";
        
        if ($result = mysqli_query($conn, $sql))
        {
            $tempArray = array();
            $resultsArray = array();

            while($row = $result->fetch_array()) 
            {
                $tempArray['comments'] = $row['comments'];
                $tempArray['recommendations'] = $row['recommendations'];
                $tempArray['approval'] = $row['approval'];

                array_push($resultsArray, $tempArray);
            }
            echo json_encode($resultsArray);
        }
    mysqli_close($conn);
?>