<?php

    $conn=mysqli_connect("localhost","root","","es_db");
    
    if(mysqli_connect_errno()){
        echo "Failed to Connect to the database: " . mysqli_connect_errno();
    }
    // get data from post method
        $emp_id = $_GET['answeredFor'];
        $performanceCycle = $_GET['performanceCycle'];

        $sql = "SELECT * FROM tbl_ans_summary WHERE answeredFor = '$emp_id' AND performanceCycle = '$performanceCycle'";
        
        if ($result = mysqli_query($conn, $sql))
        {
            $tempArray = array();
            $resultsArray = array();

            while($row = $result->fetch_array()) 
            {
                $tempArray['total_weighted_rating'] = $row['total_weighted_rating'];
                $tempArray['percentage_allocation'] = $row['percentage_allocation'];
                $tempArray['subtotal'] = $row['subtotal'];

                array_push($resultsArray, $tempArray);
            }
            echo json_encode($resultsArray);
        }
    mysqli_close($conn);
?>