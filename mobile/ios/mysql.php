<?php
class mysql
{
    var $dbhost = null;
    var $dbuser = null;
    var $dbpass = null;
    var $dbname = null;
    var $conn = null;
    var $result = null;
    var $fname = null;
    var $mname = null;
    var $lname = null;
    var $passwordid = null;
    var $ptoken = null;
    var $emp_id = null;

    function __construct()
    {
        $mysqlfile = parse_ini_file('MySQL.ini');
        $this->dbhost = $mysqlfile['dbhost'];
        $this->dbuser = $mysqlfile['dbuser'];
        $this->dbpass = $mysqlfile['dbpass'];
        $this->dbname = $mysqlfile['dbname'];
    }

    public function connection(){
        $this->conn = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->dbname);
        if (mysqli_connect_error()){
            return false;
        }
        mysqli_set_charset($this->conn, 'utf8');
        return true;
    }

    public function closeConnection(){
        if ($this->conn != null)
        {
            mysqli_close($this->conn);
        }
    }

    public function checkUsername($username){
        $sql = "SELECT username FROM tbl_sog_employee WHERE username = '$username'";
        $this->result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($this->result) <= 0 ){
            return false;
        }
        return true;
    }

    public function checkPasswordUsername($username, $password){
        $sql = "SELECT username, password_temp FROM tbl_sog_employee WHERE username = '$username'";
        $this->result = mysqli_query($this->conn, $sql);
        $row = mysqli_fetch_array($this->result);
        if ($row['password_temp'] != $password){
            return false;
        }
        return true;
    }
}

?>