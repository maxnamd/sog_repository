<?php
include_once ('mysql.php');
$mysql = new mysql();
if (isset($_POST['username'], $_POST['password_temp'])) {
    $username = htmlentities(($_POST['username']));
    $password_temp = ($_POST['password_temp']);
    $resultValue = array();
//Connection to the database
    if ($mysql->connection()) {
        //Check if username exists
        if (!$mysql->checkUsername($username)) {
            $resultValue['status'] = "Attention";
            $resultValue['message'] = "Username not found!";
            echo json_encode($resultValue);
        } else {
            //Check the password functionality
            if (!$mysql->checkPasswordUsername($username, $password_temp)) {
                $resultValue['status'] = "Password Error";
                $resultValue['message'] = "Incorrect Password!";
                echo json_encode($resultValue);
            } else {
                $resultValue['status'] = "Success";
                $resultValue['message'] = "Login Successful!";
                echo json_encode($resultValue);
            }
        }
    } else {
        $resultValue['status'] = "Error";
        $resultValue['message'] = "Could not connect to the database";
        echo json_encode($resultValue);
    }
}
?>

<?php exit; ?>