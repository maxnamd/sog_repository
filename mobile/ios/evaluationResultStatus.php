<?php

    $conn=mysqli_connect("localhost","root","","es_db");
    
    if(mysqli_connect_errno()){
        echo "Failed to Connect to the database: " . mysqli_connect_errno();
    }
    // get data from post method
        $emp_id = $_GET['emp_id'];

        $sql = "SELECT * FROM tbl_evaluation_results WHERE emp_id = '$emp_id' order by performanceCycle desc";
        
        if ($result = mysqli_query($conn, $sql))
        {
            $tempArray = array();
            $resultsArray = array();

            while($row = $result->fetch_array()) 
            {
                $tempArray['evaluationStatus'] = $row['evaluationStatus'];
                $tempArray['performance_rating'] = $row['performance_rating'];
                $tempArray['overall_rating'] = $row['overall_rating'];
                $tempArray['performanceCycle'] = $row['performanceCycle'];

                array_push($resultsArray, $tempArray);
            }
            echo json_encode($resultsArray);
        }
    mysqli_close($conn);
?>