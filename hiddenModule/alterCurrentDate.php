<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 12/10/2017
 * Time: 10:18 PM
 */

include '../connection.php';
?>

<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>SOG Evaluation System</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="../assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../assets/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../assets/bower_components/Ionicons/css/ionicons.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../assets/dist/css/skins/skin-green.css">
        <link rel="stylesheet" href="../assets/bower_components/jquery-ui-1.12.1/jquery-ui.min.css">
        <link rel="stylesheet" href="../assets/bower_components/sweetalert2/sweetalert2.min.css">
        <link rel="stylesheet" href="../assets/bower_components/alertifyjs/css/alertify.min.css">
        <link rel="stylesheet" href="../assets/bower_components/alertifyjs//css/themes/bootstrap.css">
        <link rel="stylesheet" href="../assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

        <link rel="icon" href="../assets/images/upang.png" type="image/png">


        <!-- SCRIPTS-->
        <!-- SCRIPTS-->
        <!-- SCRIPTS-->

        <script src="../assets/bower_components/jquery/dist/jquery.js"></script>

        <script src="../assets/bower_components/jquery-ui-1.12.1/jquery-ui.min.js"></script>

        <!-- End Bootstrapjs  -->
        <script src="../assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Bootstrapjs  -->


        <!-- Bootbox  JS-->
        <script src="../assets/bower_components/bootbox/bootbox.min.js"></script>
        <!-- Bootbox  JS-->

        <script src="../assets/bower_components/notifyjs/notify.min.js"></script>


        <script src="../assets/bower_components/sweetalert2/sweetalert2.min.js"></script>




        <script type="text/javascript">
            $(function() {
                $('#currentDate').datepicker( {
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    dateFormat: 'mm/yy',
                    minDate: 0,
                    onClose: function(dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    }
                });

            });
        </script>
    </head>
    <body>
        <div class="section">
            <div class="panel panel-default">
                <div class="panel-heading">
                    CHANGE CURRENT DATE
                </div>

                <div class="panel-body">
                    <form method="post">
                        <input type="text" id="" class="form-control" name="currentDate"/>

                </div>

                <div class="panel-footer">
                        <button type="submit" name="btnSubmit">SUBMIT</button>
                    </form>
                </div>
            </div>

        </div>


    </body>
</html>
<?php
if(isset($_POST['btnSubmit'])){
    $currentDate = $_POST['currentDate'];

    mysql_query("UPDATE tbl_current_date SET date = '$currentDate'")or die(mysql_errno());
    echo "<script type='text/javascript'>
        swal({
                  title: 'SUCCESS!',
                  text: 'WAHHH',
                  type: \"success\",
                  timer: 1000,
                  allowOutsideClick: false,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"alterCurrentDate.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"alterCurrentDate.php\";
                    }
                  }
                )
      </script>";

}
?>
