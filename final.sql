-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2017 at 02:00 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `es_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admins`
--

CREATE TABLE IF NOT EXISTS `tbl_admins` (
  `admin_id` varchar(100) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `firstname` varchar(60) NOT NULL,
  `middlename` varchar(60) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` text CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `password_temp` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `paths` text NOT NULL,
  `userlevel` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admins`
--

INSERT INTO `tbl_admins` (`admin_id`, `lastname`, `firstname`, `middlename`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `username`, `password`, `password_temp`, `paths`, `userlevel`, `email`) VALUES
('emp-01', 'Ayson', 'Justin Louse', 'Vinluan', 'College of Information Technology Education', '8', 'Dean', 'D1', '8', '8', 'emp-01', 'ef7374870359e618cdd732585eeb3874', 'AYSON', '../assets/uploaded_files/accounts/default.png', 'ADMIN', 'aysonjustin@gmail.com'),
('superadmin', 'Clauna', 'Levi Marion', 'Almazan', 'Human Resource Department', '7', 'Dean', 'Regular', '', '7', 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 'CLAUNA', '../assets/uploaded_files/accounts/default.png', 'SUPERADMIN', 'aysonjustinmax@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeights` int(11) NOT NULL,
  `p1_rating` int(11) NOT NULL,
  `p1_weightedRating` float NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1kpi` (
  `id` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiDesc` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1_achievements`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1_achievements` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiAchievement` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` varchar(30) NOT NULL,
  `competencyDescription` longtext NOT NULL,
  `proficiencyLevel` text NOT NULL,
  `p2_weights` float NOT NULL,
  `p2_rating` int(11) NOT NULL,
  `p2_weightedRating` float NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(30) NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2b_critical_incident`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2b_critical_incident` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(30) NOT NULL,
  `criticalIncident` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL,
  `score` int(11) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp3_rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp3_rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` varchar(200) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp4` (
  `id` int(11) NOT NULL,
  `p4_area` varchar(50) NOT NULL,
  `customerFeedback` text NOT NULL,
  `p4_score` int(11) NOT NULL,
  `p4_rating` varchar(11) NOT NULL,
  `answeredBy` varchar(80) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_summary`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_summary` (
  `id` int(11) NOT NULL,
  `performance_rating` varchar(60) NOT NULL,
  `total_weighted_rating` float NOT NULL,
  `percentage_allocation` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_summary_comments_acknowledgement`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_summary_comments_acknowledgement` (
  `id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `recommendations` text NOT NULL,
  `approval` text NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_competencyratingscale`
--

CREATE TABLE IF NOT EXISTS `tbl_competencyratingscale` (
  `id` int(11) NOT NULL,
  `competency_rating_value` int(1) NOT NULL,
  `competency_title` text NOT NULL,
  `competency_description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_competencyratingscale`
--

INSERT INTO `tbl_competencyratingscale` (`id`, `competency_rating_value`, `competency_title`, `competency_description`) VALUES
(2, 4, 'Expert/ Exemplary  ', '(Evaluation; Defines Framework Modes; Mentor, Self-Actualization, Consistently exceeds all of the acquired competency level descriptors)'),
(3, 3, 'Advanced/ Accomplished ', '(Analysis; Synthesis; Understands Framework Models, Exceeds most competency level descriptors) '),
(4, 2, 'Intermediate / Developing ', '(Unsupervised Application; Beginning Analysis, Meets most of the required competency level descriptors) '),
(5, 1, 'Beginner', ' (Comprehension; Beginning Application; Some Experience, Meets some of the competency level descriptors) '),
(6, 0, '', 'Early stages of application of the item; Behavior not yet present.'),
(7, 0, '', 'The item does not apply to your position or performance of assigned tasks and responsibilities.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_current_date`
--

CREATE TABLE IF NOT EXISTS `tbl_current_date` (
  `id` int(11) NOT NULL,
  `date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dateevaluation`
--

CREATE TABLE IF NOT EXISTS `tbl_dateevaluation` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dateevaluation`
--

INSERT INTO `tbl_dateevaluation` (`startDate`, `endDate`) VALUES
('00/0000', '00/0000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_datekpi`
--

CREATE TABLE IF NOT EXISTS `tbl_datekpi` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_datekpi`
--

INSERT INTO `tbl_datekpi` (`startDate`, `endDate`) VALUES
('00/0000', '00/0000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departments`
--

CREATE TABLE IF NOT EXISTS `tbl_departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `initials` varchar(100) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL DEFAULT 'PENDING',
  `departmentOwner` varchar(100) NOT NULL,
  `monitor` varchar(6) NOT NULL DEFAULT 'unread',
  `activation_status` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_departments`
--

INSERT INTO `tbl_departments` (`id`, `department_name`, `initials`, `kpiStatus`, `departmentOwner`, `monitor`, `activation_status`) VALUES
(5, 'Human Resource Department', 'HRD', 'PENDING', 'superadmin', 'unread', ''),
(14, 'College of Information Technology Education', 'CITE', 'PENDING', '', 'unread', ''),
(15, 'College of Management and Accountancy', 'CMA', 'PENDING', '', 'unread', ''),
(16, 'Ff', 'FF', 'PENDING', '', 'unread', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dump_evaluation_period`
--

CREATE TABLE IF NOT EXISTS `tbl_dump_evaluation_period` (
  `id` int(100) NOT NULL,
  `start_date` varchar(100) NOT NULL,
  `end_date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp1` (
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeight` int(11) NOT NULL,
  `kpiOwner` varchar(60) NOT NULL,
  `kraCreatedFor` varchar(50) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` text NOT NULL,
  `competencyDesc` text NOT NULL,
  `requiredProfLevel` varchar(50) NOT NULL,
  `weights` float NOT NULL,
  `rating` int(2) NOT NULL,
  `weightedRating` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2a`
--

INSERT INTO `tbl_eformp2a` (`id`, `competency`, `competencyDesc`, `requiredProfLevel`, `weights`, `rating`, `weightedRating`) VALUES
(11, 'Adaptability', 'The ability to adapt to the culture and environment of the school community. Able to adjust to changes and exhibits a positive attitude towards different situations.', '', 10, 0, 0),
(12, 'Creativity and Innovation', 'The ability to provide creative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approaches to identify opportunities, implement solutions, measure impact and serve customers.', '', 10, 0, 0),
(13, 'Customer Focus', 'The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.', '', 20, 0, 0),
(14, 'Team Orientation', 'Able to effectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals. ', '', 20, 0, 0),
(15, 'Technical Knowledge', 'Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession.  Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.', '', 10, 0, 0),
(16, 'Leadership Excellence', 'Upholds values and ethics.  Attracts, motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential.', '', 10, 0, 0),
(17, 'Management Excellence', 'Broad understanding of the company’s various businesses and its key stakeholders.  Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, financial, and environmental resources.', '', 10, 0, 0),
(18, 'People Development', 'The ability to plan and support the development of employees’ skills and abilities so that they can fulfill current role and achieve full potential. ', '', 10, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2b`
--

INSERT INTO `tbl_eformp2b` (`id`, `competency`) VALUES
(1, 's'),
(2, 's');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp4` (
  `id` int(11) NOT NULL,
  `areas` varchar(30) NOT NULL,
  `customerFeedback` varchar(100) NOT NULL,
  `respondentScore` int(2) NOT NULL,
  `ratingFactor` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp4`
--

INSERT INTO `tbl_eformp4` (`id`, `areas`, `customerFeedback`, `respondentScore`, `ratingFactor`) VALUES
(2, 'April - June', '', 0, 0),
(3, 'July - September', '', 0, 0),
(4, 'October - December', '', 0, 0),
(5, 'January - March', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation_requests`
--

CREATE TABLE IF NOT EXISTS `tbl_evaluation_requests` (
  `id` int(11) NOT NULL,
  `admin_id` varchar(60) NOT NULL,
  `employee_missed` varchar(60) NOT NULL,
  `employee_department` varchar(60) NOT NULL,
  `reason` text NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `request_status` varchar(30) NOT NULL DEFAULT 'SENT'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation_results`
--

CREATE TABLE IF NOT EXISTS `tbl_evaluation_results` (
  `id` int(11) NOT NULL,
  `emp_id` varchar(60) NOT NULL,
  `employee_firstName` varchar(60) NOT NULL,
  `employee_middleName` varchar(60) NOT NULL,
  `employee_lastName` varchar(60) NOT NULL,
  `employee_department` varchar(60) NOT NULL,
  `employee_position` varchar(30) NOT NULL,
  `employee_job_level` varchar(30) NOT NULL,
  `employee_years_company` int(11) NOT NULL,
  `employee_years_position` int(11) NOT NULL,
  `evaluationStatus` varchar(30) NOT NULL DEFAULT 'NOT YET STARTED',
  `performance_rating` varchar(100) NOT NULL,
  `overall_rating` varchar(100) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `ev_sog_feedback` text NOT NULL,
  `late_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_level`
--

CREATE TABLE IF NOT EXISTS `tbl_job_level` (
  `id` int(11) NOT NULL,
  `code` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `level` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_job_level`
--

INSERT INTO `tbl_job_level` (`id`, `code`, `position`, `department`, `level`) VALUES
(6, 'PC1', 'SECRETARY', 'College of Information Technology Education', 'SOG EMPLOYEE');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kpiratingscale`
--

CREATE TABLE IF NOT EXISTS `tbl_kpiratingscale` (
  `id` int(11) NOT NULL,
  `rating_value` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `weighted_rating` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kpiratingscale`
--

INSERT INTO `tbl_kpiratingscale` (`id`, `rating_value`, `description`, `weighted_rating`) VALUES
(5, '4', 'Exceeds Expectations (Performs above standards all the time)', ''),
(6, '3', 'Meets Expectations (Solid, consistent performance; Generally meets standards)', ''),
(7, '2', 'Needs Improvement (Frequently fails to meet standards)', ''),
(8, '1', 'Unacceptable (Performance is below job requirements and needs)', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kpi_requests`
--

CREATE TABLE IF NOT EXISTS `tbl_kpi_requests` (
  `id` int(11) NOT NULL,
  `reason` text NOT NULL,
  `sent_at` varchar(60) NOT NULL,
  `department` varchar(60) NOT NULL,
  `request_status` varchar(60) NOT NULL,
  `date_of_kpi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE IF NOT EXISTS `tbl_messages` (
  `id` int(11) NOT NULL,
  `subject` text NOT NULL,
  `content` text NOT NULL,
  `sender` varchar(50) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'unread',
  `checked` varchar(3) NOT NULL DEFAULT 'no',
  `sent_at` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `deleted_by_sender` varchar(100) NOT NULL DEFAULT 'no',
  `deleted_by_receiver` varchar(100) NOT NULL DEFAULT 'no',
  `trash_by_HR` varchar(100) NOT NULL DEFAULT 'no',
  `trash_by_Department` varchar(100) NOT NULL DEFAULT 'no',
  `revision` varchar(100) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `id_appeal` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE IF NOT EXISTS `tbl_notification` (
  `id` int(11) NOT NULL,
  `notification` text NOT NULL,
  `sendBy` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification_user`
--

CREATE TABLE IF NOT EXISTS `tbl_notification_user` (
  `id` int(11) NOT NULL,
  `notification` text NOT NULL,
  `sendBy` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `owner` varchar(100) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_p1kpi` (
  `kraID` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kpiDesc` text NOT NULL,
  `kpiOwner` varchar(100) NOT NULL,
  `kraCreatedFor` varchar(60) NOT NULL,
  `kpiStatus` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p2bcritinc`
--

CREATE TABLE IF NOT EXISTS `tbl_p2bcritinc` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(60) NOT NULL,
  `criticalIncident` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p2bcritinc`
--

INSERT INTO `tbl_p2bcritinc` (`id`, `competencyName`, `criticalIncident`) VALUES
(1, 's', 'Critical Incident'),
(2, 's', 'Critical Incident'),
(3, 's', 'Critical Incident'),
(4, 's', 'Critical Incident'),
(1, 's', 'Critical Incident'),
(2, 's', 'Critical Incident'),
(3, 's', 'Critical Incident'),
(4, 's', 'Critical Incident');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p3rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_p3rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p3rubric`
--

INSERT INTO `tbl_p3rubric` (`id`, `areaName`, `rubricDesc`) VALUES
(4, 'DISCIPLINE', 'Ssss'),
(0, 'DISCIPLINE', 'DISCIPLINE'),
(0, 'ATTENDANCE', 'DISCIPLINE'),
(0, 'PUNCTUALITY', 'DISCIPLINE'),
(0, 'COMPLETION OF TRAINING', 'DISCIPLINE'),
(0, '', 'ATTENDANCE'),
(0, '', 'ATTENDANCE'),
(0, '', 'ATTENDANCE'),
(0, '', 'ATTENDANCE'),
(0, '', 'PUNCTUALITY'),
(0, '', 'PUNCTUALITY'),
(0, '', 'PUNCTUALITY'),
(0, '', 'PUNCTUALITY'),
(0, '', 'COMPLETION OF TRAINING'),
(0, '', 'COMPLETION OF TRAINING'),
(0, '', 'COMPLETION OF TRAINING'),
(0, '', 'COMPLETION OF TRAINING'),
(0, 'DISCIPLINE', 'DISCIPLINE'),
(0, 'ATTENDANCE', 'DISCIPLINE'),
(0, 'PUNCTUALITY', 'DISCIPLINE'),
(0, 'COMPLETION OF TRAINING', 'DISCIPLINE'),
(0, '', 'ATTENDANCE'),
(0, '', 'ATTENDANCE'),
(0, '', 'ATTENDANCE'),
(0, '', 'ATTENDANCE'),
(0, '', 'PUNCTUALITY'),
(0, '', 'PUNCTUALITY'),
(0, '', 'PUNCTUALITY'),
(0, '', 'PUNCTUALITY'),
(0, '', 'COMPLETION OF TRAINING'),
(0, '', 'COMPLETION OF TRAINING'),
(0, '', 'COMPLETION OF TRAINING'),
(0, '', 'COMPLETION OF TRAINING');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_proficiency_level`
--

CREATE TABLE IF NOT EXISTS `tbl_proficiency_level` (
  `id` int(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `initials` text NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_proficiency_level`
--

INSERT INTO `tbl_proficiency_level` (`id`, `value`, `initials`, `title`, `content`) VALUES
(9, '3', 'B', 'Contributing with Expertise', 'Specialist. Expert implementor. Customer advocate. Problem Solver.'),
(10, '2', 'C', 'Contribution through Teams/Peers', 'Project Leader. Idea Leader. Technical Troubleshooter. Coach.'),
(11, '1', 'D', 'Contributing through Strategy Building', 'Navigator. Idea Innovator. Leader Developer. Mission Advocate.'),
(12, '0', 'NE', '', 'Early stages of application of the item; Behavior not yet present.'),
(13, '0', 'NA', '', 'The item does not apply to your position or performance of assigned tasks and responsibilities.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating_execute`
--

CREATE TABLE IF NOT EXISTS `tbl_rating_execute` (
  `ID` int(11) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `performance_rating` varchar(100) NOT NULL,
  `overall_rating` varchar(100) NOT NULL,
  `performanceCycle` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp2a` (
  `criticalIncidentCount` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp2a`
--

INSERT INTO `tbl_settingsp2a` (`criticalIncidentCount`) VALUES
(4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp3`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp3` (
  `rubricCount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp3`
--

INSERT INTO `tbl_settingsp3` (`rubricCount`) VALUES
(4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sog_employee`
--

CREATE TABLE IF NOT EXISTS `tbl_sog_employee` (
  `emp_id` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `department_head` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_temp` varchar(100) NOT NULL,
  `email_employee` text NOT NULL,
  `paths` text NOT NULL,
  `kraStatus` varchar(15) NOT NULL DEFAULT 'NOT YET STARTED'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sog_employee`
--

INSERT INTO `tbl_sog_employee` (`emp_id`, `firstname`, `middlename`, `lastname`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `department_head`, `username`, `password`, `password_temp`, `email_employee`, `paths`, `kraStatus`) VALUES
('sog-01', 'Eula Maye', 'D', 'Alcantara', 'College of Information Technology Education', '8', 'Secretary', 'D1', '8', '8', '', 'sog-01', '', 'ALCANTARA', 'aysonjustin@gmail.com', '../assets/uploaded_files/accounts/default.png', 'NOT YET STARTED');

-- --------------------------------------------------------

--
-- Table structure for table `z_logs_ev`
--

CREATE TABLE IF NOT EXISTS `z_logs_ev` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp2a`
--
ALTER TABLE `tbl_ans_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp2b`
--
ALTER TABLE `tbl_ans_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp3`
--
ALTER TABLE `tbl_ans_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp4`
--
ALTER TABLE `tbl_ans_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_summary_comments_acknowledgement`
--
ALTER TABLE `tbl_ans_summary_comments_acknowledgement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_competencyratingscale`
--
ALTER TABLE `tbl_competencyratingscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_current_date`
--
ALTER TABLE `tbl_current_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_dump_evaluation_period`
--
ALTER TABLE `tbl_dump_evaluation_period`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp1`
--
ALTER TABLE `tbl_eformp1`
  ADD PRIMARY KEY (`kraID`);

--
-- Indexes for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_evaluation_requests`
--
ALTER TABLE `tbl_evaluation_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_evaluation_results`
--
ALTER TABLE `tbl_evaluation_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_job_level`
--
ALTER TABLE `tbl_job_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kpiratingscale`
--
ALTER TABLE `tbl_kpiratingscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kpi_requests`
--
ALTER TABLE `tbl_kpi_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification_user`
--
ALTER TABLE `tbl_notification_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_rating_execute`
--
ALTER TABLE `tbl_rating_execute`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_sog_employee`
--
ALTER TABLE `tbl_sog_employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `z_logs_ev`
--
ALTER TABLE `z_logs_ev`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp2a`
--
ALTER TABLE `tbl_ans_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp2b`
--
ALTER TABLE `tbl_ans_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp3`
--
ALTER TABLE `tbl_ans_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp4`
--
ALTER TABLE `tbl_ans_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ans_summary_comments_acknowledgement`
--
ALTER TABLE `tbl_ans_summary_comments_acknowledgement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_competencyratingscale`
--
ALTER TABLE `tbl_competencyratingscale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_current_date`
--
ALTER TABLE `tbl_current_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_dump_evaluation_period`
--
ALTER TABLE `tbl_dump_evaluation_period`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_evaluation_requests`
--
ALTER TABLE `tbl_evaluation_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_evaluation_results`
--
ALTER TABLE `tbl_evaluation_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_job_level`
--
ALTER TABLE `tbl_job_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_kpiratingscale`
--
ALTER TABLE `tbl_kpiratingscale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_kpi_requests`
--
ALTER TABLE `tbl_kpi_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_notification_user`
--
ALTER TABLE `tbl_notification_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_rating_execute`
--
ALTER TABLE `tbl_rating_execute`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `z_logs_ev`
--
ALTER TABLE `z_logs_ev`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
