-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2017 at 11:55 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `es_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admins`
--

CREATE TABLE IF NOT EXISTS `tbl_admins` (
  `id` int(11) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `firstname` varchar(60) NOT NULL,
  `middlename` varchar(60) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `password_temp` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `userlevel` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admins`
--

INSERT INTO `tbl_admins` (`id`, `lastname`, `firstname`, `middlename`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `username`, `password`, `password_temp`, `userlevel`) VALUES
(1, 'Clauna', 'Levi Marion', 'Almazan', 'CITE', '2', 'CEO', 'Regular', '', '2', 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 'superadmin', 'SUPERADMIN'),
(11, 'Ayson', 'Justin Louise ', 'Vinluan', 'College of Information Technology Education', '7', 'Dean', '', '2010', '7', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'ADMIN'),
(12, 'sdf', 'sdf', 'sdf', 'College of Arts and Sciences', '1', 'sdf', 'sdf', '2016', '1', 'sdf', '9c0a2523f776c96bce27eeb5671371e0', 'SDF', 'ADMIN'),
(13, 'Luna', 'Juan ', 'Miguel', 'College of Health and Sciences', '6', 'Department Head', 'Regular', '2011', '6', 'admin2', '7e015e3ed14067228004786f0281a295', 'LUNA', 'ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeights` int(11) NOT NULL,
  `p1_rating` int(11) NOT NULL,
  `p1_weightedRating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1kpi` (
  `id` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiDesc` text NOT NULL,
  `kpiOwner` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1_achievements`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1_achievements` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiAchievement` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dateevaluation`
--

CREATE TABLE IF NOT EXISTS `tbl_dateevaluation` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dateevaluation`
--

INSERT INTO `tbl_dateevaluation` (`startDate`, `endDate`) VALUES
('10/2017', '11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_datekpi`
--

CREATE TABLE IF NOT EXISTS `tbl_datekpi` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_datekpi`
--

INSERT INTO `tbl_datekpi` (`startDate`, `endDate`) VALUES
('08/2017', '09/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departments`
--

CREATE TABLE IF NOT EXISTS `tbl_departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `initials` varchar(100) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL DEFAULT 'PENDING',
  `departmentOwner` varchar(100) NOT NULL,
  `monitor` varchar(6) NOT NULL DEFAULT 'unread'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_departments`
--

INSERT INTO `tbl_departments` (`id`, `department_name`, `initials`, `kpiStatus`, `departmentOwner`, `monitor`) VALUES
(1, 'College of Information Technology Education', 'CITE', 'APPROVED', 'admin', 'read'),
(4, 'College of Arts and Sciences', 'CAS', 'PENDING', 'sdf sdf sdf', 'unread'),
(5, 'College of Health and Sciences', 'CHS', 'PENDING', 'admin2', 'unread');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp1` (
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeight` int(11) NOT NULL,
  `kpiOwner` varchar(60) NOT NULL,
  `kpiDateCreated` varchar(11) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp1`
--

INSERT INTO `tbl_eformp1` (`kraID`, `kpiTitle`, `kpiWeight`, `kpiOwner`, `kpiDateCreated`, `kpiStatus`) VALUES
(1, 'Leadership', 0, 'ADMIN-2', '', 'SENT'),
(2, 'COMMUNICATION & COORDINATION', 0, 'admin', '', 'APPROVED'),
(3, 'OFFICE MANAGEMENT', 0, 'admin', '', 'APPROVED'),
(4, 'PREPARATION OF REPORTS', 0, 'admin', '', 'APPROVED'),
(5, 'CUSTOMER SERVICE', 0, 'admin', '', 'APPROVED');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` text NOT NULL,
  `competencyDesc` varchar(500) NOT NULL,
  `requiredProfLevel` varchar(50) NOT NULL,
  `weights` float NOT NULL,
  `rating` int(2) NOT NULL,
  `weightedRating` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2a`
--

INSERT INTO `tbl_eformp2a` (`id`, `competency`, `competencyDesc`, `requiredProfLevel`, `weights`, `rating`, `weightedRating`) VALUES
(1, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community. Able to adjust to changes and exhibits a positive attitude towards different situations.</p>\r\n', 'B - Contributing with Expertise', 10, 0, 0),
(2, 'Creativity and Innovation', '<p>&nbsp;The ability to provide creative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approaches to identify opportunities, implement solutions, measure impact and serve customers.</p>\r\n', 'C - Contributing through Teams/Peers', 10, 0, 0),
(3, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', 'C - Contributing through Teams/Peers', 20, 0, 0),
(4, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.&nbsp;</p>\r\n', 'B - Contributing with Expertise', 20, 0, 0),
(5, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession. &nbsp;Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'B - Contributing with Expertise', 10, 0, 0),
(6, 'Management Excellence', '<p>Broad understanding of the company&rsquo;s various businesses and its key stakeholders. &nbsp;Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, financial, and environmental resources.</p>\r\n', 'B - Contributing with Expertise', 10, 0, 0),
(7, 'People Development', '<p>The ability to plan and support the development of employees&rsquo; skills and abilities so that they can fulfill current role and achieve full potential. &nbsp;Actively identifies new areas for learning; regularly creating and taking advantage of learning opportunities.</p>\r\n', 'A - Contributing Independently', 10, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2b`
--

INSERT INTO `tbl_eformp2b` (`id`, `competency`) VALUES
(1, 'Adaptability'),
(2, 'Creativity and Innovation'),
(3, 'Customer Focus'),
(4, 'Team Orientation'),
(5, 'Technical Knowledge'),
(6, 'Management Excellence'),
(7, 'People Development'),
(8, 'dd');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL,
  `rubric` varchar(500) NOT NULL,
  `score` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp3`
--

INSERT INTO `tbl_eformp3` (`id`, `areas`, `rubric`, `score`) VALUES
(1, 'DISCIPLINE', '', 0),
(2, 'ATTENDANCE', '', 0),
(3, 'PUNCTUALITY', '', 0),
(4, 'COMPLETION OF TRAINING', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp4` (
  `id` int(11) NOT NULL,
  `areas` varchar(30) NOT NULL,
  `customerFeedback` varchar(100) NOT NULL,
  `respondentScore` int(2) NOT NULL,
  `ratingFactor` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp4`
--

INSERT INTO `tbl_eformp4` (`id`, `areas`, `customerFeedback`, `respondentScore`, `ratingFactor`) VALUES
(1, 'April - June', '', 0, 0),
(2, 'July - September', '', 0, 0),
(3, 'October-December', '', 0, 0),
(4, 'January-March', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE IF NOT EXISTS `tbl_notification` (
  `id` int(11) NOT NULL,
  `notification` varchar(100) NOT NULL,
  `sendBy` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`id`, `notification`, `sendBy`, `status`, `timeStamp`) VALUES
(1, 'College of Information Technology Education Sent a KPI', 'admin', 'read', '2017-08-19 13:47:09'),
(2, 'College of Information Technology Education Sent a KPI', 'admin', 'read', '2017-08-20 12:22:39'),
(3, 'College of Information Technology Education Sent a KPI', 'admin', 'read', '2017-08-20 12:22:39'),
(4, 'College of Information Technology Education Sent a KPI', 'admin', 'read', '2017-08-20 14:07:57'),
(5, 'College of Information Technology Education Sent a KPI', 'admin', 'read', '2017-08-20 14:48:30'),
(6, 'College of Information Technology Education Sent a KPI', 'admin', 'read', '2017-08-20 14:56:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_p1kpi` (
  `kraID` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kpiDesc` varchar(100) NOT NULL,
  `kpiOwner` varchar(100) NOT NULL,
  `kpiDate` varchar(11) NOT NULL,
  `kpiStatus` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p1kpi`
--

INSERT INTO `tbl_p1kpi` (`kraID`, `kpiID`, `kpiDesc`, `kpiOwner`, `kpiDate`, `kpiStatus`) VALUES
(2, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'admin', '', 'APPROVED'),
(2, 2, 'Coordinate meetings and other activities within the college', 'admin', '', 'APPROVED'),
(2, 3, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'admin', '', 'APPROVED'),
(2, 4, 'Supervise the student assistants designated to the college', 'admin', '', 'APPROVED'),
(3, 1, 'Organize office files and documents', 'admin', '', 'APPROVED'),
(3, 2, 'Supervise order and cleanliness in the office', 'admin', '', 'APPROVED'),
(3, 3, 'Regular inventory and update of office supplies', 'admin', '', 'APPROVED'),
(4, 1, 'Submit reports (e.g., Attendance, Completion of Grades) Â required by other offices (e.g., HRD, Regi', 'admin', '', 'APPROVED'),
(4, 2, 'Monitor facultys attendanceÂ ', 'admin', '', 'APPROVED'),
(4, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'admin', '', 'APPROVED'),
(5, 1, 'Answer telephone calls and/or take messages', 'admin', '', 'APPROVED'),
(5, 2, 'Man the offices frontdesk to meet walk-in customers Â  Â Â ', 'admin', '', 'APPROVED'),
(5, 3, 'Arrange customers appointments with the Dean or faculty Â  Â ', 'admin', '', 'APPROVED'),
(5, 4, 'Handle data encoding duties during enrolment', 'admin', '', 'APPROVED');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p2bcritinc`
--

CREATE TABLE IF NOT EXISTS `tbl_p2bcritinc` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(20) NOT NULL,
  `criticalIncident` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p2bcritinc`
--

INSERT INTO `tbl_p2bcritinc` (`id`, `competencyName`, `criticalIncident`) VALUES
(1, 'Adaptability', 'Critical Incident'),
(2, 'Adaptability', 'Critical Incident'),
(3, 'Adaptability', 'Critical Incident'),
(1, 'Creativity and Innov', 'Critical Incident'),
(2, 'Creativity and Innov', 'Critical Incident'),
(3, 'Creativity and Innov', 'Critical Incident'),
(1, 'Customer Focus', 'Critical Incident'),
(2, 'Customer Focus', 'Critical Incident'),
(3, 'Customer Focus', 'Critical Incident'),
(1, 'Team Orientation', 'Critical Incident'),
(2, 'Team Orientation', 'Critical Incident'),
(3, 'Team Orientation', 'Critical Incident'),
(1, 'Technical Knowledge', 'Critical Incident'),
(2, 'Technical Knowledge', 'Critical Incident'),
(3, 'Technical Knowledge', 'Critical Incident'),
(1, 'Management Excellenc', 'Critical Incident'),
(2, 'Management Excellenc', 'Critical Incident'),
(3, 'Management Excellenc', 'Critical Incident'),
(1, 'People Development', 'Critical Incident'),
(2, 'People Development', 'Critical Incident'),
(3, 'People Development', 'Critical Incident'),
(1, 'dd', 'Critical Incident'),
(2, 'dd', 'Critical Incident'),
(3, 'dd', 'Critical Incident');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p3rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_p3rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p3rubric`
--

INSERT INTO `tbl_p3rubric` (`id`, `areaName`, `rubricDesc`) VALUES
(4, 'DISCIPLINE', '4:Â Has zero record of disciplinary action on offenses other than the above policies'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.'),
(3, 'ATTENDANCE', '3: Has exceeded available leave credits.'),
(2, 'ATTENDANCE', '2:Â Has exceeded the allowable hours/frequency of tardiness'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy'),
(4, 'COMPLETION OF TRAINING', 'bbbbbbbb'),
(3, 'COMPLETION OF TRAINING', 'aaaaaa'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.'),
(4, 'Test me', 'Has zero record of disciplinary action on offenses other than the above policies');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_proficiency_level`
--

CREATE TABLE IF NOT EXISTS `tbl_proficiency_level` (
  `id` int(100) NOT NULL,
  `initials` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_proficiency_level`
--

INSERT INTO `tbl_proficiency_level` (`id`, `initials`, `title`, `content`) VALUES
(1, 'A', 'Contributing Independently', 'Learner-Doer. Individual contributor. Follower, team-player, Customer touchpoint'),
(2, 'B', 'B', 'B');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp2a` (
  `criticalIncidentCount` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp2a`
--

INSERT INTO `tbl_settingsp2a` (`criticalIncidentCount`) VALUES
(3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp3`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp3` (
  `rubricCount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp3`
--

INSERT INTO `tbl_settingsp3` (`rubricCount`) VALUES
(4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sog_employee`
--

CREATE TABLE IF NOT EXISTS `tbl_sog_employee` (
  `id` int(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `department_head` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_temp` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `lastName` varchar(60) NOT NULL,
  `firstName` varchar(60) NOT NULL,
  `middleName` varchar(60) NOT NULL,
  `userLevel` varchar(15) NOT NULL DEFAULT 'SOG'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp1`
--
ALTER TABLE `tbl_eformp1`
  ADD PRIMARY KEY (`kraID`);

--
-- Indexes for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sog_employee`
--
ALTER TABLE `tbl_sog_employee`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_sog_employee`
--
ALTER TABLE `tbl_sog_employee`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
