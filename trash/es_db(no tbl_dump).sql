-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2017 at 11:09 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `es_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admins`
--

CREATE TABLE IF NOT EXISTS `tbl_admins` (
  `admin_id` varchar(100) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `firstname` varchar(60) NOT NULL,
  `middlename` varchar(60) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` text CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `password_temp` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `paths` text NOT NULL,
  `userlevel` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admins`
--

INSERT INTO `tbl_admins` (`admin_id`, `lastname`, `firstname`, `middlename`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `username`, `password`, `password_temp`, `paths`, `userlevel`, `email`) VALUES
('admin', 'Ayson', 'Justin Louise', 'Vinluan', 'College of Information Technology Education', '7', 'Dean', 'Regular', '2010', '7', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '../assets/uploaded_files/accounts/DSC_2013.jpg', 'ADMIN', 'aysonjustinmax@gmail.com'),
('admin2', 'Liwanag', 'Aristotle', '', 'College of Management and Accountancy', '13', 'Head', 'D1', '2004', '13', 'admin2', 'c47feaececaec397b36c150ed7fb95d1', 'LIWANAG', '../assets/uploaded_files/accounts/default.png', 'ADMIN', 'test@gmail.com'),
('admin3', 'Stark', 'Howard', 'H', 'College of Health and Sciences', '12', 'Head', 'H1', '2005', '12', 'admin3', 'a9106b6bc4ae581eb39418098a2891b4', 'STARK', '../assets/uploaded_files/accounts/default.png', 'ADMIN', 'howardlovestony@gmail.com'),
('emp-999', 'Hi', 'Ha', 'He', 'College of ON', '15', 'Dean', 'D1', '2002', '15', 'emp-999', 'bf8c144140b15befb8ce662632a7b76e', 'HI', '../assets/uploaded_files/accounts/default.png', 'ADMIN', 'ah@gmail.com'),
('r', 'R', 'R', 'R', 'Asdf', '18', 'r', 'R', '1999', '18', 'r', 'e1e1d3d40573127e9ee0480caf1283d6', 'R', '../assets/uploaded_files/accounts/default.png', 'ADMIN', 'r@rr'),
('superadmin', 'Clauna', 'Levi Marion', 'Almazan', 'Human Resource Department', '7', 'Dean', 'Regular', '', '7', 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 'CLAUNA', '../assets/uploaded_files/accounts/default.png', 'SUPERADMIN', 'levimarion_clauna@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeights` int(11) NOT NULL,
  `p1_rating` int(11) NOT NULL,
  `p1_weightedRating` float NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1`
--

INSERT INTO `tbl_ans_eformp1` (`id`, `kraID`, `kpiTitle`, `kpiWeights`, `p1_rating`, `p1_weightedRating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 2, 'COMMUNICATION & COORDINATION', 25, 4, 1, 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 3, 'OFFICE MANAGEMENT', 25, 3, 0.75, 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 4, 'PREPARATION OF REPORTS', 25, 3, 0.75, 'admin', 'SOG-001', '11/2018-12/2018'),
(4, 5, 'CUSTOMER SERVICE', 25, 3, 0.75, 'admin', 'SOG-001', '11/2018-12/2018'),
(5, 10, 's', 25, 4, 1, 'admin2', 'SOG-004', '11/2018-12/2018'),
(6, 11, 'a', 25, 4, 1, 'admin2', 'SOG-004', '11/2018-12/2018'),
(7, 12, 'asd', 25, 2, 0.5, 'admin2', 'SOG-004', '11/2018-12/2018'),
(8, 13, 'ff', 26, 3, 0.78, 'admin2', 'SOG-004', '11/2018-12/2018'),
(9, 10, 's', 25, 4, 1, 'admin2', 'SOG-003', '11/2018-12/2018'),
(10, 11, 'a', 25, 3, 0.75, 'admin2', 'SOG-003', '11/2018-12/2018'),
(11, 12, 'asd', 25, 3, 0.75, 'admin2', 'SOG-003', '11/2018-12/2018'),
(12, 13, 'ff', 25, 3, 0.75, 'admin2', 'SOG-003', '11/2018-12/2018'),
(13, 10, 's', 25, 3, 0.75, 'admin2', 'SOG-004', '11/2018-12/2018'),
(14, 11, 'a', 25, 2, 0.5, 'admin2', 'SOG-004', '11/2018-12/2018'),
(15, 12, 'asd', 25, 4, 1, 'admin2', 'SOG-004', '11/2018-12/2018'),
(16, 13, 'ff', 25, 2, 0.5, 'admin2', 'SOG-004', '11/2018-12/2018'),
(17, 10, 's', 25, 4, 1, 'admin2', 'SOG-002', '11/2018-12/2018'),
(18, 11, 'a', 25, 3, 0.75, 'admin2', 'SOG-002', '11/2018-12/2018'),
(19, 12, 'asd', 25, 3, 0.75, 'admin2', 'SOG-002', '11/2018-12/2018'),
(20, 13, 'ff', 25, 4, 1, 'admin2', 'SOG-002', '11/2018-12/2018'),
(21, 10, 's', 25, 4, 1, 'admin2', 'wer', '11/2018-12/2018'),
(22, 11, 'a', 25, 3, 0.75, 'admin2', 'wer', '11/2018-12/2018'),
(23, 12, 'asd', 25, 2, 0.5, 'admin2', 'wer', '11/2018-12/2018'),
(24, 13, 'ff', 25, 3, 0.75, 'admin2', 'wer', '11/2018-12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1kpi` (
  `id` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiDesc` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1kpi`
--

INSERT INTO `tbl_ans_eformp1kpi` (`id`, `kpiID`, `kraID`, `kpiDesc`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 1, 2, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 2, 2, 'Coordinate meetings and other activities within the college', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 3, 2, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'admin', 'SOG-001', '11/2018-12/2018'),
(4, 4, 2, 'Supervise the student assistants designated to the college', 'admin', 'SOG-001', '11/2018-12/2018'),
(5, 1, 3, 'Organize office files and documents', 'admin', 'SOG-001', '11/2018-12/2018'),
(6, 2, 3, 'Supervise order and cleanliness in the office', 'admin', 'SOG-001', '11/2018-12/2018'),
(7, 3, 3, 'Regular inventory and update of office supplies', 'admin', 'SOG-001', '11/2018-12/2018'),
(8, 1, 4, 'Submit reports (e.g., Attendance, Completion of Grades) Â required by other offices (e.g., HRD, Regi', 'admin', 'SOG-001', '11/2018-12/2018'),
(9, 2, 4, 'Monitor facultys attendanceÂ ', 'admin', 'SOG-001', '11/2018-12/2018'),
(10, 3, 4, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'admin', 'SOG-001', '11/2018-12/2018'),
(11, 1, 5, 'Answer telephone calls and/or take messages', 'admin', 'SOG-001', '11/2018-12/2018'),
(12, 2, 5, 'Man the offices frontdesk to meet walk-in customers Â  Â Â ', 'admin', 'SOG-001', '11/2018-12/2018'),
(13, 3, 5, 'Arrange customers appointments with the Dean or faculty Â  Â ', 'admin', 'SOG-001', '11/2018-12/2018'),
(14, 4, 5, 'Handle data encoding duties during enrolment', 'admin', 'SOG-001', '11/2018-12/2018'),
(15, 1, 10, 'ss', 'admin2', 'SOG-004', '11/2018-12/2018'),
(16, 1, 11, 'as', 'admin2', 'SOG-004', '11/2018-12/2018'),
(17, 1, 12, 'asdasd', 'admin2', 'SOG-004', '11/2018-12/2018'),
(18, 1, 13, 'fff', 'admin2', 'SOG-004', '11/2018-12/2018'),
(19, 1, 10, 'ss', 'admin2', 'SOG-003', '11/2018-12/2018'),
(20, 1, 11, 'as', 'admin2', 'SOG-003', '11/2018-12/2018'),
(21, 1, 12, 'asdasd', 'admin2', 'SOG-003', '11/2018-12/2018'),
(22, 1, 13, 'fff', 'admin2', 'SOG-003', '11/2018-12/2018'),
(23, 1, 10, 'ss', 'admin2', 'SOG-004', '11/2018-12/2018'),
(24, 1, 11, 'as', 'admin2', 'SOG-004', '11/2018-12/2018'),
(25, 1, 12, 'asdasd', 'admin2', 'SOG-004', '11/2018-12/2018'),
(26, 1, 13, 'fff', 'admin2', 'SOG-004', '11/2018-12/2018'),
(27, 1, 10, 'ss', 'admin2', 'SOG-002', '11/2018-12/2018'),
(28, 1, 11, 'as', 'admin2', 'SOG-002', '11/2018-12/2018'),
(29, 1, 12, 'asdasd', 'admin2', 'SOG-002', '11/2018-12/2018'),
(30, 1, 13, 'fff', 'admin2', 'SOG-002', '11/2018-12/2018'),
(31, 1, 10, 'ss', 'admin2', 'wer', '11/2018-12/2018'),
(32, 1, 11, 'as', 'admin2', 'wer', '11/2018-12/2018'),
(33, 1, 12, 'asdasd', 'admin2', 'wer', '11/2018-12/2018'),
(34, 1, 13, 'fff', 'admin2', 'wer', '11/2018-12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1_achievements`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1_achievements` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiAchievement` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1_achievements`
--

INSERT INTO `tbl_ans_eformp1_achievements` (`id`, `kraID`, `kpiAchievement`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 2, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 2, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 2, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(4, 2, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(5, 3, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(6, 3, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(7, 3, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(8, 4, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(9, 4, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(10, 4, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(11, 5, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(12, 5, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(13, 5, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(14, 5, '', 'admin', 'SOG-001', '11/2018-12/2018'),
(15, 10, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(16, 11, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(17, 12, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(18, 13, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(19, 10, '', 'admin2', 'SOG-003', '11/2018-12/2018'),
(20, 11, '', 'admin2', 'SOG-003', '11/2018-12/2018'),
(21, 12, '', 'admin2', 'SOG-003', '11/2018-12/2018'),
(22, 13, '', 'admin2', 'SOG-003', '11/2018-12/2018'),
(23, 10, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(24, 11, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(25, 12, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(26, 13, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(27, 10, '', 'admin2', 'SOG-002', '11/2018-12/2018'),
(28, 11, '', 'admin2', 'SOG-002', '11/2018-12/2018'),
(29, 12, '', 'admin2', 'SOG-002', '11/2018-12/2018'),
(30, 13, '', 'admin2', 'SOG-002', '11/2018-12/2018'),
(31, 10, '', 'admin2', 'wer', '11/2018-12/2018'),
(32, 11, '', 'admin2', 'wer', '11/2018-12/2018'),
(33, 12, '', 'admin2', 'wer', '11/2018-12/2018'),
(34, 13, '', 'admin2', 'wer', '11/2018-12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` varchar(30) NOT NULL,
  `competencyDescription` longtext NOT NULL,
  `proficiencyLevel` varchar(35) NOT NULL,
  `p2_weights` float NOT NULL,
  `p2_rating` int(11) NOT NULL,
  `p2_weightedRating` float NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2a`
--

INSERT INTO `tbl_ans_eformp2a` (`id`, `competency`, `competencyDescription`, `proficiencyLevel`, `p2_weights`, `p2_rating`, `p2_weightedRating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community. Able to adjust to changes and exhibits a positive attitude towards different situations</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approaches to identify opportunities, implement solutions, measure impact and serve customers.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationship with customers and business partners.</p>\r\n', 'C Contribution through Teams/Peers ', 20, 4, 0.4, 'admin', 'SOG-001', '11/2018-12/2018'),
(4, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'D Contributing through Strategy Bui', 20, 3, 0.3, 'admin', 'SOG-001', '11/2018-12/2018'),
(5, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin', 'SOG-001', '11/2018-12/2018'),
(6, 'Leadership Excellence', '<p>Upholds values and ethics. Attracts, motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 4, 0.4, 'admin', 'SOG-001', '11/2018-12/2018'),
(7, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, finanacial, and environmental resources.</p>\r\n', 'A Contributing Independently Learne', 10, 4, 0.4, 'admin', 'SOG-001', '11/2018-12/2018'),
(8, 'People Development', '<p>The ability to plan and support the development of employeesâ€™ skills and abilities so that they can fulfill current role and achieve full potential. Actively identifies new area for learning; regularly creating and taking advantage of learning opportunities.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin', 'SOG-001', '11/2018-12/2018'),
(9, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community. Able to adjust to changes and exhibits a positive attitude towards different situations</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(10, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approaches to identify opportunities, implement solutions, measure impact and serve customers.</p>\r\n', 'B Contributing with Expertise Speci', 10, 2, 0.2, 'admin2', 'SOG-004', '11/2018-12/2018'),
(11, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationship with customers and business partners.</p>\r\n', 'A Contributing Independently Learne', 20, 3, 0.3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(12, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'B Contributing with Expertise Speci', 20, 3, 0.3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(13, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(14, 'Leadership Excellence', '<p>Upholds values and ethics. Attracts, motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(15, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, finanacial, and environmental resources.</p>\r\n', 'D Contributing through Strategy Bui', 10, 3, 0.3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(16, 'People Development', '<p>The ability to plan and support the development of employeesâ€™ skills and abilities so that they can fulfill current role and achieve full potential. Actively identifies new area for learning; regularly creating and taking advantage of learning opportunities.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 4, 0.4, 'admin2', 'SOG-004', '11/2018-12/2018'),
(17, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community. Able to adjust to changes and exhibits a positive attitude towards different situations</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin2', 'SOG-003', '11/2018-12/2018'),
(18, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approaches to identify opportunities, implement solutions, measure impact and serve customers.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin2', 'SOG-003', '11/2018-12/2018'),
(19, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationship with customers and business partners.</p>\r\n', 'B Contributing with Expertise Speci', 20, 4, 0.4, 'admin2', 'SOG-003', '11/2018-12/2018'),
(20, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'C Contribution through Teams/Peers ', 20, 3, 0.3, 'admin2', 'SOG-003', '11/2018-12/2018'),
(21, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin2', 'SOG-003', '11/2018-12/2018'),
(22, 'Leadership Excellence', '<p>Upholds values and ethics. Attracts, motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin2', 'SOG-003', '11/2018-12/2018'),
(23, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, finanacial, and environmental resources.</p>\r\n', 'D Contributing through Strategy Bui', 10, 2, 0.2, 'admin2', 'SOG-003', '11/2018-12/2018'),
(24, 'People Development', '<p>The ability to plan and support the development of employeesâ€™ skills and abilities so that they can fulfill current role and achieve full potential. Actively identifies new area for learning; regularly creating and taking advantage of learning opportunities.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin2', 'SOG-003', '11/2018-12/2018'),
(25, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community. Able to adjust to changes and exhibits a positive attitude towards different situations</p>\r\n', 'C Contribution through Teams/Peers ', 10, 4, 0.4, 'admin2', 'SOG-004', '11/2018-12/2018'),
(26, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approaches to identify opportunities, implement solutions, measure impact and serve customers.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(27, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationship with customers and business partners.</p>\r\n', 'B Contributing with Expertise Speci', 20, 3, 0.3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(28, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'D Contributing through Strategy Bui', 20, 4, 0.4, 'admin2', 'SOG-004', '11/2018-12/2018'),
(29, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin2', 'SOG-004', '11/2018-12/2018'),
(30, 'Leadership Excellence', '<p>Upholds values and ethics. Attracts, motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 4, 0.4, 'admin2', 'SOG-004', '11/2018-12/2018'),
(31, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, finanacial, and environmental resources.</p>\r\n', 'B Contributing with Expertise Speci', 10, 2, 0.2, 'admin2', 'SOG-004', '11/2018-12/2018'),
(32, 'People Development', '<p>The ability to plan and support the development of employeesâ€™ skills and abilities so that they can fulfill current role and achieve full potential. Actively identifies new area for learning; regularly creating and taking advantage of learning opportunities.</p>\r\n', 'NE  Early stages of application of ', 10, 4, 0.4, 'admin2', 'SOG-004', '11/2018-12/2018'),
(33, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community. Able to adjust to changes and exhibits a positive attitude towards different situations</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'admin2', 'SOG-002', '11/2018-12/2018'),
(34, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approaches to identify opportunities, implement solutions, measure impact and serve customers.</p>\r\n', 'A Contributing Independently Learne', 10, 4, 0.4, 'admin2', 'SOG-002', '11/2018-12/2018'),
(35, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationship with customers and business partners.</p>\r\n', 'B Contributing with Expertise Speci', 20, 2, 0.2, 'admin2', 'SOG-002', '11/2018-12/2018'),
(36, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'B Contributing with Expertise Speci', 20, 4, 0.4, 'admin2', 'SOG-002', '11/2018-12/2018'),
(37, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'admin2', 'SOG-002', '11/2018-12/2018'),
(38, 'Leadership Excellence', '<p>Upholds values and ethics. Attracts, motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 4, 0.4, 'admin2', 'SOG-002', '11/2018-12/2018'),
(39, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, finanacial, and environmental resources.</p>\r\n', 'D Contributing through Strategy Bui', 10, 4, 0.4, 'admin2', 'SOG-002', '11/2018-12/2018'),
(40, 'People Development', '<p>The ability to plan and support the development of employeesâ€™ skills and abilities so that they can fulfill current role and achieve full potential. Actively identifies new area for learning; regularly creating and taking advantage of learning opportunities.</p>\r\n', 'B Contributing with Expertise Speci', 10, 2, 0.2, 'admin2', 'SOG-002', '11/2018-12/2018'),
(41, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community. Able to adjust to changes and exhibits a positive attitude towards different situations</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin2', 'wer', '11/2018-12/2018'),
(42, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approaches to identify opportunities, implement solutions, measure impact and serve customers.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin2', 'wer', '11/2018-12/2018'),
(43, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationship with customers and business partners.</p>\r\n', 'C Contribution through Teams/Peers ', 20, 2, 0.2, 'admin2', 'wer', '11/2018-12/2018'),
(44, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'C Contribution through Teams/Peers ', 20, 3, 0.3, 'admin2', 'wer', '11/2018-12/2018'),
(45, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'admin2', 'wer', '11/2018-12/2018'),
(46, 'Leadership Excellence', '<p>Upholds values and ethics. Attracts, motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 4, 0.4, 'admin2', 'wer', '11/2018-12/2018'),
(47, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, finanacial, and environmental resources.</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin2', 'wer', '11/2018-12/2018'),
(48, 'People Development', '<p>The ability to plan and support the development of employeesâ€™ skills and abilities so that they can fulfill current role and achieve full potential. Actively identifies new area for learning; regularly creating and taking advantage of learning opportunities.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin2', 'wer', '11/2018-12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(30) NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2b`
--

INSERT INTO `tbl_ans_eformp2b` (`id`, `competency`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'Creativity and Innovation', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'Customer Focus', 'admin', 'SOG-001', '11/2018-12/2018'),
(4, 'Team Orientation', 'admin', 'SOG-001', '11/2018-12/2018'),
(5, 'Technical Knowledge', 'admin', 'SOG-001', '11/2018-12/2018'),
(6, 'Leadership Excellence', 'admin', 'SOG-001', '11/2018-12/2018'),
(7, 'Management Excellence', 'admin', 'SOG-001', '11/2018-12/2018'),
(8, 'People Development', 'admin', 'SOG-001', '11/2018-12/2018'),
(9, 'Adaptability', 'admin2', 'SOG-004', '11/2018-12/2018'),
(10, 'Creativity and Innovation', 'admin2', 'SOG-004', '11/2018-12/2018'),
(11, 'Customer Focus', 'admin2', 'SOG-004', '11/2018-12/2018'),
(12, 'Team Orientation', 'admin2', 'SOG-004', '11/2018-12/2018'),
(13, 'Technical Knowledge', 'admin2', 'SOG-004', '11/2018-12/2018'),
(14, 'Leadership Excellence', 'admin2', 'SOG-004', '11/2018-12/2018'),
(15, 'Management Excellence', 'admin2', 'SOG-004', '11/2018-12/2018'),
(16, 'People Development', 'admin2', 'SOG-004', '11/2018-12/2018'),
(17, 'Adaptability', 'admin2', 'SOG-003', '11/2018-12/2018'),
(18, 'Creativity and Innovation', 'admin2', 'SOG-003', '11/2018-12/2018'),
(19, 'Customer Focus', 'admin2', 'SOG-003', '11/2018-12/2018'),
(20, 'Team Orientation', 'admin2', 'SOG-003', '11/2018-12/2018'),
(21, 'Technical Knowledge', 'admin2', 'SOG-003', '11/2018-12/2018'),
(22, 'Leadership Excellence', 'admin2', 'SOG-003', '11/2018-12/2018'),
(23, 'Management Excellence', 'admin2', 'SOG-003', '11/2018-12/2018'),
(24, 'People Development', 'admin2', 'SOG-003', '11/2018-12/2018'),
(25, 'Adaptability', 'admin2', 'SOG-004', '11/2018-12/2018'),
(26, 'Creativity and Innovation', 'admin2', 'SOG-004', '11/2018-12/2018'),
(27, 'Customer Focus', 'admin2', 'SOG-004', '11/2018-12/2018'),
(28, 'Team Orientation', 'admin2', 'SOG-004', '11/2018-12/2018'),
(29, 'Technical Knowledge', 'admin2', 'SOG-004', '11/2018-12/2018'),
(30, 'Leadership Excellence', 'admin2', 'SOG-004', '11/2018-12/2018'),
(31, 'Management Excellence', 'admin2', 'SOG-004', '11/2018-12/2018'),
(32, 'People Development', 'admin2', 'SOG-004', '11/2018-12/2018'),
(33, 'Adaptability', 'admin2', 'SOG-002', '11/2018-12/2018'),
(34, 'Creativity and Innovation', 'admin2', 'SOG-002', '11/2018-12/2018'),
(35, 'Customer Focus', 'admin2', 'SOG-002', '11/2018-12/2018'),
(36, 'Team Orientation', 'admin2', 'SOG-002', '11/2018-12/2018'),
(37, 'Technical Knowledge', 'admin2', 'SOG-002', '11/2018-12/2018'),
(38, 'Leadership Excellence', 'admin2', 'SOG-002', '11/2018-12/2018'),
(39, 'Management Excellence', 'admin2', 'SOG-002', '11/2018-12/2018'),
(40, 'People Development', 'admin2', 'SOG-002', '11/2018-12/2018'),
(41, 'Adaptability', 'admin2', 'wer', '11/2018-12/2018'),
(42, 'Creativity and Innovation', 'admin2', 'wer', '11/2018-12/2018'),
(43, 'Customer Focus', 'admin2', 'wer', '11/2018-12/2018'),
(44, 'Team Orientation', 'admin2', 'wer', '11/2018-12/2018'),
(45, 'Technical Knowledge', 'admin2', 'wer', '11/2018-12/2018'),
(46, 'Leadership Excellence', 'admin2', 'wer', '11/2018-12/2018'),
(47, 'Management Excellence', 'admin2', 'wer', '11/2018-12/2018'),
(48, 'People Development', 'admin2', 'wer', '11/2018-12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2b_critical_incident`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2b_critical_incident` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(30) NOT NULL,
  `criticalIncident` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2b_critical_incident`
--

INSERT INTO `tbl_ans_eformp2b_critical_incident` (`id`, `competencyName`, `criticalIncident`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', 'Critical Incident #1:', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'People Development', 'Critical Incident #1:', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'People Development', 'Critical Incident #2:', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'People Development', 'Critical Incident #3:', 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'Adaptability', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'People Development', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'People Development', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'People Development', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Adaptability', 'Critical Incident #1:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'People Development', 'Critical Incident #1:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'People Development', 'Critical Incident #2:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'People Development', 'Critical Incident #3:', 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'Adaptability', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'People Development', 'Critical Incident #1:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'People Development', 'Critical Incident #2:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'People Development', 'Critical Incident #3:', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'Adaptability', 'Critical Incident #1:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'People Development', 'Critical Incident #1:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'People Development', 'Critical Incident #2:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'People Development', 'Critical Incident #3:', 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'Adaptability', 'Critical Incident #1:', 'admin2', 'wer', '11/2018-12/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'admin2', 'wer', '11/2018-12/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'admin2', 'wer', '11/2018-12/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'admin2', 'wer', '11/2018-12/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'admin2', 'wer', '11/2018-12/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'admin2', 'wer', '11/2018-12/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'admin2', 'wer', '11/2018-12/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'admin2', 'wer', '11/2018-12/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'admin2', 'wer', '11/2018-12/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'admin2', 'wer', '11/2018-12/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'admin2', 'wer', '11/2018-12/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'admin2', 'wer', '11/2018-12/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'admin2', 'wer', '11/2018-12/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'admin2', 'wer', '11/2018-12/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'admin2', 'wer', '11/2018-12/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'admin2', 'wer', '11/2018-12/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'admin2', 'wer', '11/2018-12/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'admin2', 'wer', '11/2018-12/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'admin2', 'wer', '11/2018-12/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'admin2', 'wer', '11/2018-12/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'admin2', 'wer', '11/2018-12/2018'),
(1, 'People Development', 'Critical Incident #1:', 'admin2', 'wer', '11/2018-12/2018'),
(2, 'People Development', 'Critical Incident #2:', 'admin2', 'wer', '11/2018-12/2018'),
(3, 'People Development', 'Critical Incident #3:', 'admin2', 'wer', '11/2018-12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL,
  `score` int(11) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp3`
--

INSERT INTO `tbl_ans_eformp3` (`id`, `areas`, `score`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'DISCIPLINE', 3, 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'ATTENDANCE', 2, 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'PUNCTUALITY', 3, 'admin', 'SOG-001', '11/2018-12/2018'),
(4, 'COMPLETION OF TRAINING', 3, 'admin', 'SOG-001', '11/2018-12/2018'),
(5, 'DISCIPLINE', 3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(6, 'ATTENDANCE', 4, 'admin2', 'SOG-004', '11/2018-12/2018'),
(7, 'PUNCTUALITY', 3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(8, 'COMPLETION OF TRAINING', 3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(9, 'DISCIPLINE', 4, 'admin2', 'SOG-003', '11/2018-12/2018'),
(10, 'ATTENDANCE', 4, 'admin2', 'SOG-003', '11/2018-12/2018'),
(11, 'PUNCTUALITY', 2, 'admin2', 'SOG-003', '11/2018-12/2018'),
(12, 'COMPLETION OF TRAINING', 4, 'admin2', 'SOG-003', '11/2018-12/2018'),
(13, 'DISCIPLINE', 3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(14, 'ATTENDANCE', 2, 'admin2', 'SOG-004', '11/2018-12/2018'),
(15, 'PUNCTUALITY', 3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(16, 'COMPLETION OF TRAINING', 3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(17, 'DISCIPLINE', 2, 'admin2', 'SOG-002', '11/2018-12/2018'),
(18, 'ATTENDANCE', 3, 'admin2', 'SOG-002', '11/2018-12/2018'),
(19, 'PUNCTUALITY', 4, 'admin2', 'SOG-002', '11/2018-12/2018'),
(20, 'COMPLETION OF TRAINING', 3, 'admin2', 'SOG-002', '11/2018-12/2018'),
(21, 'DISCIPLINE', 3, 'admin2', 'wer', '11/2018-12/2018'),
(22, 'ATTENDANCE', 3, 'admin2', 'wer', '11/2018-12/2018'),
(23, 'PUNCTUALITY', 4, 'admin2', 'wer', '11/2018-12/2018'),
(24, 'COMPLETION OF TRAINING', 3, 'admin2', 'wer', '11/2018-12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp3_rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp3_rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` varchar(200) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp3_rubric`
--

INSERT INTO `tbl_ans_eformp3_rubric` (`id`, `areaName`, `rubricDesc`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality', 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'admin', 'SOG-001', '11/2018-12/2018'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness', 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin', 'SOG-001', '11/2018-12/2018'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin', 'SOG-001', '11/2018-12/2018'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.', 'admin', 'SOG-001', '11/2018-12/2018'),
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'admin2', 'SOG-004', '11/2018-12/2018'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin2', 'SOG-004', '11/2018-12/2018'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin2', 'SOG-004', '11/2018-12/2018'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality', 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'admin2', 'SOG-003', '11/2018-12/2018'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.', 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.', 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness', 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin2', 'SOG-003', '11/2018-12/2018'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.', 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime', 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin2', 'SOG-003', '11/2018-12/2018'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars', 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.', 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.', 'admin2', 'SOG-003', '11/2018-12/2018'),
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'admin2', 'SOG-004', '11/2018-12/2018'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin2', 'SOG-004', '11/2018-12/2018'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin2', 'SOG-004', '11/2018-12/2018'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality', 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'admin2', 'SOG-002', '11/2018-12/2018'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.', 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.', 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness', 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin2', 'SOG-002', '11/2018-12/2018'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.', 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime', 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin2', 'SOG-002', '11/2018-12/2018'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars', 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.', 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.', 'admin2', 'SOG-002', '11/2018-12/2018'),
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'admin2', 'wer', '11/2018-12/2018'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'admin2', 'wer', '11/2018-12/2018'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality', 'admin2', 'wer', '11/2018-12/2018'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'admin2', 'wer', '11/2018-12/2018'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.', 'admin2', 'wer', '11/2018-12/2018'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.', 'admin2', 'wer', '11/2018-12/2018'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness', 'admin2', 'wer', '11/2018-12/2018'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin2', 'wer', '11/2018-12/2018'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.', 'admin2', 'wer', '11/2018-12/2018'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime', 'admin2', 'wer', '11/2018-12/2018'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'admin2', 'wer', '11/2018-12/2018'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin2', 'wer', '11/2018-12/2018'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars', 'admin2', 'wer', '11/2018-12/2018'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.', 'admin2', 'wer', '11/2018-12/2018'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'admin2', 'wer', '11/2018-12/2018'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.', 'admin2', 'wer', '11/2018-12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp4` (
  `id` int(11) NOT NULL,
  `p4_area` varchar(50) NOT NULL,
  `customerFeedback` text NOT NULL,
  `p4_score` int(11) NOT NULL,
  `p4_rating` varchar(11) NOT NULL,
  `answeredBy` varchar(80) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp4`
--

INSERT INTO `tbl_ans_eformp4` (`id`, `p4_area`, `customerFeedback`, `p4_score`, `p4_rating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'April - June', '', 4, '0.04', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'July - September', '', 2, '0.02', 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'October-December', '', 2, '0.02', 'admin', 'SOG-001', '11/2018-12/2018'),
(4, 'January-March', '', 4, '0.04', 'admin', 'SOG-001', '11/2018-12/2018'),
(5, 'April - June', '', 0, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(6, 'July - September', '', 0, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(7, 'October-December', '', 0, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(8, 'January-March', '', 0, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(9, 'April - June', '', 0, '', 'admin2', 'SOG-003', '11/2018-12/2018'),
(10, 'July - September', '', 0, '', 'admin2', 'SOG-003', '11/2018-12/2018'),
(11, 'October-December', '', 0, '', 'admin2', 'SOG-003', '11/2018-12/2018'),
(12, 'January-March', '', 0, '', 'admin2', 'SOG-003', '11/2018-12/2018'),
(13, 'April - June', '', 0, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(14, 'July - September', '', 0, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(15, 'October-December', '', 0, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(16, 'January-March', '', 0, '', 'admin2', 'SOG-004', '11/2018-12/2018'),
(17, 'April - June', '', 0, '', 'admin2', 'SOG-002', '11/2018-12/2018'),
(18, 'July - September', '', 0, '', 'admin2', 'SOG-002', '11/2018-12/2018'),
(19, 'October-December', '', 0, '', 'admin2', 'SOG-002', '11/2018-12/2018'),
(20, 'January-March', '', 0, '', 'admin2', 'SOG-002', '11/2018-12/2018'),
(21, 'April - June', '', 0, '', 'admin2', 'wer', '11/2018-12/2018'),
(22, 'July - September', '', 0, '', 'admin2', 'wer', '11/2018-12/2018'),
(23, 'October-December', '', 0, '', 'admin2', 'wer', '11/2018-12/2018'),
(24, 'January-March', '', 0, '', 'admin2', 'wer', '11/2018-12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_summary`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_summary` (
  `id` int(11) NOT NULL,
  `performance_rating` varchar(60) NOT NULL,
  `total_weighted_rating` float NOT NULL,
  `percentage_allocation` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_summary`
--

INSERT INTO `tbl_ans_summary` (`id`, `performance_rating`, `total_weighted_rating`, `percentage_allocation`, `subtotal`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 3.25, 40, 1.3, 'admin', 'SOG-001', '11/2018-12/2018'),
(2, 'PART 2: COMPETENCIES', 2.8, 30, 3.3, 'admin', 'SOG-001', '11/2018-12/2018'),
(3, 'PART 3: PROFESSIONALISM', 11, 30, 0.84, 'admin', 'SOG-001', '11/2018-12/2018'),
(4, '0', 0, 0, 0.03, 'admin', 'SOG-001', '11/2018-12/2018'),
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 3.28, 40, 1.31, 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'PART 2: COMPETENCIES', 2.4, 30, 3.9, 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'PART 3: PROFESSIONALISM', 13, 30, 0.72, 'admin2', 'SOG-004', '11/2018-12/2018'),
(4, '0', 0, 0, 0, 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 3.25, 40, 1.3, 'admin2', 'SOG-003', '11/2018-12/2018'),
(2, 'PART 2: COMPETENCIES', 2.4, 30, 4.2, 'admin2', 'SOG-003', '11/2018-12/2018'),
(3, 'PART 3: PROFESSIONALISM', 14, 30, 0.72, 'admin2', 'SOG-003', '11/2018-12/2018'),
(4, '0', 0, 0, 0, 'admin2', 'SOG-003', '11/2018-12/2018'),
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 2.75, 40, 1.1, 'admin2', 'SOG-004', '11/2018-12/2018'),
(2, 'PART 2: COMPETENCIES', 2.8, 30, 3.3, 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, 'PART 3: PROFESSIONALISM', 11, 30, 0.84, 'admin2', 'SOG-004', '11/2018-12/2018'),
(4, '0', 0, 0, 0, 'admin2', 'SOG-004', '11/2018-12/2018'),
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 3.5, 40, 1.4, 'admin2', 'SOG-002', '11/2018-12/2018'),
(2, 'PART 2: COMPETENCIES', 2.6, 30, 3.6, 'admin2', 'SOG-002', '11/2018-12/2018'),
(3, 'PART 3: PROFESSIONALISM', 12, 30, 0.78, 'admin2', 'SOG-002', '11/2018-12/2018'),
(4, '0', 0, 0, 0, 'admin2', 'SOG-002', '11/2018-12/2018'),
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 3, 40, 1.2, 'admin2', 'wer', '11/2018-12/2018'),
(2, 'PART 2: COMPETENCIES', 2.6, 30, 3.9, 'admin2', 'wer', '11/2018-12/2018'),
(3, 'PART 3: PROFESSIONALISM', 13, 30, 0.78, 'admin2', 'wer', '11/2018-12/2018'),
(4, '0', 0, 0, 0, 'admin2', 'wer', '11/2018-12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_summary_comments_acknowledgement`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_summary_comments_acknowledgement` (
  `id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `recommendations` text NOT NULL,
  `approval` text NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_summary_comments_acknowledgement`
--

INSERT INTO `tbl_ans_summary_comments_acknowledgement` (`id`, `comments`, `recommendations`, `approval`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, '<p>No comment<br></p>', '<p>\r\n</p><p>No rec<br></p>\r\n\r\n<br><p></p>', 'This is to Certify that John K Ford confirms this Evaluation for this Year.', 'admin', 'SOG-001', '11/2018-12/2018'),
(2, '<p>*comments</p>', '<p>*recommendations</p>', 'This is to Certify that Natasha F Romanoff confirms this Evaluation for this Year.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(3, '<p>$comments</p>', '<p>$recommendations</p>', 'This is to Certify that Bucky T Barnes confirms this Evaluation for this Year.', 'admin2', 'SOG-003', '11/2018-12/2018'),
(4, '<p>weraw</p>', '<p>sdfasdfas</p>', 'This is to Certify that Natasha F Romanoff confirms this Evaluation for this Year.', 'admin2', 'SOG-004', '11/2018-12/2018'),
(5, '<p>hahha</p>', '<p>ahahhahaha</p>', 'This is to Certify that Veronica H Stark confirms this Evaluation for this Year.', 'admin2', 'SOG-002', '11/2018-12/2018'),
(6, '<p>brrr</p>', '<p>brrrr</p>', 'This is to Certify that Wer Wer Wer confirms this Evaluation for this Year.', 'admin2', 'wer', '11/2018-12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_competencyratingscale`
--

CREATE TABLE IF NOT EXISTS `tbl_competencyratingscale` (
  `id` int(11) NOT NULL,
  `competency_rating_value` varchar(100) NOT NULL,
  `competency_title` varchar(100) NOT NULL,
  `competency_description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_competencyratingscale`
--

INSERT INTO `tbl_competencyratingscale` (`id`, `competency_rating_value`, `competency_title`, `competency_description`) VALUES
(3, '4', 'Expert/Exemplary', '(Evaluation; Defines Framework Modes; Mentor, Self - Actualization)'),
(4, '3', 'Advanced/Accomplished', '(Analysis; Synthesis; Understands Framework Models, Exceeds)'),
(5, '2', 'Intermediate/Developing', '(Unsupervised Application; Beginning Analysis, Meets)'),
(6, '1', 'Beginner', '(Comprehension; Beginning Application; Some Experience, Meets some)'),
(7, 'NE', '', 'Early stages of application of the item; Behavior not yet present.'),
(8, 'NA', '', 'The item does not apply to your position or performance of assigned tasks and responsibilities.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dateevaluation`
--

CREATE TABLE IF NOT EXISTS `tbl_dateevaluation` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dateevaluation`
--

INSERT INTO `tbl_dateevaluation` (`startDate`, `endDate`) VALUES
('02/2018', '12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_datekpi`
--

CREATE TABLE IF NOT EXISTS `tbl_datekpi` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_datekpi`
--

INSERT INTO `tbl_datekpi` (`startDate`, `endDate`) VALUES
('12/2017', '01/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departments`
--

CREATE TABLE IF NOT EXISTS `tbl_departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `initials` varchar(100) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL DEFAULT 'PENDING',
  `departmentOwner` varchar(100) NOT NULL,
  `monitor` varchar(6) NOT NULL DEFAULT 'unread',
  `activation_status` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_departments`
--

INSERT INTO `tbl_departments` (`id`, `department_name`, `initials`, `kpiStatus`, `departmentOwner`, `monitor`, `activation_status`) VALUES
(1, 'College of Information Technology Education', 'CITE', 'APPROVED', 'superadmin', 'read', ''),
(2, 'College of Management and Accountancy', 'CMA', 'APPROVED', 'admin2', 'read', ''),
(3, 'College of Health and Sciences', 'CHS', 'APPROVED', 'admin3', 'read', ''),
(4, 'College of ON', 'ON', 'PENDING', 'emp-999', 'unread', ''),
(5, 'Asdf', 'ASDF', 'PENDING', 'r', 'unread', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp1` (
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeight` int(11) NOT NULL,
  `kpiOwner` varchar(60) NOT NULL,
  `kpiDateCreated` varchar(11) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp1`
--

INSERT INTO `tbl_eformp1` (`kraID`, `kpiTitle`, `kpiWeight`, `kpiOwner`, `kpiDateCreated`, `kpiStatus`) VALUES
(2, 'COMMUNICATION & COORDINATION', 0, 'College of Information Technology Education', '', 'APPROVED'),
(3, 'OFFICE MANAGEMENT', 0, 'College of Information Technology Education', '', 'APPROVED'),
(4, 'PREPARATION OF REPORTS', 0, 'College of Information Technology Education', '', 'APPROVED'),
(5, 'CUSTOMER SERVICE', 0, 'College of Information Technology Education', '', 'APPROVED'),
(10, 's', 0, 'College of Management and Accountancy', '', 'APPROVED'),
(11, 'a', 0, 'College of Management and Accountancy', '', 'APPROVED'),
(12, 'asd', 0, 'College of Management and Accountancy', '', 'APPROVED'),
(13, 'ff', 0, 'College of Management and Accountancy', '', 'APPROVED'),
(14, 'ONE', 0, 'College of Health and Sciences', '', 'UPDATE'),
(15, 'ANOTHER ONE', 0, 'College of Health and Sciences', '', 'UPDATE'),
(16, 'TWO', 0, 'College of Health and Sciences', '', 'UPDATE'),
(17, 'ANOTHER TWO', 0, 'College of Health and Sciences', '', 'UPDATE');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` text NOT NULL,
  `competencyDesc` text NOT NULL,
  `requiredProfLevel` varchar(50) NOT NULL,
  `weights` float NOT NULL,
  `rating` int(2) NOT NULL,
  `weightedRating` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2a`
--

INSERT INTO `tbl_eformp2a` (`id`, `competency`, `competencyDesc`, `requiredProfLevel`, `weights`, `rating`, `weightedRating`) VALUES
(1, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community. Able to adjust to changes and exhibits a positive attitude towards different situations</p>\r\n', 'C -- Contribution through Teams/Peers', 10, 0, 0),
(2, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approaches to identify opportunities, implement solutions, measure impact and serve customers.</p>\r\n', 'C -- Contribution through Teams/Peers', 10, 0, 0),
(3, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationship with customers and business partners.</p>\r\n', 'A -- Contributing Independently', 20, 0, 0),
(4, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'B -- Contributing with Expertise', 20, 0, 0),
(5, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'B -- Contributing with Expertise', 10, 0, 0),
(6, 'Leadership Excellence', '<p>Upholds values and ethics. Attracts, motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential.</p>\r\n', 'C -- Contribution through Teams/Peers', 10, 0, 0),
(7, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, finanacial, and environmental resources.</p>\r\n', 'B -- Contributing with Expertise', 10, 0, 0),
(8, 'People Development', '<p>The ability to plan and support the development of employees&rsquo; skills and abilities so that they can fulfill current role and achieve full potential. Actively identifies new area for learning; regularly creating and taking advantage of learning opportunities.</p>\r\n', 'A -- Contributing Independently', 10, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2b`
--

INSERT INTO `tbl_eformp2b` (`id`, `competency`) VALUES
(1, 'Adaptability'),
(2, 'Creativity and Innovation'),
(3, 'Customer Focus'),
(4, 'Team Orientation'),
(5, 'Technical Knowledge'),
(6, 'Leadership Excellence'),
(7, 'Management Excellence'),
(8, 'People Development');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp3`
--

INSERT INTO `tbl_eformp3` (`id`, `areas`) VALUES
(1, 'DISCIPLINE'),
(2, 'ATTENDANCE'),
(3, 'PUNCTUALITY'),
(4, 'COMPLETION OF TRAINING');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp4` (
  `id` int(11) NOT NULL,
  `areas` varchar(30) NOT NULL,
  `customerFeedback` varchar(100) NOT NULL,
  `respondentScore` int(2) NOT NULL,
  `ratingFactor` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp4`
--

INSERT INTO `tbl_eformp4` (`id`, `areas`, `customerFeedback`, `respondentScore`, `ratingFactor`) VALUES
(1, 'April - June', '', 0, 0),
(2, 'July - September', '', 0, 0),
(3, 'October-December', '', 0, 0),
(4, 'January-March', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation_requests`
--

CREATE TABLE IF NOT EXISTS `tbl_evaluation_requests` (
  `id` int(11) NOT NULL,
  `admin_id` varchar(60) NOT NULL,
  `employee_missed` varchar(60) NOT NULL,
  `employee_department` varchar(60) NOT NULL,
  `reason` text NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `request_status` varchar(30) NOT NULL DEFAULT 'SENT'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation_results`
--

CREATE TABLE IF NOT EXISTS `tbl_evaluation_results` (
  `id` int(11) NOT NULL,
  `emp_id` varchar(60) NOT NULL,
  `employee_firstName` varchar(60) NOT NULL,
  `employee_middleName` varchar(60) NOT NULL,
  `employee_lastName` varchar(60) NOT NULL,
  `employee_department` varchar(60) NOT NULL,
  `employee_position` varchar(30) NOT NULL,
  `employee_job_level` varchar(30) NOT NULL,
  `employee_years_company` int(11) NOT NULL,
  `employee_years_position` int(11) NOT NULL,
  `evaluationStatus` varchar(30) NOT NULL DEFAULT 'NOT YET STARTED',
  `performance_rating` varchar(100) NOT NULL,
  `overall_rating` varchar(100) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `ev_sog_feedback` varchar(15) NOT NULL DEFAULT 'NO RESPONSE YET',
  `late_status` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_evaluation_results`
--

INSERT INTO `tbl_evaluation_results` (`id`, `emp_id`, `employee_firstName`, `employee_middleName`, `employee_lastName`, `employee_department`, `employee_position`, `employee_job_level`, `employee_years_company`, `employee_years_position`, `evaluationStatus`, `performance_rating`, `overall_rating`, `answeredBy`, `performanceCycle`, `ev_sog_feedback`, `late_status`) VALUES
(1, 'SOG-001', 'John', 'K', 'Ford', 'College of Information Technology Education', 'Secretary', 'S1', 6, 6, 'NOT YET STARTED', '', '', 'College of Information Technology Education', '11/2017-12/2017', 'NO RESPONSE YET', 'YES'),
(2, 'SOG-002', 'Veronica', 'H', 'Stark', 'College of Information Technology Education', 'A Sec', 'S2', 5, 5, 'NOT YET STARTED', '', '', 'College of Information Technology Education', '11/2017-12/2017', 'NO RESPONSE YET', 'YES'),
(3, 'SOG-003', 'Bucky', 'T', 'Barnes', 'College of Management and Accountancy', 'Secretary', 'S1', 6, 6, 'NOT YET STARTED', '', '', 'College of Management and Accountancy', '11/2017-12/2017', 'NO RESPONSE YET', 'YES'),
(4, 'SOG-004', 'Natasha', 'F', 'Romanoff', 'College of Management and Accountancy', 'A Sec', 'S2', 11, 11, 'NOT YET STARTED', '', '', 'College of Management and Accountancy', '11/2017-12/2017', 'NO RESPONSE YET', 'YES'),
(5, 'SOG-001', 'John', 'K', 'Ford', 'College of Information Technology Education', 'Secretary', 'S1', 6, 6, 'COMPLETED', '5.44', '5.47', 'College of Information Technology Education', '11/2018-12/2018', 'NO RESPONSE YET', ''),
(6, 'SOG-002', 'Veronica', 'H', 'Stark', 'College of Information Technology Education', 'A Sec', 'S2', 5, 5, 'PROCESSING', '5.78', '5.78', 'College of Information Technology Education', '11/2018-12/2018', 'NO RESPONSE YET', ''),
(7, 'SOG-003', 'Bucky', 'T', 'Barnes', 'College of Management and Accountancy', 'Secretary', 'S1', 6, 6, 'PROCESSING', '6.22', '6.22', 'College of Management and Accountancy', '11/2018-12/2018', 'NO RESPONSE YET', ''),
(8, 'SOG-004', 'Natasha', 'F', 'Romanoff', 'College of Management and Accountancy', 'A Sec', 'S2', 11, 11, 'PROCESSING', '5.24', '5.24', 'College of Management and Accountancy', '11/2018-12/2018', 'NO RESPONSE YET', ''),
(9, 'emp-3333', 'Gg', 'Gg', 'Gg', 'College of Information Technology Education', 'gg', 'Gg', 17, 17, 'NOT YET STARTED', '', '', '', '11/2018-12/2018', 'NO RESPONSE YET', 'YES'),
(10, 'wer', 'Wer', 'Wer', 'Wer', 'College of Management and Accountancy', 'wer', 'Wer', 13, 13, 'PROCESSING', '5.88', '5.88', '', '11/2018-12/2018', 'NO RESPONSE YET', ''),
(11, '345', 'Erterte', 'Ertert', 'Erte', 'College of Information Technology Education', 'dfg', 'Dfg', 12, 12, 'NOT YET STARTED', '', '', '', '11/2018-12/2018', 'NO RESPONSE YET', 'YES'),
(12, '345', 'Erterte', 'Ertert', 'Erte', 'College of Information Technology Education', 'dfg', 'Dfg', 12, 12, 'NOT YET STARTED', '', '', '', '02/2018-12/2018', 'NO RESPONSE YET', ''),
(13, 'emp-3333', 'Gg', 'Gg', 'Gg', 'College of Information Technology Education', 'gg', 'Gg', 17, 17, 'NOT YET STARTED', '', '', '', '02/2018-12/2018', 'NO RESPONSE YET', ''),
(14, 'SOG-001', 'John', 'K', 'Ford', 'College of Information Technology Education', 'Secretary', 'S1', 6, 6, 'NOT YET STARTED', '', '', '', '02/2018-12/2018', 'NO RESPONSE YET', ''),
(15, 'SOG-002', 'Veronica', 'H', 'Stark', 'College of Management and Accountancy', 'A Sec', 'S2', 5, 5, 'NOT YET STARTED', '', '', '', '02/2018-12/2018', 'NO RESPONSE YET', ''),
(16, 'SOG-003', 'Bucky', 'T', 'Barnes', 'College of Management and Accountancy', 'Secretary', 'S1', 6, 6, 'NOT YET STARTED', '', '', '', '02/2018-12/2018', 'NO RESPONSE YET', ''),
(17, 'SOG-004', 'Natasha', 'F', 'Romanoff', 'College of Management and Accountancy', 'A Sec', 'S2', 11, 11, 'NOT YET STARTED', '', '', '', '02/2018-12/2018', 'NO RESPONSE YET', ''),
(18, 'wer', 'Wer', 'Wer', 'Wer', 'College of Management and Accountancy', 'wer', 'Wer', 13, 13, 'NOT YET STARTED', '', '', '', '02/2018-12/2018', 'NO RESPONSE YET', ''),
(19, 'Eeee', 'Ee', 'Ee', 'Ee', 'College of Information Technology Education', 'e', 'E', 17, 17, 'NOT YET STARTED', '', '', '', '02/2018-12/2018', 'NO RESPONSE YET', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kpiratingscale`
--

CREATE TABLE IF NOT EXISTS `tbl_kpiratingscale` (
  `id` int(11) NOT NULL,
  `rating_value` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `weighted_rating` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kpiratingscale`
--

INSERT INTO `tbl_kpiratingscale` (`id`, `rating_value`, `description`, `weighted_rating`) VALUES
(15, '4', 'Exceeds Expectations (Performs above standards all the time)', '1'),
(16, '3', 'Meets Expectations (Solid, consistent performance; Generally meets standards)', '0.75'),
(17, '2', 'Needs Improvement (Frequently fails to meet standards)', '0.5'),
(18, '1', 'Unacceptable (Performance is below job requirements and needs)', '0.25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kpi_requests`
--

CREATE TABLE IF NOT EXISTS `tbl_kpi_requests` (
  `id` int(11) NOT NULL,
  `reason` text NOT NULL,
  `sent_at` varchar(60) NOT NULL,
  `department` varchar(60) NOT NULL,
  `request_status` varchar(60) NOT NULL,
  `date_of_kpi` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kpi_requests`
--

INSERT INTO `tbl_kpi_requests` (`id`, `reason`, `sent_at`, `department`, `request_status`, `date_of_kpi`) VALUES
(1, '<li class="list-group-item"> <p>First reeason</p>\r\n </li> <li class="list-group-item"> <p>Second reason</p>\r\n </li> <li class="list-group-item"> <p>Last</p>\r\n </li>', 'November 25, 2017 12:39:pm', 'College of Health and Sciences', 'APPROVED', ''),
(3, '<li class="list-group-item"> <p>try ko lang po ito ha?</p>\r\n </li>', 'November 30, 2017 11:52:pm', 'College of ON', 'SENT', '12/2017-01/2018'),
(4, '<li class="list-group-item"> <p>hehe</p>\r\n </li>', 'December 1, 2017 12:29:am', 'Asdf', 'SENT', '12/2017-01/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE IF NOT EXISTS `tbl_messages` (
  `id` int(11) NOT NULL,
  `subject` text NOT NULL,
  `content` text NOT NULL,
  `sender` varchar(50) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'unread',
  `checked` varchar(3) NOT NULL DEFAULT 'no',
  `sent_at` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `deleted_by_sender` varchar(100) NOT NULL DEFAULT 'no',
  `deleted_by_receiver` varchar(100) NOT NULL DEFAULT 'no',
  `trash_by_HR` varchar(100) NOT NULL DEFAULT 'no',
  `trash_by_Department` varchar(100) NOT NULL DEFAULT 'no',
  `revision` varchar(100) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `id_appeal` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_messages`
--

INSERT INTO `tbl_messages` (`id`, `subject`, `content`, `sender`, `receiver`, `status`, `checked`, `sent_at`, `location`, `deleted_by_sender`, `deleted_by_receiver`, `trash_by_HR`, `trash_by_Department`, `revision`, `performanceCycle`, `id_appeal`) VALUES
(1, 'REQUEST TO OPEN KRA CREATION MODULE', '<p>First reeason</p>\r\n', 'College of Health and Sciences', 'Human Resource Department', 'unread', 'no', 'November 25, 2017 12:39:pm', 'sentbox', 'no', 'no', 'no', 'no', '', '', 'College of Health and Sciences'),
(2, 'DENIED REQUEST TO REOPEN THE CREATION OF KRA', '<p>Nope</p>\r\n', 'Human Resource Department', 'College of Health and Sciences', 'unread', 'no', 'November 25, 2017 12:39:pm', 'sentbox', 'no', 'no', 'no', 'no', '', '', 'College of Health and Sciences'),
(3, 'APPEAL TO DENIED REQUEST TO REOPEN THE CREATION OF KRA MODULE', '<p>Second reason</p>\r\n', 'College of Health and Sciences', 'Human Resource Department', 'unread', 'no', 'November 25, 2017 12:39:pm', 'sentbox', 'no', 'no', 'no', 'no', '', '', 'College of Health and Sciences'),
(4, 'DENIED REQUEST TO REOPEN THE CREATION OF KRA', '<p>Welcome to the nope land</p>\r\n', 'Human Resource Department', 'College of Health and Sciences', 'unread', 'no', 'November 25, 2017 12:39:pm', 'sentbox', 'no', 'no', 'no', 'no', '', '', 'College of Health and Sciences'),
(5, 'APPEAL TO DENIED REQUEST TO REOPEN THE CREATION OF KRA MODULE', '<p>Last</p>\r\n', 'College of Health and Sciences', 'Human Resource Department', 'unread', 'no', 'November 25, 2017 12:40:pm', 'sentbox', 'no', 'no', 'no', 'no', '', '', 'College of Health and Sciences'),
(7, 'REQUEST TO OPEN KRA CREATION MODULE', '<p>try ko lang po ito ha?</p>\r\n', 'College of ON', 'Human Resource Department', 'unread', 'no', 'November 30, 2017 11:52:pm', 'sentbox', 'no', 'no', 'no', 'no', '', '', 'College of ON'),
(8, 'REQUEST TO OPEN KRA CREATION MODULE', '<p>hehe</p>\r\n', 'Asdf', 'Human Resource Department', 'unread', 'no', 'December 1, 2017 12:29:am', 'sentbox', 'no', 'no', 'no', 'no', '', '', 'Asdf'),
(9, 'DENIED REQUEST TO REOPEN THE CREATION OF KRA', '', 'Human Resource Department', 'Asdf', 'unread', 'no', 'December 1, 2017 11:09:am', 'sentbox', 'no', 'no', 'no', 'no', '', '', 'Asdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE IF NOT EXISTS `tbl_notification` (
  `id` int(11) NOT NULL,
  `notification` text NOT NULL,
  `sendBy` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`id`, `notification`, `sendBy`, `status`, `timeStamp`) VALUES
(1, 'College of Health and Sciences SUBMITTED A REQUEST TO OPEN KRA CREATION MODULE', 'College of', 'read', '2017-11-25 09:56:15'),
(2, 'College of Health and Sciences SUBMITTED AN APPEAL REGARDING TO A DECLINED REQUEST TO OPEN A CREATION OF KRA MODULE', 'College of', 'read', '2017-11-25 09:56:15'),
(3, 'College of Health and Sciences SUBMITTED AN APPEAL REGARDING TO A DECLINED REQUEST TO OPEN A CREATION OF KRA MODULE', 'College of', 'read', '2017-11-25 09:56:15'),
(4, 'College of Health and Sciences Sent a KPI', 'admin3', 'read', '2017-11-26 12:02:18'),
(5, 'College of Health and Sciences updated a KPI', 'admin3', 'read', '2017-11-26 12:02:18'),
(6, 'College of Information Technology Education SENT AN EVALUATION', 'admin', 'read', '2017-11-26 13:55:15'),
(7, 'College of Management and Accountancy SENT AN EVALUATION', 'admin2', 'read', '2017-11-26 13:55:15'),
(8, 'College of Management and Accountancy SENT AN EVALUATION', 'admin2', 'read', '2017-11-26 13:55:15'),
(9, 'College of Management and Accountancy SENT AN EVALUATION', 'admin2', 'read', '2017-11-26 13:55:15'),
(10, 'College of Management and Accountancy SENT AN EVALUATION', 'admin2', 'read', '2017-11-26 14:21:37'),
(11, 'College of Management and Accountancy SENT AN EVALUATION', 'admin2', 'read', '2017-11-29 14:38:22'),
(12, 'College of ON SUBMITTED A REQUEST TO OPEN KRA CREATION MODULE', 'College of', 'read', '2017-11-30 15:54:17'),
(13, 'College of ON SUBMITTED A REQUEST TO OPEN KRA CREATION MODULE', 'College of', 'read', '2017-11-30 15:54:17'),
(14, 'Asdf SUBMITTED A REQUEST TO OPEN KRA CREATION MODULE', 'Asdf', 'unread', '2017-11-30 16:29:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification_user`
--

CREATE TABLE IF NOT EXISTS `tbl_notification_user` (
  `id` int(11) NOT NULL,
  `notification` text NOT NULL,
  `sendBy` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `owner` varchar(100) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification_user`
--

INSERT INTO `tbl_notification_user` (`id`, `notification`, `sendBy`, `status`, `owner`, `timeStamp`) VALUES
(1, 'HR DENIED YOUR REQUEST TO OPEN THE CREATION OF KRA', 'Human Resource Department', 'unread', 'College of Health and Sciences', '2017-11-26 12:30:28'),
(2, 'HR DENIED YOUR REQUEST TO OPEN THE CREATION OF KRA', 'Human Resource Department', 'unread', 'College of Health and Sciences', '2017-11-26 12:30:28'),
(3, 'HR APPROVED YOUR REQUEST TO OPEN THE CREATION OF KRA', 'Human Resource Department', 'unread', 'College of Health and Sciences', '2017-11-26 12:30:28'),
(4, 'HR SENT A NOTE TO YOUR KRA MODULE', 'Human Resource Department', 'read', '', '2017-11-26 12:02:27'),
(5, 'HR SENT A NOTE TO YOUR KRA MODULE', 'Human Resource Department', 'unread', 'College of Health and Sciences', '2017-11-26 12:30:28'),
(6, 'HR APPROVED YOUR KRA', 'Human Resource Department', 'unread', 'College of Health and Sciences', '2017-11-26 12:30:28'),
(7, 'HR approved the evaluation for John K Ford', 'Human Resource Department', 'read', 'College of Information Technology Education', '2017-11-26 13:53:22'),
(8, 'HR DENIED YOUR REQUEST TO OPEN THE CREATION OF KRA', 'Human Resource Department', 'unread', 'Asdf', '2017-12-01 03:09:20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_p1kpi` (
  `kraID` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kpiDesc` text NOT NULL,
  `kpiOwner` varchar(100) NOT NULL,
  `kpiDate` varchar(11) NOT NULL,
  `kpiStatus` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p1kpi`
--

INSERT INTO `tbl_p1kpi` (`kraID`, `kpiID`, `kpiDesc`, `kpiOwner`, `kpiDate`, `kpiStatus`) VALUES
(2, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'College of Information Technology Education', '', 'APPROVED'),
(2, 2, 'Coordinate meetings and other activities within the college', 'College of Information Technology Education', '', 'APPROVED'),
(2, 3, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'College of Information Technology Education', '', 'APPROVED'),
(2, 4, 'Supervise the student assistants designated to the college', 'College of Information Technology Education', '', 'APPROVED'),
(3, 1, 'Organize office files and documents', 'College of Information Technology Education', '', 'APPROVED'),
(3, 2, 'Supervise order and cleanliness in the office', 'College of Information Technology Education', '', 'APPROVED'),
(3, 3, 'Regular inventory and update of office supplies', 'College of Information Technology Education', '', 'APPROVED'),
(4, 1, 'Submit reports (e.g., Attendance, Completion of Grades) Â required by other offices (e.g., HRD, Regi', 'College of Information Technology Education', '', 'APPROVED'),
(4, 2, 'Monitor facultys attendanceÂ ', 'College of Information Technology Education', '', 'APPROVED'),
(4, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'College of Information Technology Education', '', 'APPROVED'),
(5, 1, 'Answer telephone calls and/or take messages', 'College of Information Technology Education', '', 'APPROVED'),
(5, 2, 'Man the offices frontdesk to meet walk-in customers Â  Â Â ', 'College of Information Technology Education', '', 'APPROVED'),
(5, 3, 'Arrange customers appointments with the Dean or faculty Â  Â ', 'College of Information Technology Education', '', 'APPROVED'),
(5, 4, 'Handle data encoding duties during enrolment', 'College of Information Technology Education', '', 'APPROVED'),
(6, 1, 'knows how to speak english', 'EMP-001', '', 'PENDING'),
(6, 2, 'tagalog', 'EMP-001', '', 'PENDING'),
(10, 1, 'ss', 'College of Management and Accountancy', '', 'APPROVED'),
(11, 1, 'as', 'College of Management and Accountancy', '', 'APPROVED'),
(12, 1, 'asdasd', 'College of Management and Accountancy', '', 'APPROVED'),
(13, 1, 'fff', 'College of Management and Accountancy', '', 'APPROVED'),
(14, 1, 'Wqwqw', 'College of Health and Sciences', '', 'UPDATE'),
(15, 1, 'Wawaw', 'College of Health and Sciences', '', 'UPDATE'),
(16, 1, 'Wewewe', 'College of Health and Sciences', '', 'UPDATE'),
(17, 1, 'Weweweqwqw', 'College of Health and Sciences', '', 'UPDATE');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p2bcritinc`
--

CREATE TABLE IF NOT EXISTS `tbl_p2bcritinc` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(60) NOT NULL,
  `criticalIncident` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p2bcritinc`
--

INSERT INTO `tbl_p2bcritinc` (`id`, `competencyName`, `criticalIncident`) VALUES
(1, 'Adaptability', 'Critical Incident'),
(2, 'Adaptability', 'Critical Incident'),
(3, 'Adaptability', 'Critical Incident'),
(1, 'Creativity and Innovation', 'Critical Incident'),
(2, 'Creativity and Innovation', 'Critical Incident'),
(3, 'Creativity and Innovation', 'Critical Incident'),
(1, 'Customer Focus', 'Critical Incident'),
(2, 'Customer Focus', 'Critical Incident'),
(3, 'Customer Focus', 'Critical Incident'),
(1, 'Team Orientation', 'Critical Incident'),
(2, 'Team Orientation', 'Critical Incident'),
(3, 'Team Orientation', 'Critical Incident'),
(1, 'Technical Knowledge', 'Critical Incident'),
(2, 'Technical Knowledge', 'Critical Incident'),
(3, 'Technical Knowledge', 'Critical Incident'),
(1, 'Leadership Excellence', 'Critical Incident'),
(2, 'Leadership Excellence', 'Critical Incident'),
(3, 'Leadership Excellence', 'Critical Incident'),
(1, 'Management Excellence', 'Critical Incident'),
(2, 'Management Excellence', 'Critical Incident'),
(3, 'Management Excellence', 'Critical Incident'),
(1, 'People Development', 'Critical Incident'),
(2, 'People Development', 'Critical Incident'),
(3, 'People Development', 'Critical Incident');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p3rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_p3rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p3rubric`
--

INSERT INTO `tbl_p3rubric` (`id`, `areaName`, `rubricDesc`) VALUES
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.'),
(4, 'Test me', 'Has zero record of disciplinary action on offenses other than the above policies');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_proficiency_level`
--

CREATE TABLE IF NOT EXISTS `tbl_proficiency_level` (
  `id` int(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `initials` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_proficiency_level`
--

INSERT INTO `tbl_proficiency_level` (`id`, `value`, `initials`, `title`, `content`) VALUES
(1, '4', 'A', 'Contributing Independently', 'Learner-Doer. Individual contributor. Follower, team-player, Customer touchpoint'),
(3, '3', 'B', 'Contributing with Expertise', 'Specialist. Expert Implementor. Customer Advocate. Problem Solver'),
(4, '2', 'C', 'Contribution through Teams/Peers', 'Project Leader. Idea Leader. Technical Troubleshooter. Coach'),
(5, '1', 'D', 'Contributing through Strategy Building', 'Navigator. Idea Innovator. Leader Developer. Mission Advocate.'),
(6, '0', 'NE', '', 'Early stages of application of the item; Behavior not yet present'),
(7, '0', 'NA', '', 'The item does not apply to your position or performance of assigned tasks and responsibilities');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating_execute`
--

CREATE TABLE IF NOT EXISTS `tbl_rating_execute` (
  `ID` int(11) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `performance_rating` varchar(100) NOT NULL,
  `overall_rating` varchar(100) NOT NULL,
  `performanceCycle` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rating_execute`
--

INSERT INTO `tbl_rating_execute` (`ID`, `emp_id`, `performance_rating`, `overall_rating`, `performanceCycle`) VALUES
(1, 'EMP-002', '17.05', '17.05', '10/2017-11/2017'),
(2, 'EMP-002', '23.1', '23.1', '10/2018-11/2018'),
(3, 'EMP-001', '17.95', '17.95', '10/2018-11/2018'),
(4, 'EMP-002', '19.75', '19.75', '10/2018-11/2018'),
(5, 'uu', '18.3', '18.3', '10/2018-11/2018'),
(6, 'EMP-001', '18.4', '18.4', '10/2017-11/2017'),
(7, 'EMP-001', '5.55', '5.55', '10/2017-11/2017'),
(8, 'EMP-002', '6.97', '6.97', '10/2017-11/2017'),
(9, 'uu', '7.03', '7.07', '10/2017-11/2017'),
(10, 'll', '5.05', '5.09', '10/2017-11/2017'),
(11, 'EMP-002', '6.72', '6.74', '10/2018-11/2018'),
(12, 'EMP-002', '6.23', '6.26', '10/2018-11/2018'),
(13, 'EMP-001', '5.52', '5.52', '10/2019-11/2019'),
(14, 'EMP-001', '5.23', '5.26', '10/2017-11/2017'),
(15, 'EMP-002', '5.3', '5.33', '10/2017-11/2017'),
(16, 'EMP-002', '6.57', '6.61', '10/2018-11/2018'),
(17, 'EMP-004', '5.04', '5.08', '10/2017-11/2017'),
(18, 'EMP-005', '5.95', '5.98', '10/2017-11/2017'),
(19, 'SOG-001', '5.44', '5.47', '11/2018-12/2018'),
(20, 'SOG-004', '5.93', '5.93', '11/2018-12/2018'),
(21, 'SOG-003', '6.22', '6.22', '11/2018-12/2018'),
(22, 'SOG-004', '5.24', '5.24', '11/2018-12/2018'),
(23, 'SOG-002', '5.78', '5.78', '11/2018-12/2018'),
(24, 'wer', '5.88', '5.88', '11/2018-12/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp2a` (
  `criticalIncidentCount` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp2a`
--

INSERT INTO `tbl_settingsp2a` (`criticalIncidentCount`) VALUES
(3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp3`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp3` (
  `rubricCount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp3`
--

INSERT INTO `tbl_settingsp3` (`rubricCount`) VALUES
(4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sog_employee`
--

CREATE TABLE IF NOT EXISTS `tbl_sog_employee` (
  `emp_id` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `department_head` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_temp` varchar(100) NOT NULL,
  `email_employee` text NOT NULL,
  `paths` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sog_employee`
--

INSERT INTO `tbl_sog_employee` (`emp_id`, `firstname`, `middlename`, `lastname`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `department_head`, `username`, `password`, `password_temp`, `email_employee`, `paths`) VALUES
('345', 'Erterte', 'Ertert', 'Erte', 'College of Information Technology Education', '12', 'dfg', 'Dfg', '2005', '12', '', '345', '8f52762829eb9b38f43f54506e611d5c', 'ERTE', 'ertert@g', '../assets/uploaded_files/accounts/default.png'),
('Eeee', 'Ee', 'Ee', 'Ee', 'College of Information Technology Education', '17', 'e', 'E', '2000', '17', '', 'Eeee', 'a57b8491d1d8fc1014dd54bcf83b130a', 'EE', 'ee!@dd', '../assets/uploaded_files/accounts/default.png'),
('emp-3333', 'Gg', 'Gg', 'Gg', 'College of Information Technology Education', '17', 'gg', 'Gg', '2000', '17', '', 'emp-3333', '86d8d92aba9ecf9bbf89f69cb3e49588', 'GG', 'gg@gmail', '../assets/uploaded_files/accounts/default.png'),
('SOG-001', 'John', 'K', 'Ford', 'College of Information Technology Education', '6', 'Secretary', 'S1', '2011', '6', '', 'SOG-001', '2b0973da58c16475df9bf30eadb6ae7a', 'FORD', 'johnkord@gmail.com', '../assets/uploaded_files/accounts/default.png'),
('SOG-002', 'Veronica', 'H', 'Stark', 'College of Management and Accountancy', '5', 'A Sec', 'S2', '2012', '5', '', 'SOG-002', 'a9106b6bc4ae581eb39418098a2891b4', 'STARK', 'vhstark@gmail.com', '../assets/uploaded_files/accounts/default.png'),
('SOG-003', 'Bucky', 'T', 'Barnes', 'College of Management and Accountancy', '6', 'Secretary', 'S1', '2011', '6', '', 'SOG-003', '986a0eead32718566e67a3a2c841f7bf', 'BARNES', 'buckbarneshydra@gmail.com', '../assets/uploaded_files/accounts/default.png'),
('SOG-004', 'Natasha', 'F', 'Romanoff', 'College of Management and Accountancy', '11', 'A Sec', 'S2', '2006', '11', '', 'SOG-004', 'b72eec9b04da317d047d8a9ef8288917', 'ROMANOFF', 'bwidow@gmail.com', '../assets/uploaded_files/accounts/default.png'),
('wer', 'Wer', 'Wer', 'Wer', 'College of Management and Accountancy', '13', 'wer', 'Wer', '2004', '13', '', 'wer', '15a15591bf662e66b7b7b224c9b353cf', 'WER', 'wer@gmail.com', '../assets/uploaded_files/accounts/default.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp2a`
--
ALTER TABLE `tbl_ans_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp2b`
--
ALTER TABLE `tbl_ans_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp3`
--
ALTER TABLE `tbl_ans_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp4`
--
ALTER TABLE `tbl_ans_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_summary_comments_acknowledgement`
--
ALTER TABLE `tbl_ans_summary_comments_acknowledgement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_competencyratingscale`
--
ALTER TABLE `tbl_competencyratingscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp1`
--
ALTER TABLE `tbl_eformp1`
  ADD PRIMARY KEY (`kraID`);

--
-- Indexes for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_evaluation_requests`
--
ALTER TABLE `tbl_evaluation_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_evaluation_results`
--
ALTER TABLE `tbl_evaluation_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kpiratingscale`
--
ALTER TABLE `tbl_kpiratingscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kpi_requests`
--
ALTER TABLE `tbl_kpi_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification_user`
--
ALTER TABLE `tbl_notification_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_rating_execute`
--
ALTER TABLE `tbl_rating_execute`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_sog_employee`
--
ALTER TABLE `tbl_sog_employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp2a`
--
ALTER TABLE `tbl_ans_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp2b`
--
ALTER TABLE `tbl_ans_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp3`
--
ALTER TABLE `tbl_ans_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp4`
--
ALTER TABLE `tbl_ans_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tbl_ans_summary_comments_acknowledgement`
--
ALTER TABLE `tbl_ans_summary_comments_acknowledgement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_competencyratingscale`
--
ALTER TABLE `tbl_competencyratingscale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_evaluation_requests`
--
ALTER TABLE `tbl_evaluation_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_evaluation_results`
--
ALTER TABLE `tbl_evaluation_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tbl_kpiratingscale`
--
ALTER TABLE `tbl_kpiratingscale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_kpi_requests`
--
ALTER TABLE `tbl_kpi_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_notification_user`
--
ALTER TABLE `tbl_notification_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_rating_execute`
--
ALTER TABLE `tbl_rating_execute`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
