-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2017 at 05:36 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `es_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admins`
--

CREATE TABLE IF NOT EXISTS `tbl_admins` (
  `admin_id` varchar(100) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `firstname` varchar(60) NOT NULL,
  `middlename` varchar(60) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` text CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `password_temp` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `paths` text NOT NULL,
  `userlevel` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admins`
--

INSERT INTO `tbl_admins` (`admin_id`, `lastname`, `firstname`, `middlename`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `username`, `password`, `password_temp`, `paths`, `userlevel`, `email`) VALUES
('admin', 'Ayson', 'Justin Louise', 'Vinluan', 'College of Information Technology Education', '7', 'Dean', 'Regular', '2010', '7', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '../assets/uploaded_files/accounts/DSC_2013.jpg', 'ADMIN', 'aysonjustinmax@gmail.com'),
('admin2', 'Liwanag', 'Aristotle', '', 'College of Management and Accountancy', '13', 'Head', 'D1', '2004', '13', 'admin2', 'c47feaececaec397b36c150ed7fb95d1', 'LIWANAG', '../assets/uploaded_files/accounts/default.png', 'ADMIN', 'test@gmail.com'),
('superadmin', 'Clauna', 'Levi Marion', 'Almazan', 'College of Health and Sciences', '7', 'Dean', 'Regular', '', '7', 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 'CLAUNA', '../assets/uploaded_files/accounts/default.png', 'SUPERADMIN', 'levimarion_clauna@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeights` int(11) NOT NULL,
  `p1_rating` int(11) NOT NULL,
  `p1_weightedRating` float NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1`
--

INSERT INTO `tbl_ans_eformp1` (`id`, `kraID`, `kpiTitle`, `kpiWeights`, `p1_rating`, `p1_weightedRating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 2, 'COMMUNICATION & COORDINATION', 25, 4, 1, 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 3, 'OFFICE MANAGEMENT', 25, 4, 1, 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 4, 'PREPARATION OF REPORTS', 5, 3, 0.15, 'admin', 'EMP-002', '10/2018-11/2018'),
(4, 5, 'CUSTOMER SERVICE', 45, 4, 1.8, 'admin', 'EMP-002', '10/2018-11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1kpi` (
  `id` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiDesc` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1kpi`
--

INSERT INTO `tbl_ans_eformp1kpi` (`id`, `kpiID`, `kraID`, `kpiDesc`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 1, 2, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 2, 2, 'Coordinate meetings and other activities within the college', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 3, 2, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'admin', 'EMP-002', '10/2018-11/2018'),
(4, 4, 2, 'Supervise the student assistants designated to the college', 'admin', 'EMP-002', '10/2018-11/2018'),
(5, 1, 3, 'Organize office files and documents', 'admin', 'EMP-002', '10/2018-11/2018'),
(6, 2, 3, 'Supervise order and cleanliness in the office', 'admin', 'EMP-002', '10/2018-11/2018'),
(7, 3, 3, 'Regular inventory and update of office supplies', 'admin', 'EMP-002', '10/2018-11/2018'),
(8, 1, 4, 'Submit reports (e.g., Attendance, Completion of Grades) Â required by other offices (e.g., HRD, Regi', 'admin', 'EMP-002', '10/2018-11/2018'),
(9, 2, 4, 'Monitor facultys attendanceÂ ', 'admin', 'EMP-002', '10/2018-11/2018'),
(10, 3, 4, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'admin', 'EMP-002', '10/2018-11/2018'),
(11, 1, 5, 'Answer telephone calls and/or take messages', 'admin', 'EMP-002', '10/2018-11/2018'),
(12, 2, 5, 'Man the offices frontdesk to meet walk-in customers Â  Â Â ', 'admin', 'EMP-002', '10/2018-11/2018'),
(13, 3, 5, 'Arrange customers appointments with the Dean or faculty Â  Â ', 'admin', 'EMP-002', '10/2018-11/2018'),
(14, 4, 5, 'Handle data encoding duties during enrolment', 'admin', 'EMP-002', '10/2018-11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1_achievements`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1_achievements` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiAchievement` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1_achievements`
--

INSERT INTO `tbl_ans_eformp1_achievements` (`id`, `kraID`, `kpiAchievement`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 2, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 2, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 2, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(4, 2, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(5, 3, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(6, 3, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(7, 3, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(8, 4, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(9, 4, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(10, 4, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(11, 5, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(12, 5, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(13, 5, '', 'admin', 'EMP-002', '10/2018-11/2018'),
(14, 5, '', 'admin', 'EMP-002', '10/2018-11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` varchar(30) NOT NULL,
  `competencyDescription` longtext NOT NULL,
  `proficiencyLevel` varchar(35) NOT NULL,
  `p2_weights` float NOT NULL,
  `p2_rating` int(11) NOT NULL,
  `p2_weightedRating` float NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2a`
--

INSERT INTO `tbl_ans_eformp2a` (`id`, `competency`, `competencyDescription`, `proficiencyLevel`, `p2_weights`, `p2_rating`, `p2_weightedRating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community.Â </p>\r\n', 'A Contributing Independently Learne', 10, 4, 0.4, 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions</p>\r\n', 'A Contributing Independently Learne', 10, 4, 0.4, 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'Customer Focus', '<p>The ability to focus efforts on determining</p>\r\n', 'A Contributing Independently Learne', 20, 4, 0.4, 'admin', 'EMP-002', '10/2018-11/2018'),
(4, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group</p>\r\n', 'B Contributing with Expertise Speci', 20, 3, 0.3, 'admin', 'EMP-002', '10/2018-11/2018'),
(5, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin', 'EMP-002', '10/2018-11/2018'),
(6, 'Leadership Excellence', '<p>Upholds values and ethics</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin', 'EMP-002', '10/2018-11/2018'),
(7, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 2, 0.2, 'admin', 'EMP-002', '10/2018-11/2018'),
(8, 'People Development', '<p>The ability to plan and support the development of employeesâ€™ skills and abilities so that they can fulfill</p>\r\n', 'C Contribution through Teams/Peers ', 10, 2, 0.2, 'admin', 'EMP-002', '10/2018-11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(30) NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2b`
--

INSERT INTO `tbl_ans_eformp2b` (`id`, `competency`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'Creativity and Innovation', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'Customer Focus', 'admin', 'EMP-002', '10/2018-11/2018'),
(4, 'Team Orientation', 'admin', 'EMP-002', '10/2018-11/2018'),
(5, 'Technical Knowledge', 'admin', 'EMP-002', '10/2018-11/2018'),
(6, 'Leadership Excellence', 'admin', 'EMP-002', '10/2018-11/2018'),
(7, 'Management Excellence', 'admin', 'EMP-002', '10/2018-11/2018'),
(8, 'People Development', 'admin', 'EMP-002', '10/2018-11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2b_critical_incident`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2b_critical_incident` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(30) NOT NULL,
  `criticalIncident` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2b_critical_incident`
--

INSERT INTO `tbl_ans_eformp2b_critical_incident` (`id`, `competencyName`, `criticalIncident`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2018-11/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2018-11/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2018-11/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2018-11/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2018-11/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2018-11/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2018-11/2018'),
(1, 'People Development', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'People Development', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'People Development', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2018-11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL,
  `score` int(11) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp3`
--

INSERT INTO `tbl_ans_eformp3` (`id`, `areas`, `score`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'DISCIPLINE', 3, 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'ATTENDANCE', 3, 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'PUNCTUALITY', 4, 'admin', 'EMP-002', '10/2018-11/2018'),
(4, 'COMPLETION OF TRAINING', 3, 'admin', 'EMP-002', '10/2018-11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp3_rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp3_rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` varchar(200) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp3_rubric`
--

INSERT INTO `tbl_ans_eformp3_rubric` (`id`, `areaName`, `rubricDesc`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality', 'admin', 'EMP-002', '10/2018-11/2018'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'admin', 'EMP-002', '10/2018-11/2018'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness', 'admin', 'EMP-002', '10/2018-11/2018'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin', 'EMP-002', '10/2018-11/2018'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'admin', 'EMP-002', '10/2018-11/2018'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin', 'EMP-002', '10/2018-11/2018'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'admin', 'EMP-002', '10/2018-11/2018'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.', 'admin', 'EMP-002', '10/2018-11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp4` (
  `id` int(11) NOT NULL,
  `p4_area` varchar(50) NOT NULL,
  `customerFeedback` text NOT NULL,
  `p4_score` int(11) NOT NULL,
  `p4_rating` varchar(11) NOT NULL,
  `answeredBy` varchar(80) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp4`
--

INSERT INTO `tbl_ans_eformp4` (`id`, `p4_area`, `customerFeedback`, `p4_score`, `p4_rating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'April - June', '', 3, '0.03', 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'July - September', '', 3, '0.03', 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'October-December', '', 3, '0.03', 'admin', 'EMP-002', '10/2018-11/2018'),
(4, 'January-March', '', 3, '0.03', 'admin', 'EMP-002', '10/2018-11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_summary`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_summary` (
  `id` int(11) NOT NULL,
  `performance_rating` varchar(60) NOT NULL,
  `total_weighted_rating` float NOT NULL,
  `percentage_allocation` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_summary`
--

INSERT INTO `tbl_ans_summary` (`id`, `performance_rating`, `total_weighted_rating`, `percentage_allocation`, `subtotal`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 3.95, 40, 1.58, 'admin', 'EMP-002', '10/2018-11/2018'),
(2, 'PART 2: COMPETENCIES', 2.5, 30, 3.9, 'admin', 'EMP-002', '10/2018-11/2018'),
(3, 'PART 3: PROFESSIONALISM', 13, 30, 0.75, 'admin', 'EMP-002', '10/2018-11/2018'),
(4, '0', 0, 0, 0.03, 'admin', 'EMP-002', '10/2018-11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_summary_comments_acknowledgement`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_summary_comments_acknowledgement` (
  `id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `recommendations` text NOT NULL,
  `approval` text NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_summary_comments_acknowledgement`
--

INSERT INTO `tbl_ans_summary_comments_acknowledgement` (`id`, `comments`, `recommendations`, `approval`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, '<p>late comments for arwind\r\n\r\n2018 \r\n\r\n</p>', '<p>\r\n\r\nlate recommendations for arwind\r\n\r\n\r\n2018 \r\n\r\n\r\n<br></p>', '<p>\r\n\r\nlate approval for arwind 2018&nbsp;<br></p>', 'admin', 'EMP-002', '10/2018-11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_competencyratingscale`
--

CREATE TABLE IF NOT EXISTS `tbl_competencyratingscale` (
  `id` int(11) NOT NULL,
  `competency_rating_value` varchar(100) NOT NULL,
  `competency_title` varchar(100) NOT NULL,
  `competency_description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_competencyratingscale`
--

INSERT INTO `tbl_competencyratingscale` (`id`, `competency_rating_value`, `competency_title`, `competency_description`) VALUES
(3, '4', 'Expert/Exemplary', '(Evaluation; Defines Framework Modes; Mentor, Self - Actualization)'),
(4, '3', 'Advanced/Accomplished', '(Analysis; Synthesis; Understands Framework Models, Exceeds)'),
(5, '2', 'Intermediate/Developing', '(Unsupervised Application; Beginning Analysis, Meets)'),
(6, '1', 'Beginner', '(Comprehension; Beginning Application; Some Experience, Meets some)'),
(7, 'NE', '', 'Early stages of application of the item; Behavior not yet present.'),
(8, 'NA', '', 'The item does not apply to your position or performance of assigned tasks and responsibilities.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dateevaluation`
--

CREATE TABLE IF NOT EXISTS `tbl_dateevaluation` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dateevaluation`
--

INSERT INTO `tbl_dateevaluation` (`startDate`, `endDate`) VALUES
('10/2019', '11/2019');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_datekpi`
--

CREATE TABLE IF NOT EXISTS `tbl_datekpi` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_datekpi`
--

INSERT INTO `tbl_datekpi` (`startDate`, `endDate`) VALUES
('09/2017', '10/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departments`
--

CREATE TABLE IF NOT EXISTS `tbl_departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `initials` varchar(100) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL DEFAULT 'PENDING',
  `departmentOwner` varchar(100) NOT NULL,
  `monitor` varchar(6) NOT NULL DEFAULT 'unread'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_departments`
--

INSERT INTO `tbl_departments` (`id`, `department_name`, `initials`, `kpiStatus`, `departmentOwner`, `monitor`) VALUES
(1, 'College of Information Technology Education', 'CITE', 'APPROVED', 'superadmin', 'read'),
(2, 'College of Management and Accountancy', 'CMA', 'APPROVED', 'admin2', 'read');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp1` (
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeight` int(11) NOT NULL,
  `kpiOwner` varchar(60) NOT NULL,
  `kpiDateCreated` varchar(11) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp1`
--

INSERT INTO `tbl_eformp1` (`kraID`, `kpiTitle`, `kpiWeight`, `kpiOwner`, `kpiDateCreated`, `kpiStatus`) VALUES
(1, 'Leadership', 0, 'ADMIN-2', '', 'PENDING'),
(2, 'COMMUNICATION & COORDINATION', 0, 'admin', '', 'APPROVED'),
(3, 'OFFICE MANAGEMENT', 0, 'admin', '', 'APPROVED'),
(4, 'PREPARATION OF REPORTS', 0, 'admin', '', 'APPROVED'),
(5, 'CUSTOMER SERVICE', 0, 'admin', '', 'APPROVED'),
(6, 'COMMUNICATION', 0, 'EMP-001', '', 'SENT'),
(7, '2', 0, 'EMP-001', '', 'SENT'),
(8, '3', 0, 'EMP-001', '', 'SENT'),
(9, '4', 0, 'EMP-001', '', 'SENT'),
(10, 's', 0, 'admin2', '', 'APPROVED'),
(11, 'a', 0, 'admin2', '', 'APPROVED'),
(12, 'asd', 0, 'admin2', '', 'APPROVED'),
(13, 'ff', 0, 'admin2', '', 'APPROVED');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` text NOT NULL,
  `competencyDesc` varchar(500) NOT NULL,
  `requiredProfLevel` varchar(50) NOT NULL,
  `weights` float NOT NULL,
  `rating` int(2) NOT NULL,
  `weightedRating` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2a`
--

INSERT INTO `tbl_eformp2a` (`id`, `competency`, `competencyDesc`, `requiredProfLevel`, `weights`, `rating`, `weightedRating`) VALUES
(1, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community.&nbsp;</p>\r\n', 'C -- Contribution through Teams/Peers', 10, 0, 0),
(2, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions</p>\r\n', 'C -- Contribution through Teams/Peers', 10, 0, 0),
(3, 'Customer Focus', '<p>The ability to focus efforts on determining</p>\r\n', 'A -- Contributing Independently', 20, 0, 0),
(4, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group</p>\r\n', 'B -- Contributing with Expertise', 20, 0, 0),
(5, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession.</p>\r\n', 'B -- Contributing with Expertise', 10, 0, 0),
(6, 'Leadership Excellence', '<p>Upholds values and ethics</p>\r\n', 'C -- Contribution through Teams/Peers', 10, 0, 0),
(7, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders.</p>\r\n', 'B -- Contributing with Expertise', 10, 0, 0),
(8, 'People Development', '<p>The ability to plan and support the development of employees&rsquo; skills and abilities so that they can fulfill</p>\r\n', 'A -- Contributing Independently', 10, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2b`
--

INSERT INTO `tbl_eformp2b` (`id`, `competency`) VALUES
(1, 'Adaptability'),
(2, 'Creativity and Innovation'),
(3, 'Customer Focus'),
(4, 'Team Orientation'),
(5, 'Technical Knowledge'),
(6, 'Leadership Excellence'),
(7, 'Management Excellence'),
(8, 'People Development');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp3`
--

INSERT INTO `tbl_eformp3` (`id`, `areas`) VALUES
(1, 'DISCIPLINE'),
(2, 'ATTENDANCE'),
(3, 'PUNCTUALITY'),
(4, 'COMPLETION OF TRAINING');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp4` (
  `id` int(11) NOT NULL,
  `areas` varchar(30) NOT NULL,
  `customerFeedback` varchar(100) NOT NULL,
  `respondentScore` int(2) NOT NULL,
  `ratingFactor` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp4`
--

INSERT INTO `tbl_eformp4` (`id`, `areas`, `customerFeedback`, `respondentScore`, `ratingFactor`) VALUES
(1, 'April - June', '', 0, 0),
(2, 'July - September', '', 0, 0),
(3, 'October-December', '', 0, 0),
(4, 'January-March', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation_requests`
--

CREATE TABLE IF NOT EXISTS `tbl_evaluation_requests` (
  `id` int(11) NOT NULL,
  `admin_id` varchar(60) NOT NULL,
  `employee_missed` varchar(60) NOT NULL,
  `employee_department` varchar(60) NOT NULL,
  `reason` text NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `request_status` varchar(30) NOT NULL DEFAULT 'SENT'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_evaluation_requests`
--

INSERT INTO `tbl_evaluation_requests` (`id`, `admin_id`, `employee_missed`, `employee_department`, `reason`, `performanceCycle`, `request_status`) VALUES
(1, 'admin', 'EMP-001', 'College of Information Technology Education', '', '10/2018-11/2018', 'APPROVED'),
(2, 'admin', 'EMP-002', 'College of Information Technology Education', '', '10/2018-11/2018', 'APPROVED'),
(3, 'admin', 'll', 'College of Information Technology Education', 'reason for ll', '10/2018-11/2018', 'SENT'),
(4, 'admin', 'uu', 'College of Information Technology Education', 'reason for uu', '10/2018-11/2018', 'SENT');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation_results`
--

CREATE TABLE IF NOT EXISTS `tbl_evaluation_results` (
  `id` int(11) NOT NULL,
  `emp_id` varchar(60) NOT NULL,
  `employee_firstName` varchar(60) NOT NULL,
  `employee_middleName` varchar(60) NOT NULL,
  `employee_lastName` varchar(60) NOT NULL,
  `employee_department` varchar(60) NOT NULL,
  `employee_position` varchar(30) NOT NULL,
  `employee_job_level` varchar(30) NOT NULL,
  `employee_years_company` int(11) NOT NULL,
  `employee_years_position` int(11) NOT NULL,
  `evaluationStatus` varchar(30) NOT NULL DEFAULT 'NOT YET STARTED',
  `performance_rating` varchar(100) NOT NULL,
  `overall_rating` varchar(100) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `ev_sog_feedback` varchar(15) NOT NULL DEFAULT 'NO RESPONSE YET'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_evaluation_results`
--

INSERT INTO `tbl_evaluation_results` (`id`, `emp_id`, `employee_firstName`, `employee_middleName`, `employee_lastName`, `employee_department`, `employee_position`, `employee_job_level`, `employee_years_company`, `employee_years_position`, `evaluationStatus`, `performance_rating`, `overall_rating`, `answeredBy`, `performanceCycle`, `ev_sog_feedback`) VALUES
(1, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin', '10/2018-11/2018', 'NO RESPONSE YET'),
(2, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 0, 0, 'COMPLETED', '6.23', '6.26', 'admin', '10/2018-11/2018', 'NO RESPONSE YET'),
(3, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin', '10/2018-11/2018', 'NO RESPONSE YET'),
(4, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin', '10/2018-11/2018', 'NO RESPONSE YET'),
(5, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin', '10/2019-11/2019', 'NO RESPONSE YET'),
(6, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin', '10/2019-11/2019', 'NO RESPONSE YET'),
(7, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin', '10/2019-11/2019', 'NO RESPONSE YET'),
(8, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin', '10/2019-11/2019', 'NO RESPONSE YET');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kpiratingscale`
--

CREATE TABLE IF NOT EXISTS `tbl_kpiratingscale` (
  `id` int(11) NOT NULL,
  `rating_value` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `weighted_rating` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kpiratingscale`
--

INSERT INTO `tbl_kpiratingscale` (`id`, `rating_value`, `description`, `weighted_rating`) VALUES
(15, '4', 'Exceeds Expectations (Performs above standards all the time)', '1'),
(16, '3', 'Meets Expectations (Solid, consistent performance; Generally meets standards)', '0.75'),
(17, '2', 'Needs Improvement (Frequently fails to meet standards)', '0.5'),
(18, '1', 'Unacceptable (Performance is below job requirements and needs)', '0.25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE IF NOT EXISTS `tbl_messages` (
  `id` int(11) NOT NULL,
  `subject` varchar(60) NOT NULL,
  `content` text NOT NULL,
  `sender` varchar(50) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'unread',
  `checked` varchar(3) NOT NULL DEFAULT 'no',
  `sent_at` datetime NOT NULL,
  `location` varchar(100) NOT NULL,
  `deleted_by_sender` varchar(100) NOT NULL DEFAULT 'no',
  `deleted_by_receiver` varchar(100) NOT NULL DEFAULT 'no',
  `trash_by_HR` varchar(100) NOT NULL DEFAULT 'no',
  `trash_by_Department` varchar(100) NOT NULL DEFAULT 'no',
  `revision` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE IF NOT EXISTS `tbl_notification` (
  `id` int(11) NOT NULL,
  `notification` varchar(100) NOT NULL,
  `sendBy` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`id`, `notification`, `sendBy`, `status`, `timeStamp`) VALUES
(1, 'College of Information Technology Education SENT AN EVALUATION', 'admin', 'read', '2017-11-13 12:37:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification_user`
--

CREATE TABLE IF NOT EXISTS `tbl_notification_user` (
  `id` int(11) NOT NULL,
  `notification` varchar(100) NOT NULL,
  `sendBy` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `owner` varchar(60) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification_user`
--

INSERT INTO `tbl_notification_user` (`id`, `notification`, `sendBy`, `status`, `owner`, `timeStamp`) VALUES
(1, 'HR APPROVED YOUR REQUEST TO OPEN THE EVALUATION FOR uu uu|10/2018-11/2018', 'admin', 'unread', 'admin', '2017-11-13 15:14:11'),
(2, 'HR DENIED YOUR REQUEST TO OPEN THE EVALUATION FOR llsdfsdfsdfsdfs ll|10/2018-11/2018', 'admin', 'unread', 'admin', '2017-11-13 15:14:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_p1kpi` (
  `kraID` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kpiDesc` varchar(100) NOT NULL,
  `kpiOwner` varchar(100) NOT NULL,
  `kpiDate` varchar(11) NOT NULL,
  `kpiStatus` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p1kpi`
--

INSERT INTO `tbl_p1kpi` (`kraID`, `kpiID`, `kpiDesc`, `kpiOwner`, `kpiDate`, `kpiStatus`) VALUES
(2, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'admin', '', 'APPROVED'),
(2, 2, 'Coordinate meetings and other activities within the college', 'admin', '', 'APPROVED'),
(2, 3, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'admin', '', 'APPROVED'),
(2, 4, 'Supervise the student assistants designated to the college', 'admin', '', 'APPROVED'),
(3, 1, 'Organize office files and documents', 'admin', '', 'APPROVED'),
(3, 2, 'Supervise order and cleanliness in the office', 'admin', '', 'APPROVED'),
(3, 3, 'Regular inventory and update of office supplies', 'admin', '', 'APPROVED'),
(4, 1, 'Submit reports (e.g., Attendance, Completion of Grades) Â required by other offices (e.g., HRD, Regi', 'admin', '', 'APPROVED'),
(4, 2, 'Monitor facultys attendanceÂ ', 'admin', '', 'APPROVED'),
(4, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'admin', '', 'APPROVED'),
(5, 1, 'Answer telephone calls and/or take messages', 'admin', '', 'APPROVED'),
(5, 2, 'Man the offices frontdesk to meet walk-in customers Â  Â Â ', 'admin', '', 'APPROVED'),
(5, 3, 'Arrange customers appointments with the Dean or faculty Â  Â ', 'admin', '', 'APPROVED'),
(5, 4, 'Handle data encoding duties during enrolment', 'admin', '', 'APPROVED'),
(6, 1, 'knows how to speak english', 'EMP-001', '', 'PENDING'),
(6, 2, 'tagalog', 'EMP-001', '', 'PENDING'),
(10, 1, 'ss', 'admin2', '', 'APPROVED'),
(11, 1, 'as', 'admin2', '', 'APPROVED'),
(12, 1, 'asdasd', 'admin2', '', 'APPROVED'),
(13, 1, 'fff', 'admin2', '', 'APPROVED');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p2bcritinc`
--

CREATE TABLE IF NOT EXISTS `tbl_p2bcritinc` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(60) NOT NULL,
  `criticalIncident` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p2bcritinc`
--

INSERT INTO `tbl_p2bcritinc` (`id`, `competencyName`, `criticalIncident`) VALUES
(1, 'Adaptability', 'Critical Incident'),
(2, 'Adaptability', 'Critical Incident'),
(3, 'Adaptability', 'Critical Incident'),
(1, 'Creativity and Innovation', 'Critical Incident'),
(2, 'Creativity and Innovation', 'Critical Incident'),
(3, 'Creativity and Innovation', 'Critical Incident'),
(1, 'Customer Focus', 'Critical Incident'),
(2, 'Customer Focus', 'Critical Incident'),
(3, 'Customer Focus', 'Critical Incident'),
(1, 'Team Orientation', 'Critical Incident'),
(2, 'Team Orientation', 'Critical Incident'),
(3, 'Team Orientation', 'Critical Incident'),
(1, 'Technical Knowledge', 'Critical Incident'),
(2, 'Technical Knowledge', 'Critical Incident'),
(3, 'Technical Knowledge', 'Critical Incident'),
(1, 'Leadership Excellence', 'Critical Incident'),
(2, 'Leadership Excellence', 'Critical Incident'),
(3, 'Leadership Excellence', 'Critical Incident'),
(1, 'Management Excellence', 'Critical Incident'),
(2, 'Management Excellence', 'Critical Incident'),
(3, 'Management Excellence', 'Critical Incident'),
(1, 'People Development', 'Critical Incident'),
(2, 'People Development', 'Critical Incident'),
(3, 'People Development', 'Critical Incident');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p3rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_p3rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p3rubric`
--

INSERT INTO `tbl_p3rubric` (`id`, `areaName`, `rubricDesc`) VALUES
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.'),
(4, 'Test me', 'Has zero record of disciplinary action on offenses other than the above policies');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_proficiency_level`
--

CREATE TABLE IF NOT EXISTS `tbl_proficiency_level` (
  `id` int(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `initials` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_proficiency_level`
--

INSERT INTO `tbl_proficiency_level` (`id`, `value`, `initials`, `title`, `content`) VALUES
(1, '4', 'A', 'Contributing Independently', 'Learner-Doer. Individual contributor. Follower, team-player, Customer touchpoint'),
(3, '3', 'B', 'Contributing with Expertise', 'Specialist. Expert Implementor. Customer Advocate. Problem Solver'),
(4, '2', 'C', 'Contribution through Teams/Peers', 'Project Leader. Idea Leader. Technical Troubleshooter. Coach'),
(5, '1', 'D', 'Contributing through Strategy Building', 'Navigator. Idea Innovator. Leader Developer. Mission Advocate.'),
(6, '0', 'NE', '', 'Early stages of application of the item; Behavior not yet present'),
(7, '0', 'NA', '', 'The item does not apply to your position or performance of assigned tasks and responsibilities');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating_execute`
--

CREATE TABLE IF NOT EXISTS `tbl_rating_execute` (
  `ID` int(11) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `performance_rating` varchar(100) NOT NULL,
  `overall_rating` varchar(100) NOT NULL,
  `performanceCycle` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rating_execute`
--

INSERT INTO `tbl_rating_execute` (`ID`, `emp_id`, `performance_rating`, `overall_rating`, `performanceCycle`) VALUES
(1, 'EMP-002', '17.05', '17.05', '10/2017-11/2017'),
(2, 'EMP-002', '23.1', '23.1', '10/2018-11/2018'),
(3, 'EMP-001', '17.95', '17.95', '10/2018-11/2018'),
(4, 'EMP-002', '19.75', '19.75', '10/2018-11/2018'),
(5, 'uu', '18.3', '18.3', '10/2018-11/2018'),
(6, 'EMP-001', '18.4', '18.4', '10/2017-11/2017'),
(7, 'EMP-001', '5.55', '5.55', '10/2017-11/2017'),
(8, 'EMP-002', '6.97', '6.97', '10/2017-11/2017'),
(9, 'uu', '7.03', '7.07', '10/2017-11/2017'),
(10, 'll', '5.05', '5.09', '10/2017-11/2017'),
(11, 'EMP-002', '6.72', '6.74', '10/2018-11/2018'),
(12, 'EMP-002', '6.23', '6.26', '10/2018-11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp2a` (
  `criticalIncidentCount` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp2a`
--

INSERT INTO `tbl_settingsp2a` (`criticalIncidentCount`) VALUES
(3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp3`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp3` (
  `rubricCount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp3`
--

INSERT INTO `tbl_settingsp3` (`rubricCount`) VALUES
(4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sog_employee`
--

CREATE TABLE IF NOT EXISTS `tbl_sog_employee` (
  `emp_id` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `department_head` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_temp` varchar(100) NOT NULL,
  `email_employee` text NOT NULL,
  `paths` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sog_employee`
--

INSERT INTO `tbl_sog_employee` (`emp_id`, `firstname`, `middlename`, `lastname`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `department_head`, `username`, `password`, `password_temp`, `email_employee`, `paths`) VALUES
('EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', '5', 'Secretary', 'D1', '2012', '5', '', 'EMP-001', '2e6cd64c4d3aabb280846e0192338a96', 'ESTAYO', '', '../assets/uploaded_files/accounts/s4KP3lFB.jpg'),
('EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', '13', 'Assistant ', 'D1', '2004', '13', '', 'EMP-002', '6cab142f45cd157b5299e1b828e668e6', 'SANTOS', '', '../assets/uploaded_files/accounts/default.png'),
('EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', '5', 'Secretary', 'D2', '2012', '5', '', 'EMP-003', 'd41d8cd98f00b204e9800998ecf8427e', '', 'test2@gmail.com', '../assets/uploaded_files/accounts/default.png'),
('EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', '5', 'A Sec', 'D12', '2012', '5', '', 'EMP-004', 'da598699cef750ee8e5ef3b2daf47719', 'JOSE', 'tomtomtomtomtom@gmail.com', '../assets/uploaded_files/accounts/default.png'),
('ll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', '4', 'll', 'll', '2013', '4', '', 'll', '67824ecf84f5816f07b74fa956bdbcd2', 'LL', '', '../assets/uploaded_files/accounts/P_20151215_125500.jpg'),
('uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', '2', 'uu', 'uu', '2015', '2', '', 'uu', '81cf6107131a3583e2b0b762cb9c2862', 'UU', '', '../assets/uploaded_files/accounts/47e86f5632d283ec3f22145198268887deb10c29cebbe2531c7a2c47f949f942.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp2a`
--
ALTER TABLE `tbl_ans_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp2b`
--
ALTER TABLE `tbl_ans_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp3`
--
ALTER TABLE `tbl_ans_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp4`
--
ALTER TABLE `tbl_ans_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_summary_comments_acknowledgement`
--
ALTER TABLE `tbl_ans_summary_comments_acknowledgement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_competencyratingscale`
--
ALTER TABLE `tbl_competencyratingscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp1`
--
ALTER TABLE `tbl_eformp1`
  ADD PRIMARY KEY (`kraID`);

--
-- Indexes for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_evaluation_requests`
--
ALTER TABLE `tbl_evaluation_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_evaluation_results`
--
ALTER TABLE `tbl_evaluation_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kpiratingscale`
--
ALTER TABLE `tbl_kpiratingscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification_user`
--
ALTER TABLE `tbl_notification_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_rating_execute`
--
ALTER TABLE `tbl_rating_execute`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_sog_employee`
--
ALTER TABLE `tbl_sog_employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp2a`
--
ALTER TABLE `tbl_ans_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp2b`
--
ALTER TABLE `tbl_ans_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp3`
--
ALTER TABLE `tbl_ans_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp4`
--
ALTER TABLE `tbl_ans_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_ans_summary_comments_acknowledgement`
--
ALTER TABLE `tbl_ans_summary_comments_acknowledgement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_competencyratingscale`
--
ALTER TABLE `tbl_competencyratingscale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_evaluation_requests`
--
ALTER TABLE `tbl_evaluation_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_evaluation_results`
--
ALTER TABLE `tbl_evaluation_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_kpiratingscale`
--
ALTER TABLE `tbl_kpiratingscale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_notification_user`
--
ALTER TABLE `tbl_notification_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_rating_execute`
--
ALTER TABLE `tbl_rating_execute`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
