-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2017 at 12:43 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `es_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admins`
--

CREATE TABLE IF NOT EXISTS `tbl_admins` (
  `admin_id` varchar(100) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `firstname` varchar(60) NOT NULL,
  `middlename` varchar(60) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` text CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `password_temp` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `paths` text NOT NULL,
  `userlevel` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admins`
--

INSERT INTO `tbl_admins` (`admin_id`, `lastname`, `firstname`, `middlename`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `username`, `password`, `password_temp`, `paths`, `userlevel`, `email`) VALUES
('EMP-001', 'Ayson', 'Justin Louise', 'Vinluan', 'College of Information Technology Education', '16', 'Dean', 'D1', '2001', '16', 'EMP-001', 'ef7374870359e618cdd732585eeb3874', 'AYSON', '../assets/uploaded_files/accounts/2017_0601_192509_001.jpg', 'ADMIN', 'maxnamd@gmail.com'),
('EMP-002', 'Garay', 'Jude Joel', 'Tayag', 'College of Management and Accountancy', '17', 'Dean', 'D2', '2000', '17', 'EMP-002', 'e837bd0cb24543fd26d05e2290c5b57e', 'GARAY', '../assets/uploaded_files/accounts/default.png', 'ADMIN', 'jjgaray@yahoo.com'),
('superadmin', 'Clauna', 'Levi Marion', 'Almazan', 'Human Resource Department', '7', 'Dean', 'Regular', '', '7', 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 'CLAUNA', '../assets/uploaded_files/accounts/default.png', 'SUPERADMIN', 'levimarion_clauna@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeights` int(11) NOT NULL,
  `p1_rating` int(11) NOT NULL,
  `p1_weightedRating` float NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1`
--

INSERT INTO `tbl_ans_eformp1` (`id`, `kraID`, `kpiTitle`, `kpiWeights`, `p1_rating`, `p1_weightedRating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 1, 'COMMUNICATION AND COORDINATION', 25, 2, 0.5, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 2, 'OFFICE MANAGEMENT', 25, 2, 0.5, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 3, 'PREPARATION OF REPORTS', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(4, 4, 'CUSTOMER SERVICE', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(5, 1, 'COMMUNICATION AND COORDINATION', 25, 2, 0.5, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(6, 2, 'OFFICE MANAGEMENT', 25, 4, 1, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(7, 3, 'PREPARATION OF REPORTS', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(8, 4, 'CUSTOMER SERVICE', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(9, 1, 'COMMUNICATION AND COORDINATION', 25, 2, 0.5, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(10, 2, 'OFFICE MANAGEMENT', 25, 4, 1, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(11, 3, 'PREPARATION OF REPORTS', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(12, 4, 'CUSTOMER SERVICE', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(13, 1, 'COMMUNICATION AND COORDINATION', 25, 2, 0.5, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(14, 2, 'OFFICE MANAGEMENT', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(15, 3, 'PREPARATION OF REPORTS', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(16, 4, 'CUSTOMER SERVICE', 25, 4, 1, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(17, 1, 'COMMUNICATION AND COORDINATION', 25, 2, 0.5, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(18, 2, 'OFFICE MANAGEMENT', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(19, 3, 'PREPARATION OF REPORTS', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(20, 4, 'CUSTOMER SERVICE', 25, 4, 1, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(21, 1, 'COMMUNICATION AND COORDINATION', 25, 2, 0.5, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(22, 2, 'OFFICE MANAGEMENT', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(23, 3, 'PREPARATION OF REPORTS', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(24, 4, 'CUSTOMER SERVICE', 25, 3, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(25, 1, 'COMMUNICATION AND COORDINATION', 25, 2, 0.5, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(26, 2, 'OFFICE MANAGEMENT', 25, 2, 0.5, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(27, 3, 'PREPARATION OF REPORTS', 25, 4, 1, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(28, 4, 'CUSTOMER SERVICE', 25, 3, 0.75, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(29, 5, 'COMMUNICATION & COORDINATION', 25, 4, 1, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(30, 6, 'OFFICE MANAGEMENT', 25, 4, 1, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(31, 7, 'PREPARATION OF REPORTS', 25, 4, 1, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(32, 8, 'CUSTOMER SERVICE', 25, 4, 1, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(33, 5, 'COMMUNICATION & COORDINATION', 25, 1, 0.25, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(34, 6, 'OFFICE MANAGEMENT', 25, 1, 0.25, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(35, 7, 'PREPARATION OF REPORTS', 25, 1, 0.25, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(36, 8, 'CUSTOMER SERVICE', 25, 1, 0.25, 'EMP-002', 'SOG-003', '12/2017-02/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1kpi` (
  `id` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiDesc` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1kpi`
--

INSERT INTO `tbl_ans_eformp1kpi` (`id`, `kpiID`, `kraID`, `kpiDesc`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 1, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 2, 1, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 3, 1, 'Coordinate meetings and other activities within the college', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(4, 4, 1, 'Supervise the student assistants designated to the college', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(5, 1, 2, 'Organize office files and documents', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(6, 2, 2, 'Supervise order and cleanliness in the office', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(7, 3, 2, 'Regular inventory and update of office supplies', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(8, 1, 3, 'Monitor facultys attendance', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(9, 2, 3, 'Submit reports (e.g., Attendance, Completion of Grades)  required by other offices (e.g., HRD, Regis', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(10, 3, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(11, 1, 4, 'Answer telephone calls and/or take messages', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(12, 2, 4, 'Man the offices frontdesk to meet walk-in customers', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(13, 3, 4, 'Arrange customers appointments with the Dean or faculty', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(14, 4, 4, 'Handle data encoding duties during enrolment', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(15, 1, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(16, 2, 1, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(17, 3, 1, 'Coordinate meetings and other activities within the college', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(18, 4, 1, 'Supervise the student assistants designated to the college', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(19, 1, 2, 'Organize office files and documents', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(20, 2, 2, 'Supervise order and cleanliness in the office', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(21, 3, 2, 'Regular inventory and update of office supplies', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(22, 1, 3, 'Monitor facultys attendance', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(23, 2, 3, 'Submit reports (e.g., Attendance, Completion of Grades)  required by other offices (e.g., HRD, Regis', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(24, 3, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(25, 1, 4, 'Answer telephone calls and/or take messages', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(26, 2, 4, 'Man the offices frontdesk to meet walk-in customers', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(27, 3, 4, 'Arrange customers appointments with the Dean or faculty', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(28, 4, 4, 'Handle data encoding duties during enrolment', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(29, 1, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(30, 2, 1, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(31, 3, 1, 'Coordinate meetings and other activities within the college', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(32, 4, 1, 'Supervise the student assistants designated to the college', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(33, 1, 2, 'Organize office files and documents', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(34, 2, 2, 'Supervise order and cleanliness in the office', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(35, 3, 2, 'Regular inventory and update of office supplies', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(36, 1, 3, 'Monitor facultys attendance', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(37, 2, 3, 'Submit reports (e.g., Attendance, Completion of Grades)  required by other offices (e.g., HRD, Regis', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(38, 3, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(39, 1, 4, 'Answer telephone calls and/or take messages', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(40, 2, 4, 'Man the offices frontdesk to meet walk-in customers', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(41, 3, 4, 'Arrange customers appointments with the Dean or faculty', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(42, 4, 4, 'Handle data encoding duties during enrolment', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(43, 1, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(44, 2, 1, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(45, 3, 1, 'Coordinate meetings and other activities within the college', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(46, 4, 1, 'Supervise the student assistants designated to the college', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(47, 1, 2, 'Organize office files and documents', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(48, 2, 2, 'Supervise order and cleanliness in the office', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(49, 3, 2, 'Regular inventory and update of office supplies', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(50, 1, 3, 'Monitor facultys attendance', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(51, 2, 3, 'Submit reports (e.g., Attendance, Completion of Grades)  required by other offices (e.g., HRD, Regis', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(52, 3, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(53, 1, 4, 'Answer telephone calls and/or take messages', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(54, 2, 4, 'Man the offices frontdesk to meet walk-in customers', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(55, 3, 4, 'Arrange customers appointments with the Dean or faculty', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(56, 4, 4, 'Handle data encoding duties during enrolment', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(57, 1, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(58, 2, 1, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(59, 3, 1, 'Coordinate meetings and other activities within the college', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(60, 4, 1, 'Supervise the student assistants designated to the college', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(61, 1, 2, 'Organize office files and documents', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(62, 2, 2, 'Supervise order and cleanliness in the office', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(63, 3, 2, 'Regular inventory and update of office supplies', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(64, 1, 3, 'Monitor facultys attendance', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(65, 2, 3, 'Submit reports (e.g., Attendance, Completion of Grades)  required by other offices (e.g., HRD, Regis', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(66, 3, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(67, 1, 4, 'Answer telephone calls and/or take messages', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(68, 2, 4, 'Man the offices frontdesk to meet walk-in customers', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(69, 3, 4, 'Arrange customers appointments with the Dean or faculty', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(70, 4, 4, 'Handle data encoding duties during enrolment', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(71, 1, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(72, 2, 1, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(73, 3, 1, 'Coordinate meetings and other activities within the college', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(74, 4, 1, 'Supervise the student assistants designated to the college', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(75, 1, 2, 'Organize office files and documents', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(76, 2, 2, 'Supervise order and cleanliness in the office', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(77, 3, 2, 'Regular inventory and update of office supplies', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(78, 1, 3, 'Monitor facultys attendance', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(79, 2, 3, 'Submit reports (e.g., Attendance, Completion of Grades)  required by other offices (e.g., HRD, Regis', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(80, 3, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(81, 1, 4, 'Answer telephone calls and/or take messages', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(82, 2, 4, 'Man the offices frontdesk to meet walk-in customers', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(83, 3, 4, 'Arrange customers appointments with the Dean or faculty', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(84, 4, 4, 'Handle data encoding duties during enrolment', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(85, 1, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(86, 2, 1, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(87, 3, 1, 'Coordinate meetings and other activities within the college', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(88, 4, 1, 'Supervise the student assistants designated to the college', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(89, 1, 2, 'Organize office files and documents', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(90, 2, 2, 'Supervise order and cleanliness in the office', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(91, 3, 2, 'Regular inventory and update of office supplies', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(92, 1, 3, 'Monitor facultys attendance', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(93, 2, 3, 'Submit reports (e.g., Attendance, Completion of Grades)  required by other offices (e.g., HRD, Regis', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(94, 3, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(95, 1, 4, 'Answer telephone calls and/or take messages', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(96, 2, 4, 'Man the offices frontdesk to meet walk-in customers', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(97, 3, 4, 'Arrange customers appointments with the Dean or faculty', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(98, 4, 4, 'Handle data encoding duties during enrolment', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(99, 1, 5, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(100, 2, 5, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(101, 3, 5, 'Coordinate meetings and other activities within the college', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(102, 4, 5, 'Supervise the student assistants designated to the college', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(103, 1, 6, 'Organize office files and documents', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(104, 2, 6, 'Supervise order and cleanliness in the office', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(105, 3, 6, 'Regular inventory and update of office supplies', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(106, 1, 7, 'Monitor facultys attendance 				', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(107, 2, 7, 'Submit reports (e.g., Attendance, Completion of Grades)  required by other offices (e.g., HRD, Regis', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(108, 3, 7, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(109, 1, 8, 'Answer telephone calls and/or take messages', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(110, 2, 8, 'Man the offices frontdesk to meet walk-in customers ', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(111, 3, 8, 'Arrange customers appointments with the Dean or faculty', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(112, 4, 8, 'Handle data encoding duties during enrolment', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(113, 1, 5, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(114, 2, 5, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(115, 3, 5, 'Coordinate meetings and other activities within the college', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(116, 4, 5, 'Supervise the student assistants designated to the college', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(117, 1, 6, 'Organize office files and documents', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(118, 2, 6, 'Supervise order and cleanliness in the office', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(119, 3, 6, 'Regular inventory and update of office supplies', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(120, 1, 7, 'Monitor facultys attendance 				', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(121, 2, 7, 'Submit reports (e.g., Attendance, Completion of Grades)  required by other offices (e.g., HRD, Regis', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(122, 3, 7, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(123, 1, 8, 'Answer telephone calls and/or take messages', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(124, 2, 8, 'Man the offices frontdesk to meet walk-in customers ', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(125, 3, 8, 'Arrange customers appointments with the Dean or faculty', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(126, 4, 8, 'Handle data encoding duties during enrolment', 'EMP-002', 'SOG-003', '12/2017-02/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1_achievements`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1_achievements` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiAchievement` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1_achievements`
--

INSERT INTO `tbl_ans_eformp1_achievements` (`id`, `kraID`, `kpiAchievement`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(4, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(5, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(6, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(7, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(8, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(9, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(10, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(11, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(12, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(13, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(14, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(15, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(16, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(17, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(18, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(19, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(20, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(21, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(22, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(23, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(24, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(25, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(26, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(27, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(28, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(29, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(30, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(31, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(32, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(33, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(34, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(35, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(36, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(37, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(38, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(39, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(40, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(41, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(42, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(43, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(44, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(45, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(46, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(47, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(48, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(49, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(50, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(51, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(52, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(53, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(54, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(55, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(56, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(57, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(58, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(59, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(60, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(61, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(62, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(63, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(64, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(65, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(66, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(67, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(68, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(69, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(70, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(71, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(72, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(73, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(74, 1, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(75, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(76, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(77, 2, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(78, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(79, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(80, 3, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(81, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(82, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(83, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(84, 4, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(85, 1, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(86, 1, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(87, 1, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(88, 1, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(89, 2, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(90, 2, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(91, 2, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(92, 3, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(93, 3, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(94, 3, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(95, 4, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(96, 4, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(97, 4, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(98, 4, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(99, 5, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(100, 5, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(101, 5, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(102, 5, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(103, 6, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(104, 6, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(105, 6, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(106, 7, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(107, 7, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(108, 7, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(109, 8, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(110, 8, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(111, 8, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(112, 8, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(113, 5, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(114, 5, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(115, 5, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(116, 5, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(117, 6, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(118, 6, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(119, 6, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(120, 7, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(121, 7, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(122, 7, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(123, 8, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(124, 8, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(125, 8, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(126, 8, '', 'EMP-002', 'SOG-003', '12/2017-02/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` varchar(30) NOT NULL,
  `competencyDescription` longtext NOT NULL,
  `proficiencyLevel` varchar(35) NOT NULL,
  `p2_weights` float NOT NULL,
  `p2_rating` int(11) NOT NULL,
  `p2_weightedRating` float NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2a`
--

INSERT INTO `tbl_ans_eformp2a` (`id`, `competency`, `competencyDescription`, `proficiencyLevel`, `p2_weights`, `p2_rating`, `p2_weightedRating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', '<p>The ability to adopt to the cultire and environment of the scool community. Able to adjustto changes and exhibits a positive attitude towards different situations.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'Creativity and Innovation', '<p>The ability to provide reative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approcahes to identify opportunities, implements solutions, measure impact and serve customers.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', 'C Contribution through Teams/Peers ', 20, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(4, 'Team Orientation', '<p>Able to effeectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'B Contributing with Expertise Speci', 20, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(5, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firlm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'A Contributing Independently Learne', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(6, 'Leadership Excellence', '<p>Uphlds values and ethics. Attractsm motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(7, 'Adaptability', '<p>The ability to adopt to the cultire and environment of the scool community. Able to adjustto changes and exhibits a positive attitude towards different situations.</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(8, 'Creativity and Innovation', '<p>The ability to provide reative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approcahes to identify opportunities, implements solutions, measure impact and serve customers.</p>\r\n', 'A Contributing Independently Learne', 10, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(9, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', 'C Contribution through Teams/Peers ', 20, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(10, 'Team Orientation', '<p>Able to effeectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'C Contribution through Teams/Peers ', 20, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(11, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firlm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(12, 'Leadership Excellence', '<p>Uphlds values and ethics. Attractsm motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(13, 'Adaptability', '<p>The ability to adopt to the cultire and environment of the scool community. Able to adjustto changes and exhibits a positive attitude towards different situations.</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(14, 'Creativity and Innovation', '<p>The ability to provide reative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approcahes to identify opportunities, implements solutions, measure impact and serve customers.</p>\r\n', 'A Contributing Independently Learne', 10, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(15, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', 'C Contribution through Teams/Peers ', 20, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(16, 'Team Orientation', '<p>Able to effeectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'C Contribution through Teams/Peers ', 20, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(17, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firlm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(18, 'Leadership Excellence', '<p>Uphlds values and ethics. Attractsm motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(19, 'Adaptability', '<p>The ability to adopt to the cultire and environment of the scool community. Able to adjustto changes and exhibits a positive attitude towards different situations.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(20, 'Creativity and Innovation', '<p>The ability to provide reative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approcahes to identify opportunities, implements solutions, measure impact and serve customers.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(21, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', 'B Contributing with Expertise Speci', 20, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(22, 'Team Orientation', '<p>Able to effeectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'D Contributing through Strategy Bui', 20, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(23, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firlm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(24, 'Leadership Excellence', '<p>Uphlds values and ethics. Attractsm motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(25, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key strakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, financial, and environmental resources.</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(26, 'Adaptability', '<p>The ability to adopt to the cultire and environment of the scool community. Able to adjustto changes and exhibits a positive attitude towards different situations.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(27, 'Creativity and Innovation', '<p>The ability to provide reative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approcahes to identify opportunities, implements solutions, measure impact and serve customers.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(28, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', 'B Contributing with Expertise Speci', 20, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(29, 'Team Orientation', '<p>Able to effeectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'D Contributing through Strategy Bui', 20, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(30, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firlm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(31, 'Leadership Excellence', '<p>Uphlds values and ethics. Attractsm motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(32, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key strakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, financial, and environmental resources.</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(33, 'Adaptability', '<p>The ability to adopt to the cultire and environment of the scool community. Able to adjustto changes and exhibits a positive attitude towards different situations.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(34, 'Creativity and Innovation', '<p>The ability to provide reative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approcahes to identify opportunities, implements solutions, measure impact and serve customers.</p>\r\n', 'B Contributing with Expertise Speci', 10, 2, 0.2, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(35, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', 'A Contributing Independently Learne', 20, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(36, 'Team Orientation', '<p>Able to effeectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'B Contributing with Expertise Speci', 20, 2, 0.2, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(37, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firlm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'A Contributing Independently Learne', 10, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(38, 'Leadership Excellence', '<p>Uphlds values and ethics. Attractsm motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(39, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key strakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, financial, and environmental resources.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(40, 'People Development', '<p>The ability to plan and support the development of employeesÂ skills and abilities so they can fulfill current role and achieve full potential. Actively identifies new areas for learning, regularly creating and taking advantage of learning opportunities.</p>\r\n', 'A Contributing Independently Learne', 10, 4, 0.4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(41, 'Adaptability', '<p>The ability to adopt to the cultire and environment of the scool community. Able to adjustto changes and exhibits a positive attitude towards different situations.</p>\r\n', 'A Contributing Independently Learne', 10, 2, 0.2, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(42, 'Creativity and Innovation', '<p>The ability to provide reative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approcahes to identify opportunities, implements solutions, measure impact and serve customers.</p>\r\n', 'D Contributing through Strategy Bui', 10, 4, 0.4, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(43, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', 'A Contributing Independently Learne', 20, 3, 0.3, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(44, 'Team Orientation', '<p>Able to effeectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'C Contribution through Teams/Peers ', 20, 3, 0.3, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(45, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firlm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'A Contributing Independently Learne', 10, 3, 0.3, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(46, 'Leadership Excellence', '<p>Uphlds values and ethics. Attractsm motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(47, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key strakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, financial, and environmental resources.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(48, 'People Development', '<p>The ability to plan and support the development of employeesÂ skills and abilities so they can fulfill current role and achieve full potential. Actively identifies new areas for learning, regularly creating and taking advantage of learning opportunities.</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(49, 'Adaptability', '<p>The ability to adopt to the cultire and environment of the scool community. Able to adjustto changes and exhibits a positive attitude towards different situations.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 4, 0.4, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(50, 'Creativity and Innovation', '<p>The ability to provide reative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approcahes to identify opportunities, implements solutions, measure impact and serve customers.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 4, 0.4, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(51, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', 'B Contributing with Expertise Speci', 20, 4, 0.4, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(52, 'Team Orientation', '<p>Able to effeectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'B Contributing with Expertise Speci', 20, 4, 0.4, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(53, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firlm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 4, 0.4, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(54, 'Leadership Excellence', '<p>Uphlds values and ethics. Attractsm motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential</p>\r\n', 'C Contribution through Teams/Peers ', 10, 4, 0.4, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(55, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key strakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, financial, and environmental resources.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 4, 0.4, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(56, 'People Development', '<p>The ability to plan and support the development of employeesÂ skills and abilities so they can fulfill current role and achieve full potential. Actively identifies new areas for learning, regularly creating and taking advantage of learning opportunities.</p>\r\n', 'A Contributing Independently Learne', 10, 4, 0.4, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(57, 'Adaptability', '<p>The ability to adopt to the cultire and environment of the scool community. Able to adjustto changes and exhibits a positive attitude towards different situations.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 1, 0.1, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(58, 'Creativity and Innovation', '<p>The ability to provide reative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approcahes to identify opportunities, implements solutions, measure impact and serve customers.</p>\r\n', 'D Contributing through Strategy Bui', 10, 1, 0.1, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(59, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', 'B Contributing with Expertise Speci', 20, 1, 0.1, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(60, 'Team Orientation', '<p>Able to effeectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', 'C Contribution through Teams/Peers ', 20, 1, 0.1, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(61, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firlm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', 'D Contributing through Strategy Bui', 10, 1, 0.1, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(62, 'Leadership Excellence', '<p>Uphlds values and ethics. Attractsm motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential</p>\r\n', 'B Contributing with Expertise Speci', 10, 1, 0.1, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(63, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key strakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, financial, and environmental resources.</p>\r\n', 'D Contributing through Strategy Bui', 10, 1, 0.1, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(64, 'People Development', '<p>The ability to plan and support the development of employeesÂ skills and abilities so they can fulfill current role and achieve full potential. Actively identifies new areas for learning, regularly creating and taking advantage of learning opportunities.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 1, 0.1, 'EMP-002', 'SOG-003', '12/2017-02/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(30) NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2b`
--

INSERT INTO `tbl_ans_eformp2b` (`id`, `competency`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'Creativity and Innovation', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'Customer Focus', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(4, 'Team Orientation', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(5, 'Technical Knowledge', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(6, 'Leadership Excellence', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(7, 'Management Excellence', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(8, 'People Development', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(9, 'Adaptability', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(10, 'Creativity and Innovation', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(11, 'Customer Focus', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(12, 'Team Orientation', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(13, 'Technical Knowledge', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(14, 'Leadership Excellence', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(15, 'Management Excellence', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(16, 'People Development', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(17, 'Adaptability', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(18, 'Creativity and Innovation', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(19, 'Customer Focus', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(20, 'Team Orientation', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(21, 'Technical Knowledge', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(22, 'Leadership Excellence', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(23, 'Management Excellence', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(24, 'People Development', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(25, 'Adaptability', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(26, 'Creativity and Innovation', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(27, 'Customer Focus', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(28, 'Team Orientation', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(29, 'Technical Knowledge', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(30, 'Leadership Excellence', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(31, 'Management Excellence', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(32, 'People Development', 'EMP-002', 'SOG-003', '12/2017-02/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2b_critical_incident`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2b_critical_incident` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(30) NOT NULL,
  `criticalIncident` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2b_critical_incident`
--

INSERT INTO `tbl_ans_eformp2b_critical_incident` (`id`, `competencyName`, `criticalIncident`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', 'Critical Incident #1:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'People Development', 'Critical Incident #1:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'People Development', 'Critical Incident #2:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'People Development', 'Critical Incident #3:', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'Adaptability', 'Critical Incident #1:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'People Development', 'Critical Incident #1:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'People Development', 'Critical Incident #2:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'People Development', 'Critical Incident #3:', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'Adaptability', 'Critical Incident #1:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'People Development', 'Critical Incident #1:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'People Development', 'Critical Incident #2:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'People Development', 'Critical Incident #3:', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'Adaptability', 'Critical Incident #1:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'Adaptability', 'Critical Incident #2:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'Adaptability', 'Critical Incident #3:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(1, 'Customer Focus', 'Critical Incident #1:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'Customer Focus', 'Critical Incident #2:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'Customer Focus', 'Critical Incident #3:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(1, 'Team Orientation', 'Critical Incident #1:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'Team Orientation', 'Critical Incident #2:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'Team Orientation', 'Critical Incident #3:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(1, 'Management Excellence', 'Critical Incident #1:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'Management Excellence', 'Critical Incident #2:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'Management Excellence', 'Critical Incident #3:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(1, 'People Development', 'Critical Incident #1:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'People Development', 'Critical Incident #2:', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'People Development', 'Critical Incident #3:', 'EMP-002', 'SOG-003', '12/2017-02/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL,
  `score` int(11) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp3`
--

INSERT INTO `tbl_ans_eformp3` (`id`, `areas`, `score`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'DISCIPLINE', 3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'ATTENDANCE', 3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'PUNCTUALITY', 3, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(4, 'completion of training', 4, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(5, 'DISCIPLINE', 3, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(6, 'ATTENDANCE', 3, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(7, 'PUNCTUALITY', 3, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(8, 'completion of training', 4, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(9, 'DISCIPLINE', 4, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(10, 'ATTENDANCE', 4, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(11, 'PUNCTUALITY', 4, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(12, 'completion of training', 4, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(13, 'DISCIPLINE', 1, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(14, 'ATTENDANCE', 1, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(15, 'PUNCTUALITY', 1, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(16, 'completion of training', 1, 'EMP-002', 'SOG-003', '12/2017-02/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp3_rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp3_rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` varchar(200) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp3_rubric`
--

INSERT INTO `tbl_ans_eformp3_rubric` (`id`, `areaName`, `rubricDesc`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(4, 'ATTENDANCE', 'Perfect Attendance, OR has not exceeded the total leave credits entitlement.', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'ATTENDANCE', 'Has received one (1) disciplinary action related to attendance', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'ATTENDANCE', 'Habitually absent', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(4, 'PUNCTUALITY', 'Has never been late and no occurence of undertime', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime.', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'PUNCTUALITY', 'has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(4, 'completion of training', 'Has successfully completed all the required trainings/seminars.', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'completion of training', 'Has failed to complete one (1) traning/seminar due to justifiable reason.', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'completion of training', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'completion of training', 'Has failed to complete any seminar without justifiable cause.', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(4, 'ATTENDANCE', 'Perfect Attendance, OR has not exceeded the total leave credits entitlement.', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'ATTENDANCE', 'Has received one (1) disciplinary action related to attendance', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'ATTENDANCE', 'Habitually absent', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(4, 'PUNCTUALITY', 'Has never been late and no occurence of undertime', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime.', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'PUNCTUALITY', 'has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(4, 'completion of training', 'Has successfully completed all the required trainings/seminars.', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'completion of training', 'Has failed to complete one (1) traning/seminar due to justifiable reason.', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'completion of training', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'completion of training', 'Has failed to complete any seminar without justifiable cause.', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(4, 'ATTENDANCE', 'Perfect Attendance, OR has not exceeded the total leave credits entitlement.', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'ATTENDANCE', 'Has received one (1) disciplinary action related to attendance', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'ATTENDANCE', 'Habitually absent', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(4, 'PUNCTUALITY', 'Has never been late and no occurence of undertime', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime.', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'PUNCTUALITY', 'has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(4, 'completion of training', 'Has successfully completed all the required trainings/seminars.', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'completion of training', 'Has failed to complete one (1) traning/seminar due to justifiable reason.', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'completion of training', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'completion of training', 'Has failed to complete any seminar without justifiable cause.', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(4, 'ATTENDANCE', 'Perfect Attendance, OR has not exceeded the total leave credits entitlement.', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'ATTENDANCE', 'Has received one (1) disciplinary action related to attendance', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(1, 'ATTENDANCE', 'Habitually absent', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(4, 'PUNCTUALITY', 'Has never been late and no occurence of undertime', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime.', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(1, 'PUNCTUALITY', 'has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(4, 'completion of training', 'Has successfully completed all the required trainings/seminars.', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'completion of training', 'Has failed to complete one (1) traning/seminar due to justifiable reason.', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'completion of training', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(1, 'completion of training', 'Has failed to complete any seminar without justifiable cause.', 'EMP-002', 'SOG-003', '12/2017-02/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp4` (
  `id` int(11) NOT NULL,
  `p4_area` varchar(50) NOT NULL,
  `customerFeedback` text NOT NULL,
  `p4_score` int(11) NOT NULL,
  `p4_rating` varchar(11) NOT NULL,
  `answeredBy` varchar(80) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp4`
--

INSERT INTO `tbl_ans_eformp4` (`id`, `p4_area`, `customerFeedback`, `p4_score`, `p4_rating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'April - June', '', 0, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'July - September', '', 0, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'October-December', '', 0, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(4, 'January-March', '', 0, '', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(5, 'April - June', '', 0, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(6, 'July - September', '', 0, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(7, 'October-December', '', 0, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(8, 'January-March', '', 0, '', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(9, 'April - June', '', 0, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(10, 'July - September', '', 0, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(11, 'October-December', '', 0, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(12, 'January-March', '', 0, '', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(13, 'April - June', '', 0, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(14, 'July - September', '', 0, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(15, 'October-December', '', 0, '', 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(16, 'January-March', '', 0, '', 'EMP-002', 'SOG-003', '12/2017-02/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_summary`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_summary` (
  `id` int(11) NOT NULL,
  `performance_rating` varchar(60) NOT NULL,
  `total_weighted_rating` float NOT NULL,
  `percentage_allocation` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_summary`
--

INSERT INTO `tbl_ans_summary` (`id`, `performance_rating`, `total_weighted_rating`, `percentage_allocation`, `subtotal`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 2.75, 40, 1.1, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, 'PART 2: COMPETENCIES', 2.5, 30, 3.9, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(3, 'PART 3: PROFESSIONALISM', 13, 30, 0.75, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(4, '0', 0, 0, 0, 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 2.75, 40, 1.1, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(2, 'PART 2: COMPETENCIES', 2.5, 30, 3.9, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, 'PART 3: PROFESSIONALISM', 13, 30, 0.75, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(4, '0', 0, 0, 0, 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 4, 40, 1.6, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(2, 'PART 2: COMPETENCIES', 3.2, 30, 4.8, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(3, 'PART 3: PROFESSIONALISM', 16, 30, 0.96, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(4, '0', 0, 0, 0, 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 1, 40, 0.4, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(2, 'PART 2: COMPETENCIES', 0.8, 30, 1.2, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(3, 'PART 3: PROFESSIONALISM', 4, 30, 0.24, 'EMP-002', 'SOG-003', '12/2017-02/2018'),
(4, '0', 0, 0, 0, 'EMP-002', 'SOG-003', '12/2017-02/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_summary_comments_acknowledgement`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_summary_comments_acknowledgement` (
  `id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `recommendations` text NOT NULL,
  `approval` text NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_summary_comments_acknowledgement`
--

INSERT INTO `tbl_ans_summary_comments_acknowledgement` (`id`, `comments`, `recommendations`, `approval`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, '<p>comments for kobe</p>', '<p>recommendations for kobe</p>', 'This is to Certify that Kobe B Bryant confirms this Evaluation for this Year.', 'EMP-001', 'SOG-001', '12/2017-02/2018'),
(2, '<p>comments for kd</p>', '<p>recommendations for kd</p>', 'This is to Certify that Kevin KD Durant confirms this Evaluation for this Year.', 'EMP-001', 'SOG-002', '12/2017-02/2018'),
(3, '<p>comments for curry</p>', '<p>recommendations for curry</p>', 'This is to Certify that Stephen Klay Curry confirms this Evaluation for this Year.', 'EMP-002', 'SOG-004', '12/2017-02/2018'),
(4, '<p>comments for brook</p>', '<p>recommendations for brook</p>', 'This is to Certify that Russel M Westbrook confirms this Evaluation for this Year.', 'EMP-002', 'SOG-003', '12/2017-02/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_competencyratingscale`
--

CREATE TABLE IF NOT EXISTS `tbl_competencyratingscale` (
  `id` int(11) NOT NULL,
  `competency_rating_value` varchar(100) NOT NULL,
  `competency_title` varchar(100) NOT NULL,
  `competency_description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_competencyratingscale`
--

INSERT INTO `tbl_competencyratingscale` (`id`, `competency_rating_value`, `competency_title`, `competency_description`) VALUES
(1, '4', 'Expert/Exemplary', '(Evaluation; Defines Framework Modes; Mentor, Self - Actualization, Consistently exceeds all of the '),
(2, '3', 'Advanced/Accomplished', '(Analysis; Synthesis, Understands Framework Models, Exceeds most competency level descriptors)'),
(3, '2', 'Intermediate/Developing', '(Unsupervised Application; Beginning Analysis, Meets most of the required competency level descripto'),
(4, '1', 'Beginner', '(Comprehension, Beginning Application, Some Experience, Meets some of the competency level descripto'),
(5, '0', 'NE', 'Early Stages of application of the item; Behavior not yet present'),
(6, '0', 'NA', 'The item does not apply to your position or performance of assigned tasks and responsibilities.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dateevaluation`
--

CREATE TABLE IF NOT EXISTS `tbl_dateevaluation` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dateevaluation`
--

INSERT INTO `tbl_dateevaluation` (`startDate`, `endDate`) VALUES
('11/2017', '12/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_datekpi`
--

CREATE TABLE IF NOT EXISTS `tbl_datekpi` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_datekpi`
--

INSERT INTO `tbl_datekpi` (`startDate`, `endDate`) VALUES
('11/2017', '12/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departments`
--

CREATE TABLE IF NOT EXISTS `tbl_departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `initials` varchar(100) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL DEFAULT 'PENDING',
  `departmentOwner` varchar(100) NOT NULL,
  `monitor` varchar(6) NOT NULL DEFAULT 'unread'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_departments`
--

INSERT INTO `tbl_departments` (`id`, `department_name`, `initials`, `kpiStatus`, `departmentOwner`, `monitor`) VALUES
(1, 'College of Information Technology Education', 'CITE', 'APPROVED', 'EMP-001', 'read'),
(2, 'College of Management and Accountancy', 'CMA', 'APPROVED', 'EMP-002', 'read');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp1` (
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeight` int(11) NOT NULL,
  `kpiOwner` varchar(60) NOT NULL,
  `kpiDateCreated` varchar(11) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp1`
--

INSERT INTO `tbl_eformp1` (`kraID`, `kpiTitle`, `kpiWeight`, `kpiOwner`, `kpiDateCreated`, `kpiStatus`) VALUES
(1, 'COMMUNICATION AND COORDINATION', 0, 'EMP-001', '', 'APPROVED'),
(2, 'OFFICE MANAGEMENT', 0, 'EMP-001', '', 'APPROVED'),
(3, 'PREPARATION OF REPORTS', 0, 'EMP-001', '', 'APPROVED'),
(4, 'CUSTOMER SERVICE', 0, 'EMP-001', '', 'APPROVED'),
(5, 'COMMUNICATION & COORDINATION', 0, 'EMP-002', '', 'APPROVED'),
(6, 'OFFICE MANAGEMENT', 0, 'EMP-002', '', 'APPROVED'),
(7, 'PREPARATION OF REPORTS', 0, 'EMP-002', '', 'APPROVED'),
(8, 'CUSTOMER SERVICE', 0, 'EMP-002', '', 'APPROVED');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` text NOT NULL,
  `competencyDesc` text NOT NULL,
  `requiredProfLevel` varchar(50) NOT NULL,
  `weights` float NOT NULL,
  `rating` int(2) NOT NULL,
  `weightedRating` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2a`
--

INSERT INTO `tbl_eformp2a` (`id`, `competency`, `competencyDesc`, `requiredProfLevel`, `weights`, `rating`, `weightedRating`) VALUES
(1, 'Adaptability', '<p>The ability to adopt to the cultire and environment of the scool community. Able to adjustto changes and exhibits a positive attitude towards different situations.</p>\r\n', '', 10, 0, 0),
(2, 'Creativity and Innovation', '<p>The ability to provide reative or breakthrough solutions, improve existing conditions and processes using appropriate methods and approcahes to identify opportunities, implements solutions, measure impact and serve customers.</p>\r\n', '', 10, 0, 0),
(3, 'Customer Focus', '<p>The ability to focus efforts on determining, listening and meeting internal and external customer needs and develop mutually beneficial relationships with customers and business partners.</p>\r\n', '', 20, 0, 0),
(4, 'Team Orientation', '<p>Able to effeectively work, build mutual trust and respect and attain objectives in group settings. Promotes teamwork by individual understanding roles, accepting shared responsibility, define deliverables, share information and expertise, communicating and building consensus to achieve common goals.</p>\r\n', '', 20, 0, 0),
(5, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firlm and to his/her respective profession. Demonstrates a commitment to learning by proactively seeking opportunities to develop new capabilities, skills, and knowledge.</p>\r\n', '', 10, 0, 0),
(6, 'Leadership Excellence', '<p>Uphlds values and ethics. Attractsm motivates and inspires people to maximize performance, achieve organizational objectives and help realize full potential</p>\r\n', '', 10, 0, 0),
(7, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key strakeholders. Designs and executes plan to allow people to achieve results and maximizing effectiveness and sustainability of human, financial, and environmental resources.</p>\r\n', '', 10, 0, 0),
(8, 'People Development', '<p>The ability to plan and support the development of employees&nbsp;skills and abilities so they can fulfill current role and achieve full potential. Actively identifies new areas for learning, regularly creating and taking advantage of learning opportunities.</p>\r\n', '', 10, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2b`
--

INSERT INTO `tbl_eformp2b` (`id`, `competency`) VALUES
(1, 'Adaptability'),
(2, 'Creativity and Innovation'),
(3, 'Customer Focus'),
(4, 'Team Orientation'),
(5, 'Technical Knowledge'),
(6, 'Leadership Excellence'),
(7, 'Management Excellence'),
(8, 'People Development');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp3`
--

INSERT INTO `tbl_eformp3` (`id`, `areas`) VALUES
(1, 'DISCIPLINE'),
(2, 'ATTENDANCE'),
(3, 'PUNCTUALITY'),
(4, 'COMPLETION OF TRAINING');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp4` (
  `id` int(11) NOT NULL,
  `areas` varchar(30) NOT NULL,
  `customerFeedback` varchar(100) NOT NULL,
  `respondentScore` int(2) NOT NULL,
  `ratingFactor` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp4`
--

INSERT INTO `tbl_eformp4` (`id`, `areas`, `customerFeedback`, `respondentScore`, `ratingFactor`) VALUES
(1, 'April - June', '', 0, 0),
(2, 'July - September', '', 0, 0),
(3, 'October-December', '', 0, 0),
(4, 'January-March', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation_requests`
--

CREATE TABLE IF NOT EXISTS `tbl_evaluation_requests` (
  `id` int(11) NOT NULL,
  `admin_id` varchar(60) NOT NULL,
  `employee_missed` varchar(60) NOT NULL,
  `employee_department` varchar(60) NOT NULL,
  `reason` text NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `request_status` varchar(30) NOT NULL DEFAULT 'SENT'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation_results`
--

CREATE TABLE IF NOT EXISTS `tbl_evaluation_results` (
  `id` int(11) NOT NULL,
  `emp_id` varchar(60) NOT NULL,
  `employee_firstName` varchar(60) NOT NULL,
  `employee_middleName` varchar(60) NOT NULL,
  `employee_lastName` varchar(60) NOT NULL,
  `employee_department` varchar(60) NOT NULL,
  `employee_position` varchar(30) NOT NULL,
  `employee_job_level` varchar(30) NOT NULL,
  `employee_years_company` int(11) NOT NULL,
  `employee_years_position` int(11) NOT NULL,
  `evaluationStatus` varchar(30) NOT NULL DEFAULT 'NOT YET STARTED',
  `performance_rating` varchar(100) NOT NULL,
  `overall_rating` varchar(100) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `ev_sog_feedback` varchar(15) NOT NULL DEFAULT 'NO RESPONSE YET',
  `late_status` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_evaluation_results`
--

INSERT INTO `tbl_evaluation_results` (`id`, `emp_id`, `employee_firstName`, `employee_middleName`, `employee_lastName`, `employee_department`, `employee_position`, `employee_job_level`, `employee_years_company`, `employee_years_position`, `evaluationStatus`, `performance_rating`, `overall_rating`, `answeredBy`, `performanceCycle`, `ev_sog_feedback`, `late_status`) VALUES
(1, 'SOG-001', 'Kobe', 'B', 'Bryant', 'College of Information Technology Education', 'Secretary', 'S1', 46, 46, 'NOT YET STARTED', '', '', 'superadmin', '-', 'NO RESPONSE YET', 'YES'),
(2, 'SOG-002', 'Kevin', 'KD', 'Durant', 'College of Information Technology Education', 'Sub-Secretary', 'S2', 17, 17, 'NOT YET STARTED', '', '', 'superadmin', '-', 'NO RESPONSE YET', 'YES'),
(3, 'SOG-003', 'Russel', 'M', 'Westbrook', 'College of Management and Accountancy', 'Secretary', 'S1', 15, 15, 'NOT YET STARTED', '', '', 'superadmin', '-', 'NO RESPONSE YET', 'YES'),
(4, 'SOG-004', 'Stephen', 'Klay', 'Curry', 'College of Information Technology Education', 'Treasurer', 'T1', 17, 17, 'NOT YET STARTED', '', '', 'superadmin', '-', 'NO RESPONSE YET', 'YES'),
(5, 'SOG-001', 'Kobe', 'B', 'Bryant', 'College of Information Technology Education', 'Secretary', 'S1', 46, 46, 'NOT YET STARTED', '', '', 'superadmin', '12/2017-01/2018', 'NO RESPONSE YET', 'YES'),
(6, 'SOG-002', 'Kevin', 'KD', 'Durant', 'College of Information Technology Education', 'Sub-Secretary', 'S2', 17, 17, 'NOT YET STARTED', '', '', 'superadmin', '12/2017-01/2018', 'NO RESPONSE YET', 'YES'),
(7, 'SOG-003', 'Russel', 'M', 'Westbrook', 'College of Management and Accountancy', 'Secretary', 'S1', 15, 15, 'NOT YET STARTED', '', '', 'superadmin', '12/2017-01/2018', 'NO RESPONSE YET', 'YES'),
(8, 'SOG-004', 'Stephen', 'Klay', 'Curry', 'College of Information Technology Education', 'Treasurer', 'T1', 17, 17, 'NOT YET STARTED', '', '', 'superadmin', '12/2017-01/2018', 'NO RESPONSE YET', 'YES'),
(9, 'SOG-001', 'Kobe', 'B', 'Bryant', 'College of Information Technology Education', 'Secretary', 'S1', 46, 46, 'NOT YET STARTED', '', '', 'superadmin', '11/2017-12/2017', 'NO RESPONSE YET', 'YES'),
(10, 'SOG-002', 'Kevin', 'KD', 'Durant', 'College of Information Technology Education', 'Sub-Secretary', 'S2', 17, 17, 'NOT YET STARTED', '', '', 'superadmin', '11/2017-12/2017', 'NO RESPONSE YET', 'YES'),
(11, 'SOG-003', 'Russel', 'M', 'Westbrook', 'College of Management and Accountancy', 'Secretary', 'S1', 15, 15, 'NOT YET STARTED', '', '', 'superadmin', '11/2017-12/2017', 'NO RESPONSE YET', 'YES'),
(12, 'SOG-004', 'Stephen', 'Klay', 'Curry', 'College of Information Technology Education', 'Treasurer', 'T1', 17, 17, 'NOT YET STARTED', '', '', 'superadmin', '11/2017-12/2017', 'NO RESPONSE YET', 'YES'),
(13, 'SOG-001', 'Kobe', 'B', 'Bryant', 'College of Information Technology Education', 'Secretary', 'S1', 46, 46, 'COMPLETED', '5.75', '5.75', 'superadmin', '12/2017-02/2018', 'NO RESPONSE YET', ''),
(14, 'SOG-002', 'Kevin', 'KD', 'Durant', 'College of Information Technology Education', 'Sub-Secretary', 'S2', 17, 17, 'COMPLETED', '5.75', '5.75', 'superadmin', '12/2017-02/2018', 'NO RESPONSE YET', ''),
(15, 'SOG-003', 'Russel', 'M', 'Westbrook', 'College of Management and Accountancy', 'Secretary', 'S1', 15, 15, 'COMPLETED', '1.84', '1.84', 'superadmin', '12/2017-02/2018', 'NO RESPONSE YET', ''),
(16, 'SOG-004', 'Stephen', 'Klay', 'Curry', 'College of Information Technology Education', 'Treasurer', 'T1', 17, 17, 'COMPLETED', '7.36', '7.36', 'superadmin', '12/2017-02/2018', 'NO RESPONSE YET', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kpiratingscale`
--

CREATE TABLE IF NOT EXISTS `tbl_kpiratingscale` (
  `id` int(11) NOT NULL,
  `rating_value` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `weighted_rating` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kpiratingscale`
--

INSERT INTO `tbl_kpiratingscale` (`id`, `rating_value`, `description`, `weighted_rating`) VALUES
(1, '4', 'Exceeds Expectations (Performs above standards all the time)', ''),
(2, '3', 'Meets Expectations (Solid, consistent performance; Generally meets standards)', ''),
(3, '2', 'Needs Improvement (Frequently fails to meet standards)', ''),
(4, '1', 'Unacceptable (Performance is below job requirements and needs)', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE IF NOT EXISTS `tbl_messages` (
  `id` int(11) NOT NULL,
  `subject` text NOT NULL,
  `content` text NOT NULL,
  `sender` varchar(50) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'unread',
  `checked` varchar(3) NOT NULL DEFAULT 'no',
  `sent_at` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `deleted_by_sender` varchar(100) NOT NULL DEFAULT 'no',
  `deleted_by_receiver` varchar(100) NOT NULL DEFAULT 'no',
  `trash_by_HR` varchar(100) NOT NULL DEFAULT 'no',
  `trash_by_Department` varchar(100) NOT NULL DEFAULT 'no',
  `revision` varchar(100) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `id_appeal` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_messages`
--

INSERT INTO `tbl_messages` (`id`, `subject`, `content`, `sender`, `receiver`, `status`, `checked`, `sent_at`, `location`, `deleted_by_sender`, `deleted_by_receiver`, `trash_by_HR`, `trash_by_Department`, `revision`, `performanceCycle`, `id_appeal`) VALUES
(1, 'JUSTIFY EVALUATION FOR Kobe Bryant', 'Please justify your ratings for Kobe Bryant', 'Human Resource Department', 'College of Information Technology Education', 'unread', 'no', 'November 24, 2017 5:01:pm  ', 'sentbox', 'no', 'no', 'no', 'no', 'YES', '', ''),
(2, 'JUSTIFY EVALUATION FOR Kobe Bryant', 'im sorry for repeated data due to system error', 'College of Information Technology Education', 'Human Resource Department', 'unread', 'no', 'November 24, 2017 5:02:pm  ', 'sentbox', 'no', 'no', 'no', 'no', 'YES', '', ''),
(3, 'JUSTIFY EVALUATION FOR Kobe Bryant', 'okay', 'Human Resource Department', 'College of Information Technology Education', 'unread', 'no', '', 'sentbox', 'no', 'no', 'no', 'no', 'YES', '', ''),
(4, 'JUSTIFY EVALUATION FOR Kevin Durant', 'Please justify your ratings for Kevin Durant', 'Human Resource Department', 'College of Information Technology Education', 'unread', 'no', 'November 24, 2017 5:05:pm  ', 'sentbox', 'no', 'no', 'no', 'no', 'YES', '', ''),
(5, 'JUSTIFY EVALUATION FOR Kevin Durant', 'yan na po yun', 'College of Information Technology Education', 'Human Resource Department', 'unread', 'no', 'November 24, 2017 5:05:pm  ', 'sentbox', 'no', 'no', 'no', 'no', 'YES', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE IF NOT EXISTS `tbl_notification` (
  `id` int(11) NOT NULL,
  `notification` text NOT NULL,
  `sendBy` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`id`, `notification`, `sendBy`, `status`, `timeStamp`) VALUES
(1, 'College of Information Technology Education Sent a KPI', 'EMP-001', 'read', '2017-11-24 08:23:19'),
(2, 'College of Management and Accountancy Sent a KPI', 'EMP-002', 'read', '2017-11-24 08:30:44'),
(3, 'College of Information Technology Education SENT AN EVALUATION', 'EMP-001', 'read', '2017-11-24 09:01:34'),
(4, 'College of Information Technology Education REPLIED TO YOUR MESSAGE', 'EMP-001', 'read', '2017-11-24 09:05:34'),
(5, 'College of Information Technology Education SENT AN EVALUATION', 'EMP-001', 'read', '2017-11-24 09:05:34'),
(6, 'College of Information Technology Education REPLIED TO YOUR MESSAGE', 'EMP-001', 'read', '2017-11-24 09:15:54'),
(7, 'College of Management and Accountancy SENT AN EVALUATION', 'EMP-002', 'read', '2017-11-24 09:15:54'),
(8, 'College of Management and Accountancy SENT AN EVALUATION', 'EMP-002', 'read', '2017-11-24 09:15:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification_user`
--

CREATE TABLE IF NOT EXISTS `tbl_notification_user` (
  `id` int(11) NOT NULL,
  `notification` text NOT NULL,
  `sendBy` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `owner` varchar(100) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification_user`
--

INSERT INTO `tbl_notification_user` (`id`, `notification`, `sendBy`, `status`, `owner`, `timeStamp`) VALUES
(1, 'HR APPROVED YOUR KRA', 'Human Resource Department', 'unread', 'College of Information Technology Education', '2017-11-24 08:24:03'),
(2, 'HR APPROVED YOUR KRA', 'Human Resource Department', 'unread', 'College of Management and Accountancy', '2017-11-24 08:32:36'),
(3, 'Human Resource Department sent a message', 'Human Resource Department', 'unread', 'College of Information Technology Education', '2017-11-24 09:02:41'),
(4, 'Human Resource Department sent a message', 'Human Resource Department', 'unread', 'College of Information Technology Education', '2017-11-24 09:05:50'),
(5, 'Kobe B Bryant', 'Human Resource Department', 'unread', 'College of Information Technology Education', '2017-11-24 09:08:26'),
(6, 'Kevin KD Durant', 'Human Resource Department', 'unread', 'College of Information Technology Education', '2017-11-24 09:08:35'),
(7, 'Stephen Klay Curry', 'Human Resource Department', 'unread', 'College of Information Technology Education', '2017-11-24 09:13:05'),
(8, 'Russel M Westbrook', 'Human Resource Department', 'unread', 'College of Management and Accountancy', '2017-11-24 09:13:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_p1kpi` (
  `kraID` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kpiDesc` text NOT NULL,
  `kpiOwner` varchar(100) NOT NULL,
  `kpiDate` varchar(11) NOT NULL,
  `kpiStatus` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p1kpi`
--

INSERT INTO `tbl_p1kpi` (`kraID`, `kpiID`, `kpiDesc`, `kpiOwner`, `kpiDate`, `kpiStatus`) VALUES
(1, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'EMP-001', '', 'APPROVED'),
(1, 2, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'EMP-001', '', 'APPROVED'),
(1, 3, 'Coordinate meetings and other activities within the college', 'EMP-001', '', 'APPROVED'),
(1, 4, 'Supervise the student assistants designated to the college', 'EMP-001', '', 'APPROVED'),
(2, 1, 'Organize office files and documents', 'EMP-001', '', 'APPROVED'),
(2, 2, 'Supervise order and cleanliness in the office', 'EMP-001', '', 'APPROVED'),
(2, 3, 'Regular inventory and update of office supplies', 'EMP-001', '', 'APPROVED'),
(3, 1, 'Monitor facultys attendance', 'EMP-001', '', 'APPROVED'),
(3, 2, 'Submit reports (e.g., Attendance, Completion of Grades)  required by other offices (e.g., HRD, Regis', 'EMP-001', '', 'APPROVED'),
(3, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'EMP-001', '', 'APPROVED'),
(4, 1, 'Answer telephone calls and/or take messages', 'EMP-001', '', 'APPROVED'),
(4, 2, 'Man the offices frontdesk to meet walk-in customers', 'EMP-001', '', 'APPROVED'),
(4, 3, 'Arrange customers appointments with the Dean or faculty', 'EMP-001', '', 'APPROVED'),
(4, 4, 'Handle data encoding duties during enrolment', 'EMP-001', '', 'APPROVED'),
(5, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'EMP-002', '', 'APPROVED'),
(5, 2, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'EMP-002', '', 'APPROVED'),
(5, 3, 'Coordinate meetings and other activities within the college', 'EMP-002', '', 'APPROVED'),
(5, 4, 'Supervise the student assistants designated to the college', 'EMP-002', '', 'APPROVED'),
(6, 1, 'Organize office files and documents', 'EMP-002', '', 'APPROVED'),
(6, 2, 'Supervise order and cleanliness in the office', 'EMP-002', '', 'APPROVED'),
(6, 3, 'Regular inventory and update of office supplies', 'EMP-002', '', 'APPROVED'),
(7, 1, 'Monitor facultys attendance 				', 'EMP-002', '', 'APPROVED'),
(7, 2, 'Submit reports (e.g., Attendance, Completion of Grades)  required by other offices (e.g., HRD, Regis', 'EMP-002', '', 'APPROVED'),
(7, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'EMP-002', '', 'APPROVED'),
(8, 1, 'Answer telephone calls and/or take messages', 'EMP-002', '', 'APPROVED'),
(8, 2, 'Man the offices frontdesk to meet walk-in customers ', 'EMP-002', '', 'APPROVED'),
(8, 3, 'Arrange customers appointments with the Dean or faculty', 'EMP-002', '', 'APPROVED'),
(8, 4, 'Handle data encoding duties during enrolment', 'EMP-002', '', 'APPROVED');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p2bcritinc`
--

CREATE TABLE IF NOT EXISTS `tbl_p2bcritinc` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(60) NOT NULL,
  `criticalIncident` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p2bcritinc`
--

INSERT INTO `tbl_p2bcritinc` (`id`, `competencyName`, `criticalIncident`) VALUES
(1, 'Adaptability', 'Critical Incident'),
(2, 'Adaptability', 'Critical Incident'),
(3, 'Adaptability', 'Critical Incident'),
(1, 'Creativity and Innovation', 'Critical Incident'),
(2, 'Creativity and Innovation', 'Critical Incident'),
(3, 'Creativity and Innovation', 'Critical Incident'),
(1, 'Customer Focus', 'Critical Incident'),
(2, 'Customer Focus', 'Critical Incident'),
(3, 'Customer Focus', 'Critical Incident'),
(1, 'Team Orientation', 'Critical Incident'),
(2, 'Team Orientation', 'Critical Incident'),
(3, 'Team Orientation', 'Critical Incident'),
(1, 'Technical Knowledge', 'Critical Incident'),
(2, 'Technical Knowledge', 'Critical Incident'),
(3, 'Technical Knowledge', 'Critical Incident'),
(1, 'Leadership Excellence', 'Critical Incident'),
(2, 'Leadership Excellence', 'Critical Incident'),
(3, 'Leadership Excellence', 'Critical Incident'),
(1, 'Management Excellence', 'Critical Incident'),
(2, 'Management Excellence', 'Critical Incident'),
(3, 'Management Excellence', 'Critical Incident'),
(1, 'People Development', 'Critical Incident'),
(2, 'People Development', 'Critical Incident'),
(3, 'People Development', 'Critical Incident');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p3rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_p3rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p3rubric`
--

INSERT INTO `tbl_p3rubric` (`id`, `areaName`, `rubricDesc`) VALUES
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality'),
(4, 'ATTENDANCE', 'Perfect Attendance, OR has not exceeded the total leave credits entitlement.'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits'),
(2, 'ATTENDANCE', 'Has received one (1) disciplinary action related to attendance'),
(1, 'ATTENDANCE', 'Habitually absent'),
(4, 'PUNCTUALITY', 'Has never been late and no occurence of undertime'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime.'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness'),
(1, 'PUNCTUALITY', 'has received a disciplinary action related to tardiness/undertime; OR habitually tardy'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required trainings/seminars.'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) traning/seminar due to justifiable reason.'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_proficiency_level`
--

CREATE TABLE IF NOT EXISTS `tbl_proficiency_level` (
  `id` int(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `initials` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_proficiency_level`
--

INSERT INTO `tbl_proficiency_level` (`id`, `value`, `initials`, `title`, `content`) VALUES
(1, '4', 'A', 'Contributing Independently', 'Learner-Doer. Individual contributor. Follower, team-player. Costumer touchpoint'),
(2, '3', 'B', 'Contributing with Expertise', 'Specialist. Expert implementor. Customer advocate. Problem solver.'),
(3, '2', 'C', 'Contribution through Teams/Peers', 'Project Leader. Idea Leader. Technical Troubleshooter. Coach'),
(4, '1', 'D', 'Contributing through Strategy Building', 'Navigator. Idea Innovator. Leader Developer. Mission Advocate'),
(5, '0', 'NE', '', 'Early stages of application of the item; Behavior not yet present'),
(6, '0', 'NA', '', 'The item does not apply to your position or performance of assigned tasks and responsibilities.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating_execute`
--

CREATE TABLE IF NOT EXISTS `tbl_rating_execute` (
  `ID` int(11) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `performance_rating` varchar(100) NOT NULL,
  `overall_rating` varchar(100) NOT NULL,
  `performanceCycle` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rating_execute`
--

INSERT INTO `tbl_rating_execute` (`ID`, `emp_id`, `performance_rating`, `overall_rating`, `performanceCycle`) VALUES
(1, 'SOG-001', '5.75', '5.75', '12/2017-02/2018'),
(2, 'SOG-002', '5.75', '5.75', '12/2017-02/2018'),
(3, 'SOG-004', '7.36', '7.36', '12/2017-02/2018'),
(4, 'SOG-003', '1.84', '1.84', '12/2017-02/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp2a` (
  `criticalIncidentCount` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp2a`
--

INSERT INTO `tbl_settingsp2a` (`criticalIncidentCount`) VALUES
(3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp3`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp3` (
  `rubricCount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp3`
--

INSERT INTO `tbl_settingsp3` (`rubricCount`) VALUES
(4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sog_employee`
--

CREATE TABLE IF NOT EXISTS `tbl_sog_employee` (
  `emp_id` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `department_head` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_temp` varchar(100) NOT NULL,
  `email_employee` text NOT NULL,
  `paths` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sog_employee`
--

INSERT INTO `tbl_sog_employee` (`emp_id`, `firstname`, `middlename`, `lastname`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `department_head`, `username`, `password`, `password_temp`, `email_employee`, `paths`) VALUES
('SOG-001', 'Kobe', 'B', 'Bryant', 'College of Information Technology Education', '46', 'Secretary', 'S1', '1971', '46', '', 'SOG-001', 'cf0b1b9b11f21f326651ffe527b8605f', 'BRYANT', 'blackmamba@gmail.com', '../assets/uploaded_files/accounts/2017_0601_194156_006.jpg'),
('SOG-002', 'Kevin', 'KD', 'Durant', 'College of Information Technology Education', '17', 'Sub-Secretary', 'S2', '2000', '17', '', 'SOG-002', '3be381b2cbb9a71def4d731feb064be6', 'DURANT', 'kevindurant@gmail.com', '../assets/uploaded_files/accounts/default.png'),
('SOG-003', 'Russel', 'M', 'Westbrook', 'College of Management and Accountancy', '15', 'Secretary', 'S1', '2002', '15', '', 'SOG-003', '61fed7f96b386eac816e7c541192c13f', 'WESTBROOK', 'rbrokk@gmail.com', '../assets/uploaded_files/accounts/default.png'),
('SOG-004', 'Stephen', 'Klay', 'Curry', 'College of Management and Accountancy', '17', 'Treasurer', 'T1', '2000', '17', '', 'SOG-004', '278c43db2086fac59edc2f233b4097e4', 'CURRY', 'curry@gmail.com', '../assets/uploaded_files/accounts/2017_0602_001517_015.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp2a`
--
ALTER TABLE `tbl_ans_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp2b`
--
ALTER TABLE `tbl_ans_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp3`
--
ALTER TABLE `tbl_ans_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp4`
--
ALTER TABLE `tbl_ans_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_summary_comments_acknowledgement`
--
ALTER TABLE `tbl_ans_summary_comments_acknowledgement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_competencyratingscale`
--
ALTER TABLE `tbl_competencyratingscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp1`
--
ALTER TABLE `tbl_eformp1`
  ADD PRIMARY KEY (`kraID`);

--
-- Indexes for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_evaluation_requests`
--
ALTER TABLE `tbl_evaluation_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_evaluation_results`
--
ALTER TABLE `tbl_evaluation_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kpiratingscale`
--
ALTER TABLE `tbl_kpiratingscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification_user`
--
ALTER TABLE `tbl_notification_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_rating_execute`
--
ALTER TABLE `tbl_rating_execute`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_sog_employee`
--
ALTER TABLE `tbl_sog_employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=127;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=127;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp2a`
--
ALTER TABLE `tbl_ans_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp2b`
--
ALTER TABLE `tbl_ans_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp3`
--
ALTER TABLE `tbl_ans_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp4`
--
ALTER TABLE `tbl_ans_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_ans_summary_comments_acknowledgement`
--
ALTER TABLE `tbl_ans_summary_comments_acknowledgement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_competencyratingscale`
--
ALTER TABLE `tbl_competencyratingscale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_evaluation_requests`
--
ALTER TABLE `tbl_evaluation_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_evaluation_results`
--
ALTER TABLE `tbl_evaluation_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_kpiratingscale`
--
ALTER TABLE `tbl_kpiratingscale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_notification_user`
--
ALTER TABLE `tbl_notification_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_rating_execute`
--
ALTER TABLE `tbl_rating_execute`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
