<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 10/3/2017
 * Time: 9:36 PM
 */
?>

<script type="text/javascript">
    $('#weights2<?php echo $fCount;?>').on('focusin', function(){
        console.log("Saving value " + $(this).val());
        $(this).data('val', $(this).val());
    });


    $('#weights2<?php echo $fCount;?>').on('change', function(){
        var prev = $(this).data('val');
        var rem = document.getElementById('remaining').value;

        var value1 = document.getElementById('weights2<?php echo $fCount;?>').value;
        var value2 = document.getElementById('total').value;

        var test1 = value1-prev;
        var test2 = prev-value1;

        if(prev>value1){
            if(test2<prev){
                var formula1 = parseInt(rem) + parseInt(test2);
                document.getElementById('remaining').value=formula1;
                console.log("FIRST IF");
            }
            else if(rem!=value2){
                var formula1 = parseInt(rem) + parseInt(prev);
                document.getElementById('remaining').value=formula1;
                console.log("SECOND IF");
            }
        }
        else{
            if(rem!=value2){
                var formula1 = rem - value1;
                if(formula1<0){
                    BugCatch();
                }
                else{
                    document.getElementById('remaining').value=formula1;

                }
                function BugCatch(){
                    var formula1 = (parseInt(rem)+parseInt(prev)) - parseInt(value1);
                    document.getElementById('remaining').value=formula1;
                }
                console.log("FIRST IF ELSE");


            }
            else if(rem!=value2){
                var formula1 = (parseInt(rem)+parseInt(prev)) - parseInt(value1);
                document.getElementById('remaining').value=formula1;
                console.log("BUG G");
            }
            else{
                var formula1 = value2 - value1;
                document.getElementById('remaining').value=formula1;
                console.log("SECOND IF ELSE");

            }

        }
        computeSummary();
    });


</script>
