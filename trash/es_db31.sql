-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2017 at 05:48 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `es_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admins`
--

CREATE TABLE IF NOT EXISTS `tbl_admins` (
  `admin_id` varchar(100) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `firstname` varchar(60) NOT NULL,
  `middlename` varchar(60) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` text CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `password_temp` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `paths` text NOT NULL,
  `userlevel` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admins`
--

INSERT INTO `tbl_admins` (`admin_id`, `lastname`, `firstname`, `middlename`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `username`, `password`, `password_temp`, `paths`, `userlevel`, `email`) VALUES
('admin', 'Ayson', 'Justin Louise', 'Vinluan', 'College of Information Technology Education', '7', 'Dean', 'Regular', '2010', '7', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '../assets/uploaded_files/accounts/DSC_2013.jpg', 'ADMIN', 'aysonjustinmax@gmail.com'),
('admin2', 'Liwanag', 'Aristotle', '', 'College of Management and Accountancy', '13', 'Head', 'D1', '2004', '13', 'admin2', 'c47feaececaec397b36c150ed7fb95d1', 'LIWANAG', '../assets/uploaded_files/accounts/default.png', 'ADMIN', 'test@gmail.com'),
('superadmin', 'Clauna', 'Levi Marion', 'Almazan', 'College of Health and Sciences', '7', 'Dean', 'Regular', '', '7', 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 'CLAUNA', '../assets/uploaded_files/accounts/default.png', 'SUPERADMIN', 'levimarion_clauna@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeights` int(11) NOT NULL,
  `p1_rating` int(11) NOT NULL,
  `p1_weightedRating` float NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1`
--

INSERT INTO `tbl_ans_eformp1` (`id`, `kraID`, `kpiTitle`, `kpiWeights`, `p1_rating`, `p1_weightedRating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 2, 'COMMUNICATION & COORDINATION', 25, 3, 0.75, 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 3, 'OFFICE MANAGEMENT', 25, 3, 0.75, 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 4, 'PREPARATION OF REPORTS', 25, 3, 0.75, 'admin', 'EMP-001', '10/2017-11/2017'),
(4, 5, 'CUSTOMER SERVICE', 25, 3, 0.75, 'admin', 'EMP-001', '10/2017-11/2017'),
(5, 2, 'COMMUNICATION & COORDINATION', 25, 4, 1, 'admin', 'EMP-002', '10/2017-11/2017'),
(6, 3, 'OFFICE MANAGEMENT', 25, 4, 1, 'admin', 'EMP-002', '10/2017-11/2017'),
(7, 4, 'PREPARATION OF REPORTS', 25, 4, 1, 'admin', 'EMP-002', '10/2017-11/2017'),
(8, 5, 'CUSTOMER SERVICE', 25, 4, 1, 'admin', 'EMP-002', '10/2017-11/2017'),
(9, 2, 'COMMUNICATION & COORDINATION', 25, 4, 1, 'admin', 'uu', '10/2017-11/2017'),
(10, 3, 'OFFICE MANAGEMENT', 25, 4, 1, 'admin', 'uu', '10/2017-11/2017'),
(11, 4, 'PREPARATION OF REPORTS', 25, 4, 1, 'admin', 'uu', '10/2017-11/2017'),
(12, 5, 'CUSTOMER SERVICE', 25, 4, 1, 'admin', 'uu', '10/2017-11/2017'),
(13, 2, 'COMMUNICATION & COORDINATION', 25, 2, 0.5, 'admin', 'll', '10/2017-11/2017'),
(14, 3, 'OFFICE MANAGEMENT', 25, 2, 0.5, 'admin', 'll', '10/2017-11/2017'),
(15, 4, 'PREPARATION OF REPORTS', 25, 3, 0.75, 'admin', 'll', '10/2017-11/2017'),
(16, 5, 'CUSTOMER SERVICE', 25, 3, 0.75, 'admin', 'll', '10/2017-11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1kpi` (
  `id` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiDesc` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1kpi`
--

INSERT INTO `tbl_ans_eformp1kpi` (`id`, `kpiID`, `kraID`, `kpiDesc`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 1, 2, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 2, 2, 'Coordinate meetings and other activities within the college', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 3, 2, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'admin', 'EMP-001', '10/2017-11/2017'),
(4, 4, 2, 'Supervise the student assistants designated to the college', 'admin', 'EMP-001', '10/2017-11/2017'),
(5, 1, 3, 'Organize office files and documents', 'admin', 'EMP-001', '10/2017-11/2017'),
(6, 2, 3, 'Supervise order and cleanliness in the office', 'admin', 'EMP-001', '10/2017-11/2017'),
(7, 3, 3, 'Regular inventory and update of office supplies', 'admin', 'EMP-001', '10/2017-11/2017'),
(8, 1, 4, 'Submit reports (e.g., Attendance, Completion of Grades) Â required by other offices (e.g., HRD, Regi', 'admin', 'EMP-001', '10/2017-11/2017'),
(9, 2, 4, 'Monitor facultys attendanceÂ ', 'admin', 'EMP-001', '10/2017-11/2017'),
(10, 3, 4, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'admin', 'EMP-001', '10/2017-11/2017'),
(11, 1, 5, 'Answer telephone calls and/or take messages', 'admin', 'EMP-001', '10/2017-11/2017'),
(12, 2, 5, 'Man the offices frontdesk to meet walk-in customers Â  Â Â ', 'admin', 'EMP-001', '10/2017-11/2017'),
(13, 3, 5, 'Arrange customers appointments with the Dean or faculty Â  Â ', 'admin', 'EMP-001', '10/2017-11/2017'),
(14, 4, 5, 'Handle data encoding duties during enrolment', 'admin', 'EMP-001', '10/2017-11/2017'),
(15, 1, 2, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'admin', 'EMP-002', '10/2017-11/2017'),
(16, 2, 2, 'Coordinate meetings and other activities within the college', 'admin', 'EMP-002', '10/2017-11/2017'),
(17, 3, 2, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'admin', 'EMP-002', '10/2017-11/2017'),
(18, 4, 2, 'Supervise the student assistants designated to the college', 'admin', 'EMP-002', '10/2017-11/2017'),
(19, 1, 3, 'Organize office files and documents', 'admin', 'EMP-002', '10/2017-11/2017'),
(20, 2, 3, 'Supervise order and cleanliness in the office', 'admin', 'EMP-002', '10/2017-11/2017'),
(21, 3, 3, 'Regular inventory and update of office supplies', 'admin', 'EMP-002', '10/2017-11/2017'),
(22, 1, 4, 'Submit reports (e.g., Attendance, Completion of Grades) Â required by other offices (e.g., HRD, Regi', 'admin', 'EMP-002', '10/2017-11/2017'),
(23, 2, 4, 'Monitor facultys attendanceÂ ', 'admin', 'EMP-002', '10/2017-11/2017'),
(24, 3, 4, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'admin', 'EMP-002', '10/2017-11/2017'),
(25, 1, 5, 'Answer telephone calls and/or take messages', 'admin', 'EMP-002', '10/2017-11/2017'),
(26, 2, 5, 'Man the offices frontdesk to meet walk-in customers Â  Â Â ', 'admin', 'EMP-002', '10/2017-11/2017'),
(27, 3, 5, 'Arrange customers appointments with the Dean or faculty Â  Â ', 'admin', 'EMP-002', '10/2017-11/2017'),
(28, 4, 5, 'Handle data encoding duties during enrolment', 'admin', 'EMP-002', '10/2017-11/2017'),
(29, 1, 2, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'admin', 'uu', '10/2017-11/2017'),
(30, 2, 2, 'Coordinate meetings and other activities within the college', 'admin', 'uu', '10/2017-11/2017'),
(31, 3, 2, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'admin', 'uu', '10/2017-11/2017'),
(32, 4, 2, 'Supervise the student assistants designated to the college', 'admin', 'uu', '10/2017-11/2017'),
(33, 1, 3, 'Organize office files and documents', 'admin', 'uu', '10/2017-11/2017'),
(34, 2, 3, 'Supervise order and cleanliness in the office', 'admin', 'uu', '10/2017-11/2017'),
(35, 3, 3, 'Regular inventory and update of office supplies', 'admin', 'uu', '10/2017-11/2017'),
(36, 1, 4, 'Submit reports (e.g., Attendance, Completion of Grades) Â required by other offices (e.g., HRD, Regi', 'admin', 'uu', '10/2017-11/2017'),
(37, 2, 4, 'Monitor facultys attendanceÂ ', 'admin', 'uu', '10/2017-11/2017'),
(38, 3, 4, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'admin', 'uu', '10/2017-11/2017'),
(39, 1, 5, 'Answer telephone calls and/or take messages', 'admin', 'uu', '10/2017-11/2017'),
(40, 2, 5, 'Man the offices frontdesk to meet walk-in customers Â  Â Â ', 'admin', 'uu', '10/2017-11/2017'),
(41, 3, 5, 'Arrange customers appointments with the Dean or faculty Â  Â ', 'admin', 'uu', '10/2017-11/2017'),
(42, 4, 5, 'Handle data encoding duties during enrolment', 'admin', 'uu', '10/2017-11/2017'),
(43, 1, 2, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'admin', 'll', '10/2017-11/2017'),
(44, 2, 2, 'Coordinate meetings and other activities within the college', 'admin', 'll', '10/2017-11/2017'),
(45, 3, 2, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'admin', 'll', '10/2017-11/2017'),
(46, 4, 2, 'Supervise the student assistants designated to the college', 'admin', 'll', '10/2017-11/2017'),
(47, 1, 3, 'Organize office files and documents', 'admin', 'll', '10/2017-11/2017'),
(48, 2, 3, 'Supervise order and cleanliness in the office', 'admin', 'll', '10/2017-11/2017'),
(49, 3, 3, 'Regular inventory and update of office supplies', 'admin', 'll', '10/2017-11/2017'),
(50, 1, 4, 'Submit reports (e.g., Attendance, Completion of Grades) Â required by other offices (e.g., HRD, Regi', 'admin', 'll', '10/2017-11/2017'),
(51, 2, 4, 'Monitor facultys attendanceÂ ', 'admin', 'll', '10/2017-11/2017'),
(52, 3, 4, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'admin', 'll', '10/2017-11/2017'),
(53, 1, 5, 'Answer telephone calls and/or take messages', 'admin', 'll', '10/2017-11/2017'),
(54, 2, 5, 'Man the offices frontdesk to meet walk-in customers Â  Â Â ', 'admin', 'll', '10/2017-11/2017'),
(55, 3, 5, 'Arrange customers appointments with the Dean or faculty Â  Â ', 'admin', 'll', '10/2017-11/2017'),
(56, 4, 5, 'Handle data encoding duties during enrolment', 'admin', 'll', '10/2017-11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp1_achievements`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp1_achievements` (
  `id` int(11) NOT NULL,
  `kraID` int(11) NOT NULL,
  `kpiAchievement` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp1_achievements`
--

INSERT INTO `tbl_ans_eformp1_achievements` (`id`, `kraID`, `kpiAchievement`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 2, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 2, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 2, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(4, 2, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(5, 3, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(6, 3, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(7, 3, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(8, 4, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(9, 4, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(10, 4, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(11, 5, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(12, 5, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(13, 5, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(14, 5, '', 'admin', 'EMP-001', '10/2017-11/2017'),
(15, 2, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(16, 2, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(17, 2, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(18, 2, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(19, 3, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(20, 3, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(21, 3, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(22, 4, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(23, 4, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(24, 4, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(25, 5, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(26, 5, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(27, 5, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(28, 5, '', 'admin', 'EMP-002', '10/2017-11/2017'),
(29, 2, '', 'admin', 'uu', '10/2017-11/2017'),
(30, 2, '', 'admin', 'uu', '10/2017-11/2017'),
(31, 2, '', 'admin', 'uu', '10/2017-11/2017'),
(32, 2, '', 'admin', 'uu', '10/2017-11/2017'),
(33, 3, '', 'admin', 'uu', '10/2017-11/2017'),
(34, 3, '', 'admin', 'uu', '10/2017-11/2017'),
(35, 3, '', 'admin', 'uu', '10/2017-11/2017'),
(36, 4, '', 'admin', 'uu', '10/2017-11/2017'),
(37, 4, '', 'admin', 'uu', '10/2017-11/2017'),
(38, 4, '', 'admin', 'uu', '10/2017-11/2017'),
(39, 5, '', 'admin', 'uu', '10/2017-11/2017'),
(40, 5, '', 'admin', 'uu', '10/2017-11/2017'),
(41, 5, '', 'admin', 'uu', '10/2017-11/2017'),
(42, 5, '', 'admin', 'uu', '10/2017-11/2017'),
(43, 2, '', 'admin', 'll', '10/2017-11/2017'),
(44, 2, '', 'admin', 'll', '10/2017-11/2017'),
(45, 2, '', 'admin', 'll', '10/2017-11/2017'),
(46, 2, '', 'admin', 'll', '10/2017-11/2017'),
(47, 3, '', 'admin', 'll', '10/2017-11/2017'),
(48, 3, '', 'admin', 'll', '10/2017-11/2017'),
(49, 3, '', 'admin', 'll', '10/2017-11/2017'),
(50, 4, '', 'admin', 'll', '10/2017-11/2017'),
(51, 4, '', 'admin', 'll', '10/2017-11/2017'),
(52, 4, '', 'admin', 'll', '10/2017-11/2017'),
(53, 5, '', 'admin', 'll', '10/2017-11/2017'),
(54, 5, '', 'admin', 'll', '10/2017-11/2017'),
(55, 5, '', 'admin', 'll', '10/2017-11/2017'),
(56, 5, '', 'admin', 'll', '10/2017-11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` varchar(30) NOT NULL,
  `competencyDescription` longtext NOT NULL,
  `proficiencyLevel` varchar(35) NOT NULL,
  `p2_weights` float NOT NULL,
  `p2_rating` int(11) NOT NULL,
  `p2_weightedRating` float NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2a`
--

INSERT INTO `tbl_ans_eformp2a` (`id`, `competency`, `competencyDescription`, `proficiencyLevel`, `p2_weights`, `p2_rating`, `p2_weightedRating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community.Â </p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'Customer Focus', '<p>The ability to focus efforts on determining</p>\r\n', 'B Contributing with Expertise Speci', 20, 3, 0.3, 'admin', 'EMP-001', '10/2017-11/2017'),
(4, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group</p>\r\n', 'B Contributing with Expertise Speci', 20, 3, 0.3, 'admin', 'EMP-001', '10/2017-11/2017'),
(5, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin', 'EMP-001', '10/2017-11/2017'),
(6, 'Leadership Excellence', '<p>Upholds values and ethics</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin', 'EMP-001', '10/2017-11/2017'),
(7, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders.</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin', 'EMP-001', '10/2017-11/2017'),
(8, 'People Development', '<p>The ability to plan and support the development of employeesâ€™ skills and abilities so that they can fulfill</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin', 'EMP-001', '10/2017-11/2017'),
(9, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community.Â </p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin', 'EMP-002', '10/2017-11/2017'),
(10, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions</p>\r\n', 'A Contributing Independently Learne', 10, 3, 0.3, 'admin', 'EMP-002', '10/2017-11/2017'),
(11, 'Customer Focus', '<p>The ability to focus efforts on determining</p>\r\n', 'A Contributing Independently Learne', 20, 4, 0.4, 'admin', 'EMP-002', '10/2017-11/2017'),
(12, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group</p>\r\n', 'A Contributing Independently Learne', 20, 3, 0.3, 'admin', 'EMP-002', '10/2017-11/2017'),
(13, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession.</p>\r\n', 'A Contributing Independently Learne', 10, 4, 0.4, 'admin', 'EMP-002', '10/2017-11/2017'),
(14, 'Leadership Excellence', '<p>Upholds values and ethics</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin', 'EMP-002', '10/2017-11/2017'),
(15, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'admin', 'EMP-002', '10/2017-11/2017'),
(16, 'People Development', '<p>The ability to plan and support the development of employeesâ€™ skills and abilities so that they can fulfill</p>\r\n', 'C Contribution through Teams/Peers ', 10, 4, 0.4, 'admin', 'EMP-002', '10/2017-11/2017'),
(17, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community.Â </p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin', 'uu', '10/2017-11/2017'),
(18, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin', 'uu', '10/2017-11/2017'),
(19, 'Customer Focus', '<p>The ability to focus efforts on determining</p>\r\n', 'B Contributing with Expertise Speci', 20, 4, 0.4, 'admin', 'uu', '10/2017-11/2017'),
(20, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group</p>\r\n', 'B Contributing with Expertise Speci', 20, 4, 0.4, 'admin', 'uu', '10/2017-11/2017'),
(21, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession.</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin', 'uu', '10/2017-11/2017'),
(22, 'Leadership Excellence', '<p>Upholds values and ethics</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin', 'uu', '10/2017-11/2017'),
(23, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders.</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin', 'uu', '10/2017-11/2017'),
(24, 'People Development', '<p>The ability to plan and support the development of employeesâ€™ skills and abilities so that they can fulfill</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin', 'uu', '10/2017-11/2017'),
(25, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community.Â </p>\r\n', 'D Contributing through Strategy Bui', 10, 4, 0.4, 'admin', 'll', '10/2017-11/2017'),
(26, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions</p>\r\n', 'B Contributing with Expertise Speci', 10, 3, 0.3, 'admin', 'll', '10/2017-11/2017'),
(27, 'Customer Focus', '<p>The ability to focus efforts on determining</p>\r\n', 'B Contributing with Expertise Speci', 20, 3, 0.3, 'admin', 'll', '10/2017-11/2017'),
(28, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group</p>\r\n', 'C Contribution through Teams/Peers ', 20, 3, 0.3, 'admin', 'll', '10/2017-11/2017'),
(29, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession.</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'admin', 'll', '10/2017-11/2017'),
(30, 'Leadership Excellence', '<p>Upholds values and ethics</p>\r\n', 'B Contributing with Expertise Speci', 10, 4, 0.4, 'admin', 'll', '10/2017-11/2017'),
(31, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders.</p>\r\n', 'D Contributing through Strategy Bui', 10, 2, 0.2, 'admin', 'll', '10/2017-11/2017'),
(32, 'People Development', '<p>The ability to plan and support the development of employeesâ€™ skills and abilities so that they can fulfill</p>\r\n', 'C Contribution through Teams/Peers ', 10, 3, 0.3, 'admin', 'll', '10/2017-11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(30) NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2b`
--

INSERT INTO `tbl_ans_eformp2b` (`id`, `competency`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'Creativity and Innovation', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'Customer Focus', 'admin', 'EMP-001', '10/2017-11/2017'),
(4, 'Team Orientation', 'admin', 'EMP-001', '10/2017-11/2017'),
(5, 'Technical Knowledge', 'admin', 'EMP-001', '10/2017-11/2017'),
(6, 'Leadership Excellence', 'admin', 'EMP-001', '10/2017-11/2017'),
(7, 'Management Excellence', 'admin', 'EMP-001', '10/2017-11/2017'),
(8, 'People Development', 'admin', 'EMP-001', '10/2017-11/2017'),
(9, 'Adaptability', 'admin', 'EMP-002', '10/2017-11/2017'),
(10, 'Creativity and Innovation', 'admin', 'EMP-002', '10/2017-11/2017'),
(11, 'Customer Focus', 'admin', 'EMP-002', '10/2017-11/2017'),
(12, 'Team Orientation', 'admin', 'EMP-002', '10/2017-11/2017'),
(13, 'Technical Knowledge', 'admin', 'EMP-002', '10/2017-11/2017'),
(14, 'Leadership Excellence', 'admin', 'EMP-002', '10/2017-11/2017'),
(15, 'Management Excellence', 'admin', 'EMP-002', '10/2017-11/2017'),
(16, 'People Development', 'admin', 'EMP-002', '10/2017-11/2017'),
(17, 'Adaptability', 'admin', 'uu', '10/2017-11/2017'),
(18, 'Creativity and Innovation', 'admin', 'uu', '10/2017-11/2017'),
(19, 'Customer Focus', 'admin', 'uu', '10/2017-11/2017'),
(20, 'Team Orientation', 'admin', 'uu', '10/2017-11/2017'),
(21, 'Technical Knowledge', 'admin', 'uu', '10/2017-11/2017'),
(22, 'Leadership Excellence', 'admin', 'uu', '10/2017-11/2017'),
(23, 'Management Excellence', 'admin', 'uu', '10/2017-11/2017'),
(24, 'People Development', 'admin', 'uu', '10/2017-11/2017'),
(25, 'Adaptability', 'admin', 'll', '10/2017-11/2017'),
(26, 'Creativity and Innovation', 'admin', 'll', '10/2017-11/2017'),
(27, 'Customer Focus', 'admin', 'll', '10/2017-11/2017'),
(28, 'Team Orientation', 'admin', 'll', '10/2017-11/2017'),
(29, 'Technical Knowledge', 'admin', 'll', '10/2017-11/2017'),
(30, 'Leadership Excellence', 'admin', 'll', '10/2017-11/2017'),
(31, 'Management Excellence', 'admin', 'll', '10/2017-11/2017'),
(32, 'People Development', 'admin', 'll', '10/2017-11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp2b_critical_incident`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp2b_critical_incident` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(30) NOT NULL,
  `criticalIncident` text NOT NULL,
  `answeredBy` varchar(50) NOT NULL,
  `answeredFor` varchar(50) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp2b_critical_incident`
--

INSERT INTO `tbl_ans_eformp2b_critical_incident` (`id`, `competencyName`, `criticalIncident`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'Adaptability', 'Critical Incident #1:', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'Adaptability', 'Critical Incident #2:', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'Adaptability', 'Critical Incident #3:', 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'Customer Focus', 'Critical Incident #1:', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'Customer Focus', 'Critical Incident #2:', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'Customer Focus', 'Critical Incident #3:', 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'Team Orientation', 'Critical Incident #1:', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'Team Orientation', 'Critical Incident #2:', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'Team Orientation', 'Critical Incident #3:', 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'Management Excellence', 'Critical Incident #1:', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'Management Excellence', 'Critical Incident #2:', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'Management Excellence', 'Critical Incident #3:', 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'People Development', 'Critical Incident #1:', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'People Development', 'Critical Incident #2:', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'People Development', 'Critical Incident #3:', 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'Adaptability', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'Adaptability', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'Adaptability', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'Customer Focus', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'Customer Focus', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'Customer Focus', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'Team Orientation', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'Team Orientation', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'Team Orientation', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'Management Excellence', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'Management Excellence', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'Management Excellence', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'People Development', 'Critical Incident #1:', 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'People Development', 'Critical Incident #2:', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'People Development', 'Critical Incident #3:', 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'Adaptability', 'Critical Incident #1:', 'admin', 'uu', '10/2017-11/2017'),
(2, 'Adaptability', 'Critical Incident #2:', 'admin', 'uu', '10/2017-11/2017'),
(3, 'Adaptability', 'Critical Incident #3:', 'admin', 'uu', '10/2017-11/2017'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'admin', 'uu', '10/2017-11/2017'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'admin', 'uu', '10/2017-11/2017'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'admin', 'uu', '10/2017-11/2017'),
(1, 'Customer Focus', 'Critical Incident #1:', 'admin', 'uu', '10/2017-11/2017'),
(2, 'Customer Focus', 'Critical Incident #2:', 'admin', 'uu', '10/2017-11/2017'),
(3, 'Customer Focus', 'Critical Incident #3:', 'admin', 'uu', '10/2017-11/2017'),
(1, 'Team Orientation', 'Critical Incident #1:', 'admin', 'uu', '10/2017-11/2017'),
(2, 'Team Orientation', 'Critical Incident #2:', 'admin', 'uu', '10/2017-11/2017'),
(3, 'Team Orientation', 'Critical Incident #3:', 'admin', 'uu', '10/2017-11/2017'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'admin', 'uu', '10/2017-11/2017'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'admin', 'uu', '10/2017-11/2017'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'admin', 'uu', '10/2017-11/2017'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'admin', 'uu', '10/2017-11/2017'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'admin', 'uu', '10/2017-11/2017'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'admin', 'uu', '10/2017-11/2017'),
(1, 'Management Excellence', 'Critical Incident #1:', 'admin', 'uu', '10/2017-11/2017'),
(2, 'Management Excellence', 'Critical Incident #2:', 'admin', 'uu', '10/2017-11/2017'),
(3, 'Management Excellence', 'Critical Incident #3:', 'admin', 'uu', '10/2017-11/2017'),
(1, 'People Development', 'Critical Incident #1:', 'admin', 'uu', '10/2017-11/2017'),
(2, 'People Development', 'Critical Incident #2:', 'admin', 'uu', '10/2017-11/2017'),
(3, 'People Development', 'Critical Incident #3:', 'admin', 'uu', '10/2017-11/2017'),
(1, 'Adaptability', 'Critical Incident #1:', 'admin', 'll', '10/2017-11/2017'),
(2, 'Adaptability', 'Critical Incident #2:', 'admin', 'll', '10/2017-11/2017'),
(3, 'Adaptability', 'Critical Incident #3:', 'admin', 'll', '10/2017-11/2017'),
(1, 'Creativity and Innovation', 'Critical Incident #1:', 'admin', 'll', '10/2017-11/2017'),
(2, 'Creativity and Innovation', 'Critical Incident #2:', 'admin', 'll', '10/2017-11/2017'),
(3, 'Creativity and Innovation', 'Critical Incident #3:', 'admin', 'll', '10/2017-11/2017'),
(1, 'Customer Focus', 'Critical Incident #1:', 'admin', 'll', '10/2017-11/2017'),
(2, 'Customer Focus', 'Critical Incident #2:', 'admin', 'll', '10/2017-11/2017'),
(3, 'Customer Focus', 'Critical Incident #3:', 'admin', 'll', '10/2017-11/2017'),
(1, 'Team Orientation', 'Critical Incident #1:', 'admin', 'll', '10/2017-11/2017'),
(2, 'Team Orientation', 'Critical Incident #2:', 'admin', 'll', '10/2017-11/2017'),
(3, 'Team Orientation', 'Critical Incident #3:', 'admin', 'll', '10/2017-11/2017'),
(1, 'Technical Knowledge', 'Critical Incident #1:', 'admin', 'll', '10/2017-11/2017'),
(2, 'Technical Knowledge', 'Critical Incident #2:', 'admin', 'll', '10/2017-11/2017'),
(3, 'Technical Knowledge', 'Critical Incident #3:', 'admin', 'll', '10/2017-11/2017'),
(1, 'Leadership Excellence', 'Critical Incident #1:', 'admin', 'll', '10/2017-11/2017'),
(2, 'Leadership Excellence', 'Critical Incident #2:', 'admin', 'll', '10/2017-11/2017'),
(3, 'Leadership Excellence', 'Critical Incident #3:', 'admin', 'll', '10/2017-11/2017'),
(1, 'Management Excellence', 'Critical Incident #1:', 'admin', 'll', '10/2017-11/2017'),
(2, 'Management Excellence', 'Critical Incident #2:', 'admin', 'll', '10/2017-11/2017'),
(3, 'Management Excellence', 'Critical Incident #3:', 'admin', 'll', '10/2017-11/2017'),
(1, 'People Development', 'Critical Incident #1:', 'admin', 'll', '10/2017-11/2017'),
(2, 'People Development', 'Critical Incident #2:', 'admin', 'll', '10/2017-11/2017'),
(3, 'People Development', 'Critical Incident #3:', 'admin', 'll', '10/2017-11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL,
  `score` int(11) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp3`
--

INSERT INTO `tbl_ans_eformp3` (`id`, `areas`, `score`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'DISCIPLINE', 3, 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'ATTENDANCE', 3, 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'PUNCTUALITY', 3, 'admin', 'EMP-001', '10/2017-11/2017'),
(4, 'COMPLETION OF TRAINING', 3, 'admin', 'EMP-001', '10/2017-11/2017'),
(5, 'DISCIPLINE', 3, 'admin', 'EMP-002', '10/2017-11/2017'),
(6, 'ATTENDANCE', 4, 'admin', 'EMP-002', '10/2017-11/2017'),
(7, 'PUNCTUALITY', 4, 'admin', 'EMP-002', '10/2017-11/2017'),
(8, 'COMPLETION OF TRAINING', 4, 'admin', 'EMP-002', '10/2017-11/2017'),
(9, 'DISCIPLINE', 4, 'admin', 'uu', '10/2017-11/2017'),
(10, 'ATTENDANCE', 3, 'admin', 'uu', '10/2017-11/2017'),
(11, 'PUNCTUALITY', 4, 'admin', 'uu', '10/2017-11/2017'),
(12, 'COMPLETION OF TRAINING', 4, 'admin', 'uu', '10/2017-11/2017'),
(13, 'DISCIPLINE', 3, 'admin', 'll', '10/2017-11/2017'),
(14, 'ATTENDANCE', 3, 'admin', 'll', '10/2017-11/2017'),
(15, 'PUNCTUALITY', 2, 'admin', 'll', '10/2017-11/2017'),
(16, 'COMPLETION OF TRAINING', 3, 'admin', 'll', '10/2017-11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp3_rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp3_rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` varchar(200) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp3_rubric`
--

INSERT INTO `tbl_ans_eformp3_rubric` (`id`, `areaName`, `rubricDesc`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality', 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'admin', 'EMP-001', '10/2017-11/2017'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness', 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin', 'EMP-001', '10/2017-11/2017'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin', 'EMP-001', '10/2017-11/2017'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.', 'admin', 'EMP-001', '10/2017-11/2017'),
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality', 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'admin', 'EMP-002', '10/2017-11/2017'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.', 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness', 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin', 'EMP-002', '10/2017-11/2017'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime', 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin', 'EMP-002', '10/2017-11/2017'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.', 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.', 'admin', 'EMP-002', '10/2017-11/2017'),
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'admin', 'uu', '10/2017-11/2017'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'admin', 'uu', '10/2017-11/2017'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality', 'admin', 'uu', '10/2017-11/2017'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'admin', 'uu', '10/2017-11/2017'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.', 'admin', 'uu', '10/2017-11/2017'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.', 'admin', 'uu', '10/2017-11/2017'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness', 'admin', 'uu', '10/2017-11/2017'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin', 'uu', '10/2017-11/2017'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.', 'admin', 'uu', '10/2017-11/2017'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime', 'admin', 'uu', '10/2017-11/2017'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'admin', 'uu', '10/2017-11/2017'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin', 'uu', '10/2017-11/2017'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars', 'admin', 'uu', '10/2017-11/2017'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.', 'admin', 'uu', '10/2017-11/2017'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'admin', 'uu', '10/2017-11/2017'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.', 'admin', 'uu', '10/2017-11/2017'),
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies', 'admin', 'll', '10/2017-11/2017'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality', 'admin', 'll', '10/2017-11/2017'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality', 'admin', 'll', '10/2017-11/2017'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality', 'admin', 'll', '10/2017-11/2017'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.', 'admin', 'll', '10/2017-11/2017'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.', 'admin', 'll', '10/2017-11/2017'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness', 'admin', 'll', '10/2017-11/2017'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin', 'll', '10/2017-11/2017'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.', 'admin', 'll', '10/2017-11/2017'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime', 'admin', 'll', '10/2017-11/2017'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness', 'admin', 'll', '10/2017-11/2017'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy', 'admin', 'll', '10/2017-11/2017'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars', 'admin', 'll', '10/2017-11/2017'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.', 'admin', 'll', '10/2017-11/2017'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.', 'admin', 'll', '10/2017-11/2017'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.', 'admin', 'll', '10/2017-11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_eformp4` (
  `id` int(11) NOT NULL,
  `p4_area` varchar(50) NOT NULL,
  `customerFeedback` text NOT NULL,
  `p4_score` int(11) NOT NULL,
  `p4_rating` varchar(11) NOT NULL,
  `answeredBy` varchar(80) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_eformp4`
--

INSERT INTO `tbl_ans_eformp4` (`id`, `p4_area`, `customerFeedback`, `p4_score`, `p4_rating`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'April - June', '', 0, '0', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'July - September', '', 0, '0', 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'October-December', '', 0, '0', 'admin', 'EMP-001', '10/2017-11/2017'),
(4, 'January-March', '', 0, '0', 'admin', 'EMP-001', '10/2017-11/2017'),
(5, 'April - June', '', 0, '0', 'admin', 'EMP-002', '10/2017-11/2017'),
(6, 'July - September', '', 0, '0', 'admin', 'EMP-002', '10/2017-11/2017'),
(7, 'October-December', '', 0, '0', 'admin', 'EMP-002', '10/2017-11/2017'),
(8, 'January-March', '', 0, '0', 'admin', 'EMP-002', '10/2017-11/2017'),
(9, 'April - June', '', 0, '0', 'admin', 'uu', '10/2017-11/2017'),
(10, 'July - September', '', 0, '0', 'admin', 'uu', '10/2017-11/2017'),
(11, 'October-December', '', 0, '0', 'admin', 'uu', '10/2017-11/2017'),
(12, 'January-March', '', 0, '0', 'admin', 'uu', '10/2017-11/2017'),
(13, 'April - June', '', 4, '0.04', 'admin', 'll', '10/2017-11/2017'),
(14, 'July - September', '', 4, '0.04', 'admin', 'll', '10/2017-11/2017'),
(15, 'October-December', '', 4, '0.04', 'admin', 'll', '10/2017-11/2017'),
(16, 'January-March', '', 4, '0.04', 'admin', 'll', '10/2017-11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_summary`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_summary` (
  `id` int(11) NOT NULL,
  `performance_rating` varchar(60) NOT NULL,
  `total_weighted_rating` float NOT NULL,
  `percentage_allocation` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_summary`
--

INSERT INTO `tbl_ans_summary` (`id`, `performance_rating`, `total_weighted_rating`, `percentage_allocation`, `subtotal`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 3, 40, 1.2, 'admin', 'EMP-001', '10/2017-11/2017'),
(2, 'PART 2: COMPETENCIES', 2.5, 30, 3.6, 'admin', 'EMP-001', '10/2017-11/2017'),
(3, 'PART 3: PROFESSIONALISM', 12, 30, 0.75, 'admin', 'EMP-001', '10/2017-11/2017'),
(4, '0', 0, 0, 0, 'admin', 'EMP-001', '10/2017-11/2017'),
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 4, 40, 1.6, 'admin', 'EMP-002', '10/2017-11/2017'),
(2, 'PART 2: COMPETENCIES', 2.9, 30, 4.5, 'admin', 'EMP-002', '10/2017-11/2017'),
(3, 'PART 3: PROFESSIONALISM', 15, 30, 0.87, 'admin', 'EMP-002', '10/2017-11/2017'),
(4, '0', 0, 0, 0, 'admin', 'EMP-002', '10/2017-11/2017'),
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 4, 40, 1.6, 'admin', 'uu', '10/2017-11/2017'),
(2, 'PART 2: COMPETENCIES', 3.1, 30, 4.5, 'admin', 'uu', '10/2017-11/2017'),
(3, 'PART 3: PROFESSIONALISM', 15, 30, 0.93, 'admin', 'uu', '10/2017-11/2017'),
(4, '0', 0, 0, 0.04, 'admin', 'uu', '10/2017-11/2017'),
(1, 'PART 1: KEY PERFORMANCE INDICATORS', 2.5, 40, 1, 'admin', 'll', '10/2017-11/2017'),
(2, 'PART 2: COMPETENCIES', 2.5, 30, 3.3, 'admin', 'll', '10/2017-11/2017'),
(3, 'PART 3: PROFESSIONALISM', 11, 30, 0.75, 'admin', 'll', '10/2017-11/2017'),
(4, '0', 0, 0, 0.04, 'admin', 'll', '10/2017-11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans_summary_comments_acknowledgement`
--

CREATE TABLE IF NOT EXISTS `tbl_ans_summary_comments_acknowledgement` (
  `id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `recommendations` text NOT NULL,
  `approval` text NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `answeredFor` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans_summary_comments_acknowledgement`
--

INSERT INTO `tbl_ans_summary_comments_acknowledgement` (`id`, `comments`, `recommendations`, `approval`, `answeredBy`, `answeredFor`, `performanceCycle`) VALUES
(1, '<p>comment for Janessa y2017</p>', '<p>\r\n\r\nrecommendations for Janessa y2017\r\n\r\n<br></p>', '<p>I certify this evaluation for Janessa Estayo y2017</p>', 'admin', 'EMP-001', '10/2017-11/2017'),
(2, '<p>comments for&nbsp;\r\n\r\nArwind y2017</p>', '<p>\r\n\r\nrecommendtations for \r\n\r\nArwind y2017\r\n\r\n<br></p>', '<p>\r\n\r\nI certify this evaluation for Arwind y2017&nbsp;<br></p>', 'admin', 'EMP-002', '10/2017-11/2017'),
(3, '<p>comments for uu y2017</p>', '<p>\r\n\r\nrecommendations for uu y2017\r\n\r\n<br></p>', '<p>I certify the evaluation for uu this y2017</p>', 'admin', 'uu', '10/2017-11/2017'),
(4, '<p>comments for mark y2017</p>', '<p>\r\n\r\nrecommendations for mark y2017\r\n\r\n<br></p>', '<p>I certify this evaluation for mark y2017</p>', 'admin', 'll', '10/2017-11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_competencyratingscale`
--

CREATE TABLE IF NOT EXISTS `tbl_competencyratingscale` (
  `id` int(11) NOT NULL,
  `competency_rating_value` varchar(100) NOT NULL,
  `competency_title` varchar(100) NOT NULL,
  `competency_description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_competencyratingscale`
--

INSERT INTO `tbl_competencyratingscale` (`id`, `competency_rating_value`, `competency_title`, `competency_description`) VALUES
(3, '4', 'Expert/Exemplary', '(Evaluation; Defines Framework Modes; Mentor, Self - Actualization)'),
(4, '3', 'Advanced/Accomplished', '(Analysis; Synthesis; Understands Framework Models, Exceeds)'),
(5, '2', 'Intermediate/Developing', '(Unsupervised Application; Beginning Analysis, Meets)'),
(6, '1', 'Beginner', '(Comprehension; Beginning Application; Some Experience, Meets some)'),
(7, 'NE', '', 'Early stages of application of the item; Behavior not yet present.'),
(8, 'NA', '', 'The item does not apply to your position or performance of assigned tasks and responsibilities.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dateevaluation`
--

CREATE TABLE IF NOT EXISTS `tbl_dateevaluation` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dateevaluation`
--

INSERT INTO `tbl_dateevaluation` (`startDate`, `endDate`) VALUES
('10/2018', '11/2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_datekpi`
--

CREATE TABLE IF NOT EXISTS `tbl_datekpi` (
  `startDate` varchar(11) NOT NULL,
  `endDate` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_datekpi`
--

INSERT INTO `tbl_datekpi` (`startDate`, `endDate`) VALUES
('09/2017', '10/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departments`
--

CREATE TABLE IF NOT EXISTS `tbl_departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `initials` varchar(100) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL DEFAULT 'PENDING',
  `departmentOwner` varchar(100) NOT NULL,
  `monitor` varchar(6) NOT NULL DEFAULT 'unread'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_departments`
--

INSERT INTO `tbl_departments` (`id`, `department_name`, `initials`, `kpiStatus`, `departmentOwner`, `monitor`) VALUES
(1, 'College of Information Technology Education', 'CITE', 'APPROVED', 'superadmin', 'read'),
(2, 'College of Management and Accountancy', 'CMA', 'APPROVED', 'admin2', 'read');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp1`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp1` (
  `kraID` int(11) NOT NULL,
  `kpiTitle` varchar(30) NOT NULL,
  `kpiWeight` int(11) NOT NULL,
  `kpiOwner` varchar(60) NOT NULL,
  `kpiDateCreated` varchar(11) NOT NULL,
  `kpiStatus` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp1`
--

INSERT INTO `tbl_eformp1` (`kraID`, `kpiTitle`, `kpiWeight`, `kpiOwner`, `kpiDateCreated`, `kpiStatus`) VALUES
(1, 'Leadership', 0, 'ADMIN-2', '', 'PENDING'),
(2, 'COMMUNICATION & COORDINATION', 0, 'admin', '', 'APPROVED'),
(3, 'OFFICE MANAGEMENT', 0, 'admin', '', 'APPROVED'),
(4, 'PREPARATION OF REPORTS', 0, 'admin', '', 'APPROVED'),
(5, 'CUSTOMER SERVICE', 0, 'admin', '', 'APPROVED'),
(6, 'COMMUNICATION', 0, 'EMP-001', '', 'SENT'),
(7, '2', 0, 'EMP-001', '', 'SENT'),
(8, '3', 0, 'EMP-001', '', 'SENT'),
(9, '4', 0, 'EMP-001', '', 'SENT'),
(10, 's', 0, 'admin2', '', 'APPROVED'),
(11, 'a', 0, 'admin2', '', 'APPROVED'),
(12, 'asd', 0, 'admin2', '', 'APPROVED'),
(13, 'ff', 0, 'admin2', '', 'APPROVED');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2a` (
  `id` int(11) NOT NULL,
  `competency` text NOT NULL,
  `competencyDesc` varchar(500) NOT NULL,
  `requiredProfLevel` varchar(50) NOT NULL,
  `weights` float NOT NULL,
  `rating` int(2) NOT NULL,
  `weightedRating` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2a`
--

INSERT INTO `tbl_eformp2a` (`id`, `competency`, `competencyDesc`, `requiredProfLevel`, `weights`, `rating`, `weightedRating`) VALUES
(1, 'Adaptability', '<p>The ability to adapt to the culture and environment of the school community.&nbsp;</p>\r\n', 'C -- Contribution through Teams/Peers', 10, 0, 0),
(2, 'Creativity and Innovation', '<p>The ability to provide creative or breakthrough solutions, improve existing conditions</p>\r\n', 'C -- Contribution through Teams/Peers', 10, 0, 0),
(3, 'Customer Focus', '<p>The ability to focus efforts on determining</p>\r\n', 'A -- Contributing Independently', 20, 0, 0),
(4, 'Team Orientation', '<p>Able to effectively work, build mutual trust and respect and attain objectives in group</p>\r\n', 'B -- Contributing with Expertise', 20, 0, 0),
(5, 'Technical Knowledge', '<p>Possession and acquisition of sets of skills and knowledge in a function or industry to continually his/her contribution to the firm and to his/her respective profession.</p>\r\n', 'B -- Contributing with Expertise', 10, 0, 0),
(6, 'Leadership Excellence', '<p>Upholds values and ethics</p>\r\n', 'C -- Contribution through Teams/Peers', 10, 0, 0),
(7, 'Management Excellence', '<p>Broad understanding of the companys various businesses and its key stakeholders.</p>\r\n', 'B -- Contributing with Expertise', 10, 0, 0),
(8, 'People Development', '<p>The ability to plan and support the development of employees&rsquo; skills and abilities so that they can fulfill</p>\r\n', 'A -- Contributing Independently', 10, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp2b`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp2b` (
  `id` int(11) NOT NULL,
  `competency` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp2b`
--

INSERT INTO `tbl_eformp2b` (`id`, `competency`) VALUES
(1, 'Adaptability'),
(2, 'Creativity and Innovation'),
(3, 'Customer Focus'),
(4, 'Team Orientation'),
(5, 'Technical Knowledge'),
(6, 'Leadership Excellence'),
(7, 'Management Excellence'),
(8, 'People Development');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp3`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp3` (
  `id` int(11) NOT NULL,
  `areas` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp3`
--

INSERT INTO `tbl_eformp3` (`id`, `areas`) VALUES
(1, 'DISCIPLINE'),
(2, 'ATTENDANCE'),
(3, 'PUNCTUALITY'),
(4, 'COMPLETION OF TRAINING');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eformp4`
--

CREATE TABLE IF NOT EXISTS `tbl_eformp4` (
  `id` int(11) NOT NULL,
  `areas` varchar(30) NOT NULL,
  `customerFeedback` varchar(100) NOT NULL,
  `respondentScore` int(2) NOT NULL,
  `ratingFactor` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eformp4`
--

INSERT INTO `tbl_eformp4` (`id`, `areas`, `customerFeedback`, `respondentScore`, `ratingFactor`) VALUES
(1, 'April - June', '', 0, 0),
(2, 'July - September', '', 0, 0),
(3, 'October-December', '', 0, 0),
(4, 'January-March', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation_requests`
--

CREATE TABLE IF NOT EXISTS `tbl_evaluation_requests` (
  `id` int(11) NOT NULL,
  `admin_id` varchar(60) NOT NULL,
  `employee_missed` varchar(60) NOT NULL,
  `employee_department` varchar(60) NOT NULL,
  `reason` text NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `request_status` varchar(30) NOT NULL DEFAULT 'PENDING'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_evaluation_requests`
--

INSERT INTO `tbl_evaluation_requests` (`id`, `admin_id`, `employee_missed`, `employee_department`, `reason`, `performanceCycle`, `request_status`) VALUES
(1, 'admin2', 'EMP-003', 'College of Management and Accountancy', '<p>I forgot the evaluation because I was busy with other things.</p>\r\n', '10/2017-11/2017', 'PENDING'),
(2, 'admin2', 'EMP-003', 'College of Management and Accountancy', '<p><strong>WOW</strong></p>\r\n', '10/2017-11/2017', 'PENDING');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation_results`
--

CREATE TABLE IF NOT EXISTS `tbl_evaluation_results` (
  `id` int(11) NOT NULL,
  `emp_id` varchar(60) NOT NULL,
  `employee_firstName` varchar(60) NOT NULL,
  `employee_middleName` varchar(60) NOT NULL,
  `employee_lastName` varchar(60) NOT NULL,
  `employee_department` varchar(60) NOT NULL,
  `employee_position` varchar(30) NOT NULL,
  `employee_job_level` varchar(30) NOT NULL,
  `employee_years_company` int(11) NOT NULL,
  `employee_years_position` int(11) NOT NULL,
  `evaluationStatus` varchar(30) NOT NULL DEFAULT 'NOT YET STARTED',
  `performance_rating` varchar(100) NOT NULL,
  `overall_rating` varchar(100) NOT NULL,
  `answeredBy` varchar(60) NOT NULL,
  `performanceCycle` varchar(60) NOT NULL,
  `ev_sog_feedback` varchar(15) NOT NULL DEFAULT 'NO RESPONSE YET'
) ENGINE=InnoDB AUTO_INCREMENT=967 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_evaluation_results`
--

INSERT INTO `tbl_evaluation_results` (`id`, `emp_id`, `employee_firstName`, `employee_middleName`, `employee_lastName`, `employee_department`, `employee_position`, `employee_job_level`, `employee_years_company`, `employee_years_position`, `evaluationStatus`, `performance_rating`, `overall_rating`, `answeredBy`, `performanceCycle`, `ev_sog_feedback`) VALUES
(1, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'COMPLETED', '5.55', '5.55', 'admin', '10/2017-11/2017', 'NO RESPONSE YET'),
(2, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'COMPLETED', '6.97', '6.97', 'admin', '10/2017-11/2017', 'NO RESPONSE YET'),
(3, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'COMPLETED', '7.03', '7.07', 'admin', '10/2017-11/2017', 'NO RESPONSE YET'),
(4, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'COMPLETED', '5.05', '5.09', 'admin', '10/2017-11/2017', 'NO RESPONSE YET'),
(5, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin', '10/2017-11/2017', 'NO RESPONSE YET'),
(6, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin', '10/2017-11/2017', 'NO RESPONSE YET'),
(7, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(8, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(9, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(10, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(11, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(12, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(13, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(14, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(15, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(16, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(17, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(18, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(19, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(20, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(21, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(22, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(23, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(24, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(25, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(26, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(27, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(28, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(29, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(30, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(31, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(32, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(33, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(34, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(35, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(36, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(37, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(38, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(39, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(40, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(41, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(42, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(43, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(44, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(45, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(46, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(47, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(48, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(49, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(50, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(51, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(52, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(53, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(54, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(55, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(56, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(57, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(58, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(59, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(60, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(61, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(62, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(63, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(64, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(65, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(66, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(67, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(68, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(69, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(70, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(71, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(72, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(73, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(74, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(75, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(76, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(77, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(78, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(79, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(80, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(81, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(82, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(83, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(84, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(85, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(86, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(87, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(88, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(89, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(90, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(91, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(92, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(93, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(94, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(95, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(96, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(97, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(98, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(99, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(100, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(101, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(102, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(103, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(104, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(105, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(106, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(107, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(108, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(109, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(110, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(111, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(112, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(113, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(114, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(115, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(116, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(117, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(118, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(119, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(120, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(121, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(122, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(123, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(124, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(125, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(126, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(127, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(128, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(129, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(130, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(131, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(132, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(133, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(134, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(135, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(136, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(137, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(138, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(139, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(140, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(141, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(142, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(143, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(144, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(145, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(146, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(147, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(148, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(149, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(150, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(151, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(152, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(153, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(154, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(155, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(156, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(157, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(158, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(159, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(160, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(161, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(162, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(163, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(164, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(165, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(166, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(167, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(168, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(169, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(170, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(171, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(172, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(173, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(174, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(175, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(176, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(177, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(178, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(179, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(180, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(181, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(182, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(183, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(184, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(185, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(186, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(187, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(188, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(189, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(190, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(191, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(192, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(193, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(194, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(195, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(196, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(197, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(198, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(199, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(200, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(201, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(202, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(203, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(204, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(205, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(206, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(207, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(208, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(209, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(210, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(211, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(212, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(213, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(214, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(215, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(216, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(217, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(218, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(219, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(220, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(221, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(222, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(223, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(224, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(225, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(226, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(227, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(228, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(229, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(230, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(231, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(232, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(233, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(234, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(235, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(236, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(237, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(238, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(239, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(240, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(241, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(242, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(243, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(244, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(245, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(246, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(247, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(248, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(249, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(250, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(251, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(252, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(253, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(254, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(255, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(256, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(257, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(258, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(259, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(260, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(261, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(262, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(263, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(264, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(265, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(266, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(267, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(268, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(269, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(270, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(271, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(272, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(273, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET');
INSERT INTO `tbl_evaluation_results` (`id`, `emp_id`, `employee_firstName`, `employee_middleName`, `employee_lastName`, `employee_department`, `employee_position`, `employee_job_level`, `employee_years_company`, `employee_years_position`, `evaluationStatus`, `performance_rating`, `overall_rating`, `answeredBy`, `performanceCycle`, `ev_sog_feedback`) VALUES
(274, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(275, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(276, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(277, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(278, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(279, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(280, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(281, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(282, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(283, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(284, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(285, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(286, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(287, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(288, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(289, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(290, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(291, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(292, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(293, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(294, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(295, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(296, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(297, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(298, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(299, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(300, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(301, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(302, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(303, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(304, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(305, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(306, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(307, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(308, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(309, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(310, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(311, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(312, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(313, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(314, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(315, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(316, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(317, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(318, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(319, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(320, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(321, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(322, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(323, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(324, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(325, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(326, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(327, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(328, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(329, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(330, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(331, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(332, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(333, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(334, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(335, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(336, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(337, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(338, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(339, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(340, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(341, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(342, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(343, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(344, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(345, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(346, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(347, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(348, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(349, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(350, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(351, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(352, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(353, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(354, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(355, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(356, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(357, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(358, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(359, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(360, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(361, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(362, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(363, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(364, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(365, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(366, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(367, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(368, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(369, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(370, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(371, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(372, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(373, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(374, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(375, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(376, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(377, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(378, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(379, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(380, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(381, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(382, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(383, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(384, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(385, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(386, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(387, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(388, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(389, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(390, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(391, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(392, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(393, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(394, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(395, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(396, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(397, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(398, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(399, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(400, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(401, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(402, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(403, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(404, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(405, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(406, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(407, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(408, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(409, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(410, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(411, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(412, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(413, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(414, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(415, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(416, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(417, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(418, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(419, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(420, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(421, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(422, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(423, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(424, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(425, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(426, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(427, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(428, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(429, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(430, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(431, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(432, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(433, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(434, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(435, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(436, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(437, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(438, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(439, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(440, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(441, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(442, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(443, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(444, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(445, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(446, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(447, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(448, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(449, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(450, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(451, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(452, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(453, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(454, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(455, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(456, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(457, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(458, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(459, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(460, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(461, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(462, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(463, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(464, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(465, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(466, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(467, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(468, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(469, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(470, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(471, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(472, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(473, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(474, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(475, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(476, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(477, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(478, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(479, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(480, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(481, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(482, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(483, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(484, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(485, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(486, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(487, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(488, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(489, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(490, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(491, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(492, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(493, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(494, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(495, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(496, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(497, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(498, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(499, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(500, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(501, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(502, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(503, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(504, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(505, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(506, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(507, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(508, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(509, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(510, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(511, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(512, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(513, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(514, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(515, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(516, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(517, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(518, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(519, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(520, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(521, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(522, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(523, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(524, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(525, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(526, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(527, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(528, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(529, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(530, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(531, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(532, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(533, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(534, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(535, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(536, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(537, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(538, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(539, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(540, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(541, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(542, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(543, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(544, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(545, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(546, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET');
INSERT INTO `tbl_evaluation_results` (`id`, `emp_id`, `employee_firstName`, `employee_middleName`, `employee_lastName`, `employee_department`, `employee_position`, `employee_job_level`, `employee_years_company`, `employee_years_position`, `evaluationStatus`, `performance_rating`, `overall_rating`, `answeredBy`, `performanceCycle`, `ev_sog_feedback`) VALUES
(547, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(548, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(549, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(550, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(551, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(552, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(553, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(554, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(555, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(556, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(557, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(558, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(559, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(560, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(561, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(562, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(563, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(564, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(565, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(566, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(567, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(568, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(569, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(570, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(571, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(572, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(573, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(574, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(575, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(576, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(577, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(578, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(579, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(580, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(581, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(582, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(583, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(584, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(585, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(586, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(587, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(588, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(589, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(590, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(591, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(592, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(593, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(594, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(595, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(596, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(597, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(598, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(599, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(600, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(601, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(602, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(603, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(604, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(605, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(606, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(607, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(608, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(609, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(610, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(611, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(612, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(613, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(614, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(615, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(616, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(617, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(618, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(619, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(620, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(621, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(622, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(623, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(624, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(625, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(626, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(627, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(628, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(629, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(630, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(631, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(632, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(633, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(634, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(635, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(636, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(637, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(638, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(639, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(640, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(641, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(642, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(643, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(644, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(645, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(646, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(647, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(648, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(649, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(650, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(651, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(652, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(653, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(654, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(655, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(656, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(657, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(658, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(659, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(660, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(661, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(662, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(663, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(664, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(665, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(666, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(667, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(668, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(669, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(670, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(671, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(672, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(673, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(674, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(675, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(676, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(677, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(678, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(679, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(680, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(681, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(682, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(683, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(684, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(685, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(686, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(687, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(688, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(689, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(690, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(691, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(692, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(693, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(694, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(695, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(696, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(697, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(698, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(699, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(700, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(701, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(702, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(703, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(704, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(705, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(706, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(707, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(708, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(709, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(710, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(711, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(712, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(713, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(714, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(715, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(716, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(717, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(718, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(719, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(720, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(721, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(722, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(723, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(724, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(725, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(726, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(727, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(728, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(729, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(730, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(731, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(732, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(733, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(734, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(735, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(736, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(737, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(738, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(739, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(740, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(741, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(742, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(743, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(744, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(745, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(746, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(747, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(748, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(749, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(750, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(751, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(752, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(753, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(754, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(755, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(756, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(757, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(758, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(759, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(760, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(761, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(762, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(763, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(764, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(765, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(766, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(767, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(768, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(769, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(770, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(771, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(772, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(773, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(774, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(775, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(776, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(777, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(778, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(779, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(780, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(781, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(782, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(783, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(784, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(785, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(786, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(787, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(788, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(789, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(790, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(791, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(792, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(793, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(794, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(795, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(796, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(797, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(798, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(799, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(800, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(801, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(802, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(803, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(804, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(805, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(806, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(807, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(808, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(809, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(810, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(811, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(812, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(813, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(814, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(815, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(816, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(817, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(818, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET');
INSERT INTO `tbl_evaluation_results` (`id`, `emp_id`, `employee_firstName`, `employee_middleName`, `employee_lastName`, `employee_department`, `employee_position`, `employee_job_level`, `employee_years_company`, `employee_years_position`, `evaluationStatus`, `performance_rating`, `overall_rating`, `answeredBy`, `performanceCycle`, `ev_sog_feedback`) VALUES
(819, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(820, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(821, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(822, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(823, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(824, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(825, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(826, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(827, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(828, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(829, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(830, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(831, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(832, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(833, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(834, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(835, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(836, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(837, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(838, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(839, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(840, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(841, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(842, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(843, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(844, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(845, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(846, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(847, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(848, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(849, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(850, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(851, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(852, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(853, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(854, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(855, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(856, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(857, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(858, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(859, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(860, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(861, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(862, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(863, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(864, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(865, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(866, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(867, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(868, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(869, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(870, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(871, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(872, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(873, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(874, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(875, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(876, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(877, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(878, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(879, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(880, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(881, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(882, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(883, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(884, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(885, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(886, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(887, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(888, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(889, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(890, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(891, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(892, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(893, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(894, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(895, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(896, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(897, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(898, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(899, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(900, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(901, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(902, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(903, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(904, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(905, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(906, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(907, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(908, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(909, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(910, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(911, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(912, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(913, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(914, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(915, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(916, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(917, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(918, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(919, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(920, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(921, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(922, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(923, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(924, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(925, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(926, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(927, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(928, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(929, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(930, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(931, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(932, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(933, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(934, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(935, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(936, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(937, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(938, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(939, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(940, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(941, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(942, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(943, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(944, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(945, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(946, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(947, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(948, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(949, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(950, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(951, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(952, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(953, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(954, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(955, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(956, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(957, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(958, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(959, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(960, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(961, 'EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', 'Secretary', 'D1', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(962, 'EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', 'Assistant ', 'D1', 13, 13, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(963, 'EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', 'Secretary', 'D2', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(964, 'EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', 'A Sec', 'D12', 5, 5, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(965, 'll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', 'll', 'll', 4, 4, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET'),
(966, 'uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', 'uu', 'uu', 2, 2, 'NOT YET STARTED', '', '', 'admin2', '10/2018-11/2018', 'NO RESPONSE YET');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kpiratingscale`
--

CREATE TABLE IF NOT EXISTS `tbl_kpiratingscale` (
  `id` int(11) NOT NULL,
  `rating_value` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `weighted_rating` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kpiratingscale`
--

INSERT INTO `tbl_kpiratingscale` (`id`, `rating_value`, `description`, `weighted_rating`) VALUES
(15, '4', 'Exceeds Expectations (Performs above standards all the time)', '1'),
(16, '3', 'Meets Expectations (Solid, consistent performance; Generally meets standards)', '0.75'),
(17, '2', 'Needs Improvement (Frequently fails to meet standards)', '0.5'),
(18, '1', 'Unacceptable (Performance is below job requirements and needs)', '0.25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE IF NOT EXISTS `tbl_messages` (
  `id` int(11) NOT NULL,
  `subject` varchar(60) NOT NULL,
  `content` text NOT NULL,
  `sender` varchar(50) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'unread',
  `checked` varchar(3) NOT NULL DEFAULT 'no',
  `sent_at` datetime NOT NULL,
  `location` varchar(100) NOT NULL,
  `deleted_by_sender` varchar(100) NOT NULL DEFAULT 'no',
  `deleted_by_receiver` varchar(100) NOT NULL DEFAULT 'no',
  `trash_by_HR` varchar(100) NOT NULL DEFAULT 'no',
  `trash_by_Department` varchar(100) NOT NULL DEFAULT 'no',
  `revision` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE IF NOT EXISTS `tbl_notification` (
  `id` int(11) NOT NULL,
  `notification` varchar(100) NOT NULL,
  `sendBy` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`id`, `notification`, `sendBy`, `status`, `timeStamp`) VALUES
(1, 'College of Information Technology Education SENT AN EVALUATION', 'admin', 'read', '2017-11-06 02:46:40'),
(2, 'College of Information Technology Education SENT AN EVALUATION', 'admin', 'read', '2017-11-06 15:36:16'),
(3, 'College of Information Technology Education SENT AN EVALUATION', 'admin', 'read', '2017-11-09 17:02:55'),
(4, 'College of Information Technology Education SENT AN EVALUATION', 'admin', 'read', '2017-11-09 17:02:55');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification_user`
--

CREATE TABLE IF NOT EXISTS `tbl_notification_user` (
  `id` int(11) NOT NULL,
  `notification` varchar(100) NOT NULL,
  `sendBy` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `owner` varchar(60) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification_user`
--

INSERT INTO `tbl_notification_user` (`id`, `notification`, `sendBy`, `status`, `owner`, `timeStamp`) VALUES
(1, 'Janessa CariÃ±o Estayo', 'admin', '0', 'superadmin', '2017-11-06 14:52:19'),
(2, 'Arwind S Santos', 'admin', 'unread', 'superadmin', '2017-11-06 15:36:26'),
(3, 'uu uu uu', 'admin', 'unread', 'superadmin', '2017-11-08 10:27:22'),
(4, 'llsdfsdfsdfsdfs ll ll', 'admin', 'unread', 'superadmin', '2017-11-08 10:30:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p1kpi`
--

CREATE TABLE IF NOT EXISTS `tbl_p1kpi` (
  `kraID` int(11) NOT NULL,
  `kpiID` int(11) NOT NULL,
  `kpiDesc` varchar(100) NOT NULL,
  `kpiOwner` varchar(100) NOT NULL,
  `kpiDate` varchar(11) NOT NULL,
  `kpiStatus` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p1kpi`
--

INSERT INTO `tbl_p1kpi` (`kraID`, `kpiID`, `kpiDesc`, `kpiOwner`, `kpiDate`, `kpiStatus`) VALUES
(2, 1, 'Communicate announcements from key offices (e.g. Registrar, CSDL, HRD, OP) to teachers or students u', 'admin', '', 'APPROVED'),
(2, 2, 'Coordinate meetings and other activities within the college', 'admin', '', 'APPROVED'),
(2, 3, 'Coordinate with other offices for concerns from the college (e.g. Faculty load, room assignment, rep', 'admin', '', 'APPROVED'),
(2, 4, 'Supervise the student assistants designated to the college', 'admin', '', 'APPROVED'),
(3, 1, 'Organize office files and documents', 'admin', '', 'APPROVED'),
(3, 2, 'Supervise order and cleanliness in the office', 'admin', '', 'APPROVED'),
(3, 3, 'Regular inventory and update of office supplies', 'admin', '', 'APPROVED'),
(4, 1, 'Submit reports (e.g., Attendance, Completion of Grades) Â required by other offices (e.g., HRD, Regi', 'admin', '', 'APPROVED'),
(4, 2, 'Monitor facultys attendanceÂ ', 'admin', '', 'APPROVED'),
(4, 3, 'Compile and submit forms (e.g., Student Evaluation, Leave forms, Clearances) required by other offic', 'admin', '', 'APPROVED'),
(5, 1, 'Answer telephone calls and/or take messages', 'admin', '', 'APPROVED'),
(5, 2, 'Man the offices frontdesk to meet walk-in customers Â  Â Â ', 'admin', '', 'APPROVED'),
(5, 3, 'Arrange customers appointments with the Dean or faculty Â  Â ', 'admin', '', 'APPROVED'),
(5, 4, 'Handle data encoding duties during enrolment', 'admin', '', 'APPROVED'),
(6, 1, 'knows how to speak english', 'EMP-001', '', 'PENDING'),
(6, 2, 'tagalog', 'EMP-001', '', 'PENDING'),
(10, 1, 'ss', 'admin2', '', 'APPROVED'),
(11, 1, 'as', 'admin2', '', 'APPROVED'),
(12, 1, 'asdasd', 'admin2', '', 'APPROVED'),
(13, 1, 'fff', 'admin2', '', 'APPROVED');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p2bcritinc`
--

CREATE TABLE IF NOT EXISTS `tbl_p2bcritinc` (
  `id` int(11) NOT NULL,
  `competencyName` varchar(60) NOT NULL,
  `criticalIncident` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p2bcritinc`
--

INSERT INTO `tbl_p2bcritinc` (`id`, `competencyName`, `criticalIncident`) VALUES
(1, 'Adaptability', 'Critical Incident'),
(2, 'Adaptability', 'Critical Incident'),
(3, 'Adaptability', 'Critical Incident'),
(1, 'Creativity and Innovation', 'Critical Incident'),
(2, 'Creativity and Innovation', 'Critical Incident'),
(3, 'Creativity and Innovation', 'Critical Incident'),
(1, 'Customer Focus', 'Critical Incident'),
(2, 'Customer Focus', 'Critical Incident'),
(3, 'Customer Focus', 'Critical Incident'),
(1, 'Team Orientation', 'Critical Incident'),
(2, 'Team Orientation', 'Critical Incident'),
(3, 'Team Orientation', 'Critical Incident'),
(1, 'Technical Knowledge', 'Critical Incident'),
(2, 'Technical Knowledge', 'Critical Incident'),
(3, 'Technical Knowledge', 'Critical Incident'),
(1, 'Leadership Excellence', 'Critical Incident'),
(2, 'Leadership Excellence', 'Critical Incident'),
(3, 'Leadership Excellence', 'Critical Incident'),
(1, 'Management Excellence', 'Critical Incident'),
(2, 'Management Excellence', 'Critical Incident'),
(3, 'Management Excellence', 'Critical Incident'),
(1, 'People Development', 'Critical Incident'),
(2, 'People Development', 'Critical Incident'),
(3, 'People Development', 'Critical Incident');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_p3rubric`
--

CREATE TABLE IF NOT EXISTS `tbl_p3rubric` (
  `id` int(11) NOT NULL,
  `areaName` varchar(100) NOT NULL,
  `rubricDesc` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_p3rubric`
--

INSERT INTO `tbl_p3rubric` (`id`, `areaName`, `rubricDesc`) VALUES
(4, 'DISCIPLINE', 'Has zero record of disciplinary action on offenses other than the above policies'),
(3, 'DISCIPLINE', 'Has received one (1) disciplinary action on a minor offense not related to attendance/punctuality'),
(2, 'DISCIPLINE', 'Has received two (2) disciplinary action on minor offenses not related to attendance/punctuality'),
(1, 'DISCIPLINE', 'Has received more than two (2) disciplinary action on minor offenses or one (1) major offense not related to attendance/punctuality'),
(4, 'ATTENDANCE', ' Perfect attendance; OR has not exceeded the total leave credits entitlement.'),
(3, 'ATTENDANCE', 'Has exceeded available leave credits.'),
(2, 'ATTENDANCE', 'Has exceeded the allowable hours/frequency of tardiness'),
(1, 'ATTENDANCE', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy'),
(4, 'PUNCTUALITY', 'Has never been late and no occurrence of undertime.'),
(3, 'PUNCTUALITY', 'Has not exceeded the allowable hours/frequency of tardiness and/or undertime'),
(2, 'PUNCTUALITY', 'Has exceeded the allowable hours/frequency of tardiness'),
(1, 'PUNCTUALITY', 'Has received a disciplinary action related to tardiness/undertime; OR habitually tardy'),
(4, 'COMPLETION OF TRAINING', 'Has successfully completed all the required training/seminars'),
(3, 'COMPLETION OF TRAINING', 'Has failed to complete one (1) training/seminar due to justifiable reason.'),
(2, 'COMPLETION OF TRAINING', 'Has failed to complete two (2) training/seminars due to justifiable reason.'),
(1, 'COMPLETION OF TRAINING', 'Has failed to complete any seminar without justifiable cause.'),
(4, 'Test me', 'Has zero record of disciplinary action on offenses other than the above policies');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_proficiency_level`
--

CREATE TABLE IF NOT EXISTS `tbl_proficiency_level` (
  `id` int(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `initials` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_proficiency_level`
--

INSERT INTO `tbl_proficiency_level` (`id`, `value`, `initials`, `title`, `content`) VALUES
(1, '4', 'A', 'Contributing Independently', 'Learner-Doer. Individual contributor. Follower, team-player, Customer touchpoint'),
(3, '3', 'B', 'Contributing with Expertise', 'Specialist. Expert Implementor. Customer Advocate. Problem Solver'),
(4, '2', 'C', 'Contribution through Teams/Peers', 'Project Leader. Idea Leader. Technical Troubleshooter. Coach'),
(5, '1', 'D', 'Contributing through Strategy Building', 'Navigator. Idea Innovator. Leader Developer. Mission Advocate.'),
(6, '0', 'NE', '', 'Early stages of application of the item; Behavior not yet present'),
(7, '0', 'NA', '', 'The item does not apply to your position or performance of assigned tasks and responsibilities');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating_execute`
--

CREATE TABLE IF NOT EXISTS `tbl_rating_execute` (
  `ID` int(11) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `performance_rating` varchar(100) NOT NULL,
  `overall_rating` varchar(100) NOT NULL,
  `performanceCycle` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rating_execute`
--

INSERT INTO `tbl_rating_execute` (`ID`, `emp_id`, `performance_rating`, `overall_rating`, `performanceCycle`) VALUES
(1, 'EMP-002', '17.05', '17.05', '10/2017-11/2017'),
(2, 'EMP-002', '23.1', '23.1', '10/2018-11/2018'),
(3, 'EMP-001', '17.95', '17.95', '10/2018-11/2018'),
(4, 'EMP-002', '19.75', '19.75', '10/2018-11/2018'),
(5, 'uu', '18.3', '18.3', '10/2018-11/2018'),
(6, 'EMP-001', '18.4', '18.4', '10/2017-11/2017'),
(7, 'EMP-001', '5.55', '5.55', '10/2017-11/2017'),
(8, 'EMP-002', '6.97', '6.97', '10/2017-11/2017'),
(9, 'uu', '7.03', '7.07', '10/2017-11/2017'),
(10, 'll', '5.05', '5.09', '10/2017-11/2017');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp2a`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp2a` (
  `criticalIncidentCount` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp2a`
--

INSERT INTO `tbl_settingsp2a` (`criticalIncidentCount`) VALUES
(3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settingsp3`
--

CREATE TABLE IF NOT EXISTS `tbl_settingsp3` (
  `rubricCount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settingsp3`
--

INSERT INTO `tbl_settingsp3` (`rubricCount`) VALUES
(4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sog_employee`
--

CREATE TABLE IF NOT EXISTS `tbl_sog_employee` (
  `emp_id` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `years_in_company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `job_level` varchar(100) NOT NULL,
  `year_applied` varchar(100) NOT NULL,
  `years_in_position` varchar(100) NOT NULL,
  `department_head` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_temp` varchar(100) NOT NULL,
  `email_employee` text NOT NULL,
  `paths` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sog_employee`
--

INSERT INTO `tbl_sog_employee` (`emp_id`, `firstname`, `middlename`, `lastname`, `department`, `years_in_company`, `position`, `job_level`, `year_applied`, `years_in_position`, `department_head`, `username`, `password`, `password_temp`, `email_employee`, `paths`) VALUES
('EMP-001', 'Janessa', 'CariÃ±o', 'Estayo', 'College of Information Technology Education', '5', 'Secretary', 'D1', '2012', '5', '', 'EMP-001', '2e6cd64c4d3aabb280846e0192338a96', 'ESTAYO', '', '../assets/uploaded_files/accounts/s4KP3lFB.jpg'),
('EMP-002', 'Arwind', 'S', 'Santos', 'College of Information Technology Education', '13', 'Assistant ', 'D1', '2004', '13', '', 'EMP-002', '6cab142f45cd157b5299e1b828e668e6', 'SANTOS', '', '../assets/uploaded_files/accounts/default.png'),
('EMP-003', 'Mark', 'E', 'Ferrer', 'College of Management and Accountancy', '5', 'Secretary', 'D2', '2012', '5', '', 'EMP-003', 'd41d8cd98f00b204e9800998ecf8427e', '', 'test2@gmail.com', '../assets/uploaded_files/accounts/default.png'),
('EMP-004', 'Tom', 'F', 'Jose', 'College of Management and Accountancy', '5', 'A Sec', 'D12', '2012', '5', '', 'EMP-004', 'da598699cef750ee8e5ef3b2daf47719', 'JOSE', 'tomtomtomtomtom@gmail.com', '../assets/uploaded_files/accounts/default.png'),
('ll', 'llsdfsdfsdfsdfs', 'll', 'll', 'College of Information Technology Education', '4', 'll', 'll', '2013', '4', '', 'll', '67824ecf84f5816f07b74fa956bdbcd2', 'LL', '', '../assets/uploaded_files/accounts/P_20151215_125500.jpg'),
('uu', 'uu', 'uu', 'uu', 'College of Information Technology Education', '2', 'uu', 'uu', '2015', '2', '', 'uu', '81cf6107131a3583e2b0b762cb9c2862', 'UU', '', '../assets/uploaded_files/accounts/47e86f5632d283ec3f22145198268887deb10c29cebbe2531c7a2c47f949f942.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp2a`
--
ALTER TABLE `tbl_ans_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp2b`
--
ALTER TABLE `tbl_ans_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp3`
--
ALTER TABLE `tbl_ans_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_eformp4`
--
ALTER TABLE `tbl_ans_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ans_summary_comments_acknowledgement`
--
ALTER TABLE `tbl_ans_summary_comments_acknowledgement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_competencyratingscale`
--
ALTER TABLE `tbl_competencyratingscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp1`
--
ALTER TABLE `tbl_eformp1`
  ADD PRIMARY KEY (`kraID`);

--
-- Indexes for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_evaluation_requests`
--
ALTER TABLE `tbl_evaluation_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_evaluation_results`
--
ALTER TABLE `tbl_evaluation_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kpiratingscale`
--
ALTER TABLE `tbl_kpiratingscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification_user`
--
ALTER TABLE `tbl_notification_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_rating_execute`
--
ALTER TABLE `tbl_rating_execute`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_sog_employee`
--
ALTER TABLE `tbl_sog_employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_ans_eformp1`
--
ALTER TABLE `tbl_ans_eformp1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1kpi`
--
ALTER TABLE `tbl_ans_eformp1kpi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp1_achievements`
--
ALTER TABLE `tbl_ans_eformp1_achievements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp2a`
--
ALTER TABLE `tbl_ans_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp2b`
--
ALTER TABLE `tbl_ans_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp3`
--
ALTER TABLE `tbl_ans_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_ans_eformp4`
--
ALTER TABLE `tbl_ans_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_ans_summary_comments_acknowledgement`
--
ALTER TABLE `tbl_ans_summary_comments_acknowledgement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_competencyratingscale`
--
ALTER TABLE `tbl_competencyratingscale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_eformp2a`
--
ALTER TABLE `tbl_eformp2a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_eformp2b`
--
ALTER TABLE `tbl_eformp2b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_eformp3`
--
ALTER TABLE `tbl_eformp3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_eformp4`
--
ALTER TABLE `tbl_eformp4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_evaluation_requests`
--
ALTER TABLE `tbl_evaluation_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_evaluation_results`
--
ALTER TABLE `tbl_evaluation_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=967;
--
-- AUTO_INCREMENT for table `tbl_kpiratingscale`
--
ALTER TABLE `tbl_kpiratingscale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_notification_user`
--
ALTER TABLE `tbl_notification_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_proficiency_level`
--
ALTER TABLE `tbl_proficiency_level`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_rating_execute`
--
ALTER TABLE `tbl_rating_execute`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
